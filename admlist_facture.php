﻿<?php
/**
 * admlist_facture.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2019-01-30 12:00:00 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 *@version   3.1.1
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{

		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

			<!--<link rel="stylesheet" href="style.css" type="text/css" media="all" />-->>
			<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="https://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>

			<script type="text/javascript">
				jQuery(function($){
			   $.datepicker.regional['fr'] = {
				  closeText: 'Fermer',
				  prevText: '&#x3c;Préc',
				  nextText: 'Suiv&#x3e;',
				  currentText: 'Courant',
				  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
				  'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				  monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
				  'Jul','Aoû','Sep','Oct','Nov','Déc'],
				  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
				  dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
				  dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
				  weekHeader: 'Sm',
				  //dateFormat: 'dd/mm/yy',
							dateFormat: 'dd/mm/yy',
				  firstDay: 1,
				  isRTL: false,
				  showMonthAfterYear: false,
				  yearSuffix: ''};
			   $.datepicker.setDefaults($.datepicker.regional['fr']);
			});

			function changedate()
			{
				var datedebut=$( "#datepicker" ).val();
				var datefin=$( "#datepicker1" ).val();
				var user=$( "#listuser" ).val();

				window.location.href = 'admlist_facture.php?datedebut='+datedebut+'&datefin='+datefin+'&user='+user;
			}

			</script>

			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion CoWorking Haut de France</title>

			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body>

			<script>
			$(function() {
				$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
				$( "#datepicker" ).datepicker( $.datepicker.regional[ "fr" ] );
				$( "#datepicker1" ).datepicker( $.datepicker.regional[ "fr" ] );
			});
			</script>

			<?php
			include ("include/fonction_admutilisateur.php");
			admentete_page("Liste des factures");
			liste_factureV2();

			?>

			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php

}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur

    header('Location: index.php');


}
?>
