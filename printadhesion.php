<?php
/**
 * pintadhesion.php
* Ce script fait partie de l'application Gestion Coworking
* Dernière modification : $Date: 2018-06-21 15:38:14 $
* Dernière modification : $Date: 2009-10-09 07:55:48 $
* @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
* @copyright Copyright 2016-2018 Jean-René Menu
* @link      http://www.gnu.org/licenses/licenses.html
* @package   root
* @version   $Id: 3.0
* @filesource
*
* This file is part of Gestion Coworking.
*
* Gestion Coworking is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* Gestion Coworking is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Gestion Coworking; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

include('include/fonction_general.php');

$sql = "SELECT DO_NOMFICHIER FROM ETABLISSEMENT
				LEFT JOIN DOCUMENT ON DO_CODE = ET_MODELEADHESION
 				WHERE ET_ETABLISSEMENT = '".$_GET['etablissement']."'";
$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
foreach ($tab_r as $data)
{
	echo $data['DO_NOMFICHIER'];
	require ("pdf/".$data['DO_NOMFICHIER'].".php");
}

?>
