﻿<?php
/**
 * manageetab.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
    // Les informations de connexion sont bonnes, on affiche le contenu protégé

	if((isset($_GET['ACTION'])))
	{
	
	?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
			
			<!--<link rel="stylesheet" href="lib/file.css">-->
			</head>
			<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:0px; padding-left: 20px;">
			
			<?php
			
			include ("include/fonction_etab.php"); 
			admentete_page("dd");
			if ($_GET['ACTION'] == 'manage')
			{
				modif_place();
			}
			if ($_GET['ACTION'] == 'MODIFBANQUE')
			{
				modif_banque_etablissement($_GET['ETABLISSEMENT']);
			}
			
			if ($_GET['ACTION'] == 'PARAMADHESION')
			{
				modif_adhésion_etablissement($_GET['ETABLISSEMENT']);
			}
			
			if ($_GET['ACTION'] == 'MODIFICATION')
			{
				modif_etablissement($_GET['ETABLISSEMENT']);
			}
			
			if ($_GET['ACTION'] == 'BLOQUER')
			{
				bloque_etablissement($_GET['ETABLISSEMENT']);
			}
			
			if ($_GET['ACTION'] == 'DEBLOQUER')
			{
				debloque_etablissement($_GET['ETABLISSEMENT']);
			}
			
			
			//insert_appel($_POST['Nom_Client'], $_POST['DateCall'], $_POST['HeureCall'], $_SESSION['login'], $_POST['TitreCall'], $_POST['commentaires'], $_POST['Type_Call'], $_POST['NewCategorieCall']);
	  		
			?>
			
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	
	}
	else
	{
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>

			
			<link rel="stylesheet" href="lib/file.css">
			</head>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js" /></script>
			<script type="text/javascript">
			
						
			$(document).ready(function() {
			 
				if($('select[name="GESTION_FACTURATION"]').val() == 'OUI')
					{
						$('#btn_parambnq').show();
						$('#btn_parambnq').text('Afficher le paramétrage facturation');
						$('#TEXTINFO1').show();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#parambnq_'+i).attr("disabled", false);
						}
					}
					else
					{
						$('#btn_parambnq').hide();
						$('#TEXTINFO1').hide();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#parambnq_'+i).attr("disabled", true);
						}
					}	
					
				if($('select[name="GESTION_ADHESION"]').val() == 'OUI')
					{
						$('#btn_paramadh').show();
						$('#TEXTINFO2').show();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#paramadh_'+i).attr("disabled", false);
						}
					}
					else
					{
						$('#btn_paramadh').hide();
						$('#TEXTINFO2').hide();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#paramadh_'+i).attr("disabled", true);
						}
					}	
					
				if($('select[name="GESTION_CREDITCLI"]').val() == 'OUI')
					{
						$('#btn_paramccli').show();
						$('#TEXTINFO2').show();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#paramccli_'+i).attr("disabled", false);
						}
					}
					else
					{
						$('#btn_paramccli').hide();
						$('#TEXTINFO2').hide();
						var i;
						for (i = 1; i < 12; i++)
						{
							$('#paramccli_'+i).attr("disabled", true);
						}
					}	
				
				 
				$('select[name="GESTION_FACTURATION"]').change(function() { // lorsqu'on change de valeur dans la liste
				var valeur = $(this).val(); // valeur sélectionnée
				 
					if(valeur != '') { // si non vide
						if(valeur == 'OUI') { // si "ARTRE"
							$('#btn_parambnq').show();
							$('#btn_parambnq').text('Afficher le paramétrage facturation');
							$('#TEXTINFO1').show();
							var i;
							for (i = 1; i < 12; i++)
							{
								$('#parambnq_'+i).attr("disabled", false);
							}
							
							
							
						} else {
							$('#btn_parambnq').hide(); 
							$('#btn_parambnq').text('Afficher le paramétrage facturation');
							$('#TEXTINFO1').hide();	
							var i;
							for (i = 1; i < 26; i++)
							{
								$('#parambnq_'+i).attr("disabled", true);
								$('#tdparambnq_'+i).hide();
							}
							//document.getElementById('btn_'+id).innerHTML='Afficher le paramétrage facturation';
						}
					}
				});
				
				$('select[name="GESTION_CREDITCLI"]').change(function() { // lorsqu'on change de valeur dans la liste
				var valeur = $(this).val(); // valeur sélectionnée
				 
					if(valeur != '') { // si non vide
						if(valeur == 'OUI') { // si "ARTRE"
							$('#btn_paramccli').show();
							$('#btn_paramccli').text('Afficher le paramétrage du crédit client');
							$('#TEXTINFO3').show();
							var i;
							for (i = 1; i < 12; i++)
							{
								$('#paramccli_'+i).attr("disabled", false);
							}
							
						} else {
							$('#btn_paramccli').hide(); 
							$('#TEXTINFO3').hide();	
							var i;
							for (i = 1; i < 26; i++)
							{
								$('#paramccli_'+i).attr("disabled", true);
								$('#tdparamccli_'+i).hide();
							}							
													
						}
					}
				});
				
				$('select[name="GESTION_ADHESION"]').change(function() { // lorsqu'on change de valeur dans la liste
				var valeur = $(this).val(); // valeur sélectionnée
				 
					if(valeur != '') { // si non vide
						if(valeur == 'OUI') { // si "ARTRE"
							$('#btn_paramadh').show();
							$('#TEXTINFO2').show();
							
						} else {
							$('#btn_paramadh').hide(); 
							$('#TEXTINFO2').hide();							
													
						}
					}
				});
			 
			});
			
			function cacher (id)
			{
				if(document.getElementById('td'+id+'_1').style.display=="none")
				{
					if(id == 'parambnq')
						{$('#btn_parambnq').text('Cacher le paramétrage facturation');}
					if(id == 'paramadh')
						{$('#btn_paramadh').text('Cacher le paramétrage des adhésions');}
					if(id == 'paramccli')
						{$('#btn_paramccli').text('Cacher le paramétrage du crédit client');}
					var i;
					
					for (i = 1; i < 26; i++)
					{
						$('#td'+id+'_'+i).show();
					}
					//document.getElementById('btn_'+id).innerHTML='Cacher le paramétrage facturation';
				}
				else
				{
					if(id == 'parambnq')
						{$('#btn_parambnq').text('Afficher le paramétrage facturation');}
					if(id == 'paramadh')
						{$('#btn_paramadh').text('Afficher le paramétrage des adhésions');}
					if(id == 'paramccli')
						{$('#btn_paramccli').text('Afficher le paramétrage du crédit client');}
					var i;
					for (i = 1; i < 26; i++)
					{
						$('#td'+id+'_'+i).hide();
					}
					//document.getElementById('btn_'+id).innerHTML='Afficher le paramétrage facturation';
				}
			}
			
			function cachertout (id)
			{
				$('#btn_parambnq').text('Afficher le paramétrage facturation');
				$('#btn_paramadh').text('Afficher le paramétrage des adhésions');
				$('#btn_paramccli').text('Afficher le paramétrage du crédit client');
				var id1;
				id1='parambnq';
				var id2;
				id2='paramadh';
				var id3;
				id3='paramccli';
				var i;
				for (i = 1; i < 26; i++)
				{
					$('#td'+id1+'_'+i).hide();
					$('#td'+id2+'_'+i).hide();
					$('#td'+id3+'_'+i).hide();
				}
				//document.getElementById('btn_'+id).innerHTML='Afficher le paramétrage facturation';
				
			}
			</script>
			<body onload="javascript:cachertout('parambnq');">
			<?php
			include ("include/fonction_etab.php"); 
			admentete_page("Gestion des établissements");
			//barre_menu(); 
			if ((isset($_SESSION['SUPERADMIN'])) && ($_SESSION['SUPERADMIN'] == 'OUI'))
			{
				liste_etablissement();
			}
			else
			{
				modif_etablissementV2($_SESSION['ETABADMIN']);
			}

			//footer(); 
			?>
			
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
    <?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur
    
    header('Location: index.php');


}
?>