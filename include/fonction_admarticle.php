<?php
/**
 * fonction_admarticle.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include ("include/fonction_general.php");


function list_article()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
	?>

	<body>
		<!-- TABLE 1 DEBUT -->

		<style>
			#customers {
			    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			    border-collapse: collapse;
			    width: 100%;
			}

			#customers td, #customers th {
			    border: 1px solid #ddd;
			    padding: 8px;
			}

			#customers tr:nth-child(even){background-color: #f2f2f2;}

			#customers tr:hover {background-color: #ddd;}

			#customers th {
			    padding-top: 12px;
			    padding-bottom: 12px;
			    text-align: left;
			    background-color: #4CAF50;
			    color: white;
			}
		</style>
		
<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto; font-size : 14px;"cellpadding="2" cellspacing="2" id="customers">
	<form  action="" method="post">
  <tbody>
    <tr>
      <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 100px; font-weight: bold; background-color: rgb(70, 181, 147);">Code Article
	  
	  </td>
	  <td style="width: 400px ;font-family: Calibri; color: rgb(0, 1, 0);font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Désignation</td>
	  <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 100px; font-weight: bold; background-color: rgb(70, 181, 147);">Type article
	  <br>
	  <select name="TypeArticle" id="TypeArticle" >
      <?php
		if (((isset($_POST['TypeArticle'])) && ($_POST['TypeArticle'] == 'Tous')) || (!isset($_POST['TypeArticle'])))
		{
			?>
			<option value="Tous" selected="selected">Tous</option>
			<?php
		}
		if ((isset($_POST['TypeArticle'])) && ($_POST['TypeArticle'] != 'Tous'))
		{
			?>
			<option value="Tous" >Tous</option>
			<?php
		}
		
		$sql = "select AR_TYPE, CC_LIBELLE FROM ARTICLE LEFT JOIN CHOIXCODE ON CC_TYPE = 'TYPEART' AND CC_CODE = AR_TYPE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' GROUP BY AR_TYPE";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			if (isset($_POST['TypeArticle']) && $data['AR_TYPE'] == $_POST['TypeArticle'])
			{
				?>
				<option value="<?php echo $data['AR_TYPE']; ?>" selected="selected"><?php echo $data['CC_LIBELLE']; ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php echo $data['AR_TYPE']; ?>"><?php echo $data['CC_LIBELLE']; ?></option>
				<?php
			}

		}

		?>
      </select>
	  </td>
      <td style="width: 100px;font-family: Calibri; color: rgb(0, 1, 0);font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Prix</td>
      
	  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);"><button value="Valid" name="Valid">Valider</button></td>
      </tr>
	  </form>

<!-- DEBUT -->


<?php

$critere = 0;

if ((isset($_POST['ListCodeArt'])) && ($_POST['ListCodeArt'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND AR_ARTICLENO = '".$_POST['ListCodeArt']."' ";
	}
	else
	{
		$sqlcritere = " AND AR_ARTICLENO = '".$_POST['ListCodeArt']."' ";
	}
	$critere = 1;

}
if ((isset($_POST['TypeArticle'])) && ($_POST['TypeArticle'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND AR_TYPE = '".$_POST['TypeArticle']."' ";
	}
	else
	{
		$sqlcritere = " AND AR_TYPE = '".$_POST['TypeArticle']."' ";
	}
	$critere = 1;

}


if ($critere == 1)
{
	$sql = "SELECT * FROM ARTICLE LEFT JOIN CHOIXCODE ON CC_TYPE = 'TYPEART' AND CC_CODE = AR_TYPE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' " .$sqlcritere ." ORDER BY AR_CODEARTICLE";
}
else
{
	$sql = "SELECT * FROM ARTICLE LEFT JOIN CHOIXCODE ON CC_TYPE = 'TYPEART' AND CC_CODE = AR_TYPE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ORDER BY AR_CODEARTICLE";
}

$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
$nbrlignearticle = 0;
if (count($tab_r) != 0)
{	
	foreach ($tab_r as $data)
	{
		$nbrlignearticle++;
		?>
		<tr>
			<td><?php echo $data['AR_CODEARTICLE']; ?></td>
			<td><?php echo $data['AR_DESIGNATION']; ?></td>
			<td><?php echo $data['CC_LIBELLE']; ?></td>
			<td colspan="1"><?php echo $data['AR_TARIF']; ?></td>
			<td style="width: 30px;"><img border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('modif_article.php?ACTION=MODIF&numero=<?php echo $data['AR_ARTICLENO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			<td style="width: 30px;"><img border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('delete_article.php?numero=<?php echo $data['AR_ARTICLENO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
		</tr>
		
	<?php
	}
	
}
?>
	
	<tr><td colspan="6" align="center" ><input type=button value="Ajouter un article" class="bouton1" onclick="window.open('ajout_article.php', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td></tr>
			
	<?php
?>


<!-- FIN -->




	<!-- </tbody></table> -->
	<!-- TABLE 1 FIN -->

		<!-- TABLE 2 -->

	<!-- TABLE 2 FIN -->
	<?php
	}

}


function new_article()
{
?>
	<br />
<form  action="" method="post">
<table style='text-align: left; width: 474px; height: 200px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center;  color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Création d'un article</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Code de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="20" size="10" tabindex="1" name="CODEARTICLE" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="50" size="30" tabindex="2" name="LIBELLEART" required></td>
			</tr>
				<td style="width: 304px; height: 28px;"><label>Type d'article : </label></td>
			<td>
				<select name="TYPEARTICLE" required>
					<option value="">Choisir un type !</option>
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'TYPEART';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['CC_CODE']; ?>"><?php echo $r['CC_LIBELLE']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr>
			<td colspan="2">	
				<LABEL>Famille de niveau 1 : </LABEL>
				<select name="FAMNIV1" required>
					<option value="">Aucune </option>
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'FAMNIV1';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['CC_CODE']; ?>"><?php echo $r['CC_LIBELLE']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr>
			<td colspan="2">	
				<LABEL>Famille de niveau 2 : </LABEL>
				<select name="FAMNIV1" required>
					<option value="">Aucune </option>
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'FAMNIV2';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['CC_CODE']; ?>"><?php echo $r['CC_LIBELLE']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="PRIXART" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;"><label id="HEUREDEBUTTXT">Heure de début : </label></td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" id="HEUREDEBUT" name="HEUREDEBUT" required disabled></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;"><label id="HEUREFINTXT">Heure de fin : </label></td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" id="HEUREFIN" name="HEUREFIN" required disabled></td>
			</tr>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
<input type="hidden" value="AJOUT" name="action">
</form>
<?php
}

function new_article2()
{
?>
	<br />
<form  action="" method="post">
<table style='text-align: left; width: 474px; height: 200px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center;  color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Création d'un article</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Code de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="20" size="10" tabindex="1" name="CODEARTICLE" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="50" size="30" tabindex="2" name="LIBELLEART" required></td>
			</tr>
			<tr>
			<td colspan="2">	
				<LABEL>Famille de niveau 1 : </LABEL>
				<select name="FAMNIV1">
					<option value="">Aucune </option>
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'FAMNIV1';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['CC_CODE']; ?>"><?php echo $r['CC_LIBELLE']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr>
			<td colspan="2">	
				<LABEL>Famille de niveau 2 : </LABEL>
				<select name="FAMNIV2">
					<option value="">Aucune </option>
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'FAMNIV2';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['CC_CODE']; ?>"><?php echo $r['CC_LIBELLE']; ?></option>
					<?php
				}
				?>
				
				</select>
			</td></tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="4" name="PRIXART" required></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;"><label id="HEUREDEBUTTXT">Heure de début : </label></td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" id="HEUREDEBUT" name="HEUREDEBUT" required ></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;"><label id="HEUREFINTXT">Heure de fin : </label></td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" id="HEUREFIN" name="HEUREFIN" required ></td>
			</tr>
			<tr align="center">
				<input type="hidden" name="TYPEARTICLE" value="ARTRE" />
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
<input type="hidden" value="AJOUT" name="action">
</form>
<?php
}

function insert_new_article()
{
	$sql = "SELECT * FROM ARTICLE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND AR_CODEARTICLE = '".$_POST['CODEARTICLE']."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	$count = count($tab_r);
	if (count($tab_r) == 0)
	{
		$sql = 'INSERT INTO `ARTICLE`(`AR_CODEARTICLE`, `AR_DESIGNATION`, `AR_TYPE`, `AR_ETABLISSEMENT`, `AR_FAMILLE1`, `AR_FAMILLE2`, `AR_TARIF`, AR_RESAHEUREDEBUT, AR_RESAHEUREFIN) VALUES
				("'.$_POST['CODEARTICLE'].'","'.addslashes($_POST['LIBELLEART']).'","'.$_POST['TYPEARTICLE'].'","'.$_SESSION['ETABADMIN'].'","","","'.$_POST['PRIXART'].'",
				"00:00","19:00");';

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);	
		echo "Création de l'article " .$_POST['CODEARTICLE'] ." - ".$_POST['LIBELLEART']." terminée!";
	}
	else
	{
		echo "L'article " .$_POST['CODEARTICLE'] ." - ".$_POST['LIBELLEART']." existe déjà!";
	}
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function modif_article()
{
	
	if(!isset($_POST["action"]))
	{
		$sql = "select * from ARTICLE WHERE AR_ARTICLENO =" .$_GET['numero'];
		 
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
?>
		<form  enctype="multipart/form-data" action="" method="post">
		<table style='text-align: left; width: 474px;  font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; height: 28px;  color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification d'un article</td>
			<tr>
			  <td style="width: 304px; height: 28px;">Code de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="CODEARTICLE" value='<?php echo $data['AR_CODEARTICLE']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de l'article</td>
			  <td style="height: 28px; width: 300px;"><input maxlength="70" size="70" tabindex="2" name="LIBELLEART" value='<?php echo $data['AR_DESIGNATION']; ?>'></td>
			</tr>
				<td style="width: 304px; height: 28px;"><label>Famille niveau 1 : </label></td>
			<td>
				<select name="FAMNIV1" id="FAMNIV1" tabindex="3">
				<?php
				$sql = "select COUNT(CC_CODE),CC_CODE, CC_LIBELLE, 'SELECTED' AS SELECTED, 'VIDE' FROM CHOIXCODE WHERE CC_TYPE = 'FAMRESANIV1' AND CC_CODE = '".$data['AR_FAMILLE1']."'
						union 
						select 1,CC_CODE, CC_LIBELLE, '' AS SELECTED, 'VIDE' FROM CHOIXCODE WHERE CC_TYPE = 'FAMRESANIV1' AND CC_CODE <> '".$data['AR_FAMILLE1']."'
						ORDER BY SELECTED DESC;";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					if ($data1['SELECTED'] == "SELECTED")
					{
						?>
						<option value="<?php echo $data1['CC_CODE']; ?>" selected="selected"><?php echo $data1['CC_LIBELLE']; ?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $data1['CC_CODE']; ?>"><?php echo $data1['CC_LIBELLE']; ?></option>
						<?php
					}

				}
				?>
			</td></tr>
			</tr>
				<td style="width: 304px; height: 28px;"><label>Famille niveau 2 : </label></td>
			<td>
				<select name="FAMNIV2" id="FAMNIV2" tabindex="3">
				<?php
				$sql = "select COUNT(CC_CODE),CC_CODE, CC_LIBELLE, 'SELECTED' AS SELECTED, 'VIDE' FROM CHOIXCODE WHERE CC_TYPE = 'FAMRESANIV2' AND CC_CODE = '".$data['AR_FAMILLE2']."'
						union 
						select 1,CC_CODE, CC_LIBELLE, '' AS SELECTED, 'VIDE' FROM CHOIXCODE WHERE CC_TYPE = 'FAMRESANIV2' AND CC_CODE <> '".$data['AR_FAMILLE2']."'
						ORDER BY SELECTED DESC;";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					if ($data1['SELECTED'] == "SELECTED")
					{
						?>
						<option value="<?php echo $data1['CC_CODE']; ?>" selected="selected"><?php echo $data1['CC_LIBELLE']; ?></option>
						<?php
					}
					else
					{
						?>
						<option value="<?php echo $data1['CC_CODE']; ?>"><?php echo $data1['CC_LIBELLE']; ?></option>
						<?php
					}

				}
				?>
				</select>
			</td></tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix de l'article</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="4" name="PRIXART" value='<?php echo $data['AR_TARIF']; ?>'></td>
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;">Photo de l'article</td>
				<td style="height: 28px; width: 212px;">
					<input type="hidden" name="MAX_FILE_SIZE" value="250000" />
					<input tabindex="5" type="file" name="fic" size=50 />
				</td>
			</tr>
			<?php
			if(!empty($data['AR_IMAGE']))
			{
				?>
				<tr>
					<td align="center" colspan="2" style="height: 28px;">
						<img src="img/<?php echo $data['AR_IMAGE']; ?>" width="200" height="200">
					</td>
				</tr>
				<?php
			}
			
			  if ($data['AR_TYPE'] == 'ARTRE')
			  {
				  ?>
				  <tr>
					<td style="width: 304px; height: 28px;"><label id="HEUREDEBUTTXT">Heure de début : </label></td>
					<td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="6" id="HEUREDEBUT" name="HEUREDEBUT" required  value='<?php echo $data['AR_RESAHEUREDEBUT']; ?>'></td>
				  </tr>
				  <?php
			  }
			  
			 if ($data['AR_TYPE'] == 'ARTRE')
			  {
				  ?>
					<tr>
					  <td style="width: 304px; height: 28px;"><label id="HEUREFINTXT">Heure de fin : </label></td>
					  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="7" id="HEUREFIN" name="HEUREFIN" required value='<?php echo $data['AR_RESAHEUREFIN']; ?>'></td>
					</tr>
					<?php
			  }
			  ?>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		<input type="hidden" value="<?php echo $data['AR_TYPE']; ?>" name="TYPEARTICLE">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			$sql = 'select * from ARTICLE WHERE AR_ARTICLENO = "'.$_GET['numero'].'";';
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				$img_nom = $data['AR_IMAGE'];
				$codearticle = $data['AR_CODEARTICLE'];
			}
			$ret        = false;
			$img_blob   = '';
			$img_taille = 0;
			
			$taille_max = 250000;
			if(!empty($_FILES['fic']['name']))
				{
				$ret        = is_uploaded_file($_FILES['fic']['tmp_name']);
				if (!$ret) {
					echo "Problème de transfert";
					return false;
				} 
				else 
				{
					$repertoireDestination = dirname(__FILE__)."/";
					$repertoireDestination = substr( $repertoireDestination,0,strpos( $repertoireDestination, 'include' )) ;
					$repertoireDestination = $repertoireDestination  . 'img/';
					$nomDestination = 'ARTICLE_'.$_GET['numero'].substr($_FILES['fic']['name'],strpos($_FILES['fic']['name'],'.'),6);
					if (rename($_FILES['fic']['tmp_name'], $repertoireDestination.$nomDestination))
					{
						$sql = 'UPDATE ARTICLE SET AR_IMAGE = "'.$nomDestination.'" WHERE AR_ARTICLENO = "'.$_GET['numero'].'";';
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->exec($sql);
					}
					else 
					{
						echo "Le déplacement du fichier temporaire a échoué".
								" vérifiez l'existence du répertoire ".$repertoireDestination;
					}          
					
					$img_type = $_FILES['fic']['type'];
					$img_nom  = $_FILES['fic']['name'];
				}
			}
			
			
			
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			if ($_POST['TYPEARTICLE'] != 'ARTRE')
			{
				$sql = 'UPDATE ARTICLE SET AR_DESIGNATION = "'.$_POST['LIBELLEART'].'", AR_TYPE = "'.$_POST['TYPEARTICLE'].'",AR_TARIF = "'.$_POST['PRIXART'].'" WHERE AR_ARTICLENO = "'.$_GET['numero'].'";';
			}
			else
			{
				$sql = 'UPDATE ARTICLE SET AR_DESIGNATION = "'.$_POST['LIBELLEART'].'", AR_TYPE = "'.$_POST['TYPEARTICLE'].'",AR_TARIF = "'.$_POST['PRIXART'].'", AR_RESAHEUREDEBUT = "'.$_POST['HEUREDEBUT'].'",
						AR_RESAHEUREFIN = "'.$_POST['HEUREFIN'].'", AR_FAMILLE1 = "'.$_POST['FAMNIV1'].'", AR_FAMILLE2 = "'.$_POST['FAMNIV2'].'"
						WHERE AR_ARTICLENO = "'.$_GET['numero'].'";';
			}
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			echo "Mise à jour de l'article " .$codearticle ." - ".$_POST['LIBELLEART'] ." terminé";
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}