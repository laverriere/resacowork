﻿<?php
/**
 * function_admutilisateur.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2019-01-30 12:00:00 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: 3.1.1
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


include ("include/fonction_general.php");

require ("include/fonction_email.php");

//require ("scripts/constantes.php");
//require ("scripts/fonctions.php");

function create_utilisateur()
{
?>
<form id="formplaning" action="" method="post" width="30%">

<p align="center">Création d'un nouvel adhérant</p>
<p>Saisir le login (email) : <input type="text" id="newloginemail" name="newloginemail"  size="50" maxlength="250" required></p>
<label>Type d'adhérant : </label>
<select name="newloginstatut">
	<option value="ADMIN">Administrateur</option>
	<option value="COWORKER">CoWorker</option>
	<option value="PERMANANT">Permanant</option>
</select>
<input type="hidden" name="idvalid" value="<?php uniqid('', true); ?>">
<br /><br />
<input align="center" type="submit" class="bouton3" value="Valider" />
</form>
	<?php

}

function insert_utilisateur()
{
	$idvalid=uniqid('', true);

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from UTILISATEUR WHERE UT_ETABLISSEMENT = '" .$_SESSION['ETABLISSEMENT'] ."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	$sql= "SELECT count(*) as CPT from UTILISATEUR where UT_LOGIN = '".$_POST['newlogin']."';";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$NbUser = $data['CPT'];
	}

	echo $data['CPT'];
	if ($NbUser != 0)
	{
		echo "Utilisitateur " .$_POST['newlogin'] ." est deja en base";
	}
	else
	{
		$sql = "INSERT INTO UTILISATEUR (`UT_LOGIN`, UT_STATUT, `UT_ETABLISSEMENT`, UT_IDMODIF, UT_IDDATEDEMANDE) VALUES
				('".$_POST['newloginemail']."','".addslashes($_POST['newloginstatut'])."','".$_POST['newloginetabliss']."','".$idvalid."',now());";
		$sql = "INSERT INTO UTILISATEUR(UT_LOGIN, UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_ADRESSE2, UT_VILLE, UT_CODEPOSTAL, UT_EMAIL, UT_STATUT, UT_ETABLISSEMENT,
				UT_VALIDE, UT_IDMODIF, UT_IDDATEDEMANDE, UT_TIERSGESTION) VALUES
				('".addslashes($_POST['newlogin'])."','".addslashes($_POST['newloginnom'])."','".addslashes($_POST['newloginprenom'])."',
				'".addslashes($_POST['newloginadresse1'])."','".addslashes($_POST['newloginadresse2'])."','".addslashes($_POST['newloginville'])."',
				'".addslashes($_POST['newlogincodepostal'])."','".addslashes($_POST['newloginemail'])."','".addslashes($_POST['newloginstatut'])."','".addslashes($_SESSION['ETABLISSEMENT'])."','N','".$idvalid."',
				now(),'".addslashes($_POST['newlogintiers'])."');";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);

		echo "Utilisitateur " .$_POST['newlogin'] . " a été créé. Il va recevoir un email de validation.";
	}


require "PHPMailer/class.phpmailer.php";
$mail = new PHPMailer();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth   = true;
$mail->Port = 587; // Par défaut

// Authentification
$mail->Username = "scouty59530@gmail.com";
$mail->Password = "ofigckuiycqqzipk";

// Expéditeur
$mail->SetFrom('scouty59530@gmail.com', 'Nom Prénom');
// Destinataire
$mail->AddAddress('jr.menu@gmail.com', 'Nom Prénom');
// Objet
$mail->Subject = 'Objet du message';

// Votre message
$mail->MsgHTML('Contenu du message en HTML');
$mail->Body='<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<STYLE type="text/css">
#titre
{
color:white;
background-color:#097855;
width:60%;
font-weight:bold;
font-size:20px;
font-family:Times New Roman, serif;
text-transform:uppercase;
}

#text1
{
width:60%;
color:black;
font-family: "Century Gothic", Geneva, sans-serif;
	font-size: 13px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;
	line-height: 18.5714px;
text-transform:uppercase;
text-align:center;
}

#text2
{
color:black;
font-family: "Century Gothic", Geneva, sans-serif;
	font-size: 13px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;
	line-height: 18.5714px;
text-align:left;
}

</STYLE>
</head>
<body>
<table border="0" style="width: 50%;">
<tr><td ><div id="titre">Gestion CoWorking La Verriere</div></td></tr>
<tr><td><div id="text1">Inscription</div></td></tr>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">Bonjour,</div></td></td>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">Veuillez cliquer sur le lien pour activer votre compte.</div></td></td>
<tr><td><div id="text2"><a href="http://78.237.199.235/laverrieretest/login/valide_utilisateur.php?id='.$idvalid.'">Validation du compte</div></td></td>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">L\'équipe de la Vérrière.</div></td></td>
</table>
</body>
</html>';

// Envoi du mail avec gestion des erreurs
if(!$mail->Send()) {
  echo 'Erreur : ' . $mail->ErrorInfo;
} else {
  echo 'Message envoyé !';
}
 printf("uniqid('', true): %s\r\n", uniqid('', true));

}

function pwd_forget($email)
{
	echo $email;
	connectsql();

	echo $db_user;
	$idvalid=uniqid('', true);
	$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
	$sql= "SELECT count(*) as CPT from UTILISATEUR where UT_LOGIN = '".$email."';";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while ($data = mysqli_fetch_array($req))
		{
		echo $data['CPT'];
		$NbUser = $data['CPT'];
		}
	mysqli_close($conn);
	echo $NbUser;
	if ($NbUser == 0)
	{
		echo "L'utisitateur ' .$login .' n'existe pas !";
	}
	else
	{
		$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
		$sql = "UPDATE UTILISATEUR  SET UT_IDMODIF = '".$idvalid."' WHERE UT_LOGIN = '".$email."'";
		$req = $conn->query($sql) or die('Erreur SQL !<br>');
		mysqli_close($conn);

		echo "Vous allez recevoir un email !";
	}
	mysql_close();

	echo "test";

require "PHPMailer/class.phpmailer.php";
$mail = new PHPMailer();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth   = true;
$mail->Port = 587; // Par défaut

// Authentification
$mail->Username = "scouty59530@gmail.com";
$mail->Password = "ofigckuiycqqzipk";

// Expéditeur
$mail->SetFrom('scouty59530@gmail.com', 'Nom Prénom');
// Destinataire
$mail->AddAddress('jr.menu@gmail.com', 'Nom Prénom');
// Objet
$mail->Subject = 'Objet du message';

// Votre message
$mail->MsgHTML('Contenu du message en HTML');
$mail->Body='<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<STYLE type="text/css">
#titre
{
color:white;
background-color:#097855;
width:60%;
font-weight:bold;
font-size:20px;
font-family:Times New Roman, serif;
text-transform:uppercase;
}

#text1
{
width:60%;
color:black;
font-family: "Century Gothic", Geneva, sans-serif;
	font-size: 13px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;
	line-height: 18.5714px;
text-transform:uppercase;
text-align:center;
}

#text2
{
color:black;
font-family: "Century Gothic", Geneva, sans-serif;
	font-size: 13px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;
	line-height: 18.5714px;
text-align:left;
}

</STYLE>
</head>
<body>
<table border="0" style="width: 50%;">
<tr><td ><div id="titre">Gestion CoWorking La Verriere</div></td></tr>
<tr><td><div id="text1">Inscription</div></td></tr>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">Bonjour,</div></td></td>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">Veuillez cliquer sur le lien pour activer votre compte.</div></td></td>
<tr><td><div id="text2"><a href="'.$URL.'"/login/valide_utilisateur.php?id='.$idvalid.'">Validation du compte</div></td></td>
<tr><td><div id="text2"></div></td></td>
<tr><td><div id="text2">L\'équipe de la Vérrière.</div></td></td>
</table>
</body>
</html>';

// Envoi du mail avec gestion des erreurs
if(!$mail->Send()) {
  echo 'Erreur : ' . $mail->ErrorInfo;
} else {
  echo 'Message envoyé !';
}
 printf("uniqid('', true): %s\r\n", uniqid('', true));

}

function popup_valide_utilisateur()
{
?>
<form id="formplaning" action="" method="post">
<br />
<p>Saisir le mot de passe : <input type="text" id="newloginpassword1" name="newloginpassword1"  size="50" maxlength="50" required></p>
<p>Saisir le mot de passe : <input type="text" id="newloginpassword2" name="newloginpassword2"  size="50" maxlength="50" required></p>
<input type="hidden" id="validmdp" name="validmdp" value="validmdp">

<br /><br />
<input type="submit" value="Valider" />
</form>
	<?php

}

function modif_utilisateur($statut)
{
?>

	<form id="formplaning" action="" method="post">
	<input type="hidden" name="action" id="action" value="modif">
	<input type="hidden" name="statut" id="statut" value="<?php echo $statut; ?>">



  <table style="width: 100%; height: 144px; padding-left: 20px; padding-right: 20px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
    <tbody>
	<tr>
	  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Gestion des utilisateurs</td>
	<tr>
	<?php

	if ($statut == 'admin')
	{
		if (isset($_SESSION['ETABADMIN']))
		{
			$sql= "SELECT * from UTILISATEUR LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' WHERE UT_LOGIN = '" .$_GET['numero']. "';";
		}
		else
		{
			$sql= "SELECT * from UTILISATEUR WHERE UT_LOGIN = '" .$_GET['numero']. "';";
		}
		?>
		<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_GET['numero']; ?>">
		<?php
	}
	else
	{
		$sql= "SELECT * from UTILISATEUR LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' WHERE UT_LOGIN = '" .$_SESSION['login']. "';";
		?>
		<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_SESSION['login']; ?>">
		<?php
	}

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		?>
		<tr>
			<td style="width: 160px;">Login : </td>
			<td style="width: 460px;">
				<input id="newloginid" name="newloginid" size="50" maxlength="50" disabled="disabled" value="<?php echo addslashes($data['UT_LOGIN']);?>" type="text">
			</td>
			<td style="width: 200px;">Email : </td>
			<td style="width: 460px;"><input id="newloginemail" name="newloginemail" size="150" maxlength="250" required="" value="<?php echo decrypt($data['UT_EMAIL'],$data['UT_ID2']);?>" type="text"></td>
		</tr>
		<?php
		if ($statut == 'ssadmin')
		{
			?>
			<tr>
			<td style="width: 160px;">Tiers Gestion : </td>
			<td style="width: 460px;">
				<input id="newlogintiers" name="newlogintiers" size="50" maxlength="50" required="" value="<?php echo decrypt($data['UT_TIERSGESTION'],$data['UT_ID2']);?>" type="text">
			</td>
			<td style="width: 160px;">
			<td style="width: 460px;"></td>
			<td style="width: 115px; "></td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td style="width: 160px;">Nom : </td>
			<td style="width: 460px;">
				<input id="newloginnom" name="newloginnom" size="50" maxlength="50" required="" value="<?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?>" type="text">
			</td>
			<td style="width: 200px;">Prénom :</td>
			<td style="width: 460px;">
				<input style="border: 1px solid rgb(0, 0, 0);" id="newloginprenom" name="newloginprenom" size="50" maxlength="50" required="" value="<?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?>" type="text">
			</td>
		</tr>
		<tr>
			<td style="width: 160px;">Adresse : </td>
			<td style="width: 460px;">
				<input id="newloginadresse1" name="newloginadresse1" size="50" maxlength="50" required="" value="<?php if($data['UT_ADRESSE1'] != '') { echo decrypt($data['UT_ADRESSE1'],$data['UT_ID2']);} ?>" type="text">
			</td>
			<td style="width: 200px;">Adresse complémentaire : </td>
			<td style="width: 460px;">
				<input id="newloginadresse2" name="newloginadresse2" size="50" maxlength="50" value="<?php if($data['UT_ADRESSE2'] != '') { echo decrypt($data['UT_ADRESSE2'],$data['UT_ID2']);} ?>" type="text">
			</td>
		</tr>
		<tr>
			<td style="width: 160px;">Code postal : </td>
			<td style="width: 460px;">
			<input id="newlogincodepostal" name="newlogincodepostal" size="5" maxlength="5" required="" value="<?php if($data['UT_CODEPOSTAL'] != '') { echo decrypt($data['UT_CODEPOSTAL'],$data['UT_ID2']);} ?>" type="text">
			</td>
			<td style="width: 160px;">Ville : </td>
			<td style="width: 460px;">
				<input id="newloginville" name="newloginville" size="50" maxlength="50" value="<?php if($data['UT_VILLE'] != '') { echo decrypt($data['UT_VILLE'],$data['UT_ID2']);} ?>" type="text">
			</td>
		</tr>
		<?php
		if ($statut == 'admin')
		{
			?>
			<tr>
				<td style="width: 160px;">Type d'adhérant : </td>
				<td style="width: 460px;">
					<select name="newloginstatut">
					<?php
					if ($data['UT_STATUT'] == 'ADMIN')
					{
						?>
						<option value="ADMIN">Administrateur</option>
						<option value="COWORKER">CoWorker</option>
						<option value="PERMANANT">Permanant</option>
						<?php
					}
					if ($data['UT_STATUT'] == 'COWORKER')
					{
						?>
						<option value="COWORKER">CoWorker</option>
						<option value="ADMIN">Administrateur</option>
						<option value="PERMANANT">Permanant</option>
						<?php
					}
					if ($data['UT_STATUT'] == 'PERMANANT')
					{
						?>
						<option value="PERMANANT">Permanant</option>
						<option value="COWORKER">CoWorker</option>
						<option value="ADMIN">Administrateur</option>
						<?php
					}
					?>
					</select>
				</td>
				<td style="width: 160px;">Utilisateur bloqué</td>
				<td style="width: 460px;">
					<select name="newloginvalide">
					<?php
					if ($data['UT_VALIDE'] == 'O')
					{
						?>
						<option value="O">OUI</option>
						<option value="N">NON</option>
						<?php
					}
					else
					{
						?>
						<option value="N">NON</option>
						<option value="O">OUI</option>
						<?php
					}

					?>
					</select>
				</td>
			</tr>
			<?php
			if (isset($_SESSION['ETABADMIN']))
			{
				?>
				<tr>
					<td style="width: 160px;">Crédit autorisé : </td>
					<td style="width: 460px;">
						<select name="creditok">
						<?php
							echo '<OPTION value="">Vide ...</option>';
							if ($data['TE_CREDITOK'] == 'OUI')
							{
								echo '<OPTION selected="selected" value="OUI">OUI</option>';
								echo '<OPTION value="NON">NON</option>';
							}
							else
							{
								echo '<OPTION  value="OUI">OUI</option>';
								echo '<OPTION selected="selected" value="NON">NON</option>';
							}

							?>
						</SELECT>
					</td>
					<td style="width: 160px;">Montant Crédit : </td>
					<td style="width: 460px;">
						<input disabled id="newcredit" name="newcredit" size="50" maxlength="50" value="<?php echo number_format($data['TE_CREDITVAL'],2,',','') .' €';?>" type="text">
					</td>
				</tr>
				<tr><td colspan="4" style="height: 20px;"></td></tr>
				<tr>
					<td style="width: 160px;">Nouveau mot de passe : </td>
					<td style="width: 460px;">
						<input id="newloginville" name="newpassword" size="50" maxlength="50"  type="password">
					</td>
				</tr>
				<?php
			}
		}
		?>
		<?php
	}


	?>
	<tr><td colspan="6" style="height: 30px;"></td></tr>
	<?php
	if ((isset($_SESSION['SUPERADMIN'])) && ($_SESSION['SUPERADMIN'] == 'OUI'))
	{
		?>
		<tr>
		<td colspan="2">Super Utilisateur</td>
		<td style="width: 460px;">
			<select name="super_admin">
			<?php
			if ($data['UT_SUPERADMIN'] == 'OUI')
			{
				?>
				<option value="OUI">OUI</option>
				<option value="NON">NON</option>
				<?php
			}
			else
			{
				?>
				<option value="NON">NON</option>
				<option value="OUI">OUI</option>
				<?php
			}

			?>
			</select>
		</td>
		</tr>
		<tr>
		<td colspan="2">Utilisateur administrateur du site</td>
		<td style="width: 460px;">
			<select name="admin_site">
			<?php
			$sql = "SELECT ET_ETABLISSEMENT, ET_LIBELLE, (SELECT COUNT(UT_ADMINSITE) FROM UTILISATEUR WHERE UT_LOGIN = '".$_GET['numero']."' AND UT_ADMINSITE = ET_ETABLISSEMENT) AS ADMINSITE
					FROM ETABLISSEMENT
					WHERE ET_BLOQUE = 'NON'
					ORDER BY ADMINSITE DESC;";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
				?>
				<option value="<?php echo $data1['ET_ETABLISSEMENT'];?>"><?php echo $data1['ET_LIBELLE'];?></option>
				<?php
			}

			?>
			</select>
		</td>
		</tr>
		<tr><td colspan="6" style="height: 30px;"></td></tr>
		<?php
		$nbetab = 0;
		$sql = "SELECT ET_LIBELLE, ET_ETABLISSEMENT, 'OUI' as VALIDE
				FROM ETABLISSEMENT
				LEFT JOIN TIERSETAB ON TE_ETABLISSEMENT = ET_ETABLISSEMENT
				WHERE TE_LOGIN = '".$data['UT_LOGIN']."'
				UNION ALL
				SELECT ET_LIBELLE, ET_ETABLISSEMENT, 'NON' as VALIDE
				FROM ETABLISSEMENT
				WHERE ET_ETABLISSEMENT NOT IN (SELECT TE_ETABLISSEMENT FROM TIERSETAB WHERE TE_LOGIN = '".$data['UT_LOGIN']."')";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data1)
		{
			$nbetab++;
			?>

				<tr>
				<input type="hidden" name="ETAB<?php echo $nbetab;?>" id="ETAB<?php echo $nbetab;?>" value="<?php echo $data1['ET_ETABLISSEMENT']; ?>">
				<td colspan="2">Adhésion <?php echo $data1['ET_ETABLISSEMENT']  .' - ' .$data1['ET_LIBELLE'] ;?></td>
				<td>
				<?php echo '<select name="COMBOADHE' .$nbetab .'">';
				if ($data1['VALIDE'] == 'OUI')
				{
					?>
					<option value="OUI" selected="selected">OUI</option>
					<option value="NON">NON</option>
					<?php
				}
				else
				{
					?>
					<option value="OUI">OUI</option>
					<option value="NON" selected="selected">NON</option>
					<?php
				}

				?>
				</select>
				</td>
				</tr>
			<?php

		}

		?>
		<input type="hidden" name="nbetab" id="nbetab" value="<?php echo $nbetab; ?>">
		<?php
	}
	?>
	<tr>

		<td colspan="6" style="height: 30px;"></td>

	</tr>
	<tr>
			<td colspan="6" align="center">
			<input class="bouton1" value="Valider" type="submit">
		</td>

	</tr>
    </tbody>
  </table>
  <br>
</form>

<?php
}

function popup_delete_utilisateur($statut)
{
?>

	<form id="formplaning" action="" method="post">
	<input type="hidden" name="action" id="action" value="suppression">
	<input type="hidden" name="statut" id="statut" value="<?php echo $statut; ?>">


  <table style="width: 100%; height: 144px; padding-left: 20px; padding-right: 20px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
    <tbody>
	<tr>
	  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Suppression d'un utilisateur</td>
	<tr>
	<?php

	if ($statut == 'admin')
	{
		$sql= "SELECT * from UTILISATEUR WHERE UT_LOGIN = '" .$_GET['numero']. "';";
		?>
		<input type="hidden" name="newlogin" id="newlogin" value="<?php echo $_GET['numero']; ?>">
		<?php
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
		?>
		<tr>
			<td style="width: 160px;">Login : </td>
			<td style="width: 460px;">
				<input id="newloginid" name="newloginid" size="50" maxlength="50" disabled="disabled" value="<?php echo addslashes($data['UT_LOGIN']);?>" type="text">
			</td>
		</tr>
		<tr>
			<td style="width: 160px;">Nom : </td>
			<td style="width: 460px;">
				<input id="newloginnom" name="newloginnom" size="50" maxlength="50" required="" value="<?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?>" type="text" disabled>
			</td>
			<td style="width: 200px;">Prénom :</td>
			<td style="width: 460px;">
				<input style="border: 1px solid rgb(0, 0, 0);" id="newloginprenom" name="newloginprenom" size="50" maxlength="50" required="" value="<?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?>" type="text" disabled>
			</td>
		</tr>
		<tr>
			<td colspan="6" style="height: 30px;"></td>
		</tr>
		<tr>
			<td colspan="6" align="center">
				<input class="bouton1" value="Suppression" type="submit">
			</td>
		</tr>
		<?php
		}
	}
		?>
    </tbody>
  </table>
  <br>
</form>

<?php
}


function update_utilsateur()
{
	$sql= "SELECT count(*) as CPT from UTILISATEUR where UT_LOGIN = '".$_POST['newlogin']."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$NbUser = $data['CPT'];
	}

	if ($NbUser == 0)
	{
		echo "utisitateur " .$_POST['newlogin'] ." n existe pas";
	}
	else
	{
		$sql= "SELECT UT_ID1, UT_ID2 from UTILISATEUR where UT_LOGIN = '".$_POST['newlogin']."';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$ID1 = $data['UT_ID1'];
			$ID2 = $data['UT_ID2'];
		}
		if ($_POST['statut'] == 'admin')
		{
			if ((isset($_SESSION['SUPERADMIN'])) && ($_SESSION['SUPERADMIN'] == 'OUI'))
			{
				$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes(encrypt2($_POST['newloginnom'],$ID2))."', UT_PRENOM = '".addslashes(encrypt2($_POST['newloginprenom'],$ID2))."',
						UT_ADRESSE1 = '".addslashes(encrypt2($_POST['newloginadresse1'],$ID2))."',
						UT_ADRESSE2 = '".addslashes(encrypt2($_POST['newloginadresse2'],$ID2))."', UT_VILLE = '".addslashes(encrypt2($_POST['newloginville'],$ID2))."',
						UT_CODEPOSTAL = '".addslashes(encrypt2($_POST['newlogincodepostal'],$ID2))."', UT_VALIDE = '".addslashes($_POST['newloginvalide'])."',
						UT_SUPERADMIN = '".$_POST['super_admin']."', UT_ADMINSITE = '".$_POST['admin_site']."',
						UT_EMAIL = '".addslashes(encrypt2($_POST['newloginemail'],$ID2))."'
						WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				for ($i = 1; $i <= $_POST['nbetab']; $i++)
				{
					$sql = "SELECT COUNT(TE_LOGIN) AS NBTIERS FROM TIERSETAB WHERE TE_LOGIN = '" .$_POST['newlogin']. "' AND TE_ETABLISSEMENT = '".$_POST['ETAB'.$i]."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data)
					{
						if ($data['NBTIERS'] != 0)
						{
							if ($_POST['COMBOADHE'.$i] == 'OUI')
							{
								$sql = "UPDATE TIERSETAB SET TE_STATUT = 'ATTENTE', TE_DATEFINADHESION = '1900-01-01' WHERE TE_LOGIN = '" .$_POST['newlogin']. "' AND TE_ETABLISSEMENT = '".$_POST['ETAB'.$i]."';";
								$cnx_bdd = ConnexionBDD();
								$result_req = $cnx_bdd->exec($sql);
							}
							else
							{

								$sql = "DELETE FROM TIERSETAB WHERE TE_LOGIN = '" .$_POST['newlogin']. "' AND TE_ETABLISSEMENT = '".$_POST['ETAB'.$i]."';";
								$cnx_bdd = ConnexionBDD();
								$result_req = $cnx_bdd->exec($sql);

							}
						}
						else
						{
							if ($_POST['COMBOADHE'.$i] == 'OUI')
							{
								$sql = "INSERT INTO TIERSETAB (TE_LOGIN, TE_ETABLISSEMENT, TE_STATUT, TE_DEBUTADHESION, TE_DATEFINADHESION, TE_CREDITOK, TE_CREDITVAL)
										VALUES ('".$_POST['newlogin']."','".$_POST['ETAB'.$i]."','ATTENTE','1900-01-01','1900-01-01','NON',0);";
								$cnx_bdd = ConnexionBDD();
								$result_req = $cnx_bdd->exec($sql);
							}
						}

					}
				}
			}
			if (!isset($_SESSION['SUPERADMIN']))
			{
				$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes(encrypt2($_POST['newloginnom'],$ID2))."', UT_PRENOM = '".addslashes(encrypt2($_POST['newloginprenom'],$ID2))."', UT_ADRESSE1 = '".addslashes(encrypt2($_POST['newloginadresse1'],$ID2))."',
						UT_ADRESSE2 = '".addslashes(encrypt2($_POST['newloginadresse2'],$ID2))."', UT_VILLE = '".addslashes(encrypt2($_POST['newloginville'],$ID2))."',
						UT_CODEPOSTAL = '".addslashes(encrypt2($_POST['newlogincodepostal'],$ID2))."', UT_VALIDE = '".addslashes($_POST['newloginvalide'])."',
						UT_STATUT = '".addslashes($_POST['newloginstatut'])."',
						UT_EMAIL = '".addslashes(encrypt2($_POST['newloginemail'],$ID2))."'
						WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
						$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$sql = "UPDATE TIERSETAB SET TE_CREDITOK = '".$_POST['creditok']."' WHERE TE_LOGIN = '".$_POST['newlogin']. "' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}
			if(!empty($_POST['newpassword']))
			{
				$mdp = hash('sha512', $ID1.$_POST['newpassword'].$ID2);
				$sql = "UPDATE UTILISATEUR SET UT_PASSWORD = '".$mdp."'
						WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
						$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}


		}
		else
		{
		$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes($_POST['newloginnom'])."', UT_PRENOM = '".addslashes($_POST['newloginprenom'])."', UT_ADRESSE1 = '".addslashes($_POST['newloginadresse1'])."',
				UT_ADRESSE2 = '".addslashes($_POST['newloginadresse2'])."', UT_VILLE = '".addslashes($_POST['newloginville'])."',
				UT_CODEPOSTAL = '".addslashes($_POST['newlogincodepostal'])."',	UT_EMAIL = '".addslashes($_POST['newloginemail'])."'
				WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);

		echo "Utisitateur " .$_POST['newlogin']." a été modifié";
		if ($_POST['statut'] == 'admin')
		{
			?>
			<a href="javascript:myclosewindow();">Fermer</a>
			<?php
		}
		else
		{
			?>
			<a href="mon_compte.php">Fermer</a>
			<?php
		}


	}
}

function delete_utilisateur()
{
	if ((isset($_SESSION['ETABADMIN'])) && (!empty($_SESSION['ETABADMIN'])))
	{
		$sql= "SELECT count(*) as CPT from UTILISATEUR where UT_LOGIN = '".$_POST['newlogin']."';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$NbUser = $data['CPT'];
		}

		if ($NbUser == 0)
		{
			echo "utisitateur " .$_POST['newlogin'] ." n existe pas";
		}
		else
		{
			$sql = "DELETE FROM UTILISATEUR WHERE UT_LOGIN = '" .$_POST['newlogin']. "';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			$sql = "DELETE FROM TIERSETAB WHERE TE_LOGIN = '" .$_POST['newlogin']. "';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			echo "Utisitateur " .$_POST['newlogin']." a été modifié";
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}

function popup_create_facture()
{
	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	else
	{
		$user = $_GET['user'];
	}

	?>
	<form action="" method="post">
	<div id="facture" style="width:80%;">
	<label>Utilisateur</label>
	<select align="left" name="listuser" id="listuser" onchange="window.location='create_facture.php?user='+this.value;">
		<OPTION align="left">Choisir un autre utilisateur</OPTION>
		<?php
		$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'
		AND RE_FACTURE = 'NON' AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL
		GROUP BY RE_USER;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			echo  $r['RE_USER'];
			?>
			<option value="<?php echo $r['RE_USER']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php
		}
		?>
	</SELECT>

	<?php
	if (!isset($_GET['user']))
	{
		$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'NON' GROUP BY RE_USER LIMIT 1;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$userfacture =  $r['RE_USER'];
		}
	}
	else
	{
		$userfacture =  $_GET['user'];
	}

	$NbRemise = 0;
	$sql = "select UTILISATEUR.*, (select count(*) from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'OUI' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as NBJOURSFACTURE,
			(select count(*) from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as NBJOURSNONFACTURE,
			(SELECT SUM(BA_VALEUR) FROM VOUCHER WHERE BA_LOGIN = UT_LOGIN AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') AS MNTVOUCHER,
			(SELECT SUM(GLA_QTEFACT * GLA_TOTALHT) FROM LIGNEATTENTE WHERE GLA_USER = UT_LOGIN AND GLA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND GLA_VALIDE = 'OUI') AS MNTATTENTE
			from UTILISATEUR where UT_LOGIN = '" .$userfacture . "';";
//
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $r)
		{
		echo '<p align="left"><u>Login : ' .$r['UT_LOGIN'] . ' </u></p>';

		echo '<p align="left">Nom : ' .decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_PRENOM'],$r['UT_ID2']) .' </p>';
		echo '<br>';
		echo '<p align="left">Nombre de jours facturable : ' .$r['NBJOURSNONFACTURE'] . '</p>';
		echo '<p align="left">Montant en attente de facturation : ' .number_format($r['MNTATTENTE'],2,',','') . ' €</p>';
		echo '<p align="left">Montant remise : ' .number_format($r['MNTVOUCHER'],2,',','') . ' €</p>';
		echo '<br>';
		?>
		<p align="left"><u>Détail des journées facturées :</u><input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="ResaAll" name="ResaAll" value="TOUTES" onclick="cocher_tout('resa','ResaAll')"> Toutes</input></p>
		<?php
		$NbRemise = $r['MNTVOUCHER'];
		?>

		<?php

		}
	$i = 1;
	$sql = "SELECT RE_USER, RE_ZONE, RE_ZONELIBELLE,RE_NUMRESA, RE_PRIXPLACE, RE_PRIXPARPERSONNE, RE_PRIXTOTALRESA,
			UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE,
			DATE_FORMAT(RE_DATE,'%d') AS DATERESA, DATE_FORMAT(RE_DATE,'%Y') AS DATERESA1, CC_LIBELLE
			FROM RESERVATION
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
			LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
			LEFT JOIN CHOIXCODE ON CC_TYPE = 'MOIS' AND CC_CODE = RE_MOIS
			WHERE RE_USER = '" .$userfacture . "' AND RE_FACTURE = 'NON' AND RE_CREDITCLI = 'OUI' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
			ORDER BY RE_ZONE, RE_NUMRESA;";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $r)
		{
			?>
			<p align="left"><input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="resa<?php echo $i;?>" name="resa<?php echo $i;?>" value="<?php echo $r['RE_NUMRESA'];?>"><?php echo 'Reservation ' .$r['RE_ZONELIBELLE'] .' n° ' .$r['RE_NUMRESA'] .' du ' .$r['DATERESA'] .' ' .$r['CC_LIBELLE'] .' ' .$r['DATERESA1'] .' - Montant : ' .number_format($r['RE_PRIXTOTALRESA'],2,',','') .' €';?></input></p>
			<?php
			$i++;
		}
	echo '<input name="nbresa" type="hidden" value="'.$i.'">';
	echo '<br>';
	if ($NbRemise != 0)
	{
		?>
		<p align="left"><u>Remise :</u><input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="VoucherAll" name="VoucherAll" value="TOUTES" onclick="cocher_tout('remi','VoucherAll')"> Toutes</input></p>
		<?php
		$i = 0;
		$sql = "SELECT DATE_FORMAT(BA_DATECREATION,'%d/%m/%Y') AS DATEVOUCHER, BA_NUMERO, BA_LIBELLE, BA_VALEUR FROM VOUCHER WHERE BA_LOGIN = '".$userfacture."' AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND BA_VALIDE = 'OUI' AND BA_DATEREPRISE = '1900-01-01';";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
			{
				?>
				<p align="left"><input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="remise<?php echo $i;?>" name="remise<?php echo $i;?>" value="<?php echo $r['BA_NUMERO'];?>"><?php echo $r['BA_LIBELLE'] .' du ' .$r['DATEVOUCHER'] .' - Montant : ' .number_format($r['BA_VALEUR'],2,',','') .' €';?></input></p>
				<?php
				$i++;
			}
	}
	echo '<input name="nbremise" type="hidden" value="'.$i.'">';
	?>
	<br>
	<input name="login" type="hidden" value="<?php echo $userfacture;?>">
	<input name="action" type="hidden" value="CREATEFACTURE">
	<br>
	<center><input type="submit" value="Valider" /></center>
	<br>
	</form>
	</div>

		<?php
	// fin connexion
}

function creation_facture()
{

	$CritereSQL = '';
	$CritVoucherSQL = '';
	$NbrResa = 0;
	$NbrVoucher = 0;
	for ($i = 0; $i <= $_POST['nbresa'] -1; $i++)
	{

		if (isset($_POST['resa'.$i]))
		{
			if ($NbrResa == 0)
			{
				$CritereSQL = " AND RE_NUMRESA IN ('".$_POST['resa'.$i]."'";
			}
			else
			{
				$CritereSQL = $CritereSQL .",'".$_POST['resa'.$i]."'";
			}
			$NbrResa++;
		}
	}

	if ($NbrResa != 0)
	{
		$CritereSQL = $CritereSQL .") ";
	}

	for ($i = 0; $i <= $_POST['nbremise'] -1; $i++)
	{
		if (isset($_POST['remise'.$i]))
		{
			if ($NbrVoucher == 0)
			{
				$CritVoucherSQL = " AND BA_NUMERO IN ('".$_POST['remise'.$i]."'";
			}
			else
			{
				$CritVoucherSQL = $CritVoucherSQL .",'".$_POST['remise'.$i]."'";
			}
			$NbrVoucher++;
		}
	}
	if ($NbrVoucher != 0)
	{
		$CritVoucherSQL = $CritVoucherSQL .") ";
	}

	if ($NbrResa != 0)
	{
		$sql = "SELECT RE_USER, COUNT(RE_NUMRESA) AS NBJOURSFACTURE, SUM(RE_NBRPLACE) AS NBPLACE, SUM(RE_PRIXTOTALRESA) AS TOTALFACT,
				(SELECT ifnull(SUM(BA_VALEUR),0) FROM VOUCHER WHERE BA_LOGIN = '".$_POST['login']."' AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritVoucherSQL ." AND BA_VALIDE = 'OUI') AS TOTALVOUCHER,
				UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE

				FROM RESERVATION
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
				LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
				WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_CREDITCLI = 'OUI' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritereSQL ."

				GROUP BY RE_USER,   UT_NOM, UT_PRENOM, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE
				, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE LIMIT 1;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<div id="facture">
			<form action="" method="post">
			<input type="hidden" name="login" required size="50" value="<?php echo $r['RE_USER'];?>" >
			<input name="action" type="hidden" value="VALIDEFACTURE">
			<input name="CritereSQL" type="hidden" value="<?php echo $CritereSQL;?>">
			<input name="CritVoucherSQL" type="hidden" value="<?php echo $CritVoucherSQL;?>">
			<br>

			<?php
			$months = array("janvier", "février", "mars", "avril", "mai", "juin",
				"juillet", "août", "septembre", "octobre", "novembre", "décembre");
			?>
			<p align="left"><u>Date de la facture :</u> <?php echo date("d") .' ' .$months[date("m")-1] .' ' .date("Y") ;?></p>
			<br>
			<p align="left" ><u>ETABLISSEMENT :</u></p>
			<p align="left"><?php echo $r['ET_LIBELLE'] .' '. $r['ET_VILLE'];?></p>
			<br>
			<p align="left" ><u>CLIENT :</u></p>
			<p align="left"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) .' '. decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p>
			<!--< align="left"><?php echo decrypt($r['UT_ADRESSE1'],$r['UT_ID2']);?></p>
			<p align="left"><?php echo decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p> -->
			<br>
			<p align="left" >TOTAL FACTURE : <?php echo '  ' .number_format($r['TOTALFACT'],2,',','') .' €'; ?></p>
			<?php
			if ($NbrVoucher != 0)
			{
				echo '<p align="left" >TOTAL AVOIR : </u>  ' .number_format($r['TOTALVOUCHER'],2,',','') .' €</p>';
			}

			echo '<p align="left" ><u>NET A PAYER : </u>  ' .number_format(($r['TOTALFACT'] - $r['TOTALVOUCHER']),2,',','') .' €</u></p>';
			?>
			<br>
			<p align="left" ><u>DETAIL FACTURE :</u></p>
			<?php
		}
		$sql = "SELECT RE_USER, RE_ZONE, RE_ZONELIBELLE,COUNT(RE_NUMRESA) AS NBJOURSFACTURE, SUM(RE_NBRPLACE) AS NBPLACE, RE_PRIXPLACE, RE_PRIXPARPERSONNE, SUM(RE_PRIXTOTALRESA) AS TOTALFACT,
				ifnull((select max(GP_NUMERO) from PIECE),0)+ 1 as NEWFACTURENUM,
				UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE

				FROM RESERVATION
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
				LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
				WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritereSQL ."
				GROUP BY RE_USER,  RE_ZONELIBELLE,RE_ZONE, RE_PRIXPLACE, RE_PRIXPARPERSONNE,UT_NOM, UT_PRENOM, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE
				, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE ORDER BY RE_ZONE;";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>

			<p align="left"><?php echo $r['RE_ZONE'] .' - '. addslashes($r['RE_ZONELIBELLE']) .' :     '. $r['NBJOURSFACTURE'] .' Réservation - Prix de l emplacement : ' .$r['RE_PRIXPLACE'] .' € - Prix de la place : ' .$r['RE_PRIXPARPERSONNE'] . ' - Nombre de place : ' .$r['NBPLACE'] . ' € - Total = ' .$r['TOTALFACT'] .' €';?></p>

			<?php
		}

		if ($NbrVoucher != 0)
		{
			echo '<br>';
			echo '<p align="left" ><u>DETAIL AVOIR :</u></p>';

			$sql = "SELECT DATE_FORMAT(BA_DATECREATION,'%d/%m/%Y') AS DATEVOUCHER, BA_NUMERO, BA_LIBELLE, BA_VALEUR FROM VOUCHER WHERE BA_LOGIN = '".$_POST['login']."' AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritVoucherSQL ." ORDER BY BA_NUMERO;";


			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r)
			{
				?>

				<p align="left"><?php echo $r['BA_LIBELLE'] .' du ' .$r['DATEVOUCHER'] .' - Montant : ' .number_format($r['BA_VALEUR'],2,',','') .' €';?></p>

				<?php
			}
		}
		?>


		<br>
		<p align="left">Commentaire (obligatoire) : <input type="text" name="Commentaire" required size="50"></p>
		<center><input type="submit" value="Valider" /></center>
		<br>
		</form>
		<?php
		// fin connexion
	}

}

function valide_facture()
{
	$QteFact = 0;
	$reffacture = '';
	$gp_numero = 0;

	$sql = "update RESERVATION set RE_SESSIONTOKEN = '".$_SESSION['token']."'  where RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'"
			.$_POST['CritereSQL'].";" ;

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);

	$sql = "SELECT RE_USER, COUNT(RE_NUMRESA) AS NBJOURSFACTURE, SUM(RE_NBRPLACE) AS NBPLACE, SUM(RE_PRIXTOTALRESA) AS TOTALFACT,
			UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, UT_ID2, UT_EMAIL, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_ADRESSE3, ET_CODEPOSTAL, ET_VILLE, ET_ETABLISSEMENT, ET_ECHEANCEFACTURE
			FROM RESERVATION
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
			LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
			WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_CREDITCLI = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_SESSIONTOKEN = '".$_SESSION['token']."'
			GROUP BY RE_USER,   UT_NOM, UT_PRENOM, UT_ID2, ET_ETABLISSEMENT, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_ADRESSE3, ET_CODEPOSTAL, ET_VILLE, UT_EMAIL , UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_ECHEANCEFACTURE
			LIMIT 1;";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$QteFact = $data['NBJOURSFACTURE'];

		if ($data['NBJOURSFACTURE'] != 0)
		{
			$sql = "SELECT COUNT(CT_COMPTEUR) AS CPT FROM COMPTEUR WHERE CT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND CT_CODEDOC = 'FAC';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
				if ($data1['CPT'] == 0)
				{
					$sql = "INSERT INTO COMPTEUR (CT_ETABLISSEMENT, CT_DOCUMENT, CT_COMPTEUR, CT_CODEDOC) VALUES (ETABLISSEMENT, 'FACTURE',1, 'FAC');";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);

				}
			}

			$sql = "INSERT INTO PIECE (GP_TYPE, GP_USER, GP_USERNOM, GP_USERPRENOM, GP_USERADRESSE1, GP_USERADRESSE2, GP_USERCODEPOSTAL, GP_USERVILLE, GP_USEREMAIL, GP_USERID, GP_ETABLISSEMENT, GP_ETLIBELLE,
					GP_ETADRESSE1, GP_ETADRESSE2, GP_ETADRESSE3, GP_ETCODEPOSTAL, GP_ETVILLE, GP_TOTALHT, GP_TOTALQTEFACT, GP_SESSIONTOKEN, GP_DATEPIECE, GP_DATETRANSMISSION, GP_DATEPAYEMENT, GP_DATERELANCE, GP_DATEECHEANCE)
					VALUES ('FACTURE', '".$data['RE_USER']."','".$data['UT_NOM']."','".$data['UT_PRENOM']."','".$data['UT_ADRESSE1']."','".$data['UT_ADRESSE2']."','".$data['UT_CODEPOSTAL']."','".$data['UT_VILLE']."',
					'".$data['UT_EMAIL']."','".$data['UT_ID2']."','".$data['ET_ETABLISSEMENT']."','".$data['ET_LIBELLE']."','".$data['ET_ADRESSE1']."','".$data['ET_ADRESSE2']."','".$data['ET_ADRESSE3']."',
					'".$data['ET_CODEPOSTAL']."','".$data['ET_VILLE']."',".$data['TOTALFACT'].",".$data['NBJOURSFACTURE'].",'".$_SESSION['token']."', now(),'1900-01-01','1900-01-01','1900-01-01', ADDDATE(now(), INTERVAL ".$data['ET_ECHEANCEFACTURE']." DAY));";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);



			$sql = "select GP_ETABLISSEMENT, GP_NUMERO, GP_TOTALHT, GP_TOTALQTEFACT, CONCAT(DATE_FORMAT(GP_DATEPIECE,'FACT%y%m_'),CONCAT(SUBSTR('00000',1,5-LENGTH(GP_NUMERO)),GP_NUMERO)) AS REFFACTURE
					FROM PIECE WHERE GP_USER = '".$data['RE_USER']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req1 = $cnx_bdd->query($sql);
			$tab_r1 = $result_req1->fetchAll();
			foreach ($tab_r1 as $data1)
			{
				$reffacture = $data1['REFFACTURE'];
				$gp_numero = $data1['GP_NUMERO'];
				$gp_totalfact = $data1['GP_TOTALHT'];
				$gp_totalqtefact = $data1['GP_TOTALQTEFACT'];
				$gp_etablissement = $data1['GP_ETABLISSEMENT'];
				$sql = "UPDATE PIECE SET GP_REFFACTURE = '".$data1['REFFACTURE']."'  WHERE GP_USER = '".$data['RE_USER']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

			}

			$numligne = 1;
			$refzone = '';
			$sql = "SELECT RE_USER, RE_ZONE, RE_ZONELIBELLE, RE_CODEART1, RE_LIBART1, RE_QTEART1, RE_PRIXART1, RE_CODEART2, RE_LIBART2, DATE_FORMAT(RE_DATE,'Reservation du %d/%m/%Y') AS LIBELLEDATE,
					RE_QTEART2, RE_PRIXART2, RE_NUMRESA, RE_ETABLISSEMENT, RE_NUMRESA, UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE ,
					DATE_FORMAT(RE_DATE,'%d') AS DATERESA, DATE_FORMAT(RE_DATE,'%Y') AS DATERESA1, CC_LIBELLE, RE_MOIS
					FROM RESERVATION
					LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
					LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
					LEFT JOIN CHOIXCODE ON CC_TYPE = 'MOIS' AND CC_CODE = RE_MOIS
					WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_SESSIONTOKEN = '".$_SESSION['token']."'
					ORDER BY RE_ZONE, RE_NUMRESA;";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
				$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_ARTTARIF, GL_TOTALHT, GL_COMMENTAIRE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data1['RE_ETABLISSEMENT']."','".$data1['RE_USER']."','".$data1['RE_CODEART1']."','".$data1['RE_LIBART1']."',".$data1['RE_QTEART1'].",".$data1['RE_PRIXART1'].",
						".($data1['RE_QTEART1'] * $data1['RE_PRIXART1']).",'','Reservation " .$data1['RE_ZONELIBELLE'] ." n° " .$data1['RE_NUMRESA'] ." du " .$data1['DATERESA'] ." " .$data1['CC_LIBELLE'] ." " .$data1['DATERESA1']."');";
		//'Reservation ' .$r['RE_ZONELIBELLE'] .' n° ' .$r['RE_NUMRESA'] .' du ' .$r['DATERESA'] .' ' .$r['CC_LIBELLE'] .' ' .$r['DATERESA1']
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$gp_totalfact = $gp_totalfact + ($data1['RE_QTEART1'] * $data1['RE_PRIXART1']);
				$numligne++;

				if ($data1['RE_QTEART2'] > 0)
				{
					$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_ARTTARIF, GL_TOTALHT, GL_COMMENTAIRE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data1['RE_ETABLISSEMENT']."','".$data1['RE_USER']."','".$data1['RE_CODEART2']."','".$data1['RE_LIBART2']."',".$data1['RE_QTEART2'].",".$data1['RE_PRIXART2'].",
						".($data1['RE_QTEART2'] * $data1['RE_PRIXART2']).",'','Reservation " .$data1['RE_ZONELIBELLE'] ." n° " .$data1['RE_NUMRESA'] ." du " .$data1['DATERESA'] ." " .$data1['CC_LIBELLE'] ." " .$data1['DATERESA1']."');";

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
					$gp_totalfact = $gp_totalfact + ($data1['RE_QTEART2'] * $data1['RE_PRIXART2']);
					$numligne++;
				}
			}


			// Recherche des documents en attente de facturation

			$sql = "UPDATE LIGNEATTENTE SET GLA_SESSIONTOKEN = '".$_SESSION['token']."' WHERE GLA_USER = '" .$_POST['login'] . "' AND GLA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			$sql = "SELECT * FROM LIGNEATTENTE WHERE GLA_USER = '" .$_POST['login'] . "' AND GLA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND GLA_SESSIONTOKEN = '".$_SESSION['token']."' AND GLA_VALIDE = 'OUI';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data2)
			{

				$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF, GL_PRIXPARPERSONNE,
							GL_PRIXPARPLACE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$gp_etablissement."','".$_POST['login']."','".$data2['GLA_ARTICLENO']."','".$data2['GLA_ARTLIBELLE']."',
							".$data2['GLA_QTEFACT'].",".$data2['GLA_TOTALHT'].",'".$data2['GLA_COMMENTAIRE']."',".$data2['GLA_ARTTARIF'].",".$data2['GLA_PRIXPARPERSONNE'].",".$data2['GLA_PRIXPARPLACE'].",
							'".$data2['GLA_LIBELLE']."');";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				$gp_totalqtefact = $gp_totalqtefact + $data2['GLA_QTEFACT'];
				$gp_totalfact = $gp_totalfact + ($data2['GLA_QTEFACT'] * $data2['GLA_TOTALHT']);
				$numligne++;
			}

			$sql = "UPDATE LIGNEATTENTE SET GLA_VALIDE = 'NON' WHERE GLA_USER = '" .$_POST['login'] . "' AND GLA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND GLA_SESSIONTOKEN = '".$_SESSION['token']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			// Recherche des bon d'achat

			$nbrvoucher = 0;
			$sql = "SELECT BA_NUMERO, BA_VALEUR, date_format(BA_DATECREATION,'%d/%m/%Y') AS DATEBA FROM VOUCHER WHERE BA_LOGIN = '" .$_POST['login'] . "'
					AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ".$_POST['CritVoucherSQL']." AND BA_VALIDE = 'OUI';" ;

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data2)
			{
				if ($nbrvoucher == 0)
				{
					$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF, GL_PRIXPARPERSONNE,
							GL_PRIXPARPLACE, GL_LIBELLE) VALUES
							(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$gp_etablissement."','".$_POST['login']."','','',
								0,0,'',0,0,0,'');";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
					$numligne++;
				}
				$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF, GL_PRIXPARPERSONNE,
							GL_PRIXPARPLACE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$gp_etablissement."','".$_POST['login']."','AVOIR','REPRISE AVOIR',1,".($data2['BA_VALEUR'] * -1).",'',".($data2['BA_VALEUR'] * -1).",0,0,
							'Reprise Avoir n° ".$data2['BA_NUMERO']. " ".$data2['DATEBA']."');";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$sql= "UPDATE VOUCHER SET BA_DATEREPRISE = now(), BA_REFREPRISE = ".$gp_numero.", BA_VALIDE = 'NON' WHERE BA_NUMERO = ".$data2['BA_NUMERO'].";";


				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$gp_totalfact = $gp_totalfact - $data2['BA_VALEUR'];
				$nbrvoucher ++;
				$numligne++;

			}


			$sql = "UPDATE PIECE SET GP_TOTALQTEFACT = ".$gp_totalqtefact.", GP_TOTALHT = ".$gp_totalfact."  WHERE GP_USER = '".$data['RE_USER']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			$sql = "UPDATE RESERVATION SET RE_REFFACTURE = '".$reffacture."', RE_FACTURE = 'OUI'
					WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_SESSIONTOKEN = '".$_SESSION['token']."';";
			//$cnx_bdd = ConnexionBDD();
			//$result_req = $cnx_bdd->exec($sql);


			echo 'Facture enregistrée';
			?>
			<a style="margin-left: 30px" href="pdf/print_facture.php?reffacture=<?php echo $reffacture; ?> " onclick="window.open(this.href, 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><B>Impression de la facture</b></a>
			<?php
		}
		else
		{
			echo 'Facture déjà enregistrée';
			?>
			<a style="margin-left: 30px" href="print_facture.php?reffacture=<?php echo $reffacture; ?> " onclick="window.open(this.href, 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><B>Impression de la facture</b></a>
			<?php
		}

	}
}

function liste_facture()
{
	if (($_SESSION['STATUT']) == "ADMIN")
	{


		//if ($_POST['user'] != "Tous CoWorker")
		//{$Client = $_POST['Nom_Client'];}

		?>

		<br>

		<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="80%">
		<form  action="" method="post">
		<tr style="background-color:#46B593">
			 <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 250px; font-weight: bold; background-color: rgb(70, 181, 147);">Utilisateur
	  <br>
	  <select name="ListUser" id="ListUser">
	  <?php
		if (((isset($_POST['ListUser'])) && ($_POST['ListUser'] == 'Tous')) || (!isset($_POST['ListUser'])))
		{
			?>
			<option value="Tous" selected="selected">Tous</option>
			<?php
			$sql = "select GP_USER, UT_NOM, UT_PRENOM, UT_ID2 from PIECE LEFT JOIN UTILISATEUR ON UT_LOGIN = GP_USER GROUP BY GP_USER";

		}
		if ((isset($_POST['ListUser'])) && ($_POST['ListUser'] != 'Tous'))
		{
			$sql = "select GP_USER, UT_NOM, UT_PRENOM, UT_ID2  from PIECE LEFT JOIN UTILISATEUR ON UT_LOGIN = GP_USER WHERE GP_USER = '".$_POST['ListUser']."' GROUP BY GP_USER";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r)
			{
				?>
				<option value="<?php echo $r['GP_USER']; ?>" selected="selected"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
				<option value="Tous">Tous</option>
				<?php
			}

			$sql = "select GP_USER, UT_NOM, UT_PRENOM, UT_ID2  from PIECE LEFT JOIN UTILISATEUR ON UT_LOGIN = GP_USER WHERE GP_USER <> '".$_POST['ListUser']."' GROUP BY GP_USER";
		}
		// Après Antoine
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<option value="<?php echo $r['GP_USER']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php

		}




		?>
      </select>
	  </td>
			<td align="left" style="width:20%"> Référence Facture</td>
			<td align="left" style="width:20%"> Date Facture</td>
			<td align="left" style="width:25%"> Montant</td>
		<td style="text-align: center; width: 50px; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Facturé
	  <br>
	  <select name="PAYE" id="PAYE">
			<?php
			if (isset($_POST['PAYE']) && $_POST['PAYE'] == 'OUI')
			{
				?>
				<option value="Oui" >Oui</option>
				<option value="Tous" >Tous</option>
				<option value="Non" >Non</option>
				<?php
			}
			elseif (isset($_POST['PAYE']) && $_POST['PAYE'] == 'NON')
			{
				?>
				<option value="Non" >Non</option>
				<option value="Tous" >Tous</option>
				<option value="Oui" >Oui</option>
				<?php
			}
			else
			{
				?>
				<option value="Tous" >Tous</option>
				<option value="OUI" >Oui</option>
				<option value="NON" >Non</option>
				<?php
			}
			?>
	  </select>
	  </td>
	  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);"><button value="Valid" name="Valid">Valider</button></td>
			<td align="left" style="width:20%"> </td>
		</tr>
		</form>
		<?php

		$critere = 0;
		$sqlcritere = " WHERE GP_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ";
		if ((isset($_POST['ListUser'])) && ($_POST['ListUser'] != 'Tous'))
		{
			if ($critere == 1)
			{
				$sqlcritere = $sqlcritere ." AND GP_USER = '".$_POST['ListUser']."' ";
			}
			else
			{
				$sqlcritere = $sqlcritere ." AND GP_USER = '".$_POST['ListUser']."' ";
			}
			$critere = 1;

		}

		if ((isset($_POST['PAYE'])) && ($_POST['PAYE'] != 'Tous'))
		{
			if ($critere == 1)
			{
				$sqlcritere = $sqlcritere ." AND GP_PAYEMENT = '".$_POST['PAYE']."' ";
			}
			else
			{
				$sqlcritere = $sqlcritere ." AND GP_PAYEMENT = '".$_POST['PAYE']."' ";
			}
			$critere = 1;

		}


		if (($_SESSION['STATUT']) == "ADMIN")
		{
			$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m %Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT, GP_USERID, GP_USERNOM, GP_USERPRENOM,
					GP_PAYEMENT FROM PIECE ".$sqlcritere." order by GP_REFFACTURE";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
			{
			?>
			<tr>
			<td><?php echo decrypt($data['GP_USERPRENOM'], $data['GP_USERID']) . " ". decrypt($data['GP_USERNOM'], $data['GP_USERID']); ?></td>
			<td align="left" ><b><?php echo $data['GP_REFFACTURE']; ?></b></a></td>
			<td align="left" ><?php echo $data['JOUR']; ?></td>

			<td align="left" ><?php echo number_format($data['GP_TOTALHT'],2,',',''); ?></td>
			<td align="left" ><?php echo $data['GP_PAYEMENT']; ?></td>
			<td align="center" ><img border="0" src="img/logo_print.jpg" width="25" height="25" onclick="window.open('pdf/print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			<td align="center" ><img border="0" src="img/logo-mail.jpg" width="25" height="25" onclick="window.open('email_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr>
			<?php
			}
		?>
		</table></center>

		<?php
	}
}

function newpassword()
{
?>
<form action="" method="post">
<br />
<p>Saisir le login (email) : <input type="text" id="newloginemail" name="newloginemail"  size="50" maxlength="250" required></p>
<input type="hidden" id="TypeAction" name="TypeAction" value="demande">

<br /><br />
<input type="submit" value="Valider" />
</form>
	<?php

}

function liste_adherant()
{
	connectsql();
	if ($_POST['user'] != "Tous CoWorker")
	{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr style="background-color:#46B593">
		<td align="left" style="width:20%"> Référence Facture</td>
		<td align="left" style="width:20%"> Date Facture</td>
		<td align="left" style="width:25%"> Montant</td>
		<td align="left" style="width:20%"> </td>
	</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-color:white">
	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m %Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE order by GP_DATEPIECE, GP_USER";
	}
	else
	{
		$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m/%Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT FROM PIECE where GP_USER = '" .$_SESSION['login']. "' order by GP_DATEPIECE, GP_USER";
	}
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
		{
		?>
		<tr>

		<td align="left" style="width:20%"><b><?php echo $data['GP_REFFACTURE']; ?></b></a></td>
		<td align="left" style="width:20%"><?php echo $data['JOUR']; ?></td>
		<td align="left" style="width:25%"><?php echo number_format($data['GP_TOTALHT'],2,',',''); ?></td>
		<td align="center" style="width:10%"><img border="0" src="img/logo_print.jpg" width="25" height="25" onclick="window.open('print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
		<td align="center" style="width:10%"><img border="0" src="img/logo-mail.jpg" width="25" height="25" onclick="window.open('email_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
		</tr>
		<?php
		}
	mysql_close;
	?>
	</table></center>

	<?php
}

function affiche_user()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
		// déclaration des variables
		$nbruseraffich = 0;
		?>



		<body>


		<center><div id="support1">
		<form  enctype="multipart/form-data" action="" method="post">
		<table  style=""width: 80%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">

		<tbody>
			<tr>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 250px;">Login</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 200px;">Nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 100px;">Pr&eacute;nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 300px;">Email</td>
				<?php
				if (!isset($_SESSION['SUPERADMIN']))
				{
				echo '<td style="text-align: center; font-family: Calibri; width: 80px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date début adhésion</td>';
				echo '<td style="text-align: center; font-family: Calibri; width: 81px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date fin adhésion</td>';
				}
				?>
				<td colspan="2" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);">
				<span style="font-family: Calibri;"><input id="boutonnew" class="bouton3" type="button" value="Nouveau" style="width: 60px;" onclick="javascript:cacher(<?php echo $nbruseraffich * 100; ?>);"></span>
				</td>
			</tr>


		<?php


		// Début Affichage de la bande de création d'un nouvelle utilisateur.
		?>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo ($nbruseraffich * 100); ?>" name="tdparamuser_000_<?php echo $nbruseraffich; ?>" id="tdparamuser_000_<?php echo $nbruseraffich; ?>" class="liste1" colspan="8">Création d'un utilisateur </td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 23); ?>" colspan="8" class="liste1">
					<label>Personne Morale ou Physique</label>
					<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERTYPE<?php echo $nbruseraffich;?>">
						<OPTION  value="MORALE">Personne Morale</option>
						<OPTION selected="selected" value="PHYSIQUE">Personne Physique</option>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; padding-bottom=5px;">
			<td colspan="8" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 34); ?>" class="liste1">
				<label>Nom de la structure :</label>
				<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USERNOMSTRUCTURE<?php echo $nbruseraffich; ?>" class="liste1" value="Saisir le nom de la structure" >
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td colspan="2" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 1); ?>" class="liste1">
				<label>Nom :</label>
				<input type="text" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERNOM<?php echo $nbruseraffich; ?>" value="Saisir le nom" >
			</td>
			<td colspan ="2" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 2); ?>" colspan="2" class="liste1">
				<label>Prénom :</label>
				<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERPRENOM<?php echo $nbruseraffich; ?>" value="Saisir le prénom" >
			</td>
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 24); ?>" colspan="6" class="liste1">
					<label>Civilité</label>
					<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="USERCIVILITE<?php echo $nbruseraffich;?>" style="width: 150px;" required>
					<?php
						$sql = "SELECT CC_CODE, CC_LIBELLE,  '' AS SELECTED, 'VIDE', CC_TYPE
										FROM CHOIXCODE WHERE CC_TYPE in ('TYPE_MORALE','TYPE_PHYSIQUE')
										UNION ALL
										SELECT '' AS CC_CODE, '' AS CC_LIBELLE, 'SELECTED' AS SELECTED, 'VIDE', '' AS CC_TYPE
										ORDER BY SELECTED DESC";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data1)
						{
							if($data1['SELECTED'] == 'SELECTED')
							{
								echo '<OPTION selected="selected" value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
							}
							else
							{
								echo '<OPTION value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
							}
						}

						?>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; padding-bottom=5px;">
			<td colspan="8" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 3); ?>" colspan="5" class="liste1">
				<label>Email :</label>
				<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USEREMAIL<?php echo $nbruseraffich; ?>" class="liste1" value="Saisir l'email" >
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 4); ?>" colspan="3" style="height: 20px; width: 212px;padding-bottom: 5px;">
				<label>Adresse : </label>
				<input type="text" id="paramuser_004_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE1<?php echo $nbruseraffich; ?>" style="width: 300px;" value="Saisir l'adresse">
			</td>
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 5); ?>" colspan="5" style="height: 20px; width: 212px;padding-bottom: 5px;">
				<label>Adresse complémentaire : </label>
				<input type="text" id="paramuser_005_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE2<?php echo $nbruseraffich; ?>" style="width: 300px;" value="Saisir l'adresse complémentaire (facultatif)" >
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 6); ?>" colspan="3" style="height: 20px; width: 212px;padding-bottom: 5px;">
				<label>Code Postal : </label>
				<input type="text" id="paramuser_006_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERCPOSTAL<?php echo $nbruseraffich; ?>" value="Saisir le code postal" >
			</td>
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 7); ?>" colspan="5" style="height: 20px; width: 212px;padding-bottom: 5px;">
				<label>Ville : </label>
				<input type="text" id="paramuser_007_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERVILLE<?php echo $nbruseraffich; ?>" style="width: 300px;" value="Saisir la ville" >
			</td>
		</tr>

		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 8); ?>" colspan="8" class="liste1"/>
		</tr>

		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 13); ?>" colspan="8" class="liste1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 14); ?>" colspan="1" class="liste1">Administrateur de l'établissement: </td>
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 15); ?>" colspan="7" class="liste1">
					<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="USERADMINSITE<?php echo $nbruseraffich;?>" style="width: 100px;">
						<option selected="selected" value="">Non</option>
						<option value="<?php echo $_SESSION['ETABADMIN'];?>">Oui</option>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 16); ?>" colspan="8" class="liste1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 17); ?>" colspan="2" class="liste1"><b>Voulez vous créer cette utilisateur ?</b></td>
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 18); ?>" colspan="6" class="liste1">
					<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERCREATOK<?php echo $nbruseraffich;?>" style="width: 60px;">
						<OPTION  value="OUI">OUI</option>
						<OPTION selected="selected" value="NON">NON</option>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 20); ?>" colspan="8" class="liste1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 21); ?>" colspan="8" align="center">
				<input class="bouton1" value="Valider" type="submit">
			</td>
		<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 22); ?>" colspan="8" class="liste1">
						<input type="hidden" id="paramuser_011_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMODIF<?php echo $nbruseraffich; ?>" style="width: 300px;" value='NON'>

				</td>
		</tr>
		<?php
		// Fin Affichage de la bande de création d'un nouvelle utilisateur.

		//$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$nbretabvalid = 0;
		if (isset($_GET['PAGE']))
		{
			if ($_GET['PAGE'] == 0)
			{
				$limitmin = 0;
				$pagesuiv = 1;
				$pageprec = 0;
			}
			else
			{
				$limitmin = $_GET['PAGE'] * 10;
				$pagesuiv = $_GET['PAGE'] + 1;
				$pageprec = $_GET['PAGE'] - 1;
			}
		}
		else
		{
			$limitmin = 0;
			$pagesuiv = 1;
			$pageprec = 0;
		}
		$sql = "SELECT COUNT(ET_ETABLISSEMENT) AS CPT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbretabvalid = $data['CPT'];
		}

		if ((isset($_SESSION['SUPERADMIN'])) && ($_SESSION['SUPERADMIN'] == 'OUI'))
		{
			$sql = "select  '' as DEBUTADHESION, '' as FINADHESION, UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, UT_ID2 from UTILISATEUR
					WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL ORDER BY UT_LOGIN LIMIT ".$limitmin.",10";
		}
		else
		{
			if ($nbretabvalid == 1)
			{
				$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
								datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
								UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL,
								UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
								UT_ID2
								FROM UTILISATEUR
								LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN
								LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND (TE_ETABLISSEMENT IS NULL OR TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."')
								ORDER BY UT_LOGIN
								LIMIT ".$limitmin.",10";
			}
			else
			{
				$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
								datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
								UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL,
								UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
								UT_ID2
								FROM UTILISATEUR
								LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN
								LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								ORDER BY UT_LOGIN
								LIMIT ".$limitmin.",10";
			}
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbruseraffich++;
			?>
			<tr>
			<td style="text-align: left; width: 250px;"><b><?php echo $data['UT_LOGIN']; ?></b></td>
			<td style="width: 200px;"><?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?></td>
			<td style="width: 100px;"><?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?></td>
			<td style="width: 300px;"><?php echo decrypt($data['UT_EMAIL'],$data['UT_ID2']); ?></td>
			<?php
			if (isset($_SESSION['ETABADMIN']))
			{
				?>
				<td style="text-align: center; width: 80px;"><?php echo $data['DEBUTADHESION']; ?></td>
				<td style="text-align: center; width: 81px;"><?php echo $data['FINADHESION']; ?></td>
				<?php
			}
			?>


			<td style="width: 30px;"><img class="photo" id="website2" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:cacher(<?php echo $nbruseraffich * 100; ?>);"></td>
			<td style="width: 30px;"><img class="photo" id="website2" border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('manageuser.php?ACTION=suppression&numero=<?php echo $data['UT_LOGIN']; ?>', 'exemple', 'height=200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr>

			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php echo ($nbruseraffich * 100); ?>" name="tdparamuser_000_<?php echo $nbruseraffich; ?>" id="tdparamuser_000_<?php echo $nbruseraffich; ?>" class="liste1" colspan="8">Modification de l'utilisateur </td>
			</tr>

			<?php
			if ($data['UT_TYPE'] == 'MORALE')
			{
				?>
				<tr style="background-color: #edf1f6; ">
					<td colspan="3" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 23); ?>" class="liste1">
						<label>Nom de la structure : </label>
						<input type="text" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERNOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?>' >
						<input type="hidden" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERPRENOM<?php echo $nbruseraffich; ?>" value='' >
					</td>
					<td colspan="1" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 24); ?>" class="liste1">
						<label>Type de société : </label>

						<select id="paramuser_001_<?php echo $nbruseraffich ; ?>" name="USERCIVILITE<?php echo $nbruseraffich;?>" style="width: 150px;">
						<?php
							if (!empty($data['UT_CIVILITE']))
							{ $civilite = decrypt($data['UT_CIVILITE'], $data['UT_ID2']); }
							else { $civilite = ''; }

							echo '<OPTION selected="selected" value="'.$civilite.'">'.$civilite.'</option>';
							$sql = "SELECT CC_LIBELLE AS CIVILITE, '' AS SELECTED, 'VIDE'
											FROM CHOIXCODE
											WHERE CC_TYPE = 'TYPE_MORALE'
											ORDER BY SELECTED, CC_LIBELLE DESC";
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->query($sql);
							$tab_r = $result_req->fetchAll();
							foreach ($tab_r as $data1)
							{
								if($data1['SELECTED'] == 'SELECTED')
								{
									echo '<OPTION selected="selected" value="'.$data1['CIVILITE'].'">'.$data1['CIVILITE'].'</option>';
								}
								else
								{
									echo '<OPTION value="'.$data1['CIVILITE'].'">'.$data1['CIVILITE'].'</option>';
								}
							}

							?>
						</SELECT>
					</td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 25); ?>" colspan="4" class="liste1">
						<label>Personne Morale ou Physique</label>
						<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERTYPE<?php echo $nbruseraffich;?>">
						<?php
						if ($data['UT_TYPE'] == 'MORALE')
						{
							?>
							<OPTION selected="selected" value="MORALE">Personne Morale</option>
							<OPTION value="PHYSIQUE">Personne Physique</option>
							<?php
						}
						else
						{
							?>
							<OPTION value="MORALE">Personne Morale</option>
							<OPTION selected="selected" value="PHYSIQUE">Personne Physique</option>
							<?php
						}
						?>
						</SELECT>
					</td>


				</tr>
				<tr style="background-color: #edf1f6; ">
					<td colspan="2" id="tdparamuser_<?php echo (($nbruseraffich * 100) + 1); ?>" class="liste1">
						<label>Nom du contact : </label>
						<input type="text" colspan="6" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERCONTNOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_CONTACTNOM'] != '') { echo decrypt($data['UT_CONTACTNOM'],$data['UT_ID2']);} ?>' ></td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 2); ?>" colspan="6" class="liste1"><label>Prénom du contact : </label><input id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERCONTPRENOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_CONTACTPRENOM'] != '') { echo decrypt($data['UT_CONTACTPRENOM'],$data['UT_ID2']);} ?>' ></td>
				</tr>
				<?php
			}
			else
			{
				?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 1); ?>" class="liste1">
						<label>Nom : </label>
						<input type="text" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERNOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?>' >
					</td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 2); ?>" colspan="2" class="liste1">
						<label>Prénom : </label>
						<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERPRENOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?>' >
					</td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 24); ?>" colspan="1" class="liste1">
						<label>Civilité : </label>
						<select id="paramuser_001_<?php echo $nbruseraffich ; ?>" name="USERCIVILITE<?php echo $nbruseraffich;?>" style="width: 150px;">
						<?php
							if (!empty($data['UT_CIVILITE']))
							{ $civilite = decrypt($data['UT_CIVILITE'], $data['UT_ID2']); }
							else { $civilite = ''; }

							echo '<OPTION selected="selected" value="'.$civilite.'">'.$civilite.'</option>';
							$sql = "SELECT CC_LIBELLE AS CIVILITE, '' AS SELECTED, 'VIDE'
											FROM CHOIXCODE
											WHERE CC_TYPE = 'TYPE_PHYSIQUE'
											ORDER BY SELECTED, CC_LIBELLE DESC";
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->query($sql);
							$tab_r = $result_req->fetchAll();
							foreach ($tab_r as $data1)
							{
								if($data1['SELECTED'] == 'SELECTED')
								{
									echo '<OPTION selected="selected" value="'.$data1['CIVILITE'].'">'.$data1['CIVILITE'].'</option>';
								}
								else
								{
									echo '<OPTION value="'.$data1['CIVILITE'].'">'.$data1['CIVILITE'].'</option>';
								}
							}

							?>
						</SELECT>
					</td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 25); ?>" colspan="4" class="liste1">
						<label>Personne Morale ou Physique</label>
						<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERTYPE<?php echo $nbruseraffich;?>">
						<?php
						if ($data['UT_TYPE'] == 'MORALE')
						{
							?>
							<OPTION selected="selected" value="MORALE">Personne Morale</option>
							<OPTION value="PHYSIQUE">Personne Physique</option>
							<?php
						}
						else
						{
							?>
							<OPTION value="MORALE">Personne Morale</option>
							<OPTION selected="selected" value="PHYSIQUE">Personne Physique</option>
							<?php
						}
						?>
						</SELECT>
					</td>
				</tr>
				<?php
			}

			 ?>
			<tr style="background-color: #edf1f6; ">

				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 3); ?>" colspan="8" class="liste1">
					<label>Email : </label>
					<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USEREMAIL<?php echo $nbruseraffich; ?>" class="liste1" value='<?php if($data['UT_EMAIL'] != '') { echo decrypt($data['UT_EMAIL'],$data['UT_ID2']);} ?>' >
				</td>
			</tr>

			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 4); ?>" colspan="3" class="liste1">
					<label>Adresse : </label>
					<input type="text" id="paramuser_004_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE1<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_ADRESSE1'] != '') { echo decrypt($data['UT_ADRESSE1'],$data['UT_ID2']);} ?>' >
				</td>
				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 5); ?>" colspan="5" class="liste1">
					<label>Adresse complémentaire : </label>
					<input type="text" id="paramuser_005_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE2<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_ADRESSE2'] != '') { echo decrypt($data['UT_ADRESSE2'],$data['UT_ID2']);} ?>' >
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 6); ?>" colspan="3" class="liste1">
					<label>Code postal : </label>
					<input type="text" id="paramuser_006_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERCPOSTAL<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_CODEPOSTAL'] != '') { echo decrypt($data['UT_CODEPOSTAL'],$data['UT_ID2']);} ?>' >
				</td>
				<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 7); ?>" colspan="5" class="liste1">
					<label>Ville : </label>
					<input type="text" id="paramuser_007_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERVILLE<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_VILLE'] != '') { echo decrypt($data['UT_VILLE'],$data['UT_ID2']);} ?>' >
				</td>
			</tr>
			<?php

			if ((isset($data['ET_GESTIONCREDITCLIENT'])) && ($data['ET_GESTIONCREDITCLIENT'] == 'OUI'))
			{
				?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 8); ?>" colspan="8" class="liste1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 9); ?>" colspan="1" class="liste1">Crédit autorisé : </td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 10); ?>" colspan="1" class="liste1">
							<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERCREDITOK<?php echo $nbruseraffich;?>" style="width: 60px;">
							<?php
								if ($data['TE_CREDITOK'] == 'OUI')
								{
									echo '<OPTION selected="selected" value="OUI">OUI</option>';
									echo '<OPTION value="NON">NON</option>';
								}
								else
								{
									echo '<OPTION  value="OUI">OUI</option>';
									echo '<OPTION selected="selected" value="NON">NON</option>';
								}

								?>
							</SELECT>
					</td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 11); ?>" colspan="1" class="liste1">Montant du crédit : </td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 12); ?>" colspan="5" class="liste1"><input id="paramuser_009_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMNTCREDIT<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if(!empty($data['TE_CREDITVAL'])) { echo $data['TE_CREDITVAL'];} ?>' disabled></td>
				</tr>
				<?php
			}
			 ?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 13); ?>" colspan="8" class="liste1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 14); ?>" colspan="1" class="liste1">Administrateur de l'établissement: </td>
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 15); ?>" colspan="7" class="liste1">
							<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="USERADMINSITE<?php echo $nbruseraffich;?>" class="liste1">
							<?php
							if ($data['UT_ADMINSITE'] == $_SESSION['ETABADMIN'])
							{
								?>
								<option selected value="<?php echo $data['UT_ADMINSITE'];?>">Oui</option>
								<option value="">Non</option>
								<?php
							}
							else
							{
								?>
								<option value="<?php echo $data['UT_ADMINSITE'];?>">Oui</option>
								<option selected value="">Non</option>
								<?php
							}
							?>
							</SELECT>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 16); ?>" colspan="8" class="liste1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 17); ?>" colspan="3" align="center" class="liste1">
						<input name="boutonvalid" id="boutonvalid" class="bouton1" style="display: none;" value="Valider" type="submit">
					</td>
						<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 26); ?>" colspan="3" align="center" class="liste1">
						<input name="boutonmdp_<?php echo $nbruseraffich; ?>" id="boutonmdp_<?php echo $nbruseraffich; ?>" class="bouton1" style="display: none;" value="Nouveau mot de passe" type="submit">
					</td>
					<!--<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 25); ?>" colspan="5" align="center">
						<input class="bouton1" type="button" value="Demander un nouveau mot de passe" style="width: 260px;" onclick="window.open('newmdpadmin.php?newmdp=<?php echo $data['UT_LOGIN']; ?>', 'exemple', 'height=1200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/>
					</td>-->
				</tr>
			<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php echo (($nbruseraffich * 100) + 18); ?>" colspan="8" class="liste1">
							<input type="hidden" id="paramuser_011_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMODIF<?php echo $nbruseraffich; ?>" style="width: 300px;" value='NON'>
							<input type="hidden" id="paramuser_012_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERLOGIN<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_LOGIN'];?>'>
							<input type="hidden" id="paramuser_013_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERID<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_ID2'];?>'>
							<input type="hidden" id="paramuser_017_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERIDTYPE<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_TYPE'];?>'>
					</td>
				</tr>


		<?php
		}

		?>
		<tr><td><input type="hidden" name="nbruseraffich" id="nbruseraffich" value="<?php echo $nbruseraffich; ?>"></input></td></tr>
		<tr>
			<?php

			if($pageprec >= 0)
			{
				?>
				<td colspan="3" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/prec-black.png" width="25" height="25" onClick="window.location.href='manageuser.php?PAGE=<?php echo $pageprec; ?>'"/></td>
				<?php
			}
			else
			{
				?>
				<td colspan="3 "style="width: 30px;"></td>
				<?php
			}
			 ?>

		<td colspan="5" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/suiv-black.png" width="25" height="25" onClick="window.location.href='manageuser.php?PAGE=<?php echo $pagesuiv; ?>'"/></td>
		</tr>
		</table>

	</form>
	</center>
	<?php
	}

}

function new_updateuser()
{
	for ($i = 1; $i <= $_POST['nbruseraffich']; $i++)
	{
	if(isset($_POST['boutonmdp_'.$i]))
		{

			$rand =  rand(1,99999999);
			$sql = "UPDATE UTILISATEUR SET UT_VERIF = '".$rand."', UT_PASSWORD = 'EDFRTGHYJMM' WHERE UT_LOGIN = '".$_POST['USERLOGIN'.$i]."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
			emailNmdp($_POST['USEREMAIL'.$i], $rand, $_POST['USERLOGIN'.$i]);
			$mdp = 'change';
		}
	}
	if (!isset($mdp))
	{
		if ($_POST['USERCREATOK0'] == 'OUI')
		{

			$creatuser = 0;
			if ($_POST['USERNOM0'] != '') {$creatuser++; $NEWNOM = $_POST['USERNOM0'];}
			if ($_POST['USERPRENOM0'] != '') {$creatuser++; $NEWPRENOM = $_POST['USERPRENOM0'];}
			if ($_POST['USEREMAIL0'] != '') {$creatuser++;$NEWEMAIL = $_POST['USEREMAIL0'];}

			if (empty($_POST['USERADMINSITE0']))
			{
				$NEWUSERADMINSITE = '';
			}
			else {
					$NEWUSERADMINSITE = $_POST['USERADMINSITE0'];
			}

			if (empty($_POST['USERTIERSCOMPTA0']))
			{
				$NEWUSERCOMPTATIERS = '';
			}
			else {
					$NEWUSERCOMPTATIERS = $_POST['USERTIERSCOMPTA0'];
			}

			if (empty($_POST['USERCREDITOK0']))
			{
				$NEWUSERCREDITOK = '';
			}
			else {
					$NEWUSERCREDITOK = $_POST['USERCREDITOK0'];
			}

			if (empty($_POST['ADHESION0']))
			{
				$NEWUSERADHESION = '';
			}
			else {
					$NEWUSERADHESION = $_POST['ADHESION0'];
			}

			if (empty($_POST['DATEDEBUTADH0']))
			{
				$NEWUSERDATEDEBADHESION = '';
			}
			else
			{
				if($NEWUSERADHESION == 'VALIDE')
				{
					$tmp = explode("/", $_POST['DATEDEBUTADH0']);
					$NEWUSERDATEDEBADHESION = $tmp[2]."-".$tmp[1]."-".$tmp[0];
				}
				else
				{
					$NEWUSERDATEDEBADHESION = "1900-01-01";
				}
			}

			if (empty($_POST['DATEFINADH0']))
			{
				$NEWUSERDATEFINADHESION = '';
			}
			else
			{
				if($NEWUSERADHESION == 'VALIDE')
				{
					$tmp = explode("/", $_POST['DATEFINADH0']);
					$NEWUSERDATEFINADHESION = $tmp[2]."-".$tmp[1]."-".$tmp[0];
				}
				else
				{
					$NEWUSERDATEFINADHESION = "1900-01-01";
				}
			}

			if ($creatuser == 3)
			{
				$id1 = rand(1,99999999);
				$id2 = rand(1,99999999);
				$sql = 'UPDATE UTILISATEUR SET UT_TOKEN = "" WHERE UT_TOKEN = "'.$_SESSION['token'].'";';
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$sql = 'INSERT INTO UTILISATEUR (UT_TOKEN, UT_NOM, UT_PRENOM, UT_EMAIL, UT_ADMINSITE, UT_STATUT, UT_PASSWORD, UT_ID1, UT_ID2, UT_IDMODIF, UT_IDDATEDEMANDE,UT_VERIF,UT_TEL,
						UT_LASTCONNECT,	UT_LASTMODIF)
						VALUES ("'.$_SESSION['token'].'","'.encrypt($NEWNOM,$id2).'","'.encrypt($NEWPRENOM,$id2).'","'.encrypt($NEWEMAIL,$id2).'","'.$NEWUSERADMINSITE.'","COWORKER",
						"",'.$id1.','.$id2.',"","1900-01-01","","","1900-01-01",now())';

				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$sql = "SELECT UT_IDUSER FROM UTILISATEUR WHERE UT_TOKEN = '".$_SESSION['token']."' AND UT_LOGIN = ''";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					$sql = "UPDATE UTILISATEUR SET UT_LOGIN = 'C".str_pad($data1['UT_IDUSER'],4,"0",STR_PAD_LEFT)."', UT_TOKEN = '' WHERE UT_TOKEN = '".$_SESSION['token']."' AND UT_LOGIN = ''";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
					$login = "C".str_pad($data1['UT_IDUSER'],8,"0",STR_PAD_LEFT);

					emailInscriUser($NEWEMAIL, '1234', "C".str_pad($data1['UT_IDUSER'],4,"0",STR_PAD_LEFT));
					$sql = 'INSERT INTO `tiersetab`(`TE_LOGIN`, `TE_ETABLISSEMENT`, `TE_DATEVALIDATION`, `TE_DEBUTADHESION`, `TE_DATEFINADHESION`, `TE_COCHE1`,
							`TE_TEXTCOCHE1`, `TE_COCHE2`, `TE_TEXTCOCHE2`, `TE_TEXTRGPD`, `TE_STATUT`, `TE_CREDITOK`, `TE_CREDITVAL`, `TE_ARTADHESIONCODE`,
							`TE_ARTADHESIONLIB`, `TE_ARTADHESIONPRIX`, `TE_ADHESIONPAYE`,
							TE_VALIDEUR, TE_ID, TE_COMPTATIERS) VALUES
							("C'.str_pad($data1['UT_IDUSER'],4,"0",STR_PAD_LEFT).'","'.$_SESSION['ETABADMIN'].'","1900-01-01","'.$NEWUSERDATEDEBADHESION.'","'.$NEWUSERDATEFINADHESION.'",
							"NON","","NON","","","'.$NEWUSERADHESION.'","'.$NEWUSERCREDITOK.'",0,"","",0,"NON","","","'.$NEWUSERCOMPTATIERS.'");';

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);

				}


			}
		}

		for ($i = 1; $i <= $_POST['nbruseraffich']; $i++)
		{
			if(isset($_POST['boutonvalid_'.$i]))
			//if ($_POST['USERMODIF'.$i] == 'OUI')
			{
				if (empty($_POST['USERTIERSCOMPTA'.$i]))
				{
					$NEWUSERCOMPTATIERS = '';
				}
				else {
						$NEWUSERCOMPTATIERS = $_POST['USERTIERSCOMPTA'.$i];
				}

				if (empty($_POST['USERCREDITOK'.$i]))
				{
					$NEWUSERCREDITOK = '';
				}
				else {
						$NEWUSERCREDITOK = $_POST['USERCREDITOK'.$i];
				}

				if (empty($_POST['ADHESION'.$i]))
				{
					$NEWUSERADHESION = '';
				}
				else {
						$NEWUSERADHESION = $_POST['ADHESION'.$i];
				}

				if (empty($_POST['DATEDEBUTADH'.$i]))
				{
					$NEWUSERDATEDEBADHESION = '';
				}
				else
				{
					if($NEWUSERADHESION == 'VALIDE')
					{
						$tmp = explode("/", $_POST['DATEDEBUTADH'.$i]);
						$NEWUSERDATEDEBADHESION = $tmp[2]."-".$tmp[1]."-".$tmp[0];
					}
					else
					{
						$NEWUSERDATEDEBADHESION = "1900-01-01";
					}
				}

				if (empty($_POST['DATEFINADH'.$i]))
				{
					$NEWUSERDATEFINADHESION = '';
				}
				else
				{
					if($NEWUSERADHESION == 'VALIDE')
					{
						$tmp = explode("/", $_POST['DATEFINADH'.$i]);
						$NEWUSERDATEFINADHESION = $tmp[2]."-".$tmp[1]."-".$tmp[0];
					}
					else
					{
						$NEWUSERDATEFINADHESION = "1900-01-01";
					}
				}

				$ID2 = $_POST['USERID'.$i];
				$sql = "UPDATE UTILISATEUR SET UT_NOM = '".addslashes(encrypt2($_POST['USERNOM'.$i],$ID2))."', UT_PRENOM = '".addslashes(encrypt2($_POST['USERPRENOM'.$i],$ID2))."',

								UT_ADMINSITE = '".$_POST['USERADMINSITE'.$i]."', UT_EMAIL = '".encrypt2($_POST['USEREMAIL'.$i],$ID2)."'
								WHERE UT_LOGIN = '".$_POST['USERLOGIN'.$i]."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				if ((isset($_POST['ADHESION'.$i])) && ($_POST['ADHESION'.$i] == 'VALIDE'))
				{
					$sql = "SELECT COUNT(TE_LOGIN) AS CPT FROM TIERSETAB WHERE TE_LOGIN = '".$_POST['USERLOGIN'.$i]."' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						if ($data1['CPT'] == 0)
						{
							$sql = "INSERT INTO TIERSETAB (TE_LOGIN, TE_ETABLISSEMENT, TE_STATUT, TE_DEBUTADHESION, TE_DATEFINADHESION, TE_CREDITOK, TE_CREDITVAL, TE_COCHE1, TE_TEXTCOCHE1, TE_COCHE2, TE_TEXTCOCHE2,
									TE_TEXTRGPD, TE_ARTADHESIONCODE, TE_ARTADHESIONLIB, TE_ARTADHESIONPRIX, TE_ADHESIONPAYE, TE_VALIDEUR, TE_ID, TE_COMPTATIERS)
									VALUES ('".$_POST['USERLOGIN'.$i]."','".$_SESSION['ETABADMIN']."','".$NEWUSERADHESION."','".$NEWUSERDATEDEBADHESION."',
									'".$NEWUSERDATEFINADHESION."','NON',0,'NON','','NON','','', '', '', 0, 'NON','','','".$NEWUSERCOMPTATIERS."');";
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->exec($sql);
						}
						else
						{
							$sql = 'UPDATE TIERSETAB SET TE_STATUT = "'.$NEWUSERADHESION.'", TE_DEBUTADHESION = "'.$NEWUSERDATEDEBADHESION.'", TE_DATEFINADHESION = "'.$NEWUSERDATEFINADHESION.'",
									TE_COMPTATIERS = "'.$NEWUSERCOMPTATIERS.'"
									WHERE TE_LOGIN = "'.$_POST['USERLOGIN'.$i].'" AND TE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'";';
							echo $sql;
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->exec($sql);
						}
					}
				}
				if (isset($_POST['USERCREDITOK'.$i]))
				{
					$sql = "UPDATE TIERSETAB SET TE_CREDITOK = '".$_POST['USERCREDITOK'.$i]."'
									WHERE TE_LOGIN = '".$_POST['USERLOGIN'.$i]."' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
				}
			}
		}
	}
}

function nouvel_utilisateur()
{
?>

<form id="formplaning" action="" method="post">
	<input type="hidden" name="action" id="action" value="ajout">
  <table style="width: 1516px; height: 144px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
    <tbody>
	<?php
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql= "SELECT * from UTILISATEUR WHERE UT_LOGIN = '" .$_SESSION['login']. "';";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
		while($data = mysqli_fetch_array($req))
		{
		?>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Login : </td>
			<td style="width: 460px;">
				<input id="newlogin" name="newlogin" size="50" maxlength="50" required="" type="text">
			</td>
			<td style="width: 160px;">Email : </td>
			<td style="width: 460px;"><input id="newloginemail" name="newloginemail" size="150" maxlength="250" required="" type="text"></td>
			<td style="width: 115px; "></td>
		</tr>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Tiers Gestion : </td>
			<td style="width: 460px;">
				<input id="newlogintiers" name="newlogintiers" size="50" maxlength="50" required="" type="text">
			</td>
			<td style="width: 160px;">
			<td style="width: 460px;"></td>
			<td style="width: 115px; "></td>
		</tr>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Nom : </td>
			<td style="width: 460px;">
				<input id="newloginnom" name="newloginnom" size="50" maxlength="50" required="" type="text">
			</td>
			<td style="width: 160px;">Prénom :</td>
			<td style="width: 460px;">
				<input style="border: 1px solid rgb(0, 0, 0);" id="newloginprenom" name="newloginprenom" size="50" maxlength="50" required="" type="text">
			</td>
			<td style="width: 115px; "></td>
		</tr>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Adresse : </td>
			<td style="width: 460px;">
				<input id="newloginadresse1" name="newloginadresse1" size="50" maxlength="50" type="text">
			</td>
			<td style="width: 160px;">Adresse complémentaire : </td>
			<td style="width: 460px;">
				<input id="newloginadresse2" name="newloginadresse2" size="50" maxlength="50" type="text">
			</td>
			<td style="width: 115px; "></td>
		</tr>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Code postal : </td>
			<td style="width: 460px;">
			<input id="newlogincodepostal" name="newlogincodepostal" size="5" maxlength="5" type="text">
			</td>
			<td style="width: 160px;">Ville : </td>
			<td style="width: 460px;">
				<input id="newloginville" name="newloginville" size="50" maxlength="50" type="text">
			</td>
			<td style="width: 115px; "></td>
		</tr>
		<tr>
			<td style="width: 115px; "></td>
			<td style="width: 160px;">Type d'adhérant : </td>
			<td style="width: 460px;">
				<select name="newloginstatut">
					<option value="ADMIN">Administrateur</option>
					<option value="COWORKER">CoWorker</option>
					<option value="PERMANANT">Permanant</option>
				</select>
			</td>
			<td style="width: 160px;"></td>
			<td style="width: 460px;"></td>
			<td style="width: 115px; "></td>
		</tr>
		<?php
	}


	?>
	<tr>
		<td style="width: 115px; height: 30px;"></td>
		<td style="width: 160px; height: 30px;"></td>
		<td style="width: 460px; height: 30px;"></td>
		<td style="width: 160px; height: 30px;"></td>
		<td style="width: 460px; height: 30px;"></td>
		<td style="width: 115px; height: 30px;"></td>
	</tr>
	<tr>
		<td style="width: 115px; "></td>
		<td style="width: 160px text-align: left;">
			<input class="bouton1" value="Valider" type="submit">
		</td>
		<td style="width: 460px; text-align: right;">
			<a href="modif_password.php" onclick="window.open(this.href, 'exemple', 'height=400, width=500, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;">
			<input class="bouton1" value="Nouveau mot de passe" type="submit">
		</td>
		<td style="width: 160px; text-align: center;">

		</td>
		<td style="width: 460px; text-align: left;"></td>
		<td style="width: 115px; "></td>
	</tr>
    </tbody>
  </table>
  <br>
</form>

<?php
}


function popup_create_adhesion()
{

	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	else
	{
		$user = $_GET['user'];
	}

	?>
	<form action="" method="post">
	<div id="facture" style="width:40%;">
	<label>Utilisateur</label>
	<select align="left" name="listuser" id="listuser" onchange="window.location='create_adhesion.php?user='+this.value;">
		<OPTION align="left">Choisir un autre utilisateur</OPTION>
		<?php
		$sql = "select TE_LOGIN, UT_NOM, UT_PRENOM, UT_ID2  from TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
		AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_STATUT <> 'VALIDE'
		GROUP BY TE_LOGIN;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<option value="<?php echo $r['TE_LOGIN']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php
		}
		?>
	</SELECT>
	<?php

	if (!isset($_GET['user']))
	{
		$sql = "select TE_LOGIN, UT_NOM, UT_PRENOM, UT_ID2  from TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT <> 'VALIDE'
		AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL
		GROUP BY TE_LOGIN LIMIT 1;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$userfacture =  $r['TE_LOGIN'];
		}
	}
	else
	{
		$userfacture =  $_GET['user'];
	}

	$sql = "select case when ET_TYPEADHESION = 'ANNEEENCOUR' then date_format(now(),'01/01/%Y') else date_format(now(),'%d/%m/%Y') end  AS DATEMIN,
			case when ET_TYPEADHESION = 'ANNEEENCOUR' then date_format(now(),'31/12/%Y') else date_format(date_add(now(), INTERVAL 1 YEAR),'%d/%m/%Y') end  AS DATEMAX,

			UT_LOGIN, TE_LOGIN, UT_NOM, UT_PRENOM, UT_ID2  from TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
			WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT <> 'VALIDE'
			AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_LOGIN = '" .$userfacture . "'
			GROUP BY TE_LOGIN;";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $r)
		{
		echo '<p align="left"><u>Login : ' .$r['UT_LOGIN'] . ' </u></p>';

		echo '<p align="left">Nom : ' .decrypt($r['UT_NOM'],$r['UT_ID2']) . ' </p>';
		echo '<p align="left">Prénom : ' .decrypt($r['UT_PRENOM'],$r['UT_ID2']) . ' </p>';
		?>
		<br>

		<input name="login" type="hidden" value="<?php echo $r['TE_LOGIN'];?>">
		<input name="action" type="hidden" value="CREATEADHESION">
		<p align="left">Date début: <input name="DateDebut" id="datepicker" type="text" size="28" value="<?php echo $r['DATEMIN'];?>" size="12" required/></p>
		<p align="left">Date de fin: <input name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $r['DATEMAX'];?>" size="12" required/></p>
		<br>
		<center><input type="submit" value="Valider" /></center>
		<br>
		<?php

		}
	?>
	</form>
	</div>

		<?php
	// fin connexion
}

function creation_adhesion()
{


	$sql = "SELECT TE_LOGIN, 1 AS QTEARTICLE, AR_CODEARTICLE, AR_DESIGNATION, AR_TARIF,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_VALEUR ELSE 0 END AS MONTANTPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_LIBELLE ELSE '' END AS LIBELLEPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_TYPEREMISE ELSE '' END AS TYPEPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_REMISEDIRECT ELSE '' END AS ACTIONPROMO,
			UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE
			FROM TIERSETAB
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
			LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTADHESION AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
			LEFT JOIN PROMOTION ON PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_DECLENCHEUR = AR_CODEARTICLE
			LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
			WHERE TE_LOGIN = '" .$_POST['login'] . "' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ";
//

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $r)
	{
		?>
		<div id="facture">
		<form action="" method="post">
		<input type="hidden" name="login" required size="50" value="<?php echo $r['TE_LOGIN'];?>" >
		<input name="action" type="hidden" value="VALIDEADHESION">
		<br>
		<?php
		$months = array("janvier", "février", "mars", "avril", "mai", "juin",
			"juillet", "août", "septembre", "octobre", "novembre", "décembre");
		?>
		<p align="left"><u>Date de la facture :</u> <input name="DateDebut" id="datepicker" type="text" size="28" value="<?php echo date("d") .'/'. date("m") . '/'.date("Y");?>" size="12" required/></p>

		<br>
		<p align="left" ><u>ETABLISSEMENT :</u></p>
		<p align="left"><?php echo $r['ET_LIBELLE'] .' '. $r['ET_VILLE'];?></p>
		<br>
		<p align="left" ><u>ADHERENT :</u></p>
		<p align="left"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) .' '. decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p>

		<br>
		<p align="left" ><u>DETAIL FACTURE : </u>  <?php echo '  ' .$r['AR_CODEARTICLE'] .' - ' .$r['AR_DESIGNATION'] .' : Prix HT = ' .$r['AR_TARIF'] .' €'; ?></u></p>
		<br>
		<p align="left" ><u>TOTAL FACTURE HT : </u>  <?php echo '  ' .$r['AR_TARIF'] .' €'; ?></u></p>
		<br>
		<?php
		if ($r['MONTANTPROMO']!=0)
		{
			?>
			<p align="left" ><u>Remise obtenu : </u>  <?php echo '  ' .$r['LIBELLEPROMO'] .' : ' .number_format($r['MONTANTPROMO'],2,',','') .' €'; ?></p>
			<br>
			<?php
		}
		echo '<label ><u>ADHERENT :</u></p>';
		echo '<select name="TYPEFACTURE">';
		echo '<OPTION value="rien" selected="selected">Choisir quand facturer !</option>';
		echo '<OPTION value="1" >Maintenant</option>';
		echo '<OPTION value="0" >Plus tard</option>';
		echo '</SELECT>';


	}
	?>
	<br>
		<p align="left">Commentaire (obligatoire) : <input type="text" name="Commentaire" required size="50"></p>
		<center><input type="submit" value="Valider" /></center>
		<br>
		</form>
		<?php
	// fin connexion

}
function valide_adhesion()
{
	$QteFact = 0;
	$reffacture = '';
	$gp_numero = 0;

	$sql = "SELECT
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_VALEUR ELSE 0 END AS MONTANTPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_LIBELLE ELSE '' END AS LIBELLEPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_TYPEREMISE ELSE '' END AS TYPEPROMO,
			CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = 'OUI') THEN PR_REMISEDIRECT ELSE '' END AS ACTIONPROMO,

			FROM TIERSETAB
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
			LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTADHESION AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
			LEFT JOIN PROMOTION ON PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_DECLENCHEUR = AR_CODEARTICLE
			LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
			LEFT JOIN CHOIXCODE ON CC_TYPE = 'MOIS' AND CC_CODE =  DATE_FORMAT(now(),'%m')
			WHERE TE_LOGIN = '" .$_POST['login'] . "' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{

		if ($data['QTEARTICLE'] != 0)
		{
			if ($_POST['TYPEFACTURE'] == 1)
			{
				$sql = "INSERT INTO PIECE (GP_TYPE, GP_USER, GP_USERNOM, GP_USERPRENOM, GP_USERADRESSE1, GP_USERADRESSE2, GP_USERCODEPOSTAL, GP_USERVILLE, GP_USEREMAIL, GP_USERID, GP_ETABLISSEMENT, GP_ETLIBELLE,
						GP_ETADRESSE1, GP_ETADRESSE2, GP_ETADRESSE3, GP_ETCODEPOSTAL, GP_ETVILLE, GP_TOTALHT, GP_TOTALQTEFACT, GP_SESSIONTOKEN, GP_DATEPIECE, GP_DATETRANSMISSION, GP_DATEPAYEMENT, GP_DATERELANCE, GP_DATEECHEANCE)
						VALUES ('ADHESION', '".$data['TE_LOGIN']."','".$data['UT_NOM']."','".$data['UT_PRENOM']."','".$data['UT_ADRESSE1']."','".$data['UT_ADRESSE2']."','".$data['UT_CODEPOSTAL']."','".$data['UT_VILLE']."',
						'".$data['UT_EMAIL']."','".$data['UT_ID2']."','".$data['ET_ETABLISSEMENT']."','".$data['ET_LIBELLE']."','".$data['ET_ADRESSE1']."','".$data['ET_ADRESSE2']."','".$data['ET_ADRESSE3']."',
						'".$data['ET_CODEPOSTAL']."','".$data['ET_VILLE']."',".$data['QTEARTICLE'].",0,'".$_SESSION['token']."', now(),'1900-01-01','1900-01-01','1900-01-01', ADDDATE(now(), INTERVAL ".$data['ET_ECHEANCEFACTURE']." DAY));";

				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				$sql = "select GP_NUMERO, CONCAT(DATE_FORMAT(GP_DATEPIECE,'AD%y%m_'),CONCAT(SUBSTR('00000',1,5-LENGTH(GP_NUMERO)),GP_NUMERO)) AS REFFACTURE FROM PIECE WHERE GP_USER = '".$data['TE_LOGIN']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req1 = $cnx_bdd->query($sql);
				$tab_r1 = $result_req1->fetchAll();
				foreach ($tab_r1 as $data1)
				{
					$reffacture = $data1['REFFACTURE'];
					$gp_numero = $data1['GP_NUMERO'];
					$sql = "UPDATE PIECE SET GP_REFFACTURE = '".$data1['REFFACTURE']."'  WHERE GP_USER = '".$data['TE_LOGIN']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);

				}

				$numligne = 1;
				$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF,
							GL_PRIXPARPERSONNE, GL_PRIXPARPLACE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data['ET_ETABLISSEMENT']."','".$data['TE_LOGIN']."','".$data['AR_CODEARTICLE']."','".$data['AR_DESIGNATION']."',
						".$data['QTEARTICLE'].",".$data['AR_TARIF'].",".$data['AR_TARIF'].",0,'0.00','0.00',
						date_format(now(),'Adhésion du : %d/%m/%Y');";

				//$cnx_bdd = ConnexionBDD();
				//$result_req = $cnx_bdd->exec($sql);



	//
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$numligne++;
					/////
			}
			else
			{
				$numligne = 1;
				$sql = "INSERT INTO LIGNEATTENTE (GLA_NUMLIGNE, GLA_DATECREATE, GLA_VALIDE, GLA_DATEPIECE, GLA_ETABLISSEMENT, GLA_USER, GLA_ARTICLENO, GLA_ARTLIBELLE, GLA_QTEFACT, GLA_TOTALHT, GLA_COMMENTAIRE,
							GLA_ARTTARIF, GLA_PRIXPARPERSONNE, GLA_PRIXPARPLACE, GLA_LIBELLE, GLA_NUMERO, GLA_REFFACTURE, GLA_SESSIONTOKEN) VALUES
						(".$numligne.",now(),'OUI', now(), '".$data['ET_ETABLISSEMENT']."','".$data['TE_LOGIN']."','".$data['AR_CODEARTICLE']."','".$data['AR_DESIGNATION']."',
						".$data['QTEARTICLE'].",".($data['AR_TARIF'] * $data['QTEARTICLE']).",'',".$data['AR_TARIF'].",'0.00','0.00', date_format(now(),'Adhésion du : %d/%m/%Y'),0,'','".$_SESSION['token']."');";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				//$numindexligne++;
			}
			if ($data['MONTANTPROMO']!= 0)
			{
				$sql = "INSERT INTO `VOUCHER`(`BA_LIBELLE`, BA_LOGIN, BA_ETABLISSEMENT,`BA_VALEUR`, `BA_DATECREATION`, `BA_DATEREPRISE`, `BA_VALIDE`, `BA_REFREPRISE`) VALUES
						('".$data['LIBELLEPROMO']."','".$data['TE_LOGIN']."','".$data['ET_ETABLISSEMENT']."','".$data['MONTANTPROMO']."',now(),'1900-01-01','OUI','');";

				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}
			$sql = "UPDATE TIERSETAB SET TE_DEBUTADHESION = '".$data['DATEDEBINSERT']."', TE_DATEFINADHESION = '".$data['DATEFININSERT']."',
					TE_STATUT = 'VALIDE'
					WHERE TE_LOGIN = '" .$_POST['login'] . "' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ;";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);


			echo 'Adhésion enregistrée';
			if ($_POST['TYPEFACTURE'] == 1)
			{
				?>
				<a style="margin-left: 30px" href="pdf/print_facture.php?reffacture=<?php echo $reffacture; ?> " onclick="window.open(this.href, 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><B>Impression de la facture</b></a>
				<?php
			}
		}
		else
		{
			echo 'Adhésion déjà enregistrée';
			if ($_POST['TYPEFACTURE'] == 1)
			{
				?>
				<a style="margin-left: 30px" href="print_facture.php?reffacture=<?php echo $reffacture; ?> " onclick="window.open(this.href, 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><B>Impression de la facture</b></a>
				<?php
			}
		}

	}
}


function popup_create_facture_old()
{
	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	else
	{
		$user = $_GET['user'];
	}

	?>
	<form action="" method="post">
	<div id="facture" style="width:40%;">
	<label>Utilisateur</label>
	<select align="left" name="listuser" id="listuser" onchange="window.location='create_facture.php?user='+this.value;">
		<OPTION align="left">Choisir un autre utilisateur</OPTION>
		<?php
		$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'
		AND RE_FACTURE = 'NON' AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL
		GROUP BY RE_USER;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			echo  $r['RE_USER'];
			?>
			<option value="<?php echo $r['RE_USER']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php
		}
		?>
	</SELECT>

	<?php
	if (!isset($_GET['user']))
	{
		$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'NON' GROUP BY RE_USER LIMIT 1;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$userfacture =  $r['RE_USER'];
		}
	}
	else
	{
		$userfacture =  $_GET['user'];
	}

	$sql = "select UTILISATEUR.*, (select count(*) from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'OUI' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as NBJOURSFACTURE,
	(select count(*) from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as NBJOURSNONFACTURE,
	(select date_format(min(RE_DATE),'%d/%m/%Y') from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as DATEMIN,
	(select date_format(max(RE_DATE),'%d/%m/%Y') from RESERVATION where RE_USER = UT_LOGIN AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."') as DATEMAX,
	ifnull((select max(GP_NUMERO) from PIECE where GP_USER = UT_LOGIN),0)+ 1 as NEWFACTURENUM  from UTILISATEUR where UT_LOGIN = '" .$userfacture . "';";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $r)
		{
		echo '<p align="left"><u>Login : ' .$r['UT_LOGIN'] . ' </u></p>';

		echo '<p align="left">Nom : ' .decrypt($r['UT_NOM'],$r['UT_ID2']) . ' </p>';
		echo '<p align="left">Prénom : ' .decrypt($r['UT_PRENOM'],$r['UT_ID2']) . ' </p>';
		echo '<p align="left">Nombre de jours facturé : ' .$r['NBJOURSFACTURE'] . '</p>';
		echo '<p align="left">Nombre de jours à facturer : ' .$r['NBJOURSNONFACTURE'] . '</p>';
		?>
		<br>

		<input name="login" type="hidden" value="<?php echo $r['UT_LOGIN'];?>">
		<input name="action" type="hidden" value="CREATEFACTURE">
		<p align="left">Date début: <input name="DateDebut" id="datepicker" type="text" size="28" value="<?php echo $r['DATEMIN'];?>" size="12" required/></p>
		<p align="left">Date de fin: <input name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $r['DATEMAX'];?>" size="12" required/></p>
		<br>
		<center><input type="submit" value="Valider" /></center>
		<br>
		<?php

		}
	?>
	</form>
	</div>

		<?php
	// fin connexion
}

function popup_create_vente()
{
	//echo parse_url ( $_SERVER [ 'REQUEST_URI' ] ) ;
	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	else
	{
		$user = $_GET['user'];
	}

	//echo $_POST['listarticle'];

	?>
	<form action="" method="post" name="test">
	<div id="facture" style="width:80%;">

	<label>Utilisateur</label>
	<select align="left" name="listuser" id="listuser" onchange="window.location='vente.php?user='+this.value;">
		<OPTION align="left">Choisir un autre utilisateur</OPTION>
		<?php
		$sql = "select * FROM TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT = 'VALIDE'
		AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<option value="<?php echo $r['TE_LOGIN']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php
		}
		?>
	</SELECT>
	<?php
	if (isset($_GET['user']))
	{
		$sql = "select * FROM TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT = 'VALIDE'
		AND TE_LOGIN = '".$_GET['user']."';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<table align="center" style='text-align: left; width: 900px; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="1" cellpadding="2" cellspacing="2">
				<tbody>
					<tr>
						<td style="width: 100px;height: 10px;"><u>Login :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo $r['UT_LOGIN']; ?>'</td>
					</tr>
					<tr>
						<td style="width: 100px;height: 10px "><u>Nom :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_PRENOM'],$r['UT_ID2']); ?>'</td>

					</tr>
					<tr>
						<td style="width: 100px;height: 10px "><u>Adresse :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' - ' .decrypt($r['UT_VILLE'],$r['UT_ID2']); ?>'</td>

					</tr>
					<tr>
						<td align="center" style="width: 100px;height: 10px "><u>Article</u></td>
					</tr>
					<tr>
						<input name="login" type="hidden" value="<?php echo $userfacture;?>">
						<td style="width: 100px;">
						<select align="left" name="listarticle" id="listarticle" onChange="this.form.submit()">
							<OPTION align="left">Choisir un article</OPTION>
							<?php
							$sql = "select * FROM ARTICLE WHERE AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND AR_TYPE = 'ARTVE';";
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->query($sql);
							$tab_r = $result_req->fetchAll();
							foreach ($tab_r as $r)
							{
								?>
								<option value="<?php echo $r['AR_ARTICLENO']; ?>"><?php echo $r['AR_DESIGNATION']; ?></option>
								<?php
							}
							?>
						</SELECT>
						</td>
					</tr>
					<?php
					for ($i = 0; $i <= 10; $i++)
					{
						?>
						<tr>

							<td style="width: 100px;">
								<SELECT align="left" name="art1" id="art1">
								<OPTION> 1 </OPTION>
								</SELECT>
							</td>

						</tr>
						<?php
					}
					?>
					<tr>
						<input name="login" type="hidden" value="<?php echo $userfacture;?>">
						<input name="action" type="hidden" value="CREATEFACTURE">
						<td align="center" colspan="4"><input type="submit" value="Valider" /></td>
					</tr>
				</tbody>
			</table>
			<?php

		}
	}
	?>

	</form>
	</div>

		<?php
	// fin connexion
}

function creation_vente()
{

	$CritereSQL = '';
	$CritVoucherSQL = '';
	$NbrResa = 0;
	$NbrVoucher = 0;
	for ($i = 0; $i <= $_POST['nbresa'] -1; $i++)
	{

		if (isset($_POST['resa'.$i]))
		{
			if ($NbrResa == 0)
			{
				$CritereSQL = " AND RE_NUMRESA IN ('".$_POST['resa'.$i]."'";
			}
			else
			{
				$CritereSQL = $CritereSQL .",'".$_POST['resa'.$i]."'";
			}
			$NbrResa++;
		}
	}

	if ($NbrResa != 0)
	{
		$CritereSQL = $CritereSQL .") ";
	}

	for ($i = 0; $i <= $_POST['nbremise'] -1; $i++)
	{
		if (isset($_POST['remise'.$i]))
		{
			if ($NbrVoucher == 0)
			{
				$CritVoucherSQL = " AND BA_NUMERO IN ('".$_POST['remise'.$i]."'";
			}
			else
			{
				$CritVoucherSQL = $CritVoucherSQL .",'".$_POST['remise'.$i]."'";
			}
			$NbrVoucher++;
		}
	}
	if ($NbrVoucher != 0)
	{
		$CritVoucherSQL = $CritVoucherSQL .") ";
	}

	if ($NbrResa != 0)
	{
		$sql = "SELECT RE_USER, COUNT(RE_NUMRESA) AS NBJOURSFACTURE, SUM(RE_NBRPLACE) AS NBPLACE, SUM(RE_PRIXTOTALRESA) AS TOTALFACT,
				(SELECT ifnull(SUM(BA_VALEUR),0) FROM VOUCHER WHERE BA_LOGIN = '".$_POST['login']."' AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritVoucherSQL ." AND BA_VALIDE = 'OUI') AS TOTALVOUCHER,
				UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE

				FROM RESERVATION
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
				LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
				WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritereSQL ."

				GROUP BY RE_USER,   UT_NOM, UT_PRENOM, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE
				, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE LIMIT 1;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<div id="facture">
			<form action="" method="post">
			<input type="hidden" name="login" required size="50" value="<?php echo $r['RE_USER'];?>" >
			<input name="action" type="hidden" value="VALIDEFACTURE">
			<input name="CritereSQL" type="hidden" value="<?php echo $CritereSQL;?>">
			<input name="CritVoucherSQL" type="hidden" value="<?php echo $CritVoucherSQL;?>">
			<br>

			<?php
			$months = array("janvier", "février", "mars", "avril", "mai", "juin",
				"juillet", "août", "septembre", "octobre", "novembre", "décembre");
			?>
			<p align="left"><u>Date de la facture :</u> <?php echo date("d") .' ' .$months[date("m")-1] .' ' .date("Y") ;?></p>
			<br>
			<p align="left" ><u>ETABLISSEMENT :</u></p>
			<p align="left"><?php echo $r['ET_LIBELLE'] .' '. $r['ET_VILLE'];?></p>
			<br>
			<p align="left" ><u>CLIENT :</u></p>
			<p align="left"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) .' '. decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p>
			<!--< align="left"><?php echo decrypt($r['UT_ADRESSE1'],$r['UT_ID2']);?></p>
			<p align="left"><?php echo decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p> -->
			<br>
			<p align="left" >TOTAL FACTURE : <?php echo '  ' .number_format($r['TOTALFACT'],2,',','') .' €'; ?></p>
			<?php
			if ($NbrVoucher != 0)
			{
				echo '<p align="left" >TOTAL AVOIR : </u>  ' .number_format($r['TOTALVOUCHER'],2,',','') .' €</p>';
			}

			echo '<p align="left" ><u>NET A PAYER : </u>  ' .number_format(($r['TOTALFACT'] - $r['TOTALVOUCHER']),2,',','') .' €</u></p>';
			?>
			<br>
			<p align="left" ><u>DETAIL FACTURE :</u></p>
			<?php
		}
		$sql = "SELECT RE_USER, RE_ZONE, RE_ZONELIBELLE,COUNT(RE_NUMRESA) AS NBJOURSFACTURE, SUM(RE_NBRPLACE) AS NBPLACE, RE_PRIXPLACE, RE_PRIXPARPERSONNE, SUM(RE_PRIXTOTALRESA) AS TOTALFACT,
				ifnull((select max(GP_NUMERO) from PIECE),0)+ 1 as NEWFACTURENUM,
				UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE

				FROM RESERVATION
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
				LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
				WHERE RE_USER = '" .$_POST['login'] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritereSQL ."
				GROUP BY RE_USER,  RE_ZONELIBELLE,RE_ZONE, RE_PRIXPLACE, RE_PRIXPARPERSONNE,UT_NOM, UT_PRENOM, UT_ID2, ET_LIBELLE, ET_ADRESSE1, ET_ADRESSE2, ET_CODEPOSTAL, ET_VILLE
				, UT_ADRESSE1, UT_CODEPOSTAL, UT_VILLE ORDER BY RE_ZONE;";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>

			<p align="left"><?php echo $r['RE_ZONE'] .' - '. addslashes($r['RE_ZONELIBELLE']) .' :     '. $r['NBJOURSFACTURE'] .' Réservation - Prix de l emplacement : ' .$r['RE_PRIXPLACE'] .' € - Prix de la place : ' .$r['RE_PRIXPARPERSONNE'] . ' - Nombre de place : ' .$r['NBPLACE'] . ' € - Total = ' .$r['TOTALFACT'] .' €';?></p>

			<?php
		}

		if ($NbrVoucher != 0)
		{
			echo '<br>';
			echo '<p align="left" ><u>DETAIL AVOIR :</u></p>';

			$sql = "SELECT DATE_FORMAT(BA_DATECREATION,'%d/%m/%Y') AS DATEVOUCHER, BA_NUMERO, BA_LIBELLE, BA_VALEUR FROM VOUCHER WHERE BA_LOGIN = '".$_POST['login']."' AND BA_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'" .$CritVoucherSQL ." ORDER BY BA_NUMERO;";


			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r)
			{
				?>

				<p align="left"><?php echo $r['BA_LIBELLE'] .' du ' .$r['DATEVOUCHER'] .' - Montant : ' .number_format($r['BA_VALEUR'],2,',','') .' €';?></p>

				<?php
			}
		}
		?>


		<br>
		<p align="left">Commentaire (obligatoire) : <input type="text" name="Commentaire" required size="50"></p>
		<center><input type="submit" value="Valider" /></center>
		<br>
		</form>
		<?php
		// fin connexion
	}

}

function popup_credite_compte()
{
	//echo parse_url ( $_SERVER [ 'REQUEST_URI' ] ) ;
	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	else
	{
		$user = $_GET['user'];
	}

	//echo $_POST['listarticle'];

	?>
	<form action="" method="post" name="test">
	<div id="facture" style="width:80%;">

	<label>Utilisateur</label>
	<select align="left" name="listuser" id="listuser" onchange="window.location='crediter_user.php?user='+this.value;">
		<OPTION align="left">Choisir un autre utilisateur</OPTION>
		<?php
		$sql = "select * FROM TIERSETAB LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT = 'VALIDE'
		AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_CREDITOK = 'OUI';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<option value="<?php echo $r['TE_LOGIN']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php
		}
		?>
	</SELECT>
	<?php
	if (isset($_GET['user']))
	{
		$sql = "select * FROM TIERSETAB
				LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
				LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTMONNAIE AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
				WHERE TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND TE_STATUT = 'VALIDE' AND TE_CREDITOK = 'OUI'
				AND TE_LOGIN = '".$_GET['user']."';";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<table align="center" style='text-align: left; width: 900px; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
				<tbody>
					<tr><td style="width: 100px;height: 20px "></td></tr>
					<tr>

						<td style="width: 100px;height: 10px;"><u>Login :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo $r['UT_LOGIN']; ?>'</td>
					</tr>
					<tr>
						<td style="width: 100px;height: 10px "><u>Nom :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_PRENOM'],$r['UT_ID2']); ?>'</td>

					</tr>
					<tr>
						<td style="width: 100px;height: 10px "><u>Adresse :</u></td>
						<td colspan="3" style="width: 212px;"><?php echo decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' - ' .decrypt($r['UT_VILLE'],$r['UT_ID2']); ?>'</td>

					</tr>
					<tr><td style="width: 100px;height: 20px "></td></tr>
					<tr>


						<td style="height: 28px; width: 212px;"><input disabled maxlength="30" size="30" tabindex="1" name="CODEART" id="CODEART" value="<?php echo $r['ET_ARTMONNAIE']; ?>"></td>
						<td style="width: 100px;height: 10px "><input disabled maxlength="30" size="30" tabindex="1" name="ARTLIB" id="ARTLIB" value="<?php echo $r['AR_DESIGNATION']; ?>"></td>
						<td style="width: 150px;height: 10px "><u>Montant à créditer :</u></td>
						<td style="height: 28px; width: 250px;"><input maxlength="6" size="6" tabindex="2" name="MONTANT" id="MONTANT" required ></td>

					</tr>
					<tr><td style="width: 100px;height: 20px "></td></tr>

					<tr>
						<input name="login" type="hidden" value="<?php $_GET['user'];?>">
						<input name="action" type="hidden" value="CREATECREDITCPT">
						<td align="center" colspan="4"><input type="submit" value="Validder" /></td>
					</tr>
				</tbody>
			</table>
			<?php

		}
	}
	?>

	</form>
	</div>

		<?php
	// fin connexion
}

function creation_creditcompte()
{
	echo $_POST['MONTANT'];
	echo $_POST['CODEART'];
	if ($_POST['MONTANT'] != 0 && $_GET['user'] != '')
	{
		$sql = "SELECT * FROM TIERSETAB
				LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
				LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTMONNAIE AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
				WHERE TE_LOGIN = '".$_GET['user']."' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ;";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<div id="facture">
			<form action="" method="post">
			<input type="hidden" name="login" required size="50" value="<?php echo $r['TE_LOGIN'];?>" >
			<input type="hidden" name="MNTCREDITCPT" required size="50" value="<?php echo $_POST['MONTANT'];?>" >
			<input name="action" type="hidden" value="VALIDECREDITCPT">
			<br>
			<?php
			$months = array("janvier", "février", "mars", "avril", "mai", "juin",
				"juillet", "août", "septembre", "octobre", "novembre", "décembre");
			?>
			<p align="left"><u>Date de la facture :</u> <?php echo date("d") .' ' .$months[date("m")-1] .' ' .date("Y") ;?></p>
			<br>
			<p align="left" ><u>ETABLISSEMENT :</u></p>
			<p align="left"><?php echo $r['ET_LIBELLE'] .' '. $r['ET_VILLE'];?></p>
			<br>
			<p align="left" ><u>ADHERENT :</u></p>
			<p align="left"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) .' '. decrypt($r['UT_NOM'],$r['UT_ID2']) .' - ' .decrypt($r['UT_ADRESSE1'],$r['UT_ID2']) .' - ' .decrypt($r['UT_CODEPOSTAL'],$r['UT_ID2']) .' '. decrypt($r['UT_VILLE'],$r['UT_ID2']);?></p>
			<br>
			<p align="left" >VALEUR ACTUEL DU CREDIT : <?php echo '  ' .number_format($r['TE_CREDITVAL'],2,',','') .' €'; ?></p>
			<br>
			<p align="left" >MONTANT AJOUTE SUR LE COMPTE : <?php echo '  ' .number_format($_POST['MONTANT'],2,',','') .' €'; ?></p>
			<br>
			<p align="left" >NOUVELLE VALEUR DU COMPTE : <?php echo '  ' .number_format(($_POST['MONTANT'] + $r['TE_CREDITVAL']),2,',','') .' €'; ?></p>

			<br>
		<?php
		}




		?>
		<br>
		<p align="left">Commentaire (obligatoire) : <input type="text" name="Commentaire" required size="50"></p>
		<center><input type="submit" value="Valider" /></center>
		<br>
		</form>

		<?php
	}
	// fin connexion

}

function liste_adhesion()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
		// déclaration des variables
		$nbruseraffich = 0;
		?>



		<body>


		<center><div id="support1">
		<form  enctype="multipart/form-data" action="" method="post">
		<table  style=""width: 80%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">

		<tbody>
			<tr>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 250px;">Login</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 200px;">Nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 100px;">Pr&eacute;nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 300px;">Email</td>
				<?php
				if (!isset($_SESSION['SUPERADMIN']))
				{
				echo '<td style="text-align: center; font-family: Calibri; width: 80px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date début adhésion</td>';
				echo '<td style="text-align: center; font-family: Calibri; width: 81px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date fin adhésion</td>';
				}
				?>
				<td colspan="2" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);">

				</td>
			</tr>


		<?php

		//$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$nbretabvalid = 0;
		if (isset($_GET['PAGE']))
		{
			if ($_GET['PAGE'] == 0)
			{
				$limitmin = 0;
				$pagesuiv = 1;
				$pageprec = 0;
			}
			else
			{
				$limitmin = $_GET['PAGE'] * 10;
				$pagesuiv = $_GET['PAGE'] + 1;
				$pageprec = $_GET['PAGE'] - 1;
			}
		}
		else
		{
			$limitmin = 0;
			$pagesuiv = 1;
			$pageprec = 0;
		}
		$sql = "SELECT COUNT(ET_ETABLISSEMENT) AS CPT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbretabvalid = $data['CPT'];
		}
		if ($nbretabvalid == 1)
		{
			$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
							datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
							UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, TE_STATUT,
							UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
							UT_ID2
							FROM TIERSETAB
							LEFT JOIN UTILISATEUR ON TE_LOGIN = UT_LOGIN
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
							WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'

							ORDER BY UT_LOGIN
							LIMIT ".$limitmin.",10";
		}
		else
		{
			$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
							datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
							UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, TE_STATUT,
							UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
							UT_ID2
							FROM UTILISATEUR
							LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN
							LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
							WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
							ORDER BY UT_LOGIN
							LIMIT ".$limitmin.",10";
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbruseraffich++;
			?>
			<tr>
			<?php
			if (($data['ADHESIONOK'] == 'NON') || ($data['TE_STATUT'] != 'VALIDE'))
				{
				?>
				<td style="text-align: left; width: 250px; color: red"><b><?php echo $data['UT_LOGIN']; ?></b></td>
				<td style="width: 200px;color: red"><?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?></td>
				<td style="width: 100px;color: red"><?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?></td>
				<td style="width: 300px;color: red"><?php echo decrypt($data['UT_EMAIL'],$data['UT_ID2']); ?></td>
				<?php
				}
			else
				{
				?>
				<td style="text-align: left; width: 250px; color: green"><b><?php echo $data['UT_LOGIN']; ?></b></td>
				<td style="width: 200px; color: green"><?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?></td>
				<td style="width: 100px; color: green"><?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?></td>
				<td style="width: 300px; color: green"><?php echo decrypt($data['UT_EMAIL'],$data['UT_ID2']); ?></td>
				<?php
				}

			if (isset($_SESSION['ETABADMIN']))
			{
				?>
				<td style="text-align: center; width: 80px;"><?php echo $data['DEBUTADHESION']; ?></td>
				<td style="text-align: center; width: 81px;"><?php echo $data['FINADHESION']; ?></td>
				<?php
			}
			?>


			<td style="width: 30px;"><img class="photo" id="website2" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:cacher(<?php echo $nbruseraffich * 100; ?>);"></td>
			<!--<td style="width: 30px;"><img class="photo" id="website2" border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('manageuser.php?ACTION=suppression&numero=<?php echo $data['UT_LOGIN']; ?>', 'exemple', 'height=200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>-->
			</tr>



			<?php
			if (!empty($data['UT_CIVILITE']))
			{ $civilite = decrypt($data['UT_CIVILITE'], $data['UT_ID2']); }
			else { $civilite = ''; }

			if (!empty($data['UT_NOM']))
			{ $nom = decrypt($data['UT_NOM'],$data['UT_ID2']); }
			else
			{ $nom = ''; }
			if (!empty($data['UT_PRENOM']))
			{ $prenom = decrypt($data['UT_PRENOM'],$data['UT_ID2']); }
			else
			{ $prenom = ''; }

			$sql = "select case when ET_TYPEADHESION = 'ANNEEENCOUR' then date_format(now(),'01/01/%Y') else date_format(now(),'%d/%m/%Y') end  AS DATEMIN,
					case when ET_TYPEADHESION = 'ANNEEENCOUR' then date_format(now(),'31/12/%Y') else date_format(date_add(now(), INTERVAL 1 YEAR),'%d/%m/%Y') end  AS DATEMAX,
					ET_ADHESIONCOCHE1, ET_ADHESIONCOCHE2, ET_ADHESIONRGPD1, ET_ARTADHESION, AR_DESIGNATION, AR_TARIF
					FROM ETABLISSEMENT
					LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTADHESION AND AR_ETABLISSEMENT = ET_ETABLISSEMENT
					WHERE ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r)
			{
				$datedebutadh = $r['DATEMIN'];
				$datefinadh = $r['DATEMAX'];
				$textecoche1 = $r['ET_ADHESIONCOCHE1'];
				$textecoche2 = $r['ET_ADHESIONCOCHE2'];
				$textergpd = $r['ET_ADHESIONRGPD1'];
				$cotisation = $r['AR_DESIGNATION'] .' - ' .$r['AR_TARIF'] .' €';
				$articleadhcode = $r['ET_ARTADHESION'];
				$articleadhlib = $r['AR_DESIGNATION'];
				$articleadhtarif = $r['AR_TARIF'];
			}

			?>

			<tr style="background-color: #edf1f6; ">
				<td colspan="8" id="tdparamadh_<?php echo (($nbruseraffich * 100) + 1); ?>" class="liste1">
					<label>Nom - Prénom : </label>
					<input type="text" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" class="liste1" name="ADHLIGNE1_<?php echo $nbruseraffich; ?>" value='<?php echo $civilite .' '. $nom .' '. $prenom; ?>' disabled>
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 2); ?>" colspan="8" class="liste1">
					<label>Email : </label>
					<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="ADHLIGNE_2<?php echo $nbruseraffich; ?>" class="liste1" value='<?php if($data['UT_EMAIL'] != '') { echo decrypt($data['UT_EMAIL'],$data['UT_ID2']);} ?>' disabled>
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">

				<?php
				if (($data['ADHESIONOK'] == 'NON') || ($data['TE_STATUT'] != 'VALIDE'))
				{
				?>
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 3); ?>" colspan="3" class="liste1">
					<label>Date début d'adhésion : </label>
					<input name="DATEDEBUTADH<?php echo $nbruseraffich; ?>" id="datepicker_<?php echo $nbruseraffich; ?>" type="text" size="28" value="<?php echo $datedebutadh;?>" size="12" required/>
				</td>
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 4); ?>" colspan="5" class="liste1">
					<label>Date fin d'adhésion : </label>
					<input name="DATEFINADH<?php echo $nbruseraffich; ?>" id="datepicker1_<?php echo $nbruseraffich; ?>" type="text" size="28" value="<?php echo $datefinadh;?>" size="12" required/>
				</td>
				<?php
				}
				else
				{
				?>
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 3); ?>" colspan="3" class="liste1">
					<label>Date début d'adhésion : </label>
					<input name="DATEDEBUTADH<?php echo $nbruseraffich; ?>" id="datepicker" type="text" size="28" value="<?php echo $datedebutadh;?>" size="12" disabled/>
				</td>
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 4); ?>" colspan="5" class="liste1">
					<label>Date fin d'adhésion : </label>
					<input name="DATEFINADH<?php echo $nbruseraffich; ?>" id="datepicker1" type="text" size="28" value="<?php echo $datefinadh;?>" size="12" disabled/>
				</td>
				<?php
				}
				?>


			</tr>
			<?php
			if (($data['ADHESIONOK'] == 'NON') || ($data['TE_STATUT'] != 'VALIDE'))
			{
				?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 5); ?>" colspan="8" class="liste1">
						<label>Agissant en qualité de : </label>
						<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="ADHLIGNE_3<?php echo $nbruseraffich; ?>" class="liste1" value='<?php echo 'Personne ' .$data['UT_TYPE']; ?>' disabled>
					</td>
				</tr>

				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 6); ?>" colspan="3" class="liste1">
						<label>Adresse : </label>
						<input type="text" id="paramuser_004_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE1<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_ADRESSE1'] != '') { echo decrypt($data['UT_ADRESSE1'],$data['UT_ID2']);} ?>' disabled>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 7); ?>" colspan="5" class="liste1">
						<label>Adresse complémentaire : </label>
						<input type="text" id="paramuser_005_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERADRESSE2<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_ADRESSE2'] != '') { echo decrypt($data['UT_ADRESSE2'],$data['UT_ID2']);} ?>' disabled>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 8); ?>" colspan="3" class="liste1">
						<label>Code postal : </label>
						<input type="text" id="paramuser_006_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERCPOSTAL<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_CODEPOSTAL'] != '') { echo decrypt($data['UT_CODEPOSTAL'],$data['UT_ID2']);} ?>' disabled>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 9); ?>" colspan="5" class="liste1">
						<label>Ville : </label>
						<input type="text" id="paramuser_007_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERVILLE<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if($data['UT_VILLE'] != '') { echo decrypt($data['UT_VILLE'],$data['UT_ID2']);} ?>' disabled>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 10); ?>" colspan="1" class="liste1" align="center">
							<select id="CHECKADHESION_<?php echo $nbruseraffich ; ?>" name="ADHCOCHE1_<?php echo $nbruseraffich;?>" style="width: 60px;" required>
								<OPTION  value="OUI">OUI</option>
								<OPTION selected disabled value="">NON</option>
							</SELECT>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 11); ?>" colspan="7" class="liste1">
						<TEXTAREA style="width: 600px;" id="parambnq_11" name="TEXTCOCHE_<?php echo $nbruseraffich;?>" rows="4" cols="400" disabled><?php echo $textecoche1; ?> </TEXTAREA>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 12); ?>" colspan="1" class="liste1" align="center">
							<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="ADHCOCHE2_<?php echo $nbruseraffich;?>" style="width: 60px;">
								<OPTION  value="OUI">OUI</option>
								<OPTION selected="selected" value="NON">NON</option>
							</SELECT>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 13); ?>" colspan="7" class="liste1">
						<TEXTAREA style="width: 600px;" id="parambnq_11" name="TEXTCOCHE2_<?php echo $nbruseraffich;?>" rows="4" cols="400" disabled><?php echo $textecoche2; ?> </TEXTAREA>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 14); ?>" colspan="1" class="liste1" align="center">
							<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="ADHRGPD<?php echo $nbruseraffich;?>" style="width: 60px;">
								<OPTION  value="OUI">OUI</option>
								<OPTION selected="selected" value="NON">NON</option>
							</SELECT>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 15); ?>" colspan="7" class="liste1">
						<TEXTAREA style="width: 600px;" id="parambnq_11" name="TEXTRGPD_<?php echo $nbruseraffich;?>" rows="6" cols="700" disabled><?php echo $textergpd; ?> </TEXTAREA>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 16); ?>" colspan="3" class="liste1">
						<label>Cotisation : </label>
						<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="ADHCOTIS_<?php echo $nbruseraffich; ?>" class="liste1" value='<?php echo $cotisation; ?>' disabled>
						<input type="hidden" name="ARTADHCODE_<?php echo $nbruseraffich; ?>" value="<?php echo $articleadhcode;?>"/>
						<input type="hidden" name="ARTADHLIB_<?php echo $nbruseraffich; ?>" value="<?php echo $articleadhlib;?>"/>
						<input type="hidden" name="ARTADHPRIX_<?php echo $nbruseraffich; ?>" value="<?php echo $articleadhtarif;?>"/>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 25); ?>" colspan="5" class="liste1" align="center">
							<label>Paiement immédiat de l'adhésion : </label>
							<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="ADHPAYE_<?php echo $nbruseraffich;?>" style="width: 60px;">
								<OPTION  value="OUI">OUI</option>
								<OPTION selected="selected" value="NON">NON</option>
							</SELECT>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 17); ?>" colspan="1" class="liste1">Validation de l'adhésion : </td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 18); ?>" colspan="1" class="liste1">
							<select id="ADHVALID_<?php echo $nbruseraffich;?>" name="ADHVALID_<?php echo $nbruseraffich;?>" style="width: 60px;" required>
								<OPTION  value="OUI">OUI</option>
								<OPTION selected disabled value="">NON</option>
							</SELECT>
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 19); ?>" colspan="6" class="liste1">
						<label>Date validation de l'adhésion : </label>
						<input name="DATEVALIDADH<?php echo $nbruseraffich; ?>" id="datepicker2_<?php echo $nbruseraffich; ?>" type="text" size="28" value="<?php echo $datedebutadh;?>" size="12" required/>
					</td>
					<?php
					}
					?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 20); ?>" colspan="8" class="liste1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<?php
					if (($data['ADHESIONOK'] == 'NON') || ($data['TE_STATUT'] != 'VALIDE'))
					{
					?>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 21); ?>" colspan="3" align="center">
						<input name="boutonvalid" class="bouton1" value="Valider" type="submit">
					</td>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 22); ?>" colspan="5" align="center">
						<input class="bouton1" type="button" value="Imprimer adhesion" style="width: 160px;" onclick="window.open('pdf/ad1_printadhesionstd.php?adherant=<?php echo $data['UT_LOGIN'] .'&etablissement=' .$_SESSION['ETABADMIN']; ?>', 'exemple', 'height=1200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/>
					</td>
					<?php
					}
					else
					{
					?>
					<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 23); ?>" colspan="8" align="center">
						<input class="bouton1" type="button" value="Imprimer adhesion" style="width: 160px;" onclick="window.open('pdf/ad1_printadhesionstd.php?adherant=<?php echo $data['UT_LOGIN'] .'&etablissement=' .$_SESSION['ETABADMIN']; ?>', 'exemple', 'height=1200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/>
					</td>
					<?php
					}
					?>
				</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparamadh_<?php echo (($nbruseraffich * 100) + 24); ?>" colspan="8" class="liste1">
					<input type="hidden" id="paramuser_012_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERLOGIN<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_LOGIN'];?>'>
					<input type="hidden" id="paramuser_013_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERID<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_ID2'];?>'>
					<input type="hidden" id="paramuser_017_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERIDTYPE<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_TYPE'];?>'>
				</td>
			</tr>


		<?php
		}

		?>
		<tr><td><input type="hidden" name="nbruseraffich" id="nbruseraffich" value="<?php echo $nbruseraffich; ?>"></input></td></tr>
		<tr>
			<?php

			if($pageprec >= 0)
			{
				?>
				<td colspan="3" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/prec-black.png" width="25" height="25" onClick="window.location.href='create_adhesion.php?PAGE=<?php echo $pageprec; ?>'"/></td>
				<?php
			}
			else
			{
				?>
				<td colspan="3 "style="width: 30px;"></td>
				<?php
			}
			 ?>

		<td colspan="5" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/suiv-black.png" width="25" height="25" onClick="window.location.href='create_adhesion.php?PAGE=<?php echo $pagesuiv; ?>'"/></td>
		</tr>
		</table>

	</form>
	</center>
	<?php
	}

}

function newvalideadhesion()
{

	for ($i = 1; $i <= $_POST['nbruseraffich']; $i++)
	{
		if ((isset($_POST['ADHVALID_'.$i])) && $_POST['ADHVALID_'.$i] == 'OUI')
		{
			$sql = "SELECT * FROM ETABLISSEMENT where ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				$tmp = explode("/", $_POST['DATEVALIDADH'.$i]);
				$datevalidadh = $tmp[2]."-".$tmp[1]."-".$tmp[0];
				$tmp = explode("/", $_POST['DATEDEBUTADH'.$i]);
				$datedebutadh = $tmp[2]."-".$tmp[1]."-".$tmp[0];
				$tmp = explode("/", $_POST['DATEFINADH'.$i]);
				$datefinadh = $tmp[2]."-".$tmp[1]."-".$tmp[0];
				$id2 = rand(1,99999999);

				$sql = 'UPDATE TIERSETAB SET TE_DATEVALIDATION = "'.$datevalidadh.'", TE_DEBUTADHESION = "'.$datedebutadh.'", TE_DATEFINADHESION = "'.$datefinadh.'",
						TE_COCHE1 = "'.$_POST['ADHCOCHE1_'.$i].'", TE_TEXTCOCHE1 = "'.$data['ET_ADHESIONCOCHE1'].'", TE_COCHE2 = "'.$_POST['ADHCOCHE2_'.$i].'", TE_TEXTCOCHE2 = "'.$data['ET_ADHESIONCOCHE2'].'",
						TE_TEXTRGPD = "'.$data['ET_ADHESIONRGPD1'].'", TE_ARTADHESIONCODE = "'.$_POST['ARTADHCODE_'.$i].'", TE_ARTADHESIONLIB = "'.$_POST['ARTADHLIB_'.$i].'",
						TE_ARTADHESIONPRIX = "'.$_POST['ARTADHPRIX_'.$i].'", TE_ADHESIONPAYE = "'.$_POST['ADHPAYE_'.$i].'", TE_STATUT = "VALIDE",
						TE_VALIDEUR = "'.encrypt($_SESSION['NOM'] .' ' .$_SESSION['PRENOM'],$id2).'", TE_ID = "'.$id2.'"
						WHERE TE_LOGIN = "'.$_POST['USERLOGIN'.$i].'" AND TE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'"';
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);

				$sql = 'SELECT * FROM UTILISATEUR WHERE UT_LOGIN = "'.$_POST['USERLOGIN'.$i].'";';
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data2)
				{
					emailAdhesionValid(decrypt($data2['UT_EMAIL'],$data2['UT_ID2']), '1234', $_POST['USERLOGIN'.$i], $data['ET_LIBELLE'], $data['ET_EMAIL']);
				}

				if ((isset($_POST['ADHPAYE_'.$i])) && $_POST['ADHPAYE_'.$i] == 'OUI')
				{
					$sql = 'SELECT *, DATE_FORMAT(now(),"AD%y%m_") AS DATEREF FROM UTILISATEUR WHERE UT_LOGIN = "'.$_POST['USERLOGIN'.$i].'";';
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						$cpt = 1;
						$sql = "SELECT CT_COMPTEUR AS CPT FROM COMPTEUR WHERE CT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND CT_CODEDOC = 'ADH';";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data2)
						{

								$cpt = $data2['CPT'];
								$cpt++;
								$sql = "UPDATE COMPTEUR SET CT_COMPTEUR = ".$cpt." WHERE CT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND CT_CODEDOC = 'ADH'";
								$cnx_bdd = ConnexionBDD();
								$result_req = $cnx_bdd->exec($sql);

						}
						if ($cpt == 1)
						{
							$sql = "INSERT INTO COMPTEUR (CT_ETABLISSEMENT, CT_DOCUMENT, CT_COMPTEUR, CT_CODEDOC) VALUES ('".$_SESSION['ETABADMIN']."', 'COTISATION',1, 'ADH');";
							$cnx_bdd = ConnexionBDD();
							$result_req = $cnx_bdd->exec($sql);
						}

						$reffacture = $data1['DATEREF'].str_pad($cpt,6,"0",STR_PAD_LEFT);
						$totalht = 0;
						$totalqte = 0;


						$sql = 'DELETE FROM PIECE WHERE GP_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'" AND GP_SESSIONTOKEN = "'.$_SESSION['token'].'" AND GP_TYPE = "COTISATION";';
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->exec($sql);

						$sql = 'INSERT INTO PIECE(GP_TYPE, GP_USER, GP_USERNOM, GP_DATEPIECE, GP_ETABLISSEMENT, GP_USERPRENOM, GP_USERADRESSE1, GP_USERADRESSE2, GP_USERADRESSE3, GP_USERVILLE, GP_USERCODEPOSTAL,
								GP_USERINITIALE, GP_USERID, GP_ETLIBELLE, GP_ETADRESSE1, GP_ETADRESSE2, GP_ETADRESSE3, GP_ETCODEPOSTAL, GP_ETVILLE, GP_USEREMAIL, GP_TOTALHT, GP_TOTALQTEFACT, GP_REFFACTURE, GP_TRANSMISE,
								GP_DATETRANSMISSION, GP_PAYEMENT, GP_DATEPAYEMENT, GP_RELANCE, GP_DATERELANCE, GP_NOMBRERELANCE, GP_REFPAYMENT, GP_MODEPAIE, GP_SESSIONTOKEN, GP_DATEECHEANCE, GP_CREATEUR)
								VALUES
								("COTISATION", "'.$_POST['USERLOGIN'.$i].'", "'.$data1['UT_NOM'].'", now(), "'.$_SESSION['ETABADMIN'].'", "'.$data1['UT_PRENOM'].'",
								"'.$data1['UT_ADRESSE1'].'", "'.$data1['UT_ADRESSE2'].'", "", "'.$data1['UT_VILLE'].'",
								"'.$data1['UT_CODEPOSTAL'].'", "", "'.$data1['UT_ID2'].'", "'.$data['ET_LIBELLE'].'", "'.$data['ET_ADRESSE1'].'", "'.$data['ET_ADRESSE2'].'", "",
								 "'.$data['ET_CODEPOSTAL'].'", "'.$data['ET_VILLE'].'", "'.$data1['UT_EMAIL'].'", '.$_POST['ARTADHPRIX_'.$i].',1, "'.$reffacture.'", "NON", "1900-01-01", "NON", "1900-01-01", "NON", "1900-01-01", 0, "",
								 "", "'.$_SESSION['token'].'",ADDDATE(now(), INTERVAL '.$data['ET_ECHEANCEFACTURE'].' DAY), "'.encrypt($_SESSION['NOM'] .' ' .$_SESSION['PRENOM'],$data1['UT_ID2']).'");';
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->exec($sql);

						$gp_numero = 0;
						$sql = 'SELECT GP_NUMERO FROM PIECE WHERE GP_SESSIONTOKEN = "'.$_SESSION['token'].'" AND GP_REFFACTURE = "'.$reffacture.'" AND GP_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'" ;';
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data2)
						{
							$gp_numero = $data2['GP_NUMERO'];
						}

						$sql = 'INSERT INTO ligne(GL_NUMLIGNE, GL_LIBELLE, GL_NUMERO, GL_ETABLISSEMENT, GL_USER, GL_DATEPIECE, GL_ARTICLENO, GL_ARTLIBELLE, GL_ARTFAMILLE1, GL_ARTFAMILLE2, GL_ARTTARIF, GL_QTEFACT,
								GL_TOTALHT, GL_REFFACTURE, GL_COMMENTAIRE, GL_PRIXPARPERSONNE, GL_PRIXPARPLACE)
								VALUES
								(1,"COTISATION ADHESION '.$data['ET_LIBELLE'].' du '.$datevalidadh.'", '.$gp_numero.', "'.$_SESSION['ETABADMIN'].'", "", now(), "'.$_POST['ARTADHCODE_'.$i].'", "'.$_POST['ARTADHLIB_'.$i].'", "", "",'.$_POST['ARTADHPRIX_'.$i].', 1, '.$_POST['ARTADHPRIX_'.$i].',
								"'.$reffacture.'","",0,0);';
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->exec($sql);

						$sql = 'SELECT
								CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = "OUI") THEN PR_VALEUR ELSE 0 END AS MONTANTPROMO,
								CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = "OUI") THEN PR_LIBELLE ELSE "" END AS LIBELLEPROMO,
								CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = "OUI") THEN PR_TYPEREMISE ELSE "" END AS TYPEPROMO,
								CASE WHEN (PR_DECLENCHEUR = AR_CODEARTICLE AND PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_ACTIVE = "OUI") THEN PR_REMISEDIRECT ELSE "" END AS ACTIONPROMO

								FROM TIERSETAB
								LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
								LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTADHESION AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
								LEFT JOIN PROMOTION ON PR_ETABLISSEMENT = TE_ETABLISSEMENT AND PR_DECLENCHEUR = AR_CODEARTICLE
								LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
								WHERE TE_LOGIN = "'.$_POST['USERLOGIN'.$i].'" AND TE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'"';

						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data2)
						{
							if ($data2['MONTANTPROMO']!= 0)
							{
								$sql = "INSERT INTO `VOUCHER`(`BA_LIBELLE`, BA_LOGIN, BA_ETABLISSEMENT,`BA_VALEUR`, `BA_DATECREATION`, `BA_DATEREPRISE`, `BA_VALIDE`, `BA_REFREPRISE`) VALUES
										('".$data2['LIBELLEPROMO']."','".$_POST['USERLOGIN'.$i]."','".$_SESSION['ETABADMIN']."','".$data2['MONTANTPROMO']."',now(),'1900-01-01','OUI','');";

								$cnx_bdd = ConnexionBDD();
								$result_req = $cnx_bdd->exec($sql);
							}
						}

					}
				}

			}


		}
	}
}

function valide_creditcompte()
{
	$QteFact = 0;
	$reffacture = '';
	$gp_numero = 0;

	$sql = "SELECT * FROM TIERSETAB
				LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
				LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
				LEFT JOIN ARTICLE ON AR_CODEARTICLE = ET_ARTMONNAIE AND AR_ETABLISSEMENT = TE_ETABLISSEMENT
				WHERE TE_LOGIN = '".$_GET['user']."' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ;";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$sql = "INSERT INTO PIECE (GP_TYPE, GP_USER, GP_USERNOM, GP_USERPRENOM, GP_USERADRESSE1, GP_USERADRESSE2, GP_USERCODEPOSTAL, GP_USERVILLE, GP_USEREMAIL, GP_USERID, GP_ETABLISSEMENT, GP_ETLIBELLE,
				GP_ETADRESSE1, GP_ETADRESSE2, GP_ETADRESSE3, GP_ETCODEPOSTAL, GP_ETVILLE, GP_TOTALHT, GP_TOTALQTEFACT, GP_SESSIONTOKEN, GP_DATEPIECE, GP_DATETRANSMISSION, GP_DATEPAYEMENT, GP_DATERELANCE, GP_DATEECHEANCE)
				VALUES ('ADHESION', '".$data['TE_LOGIN']."','".$data['UT_NOM']."','".$data['UT_PRENOM']."','".$data['UT_ADRESSE1']."','".$data['UT_ADRESSE2']."','".$data['UT_CODEPOSTAL']."','".$data['UT_VILLE']."',
				'".$data['UT_EMAIL']."','".$data['UT_ID2']."','".$data['ET_ETABLISSEMENT']."','".$data['ET_LIBELLE']."','".$data['ET_ADRESSE1']."','".$data['ET_ADRESSE2']."','".$data['ET_ADRESSE3']."',
				'".$data['ET_CODEPOSTAL']."','".$data['ET_VILLE']."',".$_POST['MNTCREDITCPT'].",1,'".$_SESSION['token']."', now(),'1900-01-01','1900-01-01','1900-01-01', ADDDATE(now(), INTERVAL ".$data['ET_ECHEANCEFACTURE']." DAY));";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);


		$sql = "select GP_NUMERO, CONCAT(DATE_FORMAT(GP_DATEPIECE,'FACT%y%m_'),CONCAT(SUBSTR('00000',1,5-LENGTH(GP_NUMERO)),GP_NUMERO)) AS REFFACTURE FROM PIECE WHERE GP_USER = '".$data['TE_LOGIN']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
		$cnx_bdd = ConnexionBDD();
		$result_req1 = $cnx_bdd->query($sql);
		$tab_r1 = $result_req1->fetchAll();
		foreach ($tab_r1 as $data1)
		{
			$reffacture = $data1['REFFACTURE'];
			$gp_numero = $data1['GP_NUMERO'];
			$sql = "UPDATE PIECE SET GP_REFFACTURE = '".$data1['REFFACTURE']."'  WHERE GP_USER = '".$data['TE_LOGIN']."' AND GP_SESSIONTOKEN = '".$_SESSION['token']."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

		}

		$numligne = 1;
		$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF,
					GL_PRIXPARPERSONNE, GL_PRIXPARPLACE, GL_LIBELLE) VALUES
				(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data['ET_ETABLISSEMENT']."','".$data['TE_LOGIN']."','".$data['AR_CODEARTICLE']."','".$data['AR_DESIGNATION']."',
				1,".$_POST['MNTCREDITCPT'].",'',".$_POST['MNTCREDITCPT'].",'0.00','0.00',
				date_format(now(),'Ajout au compte crédit du : %d/%m/%Y'));";

		//$cnx_bdd = ConnexionBDD();
		//$result_req = $cnx_bdd->exec($sql);



//
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);
		$numligne++;
				/////


		if ($data['MONTANTPROMO']!= 0)
		{
			$sql = "INSERT INTO `VOUCHER`(`BA_LIBELLE`, BA_LOGIN, BA_ETABLISSEMENT,`BA_VALEUR`, `BA_DATECREATION`, `BA_DATEREPRISE`, `BA_VALIDE`, `BA_REFREPRISE`) VALUES
					('".$data['LIBELLEPROMO']."','".$data['TE_LOGIN']."','".$data['ET_ETABLISSEMENT']."','".$data['MONTANTPROMO']."',now(),'1900-01-01','OUI','');";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
		}
		$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = ".($data['TE_CREDITVAL'] + $_POST['MNTCREDITCPT'])."
				WHERE TE_LOGIN = '" .$_POST['login'] . "' AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ;";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->exec($sql);


		echo 'Crédit ajouté';

			?>
			<a style="margin-left: 30px" href="pdf/print_facture.php?reffacture=<?php echo $reffacture; ?> " onclick="window.open(this.href, 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"><B>Impression de la facture</b></a>
			<?php

	}


}

function cloture_resa()
{

	$i = 0;
	if (isset($_GET['datedebut']))
	{
		$datedebut = $_GET['datedebut'];
		$datefin = $_GET['datefin'];
	}
	else
	{
		$datedebut = date("d") .'/'. date("m") . '/'.date("Y");
		$datefin = date("d") .'/'. date("m") . '/'.date("Y");
		$sql = "select date_format(MIN(RE_DATE),'%d/%m/%Y') as DATEDEBUT, MAX(RE_DATE),LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),
				date_format(LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),'%d/%m/%Y') as DATEFIN
				FROM RESERVATION
				where RE_VALIDEE = 'OUI' AND RE_FACTURE = 'NON' AND (RE_PANIER IS NULL OR RE_PANIER <> 'OUI') AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$datedebut = $r['DATEDEBUT'];
			$datefin = $r['DATEFIN'];
		}
	}
	if (empty($datedebut))
	{
		$datedebut = date("d") .'/'. date("m") . '/'.date("Y");
		$datefin = date("d") .'/'. date("m") . '/'.date("Y");
	}
	?>
	<form action="" method="post">
	<center><div id="support1">
	<table  style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="5" style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 40%;" align="center">Cloture </td>


		</tr>
		<tr>
			<td colspan="5" style="background-color: #edf1f6; ">Critère :</td>
		</tr>
		<tr>
			<td colspan="5" style="background-color: #edf1f6; ">
				<label>Date début: </label>
				<input name="DateDebut" style="padding-left: 50px;" id="datepicker" type="text" size="28" value="<?php echo $datedebut;?>" size="12" required onchange="changedate()"/>
				<label style="padding-left: 50px;">Date fin: </label>
				<input style="padding-left: 50px;" name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $datefin;?>" size="12" required onchange="changedate()"/>
			</td>


		</tr>
		<tr>
			<td colspan="5" style="background-color: #edf1f6; "><label style="padding-left: 50px;"></label> </td>


		</tr>
		<tr>
			<td id="lignetr1" style="text-align: center;" align="center">
				<input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="ckeckresa<?php echo $i;?>" name="ckeckresa<?php echo $i;?>" value="TOUS" onclick="cocher_tout(this)"></input>
			</td>
			<td colspan="4" id="lignetr1" align="left">
				<label>Tous</label>
			</td>
		<?php
		$tmp = explode("/", $datedebut);
		$datedebutrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		$tmp = explode("/", $datefin);
		$datefinrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2, COUNT(RE_NUMRESA) AS NBRESA, SUM(RE_PRIXTOTALRESA) AS MNTTLRESA
				from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
				WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' AND RE_FACTURE = 'NON' AND UT_NOM IS NOT NULL
				AND UT_PRENOM IS NOT NULL
				AND (RE_PANIER IS NULL OR RE_PANIER <> 'OUI')
				AND RE_DATE >= '".$datedebutrech."' AND RE_DATE <= '".$datefinrech."'
				GROUP BY RE_USER, UT_NOM, UT_PRENOM, UT_ID2;";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{

			$i++;
			?>
			<tr>
				<td style="font-family: Calibri; color: rgb(1, 1, 0); font-weight: bold; ; text-align: center; vertical-align: middle;width: 10%;" align="center">
					<input style="width:20px;box-shadow: 0px 0px 0px" type="checkbox" id="ckeckresa<?php echo $i;?>" name="ckeckresa<?php echo $i;?>" value="<?php echo $r['RE_USER'];?>"></input>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold;  text-align: left; vertical-align: middle;width: 20%;" align="left">
					<label><?php echo $r['RE_USER']; ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold;  text-align: left; vertical-align: middle;width: 30%;" align="left">
					<label><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 20%;" align="left">
					<label><?php echo "Nombre de réservation : " .$r['NBRESA']; ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 20%;" align="left">
					<label><?php echo "Montant total : " .number_format($r['MNTTLRESA'],2,',','') ." €"; ?></label>
				</td>

				

			</tr>
			<?php


		}
		?>
		<tr>
			<td colspan="5" >
				<label style="padding-left: 50px;"></label>
				<input type="hidden" name="NBRCLOTURE" value="<?php echo $i;?>"</input>
			</td>
		</tr>
		<tr>
			<td colspan="5" align="center">
				<input name="boutonvalid" class="bouton1" value="Valider" type="submit">
			</td>
		</tr>


	</form>
	</div>

		<?php
	// fin connexion
}

function valid_cloture()
{

	for ($i = 1; $i <= $_POST['NBRCLOTURE']; $i++)
	{
		if (isset($_POST['ckeckresa'.$i]))
		{
			$_SESSION['tokenresa'] = random(25);

			$tmp = explode("/", $_POST['DateDebut']);
			$datedebutrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
			$tmp = explode("/", $_POST['DateFin']);
			$datefinrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
			$QteFact = 0;
			$reffacture = '';
			$gp_numero = 0;

			$sql = "update RESERVATION set RE_SESSIONTOKEN = ''
					where RE_USER = '" .$_POST['ckeckresa'.$i] . "' AND RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
					AND RE_SESSIONTOKEN = '".$_SESSION['tokenresa']."' ;";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			$sql = "update RESERVATION set RE_SESSIONTOKEN = '".$_SESSION['tokenresa']."'
					where RE_FACTURE = 'NON' AND RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
					AND RE_USER = '".$_POST['ckeckresa'.$i]."' AND RE_DATE >= '".$datedebutrech."' AND RE_DATE <= '".$datefinrech."';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);

			$cpt = 1;
			$sql = "SELECT CT_COMPTEUR AS CPT FROM COMPTEUR WHERE CT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND CT_CODEDOC = 'CLO';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data2)
			{

					$cpt = $data2['CPT'];
					$cpt++;
					$sql = "UPDATE COMPTEUR SET CT_COMPTEUR = ".$cpt." WHERE CT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND CT_CODEDOC = 'CLO'";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);

			}
			if ($cpt == 1)
			{
				$sql = "INSERT INTO COMPTEUR (CT_ETABLISSEMENT, CT_DOCUMENT, CT_COMPTEUR, CT_CODEDOC) VALUES ('".$_SESSION['ETABADMIN']."', 'COTISATION',1, 'CLO');";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}

			$reffacture = 'CLO'.date("d") .date("m") . date("Y").$_SESSION['ETABADMIN'].str_pad($cpt,6,"0",STR_PAD_LEFT);

			$nbrresa = 0;
			$mnttlresa = 0;
			$sql = "select RE_USER, COUNT(RE_NUMRESA) AS NBRESA, SUM(RE_PRIXTOTALRESA) AS MNTTLRESA
					from RESERVATION
					WHERE RE_USER = '".$_POST['ckeckresa'.$i]."' AND RE_SESSIONTOKEN = '".$_SESSION['tokenresa']."' ;";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data2)
			{

				$nbrresa = $data2['NBRESA'];
				$mnttlresa = $data2['MNTTLRESA'];
			}

			$sql = 'SELECT *
			FROM RESERVATION
			LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
			WHERE RE_USER = "' .$_POST['ckeckresa'.$i] . '" AND RE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'"
			AND RE_SESSIONTOKEN = "'.$_SESSION['tokenresa'].'"
			LIMIT 1	;';

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
				$sql = 'INSERT INTO PIECE(GP_TYPE, GP_USER, GP_USERNOM, GP_DATEPIECE, GP_ETABLISSEMENT, GP_USERPRENOM, GP_USERADRESSE1, GP_USERADRESSE2, GP_USERADRESSE3, GP_USERVILLE, GP_USERCODEPOSTAL,
						GP_USERINITIALE, GP_USERID, GP_ETLIBELLE, GP_ETADRESSE1, GP_ETADRESSE2, GP_ETADRESSE3, GP_ETCODEPOSTAL, GP_ETVILLE, GP_USEREMAIL, GP_TOTALHT, GP_TOTALQTEFACT, GP_REFFACTURE, GP_TRANSMISE,
						GP_DATETRANSMISSION, GP_PAYEMENT, GP_DATEPAYEMENT, GP_RELANCE, GP_DATERELANCE, GP_NOMBRERELANCE, GP_REFPAYMENT, GP_MODEPAIE, GP_SESSIONTOKEN, GP_DATEECHEANCE, GP_CREATEUR)
						VALUES
						("CLOTURE", "'.$_POST['ckeckresa'.$i].'", "'.$data1['UT_NOM'].'", now(), "'.$_SESSION['ETABADMIN'].'", "'.$data1['UT_PRENOM'].'",
						"'.$data1['UT_ADRESSE1'].'", "'.$data1['UT_ADRESSE2'].'", "", "'.$data1['UT_VILLE'].'",
						"'.$data1['UT_CODEPOSTAL'].'", "", "'.$data1['UT_ID2'].'", "'.$data1['ET_LIBELLE'].'", "'.$data1['ET_ADRESSE1'].'", "'.$data1['ET_ADRESSE2'].'", "",
						 "'.$data1['ET_CODEPOSTAL'].'", "'.$data1['ET_VILLE'].'", "'.$data1['UT_EMAIL'].'", '.$mnttlresa.','.$nbrresa.', "'.$reffacture.'", "NON", "1900-01-01", "NON", "1900-01-01", "NON", "1900-01-01", 0, "",
						 "", "'.$_SESSION['tokenresa'].'",ADDDATE(now(), INTERVAL '.$data1['ET_ECHEANCEFACTURE'].' DAY), "'.encrypt($_SESSION['NOM'] .' ' .$_SESSION['PRENOM'],$data1['UT_ID2']).'");';

				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}

			$gp_numero = 0;
			$sql = 'SELECT GP_NUMERO FROM PIECE WHERE GP_REFFACTURE = "'.$reffacture.'" AND GP_SESSIONTOKEN = "'.$_SESSION['tokenresa'].'" AND GP_USER = "'.$_POST['ckeckresa'.$i].'";';
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data2)
			{
				$gp_numero = $data2['GP_NUMERO'];
			}

			$numligne = 1;

			$sql = 'SELECT *, (RE_QTEART1 * RE_PRIXART1) AS PRIXTLART1, (RE_QTEART2 * RE_PRIXART2) AS PRIXTLART2, date_format(RE_DATE,"%d %m %Y") AS DATERESA
			FROM RESERVATION
			LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
			WHERE RE_USER = "' .$_POST['ckeckresa'.$i] . '" AND RE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'"
			AND RE_SESSIONTOKEN = "'.$_SESSION['tokenresa'].'"
			ORDER BY RE_NUMRESA;';


			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{

				$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF,
							GL_PRIXPARPERSONNE, GL_PRIXPARPLACE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data1['ET_ETABLISSEMENT']."','".$_POST['ckeckresa'.$i]."',
						'".$data1['RE_CODEART1']."','".$data1['RE_LIBART1']."',
						".$data1['RE_QTEART1'].",".$data1['PRIXTLART1'].",'',".$data1['RE_PRIXART1'].",'0.00','0.00',
						'Réservation n° ".$data1['RE_NUMRESA']." du : ".$data1['DATERESA']."');";

				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				$numligne++;
				if (!empty($data1['RE_CODEART2']))
				{
					$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF,
							GL_PRIXPARPERSONNE, GL_PRIXPARPLACE, GL_LIBELLE) VALUES
						(".$numligne.",'".$reffacture."',". $gp_numero.", now(), '".$data1['ET_ETABLISSEMENT']."','".$_POST['ckeckresa'.$i]."',
						'".$data1['RE_CODEART2']."','".$data1['RE_LIBART2']."',
						".$data1['RE_QTEART2'].",".$data1['PRIXTLART2'].",'',".$data1['RE_PRIXART2'].",'0.00','0.00',
						'Réservation n° ".$data1['RE_NUMRESA']." du : ".$data1['DATERESA']."');";

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
					$numligne++;
				}
			}

			$sql = 'UPDATE RESERVATION SET RE_FACTURE = "OUI", RE_REFFACTURE = "'.$reffacture.'" WHERE RE_USER = "' .$_POST['ckeckresa'.$i] . '" AND RE_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'"
					AND RE_SESSIONTOKEN = "'.$_SESSION['tokenresa'].'";';

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->exec($sql);
		}

	}
}

function liste_resaV2()
{

	$i = 0;
	if (isset($_GET['datedebut']))
	{
		$datedebut = $_GET['datedebut'];
		$datefin = $_GET['datefin'];
	}
	else
	{
		$datedebut = date("d") .'/'. date("m") . '/'.date("Y");
		$datefin = date("d") .'/'. date("m") . '/'.date("Y");
		$sql = "select date_format(MIN(RE_DATE),'%d/%m/%Y') as DATEDEBUT, MAX(RE_DATE),LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),
				date_format(LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),'%d/%m/%Y') as DATEFIN
				FROM RESERVATION
				where RE_VALIDEE = 'OUI' AND (RE_PANIER IS NULL OR RE_PANIER <> 'OUI') AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$datedebut = $r['DATEDEBUT'];
			$datefin = date("d") .'/'. date("m") . '/'.date("Y");
		}
	}

	$userselect = '';
	if(isset($_GET['user']))
	{
		if($_GET['user']!='tous')
		{
			$userselect = $_GET['user'];
		}
	}

	$clotureselect = 'tous';
	if(isset($_GET['cloture']))
	{
		if($_GET['cloture']!='tous')
		{
			$clotureselect = $_GET['cloture'];
		}
		else
		{
			$clotureselect = 'tous';
		}
	}

	?>
	<center><div id="support1">
	<table  style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="7" style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 40%;" align="center">Liste des réservation </td>


		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; ">Critère :</td>
		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; ">
				<label>Date début: </label>
				<input name="DateDebut" style="padding-left: 50px;" id="datepicker" type="text" size="28" value="<?php echo $datedebut;?>" size="12" required onchange="changedate()"/>
				<label style="padding-left: 50px;">Date fin: </label>
				<input style="padding-left: 50px;" name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $datefin;?>" size="12" required onchange="changedate()"/>
			</td>
		</tr>

		<tr>
			<td colspan="2" style="background-color: #edf1f6; ">
				<label>Utilisateur : </label>
				<select align="left" name="listuser" id="listuser" onchange="changedate()"/>
					<?php
					$sql = 'select RE_USER, UT_NOM, UT_PRENOM, UT_ID2, case when RE_USER = "'.$userselect.'" then "SELECTED" else "" end as SELECTED
							from RESERVATION
							LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER
							WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL
							group by RE_USER
							union ALL
							select "tous" as RE_USER, "" as UT_NOM, "" as UT_PRENOM, "" as UT_ID2 ,  case when "'.$userselect.'" = " " then "SELECTED" else " " end as SELECTED
							ORDER BY SELECTED DESC;';

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $r)
					{
						if ($r['RE_USER'] == 'tous')
						{
							?>
							<option value="<?php echo $r['RE_USER']; ?>">Tous</option>
							<?php
						}
						else
						{
							?>
							<option value="<?php echo $r['RE_USER']; ?>"><?php echo decrypt2($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
							<?php
						}
					}
					?>
				</SELECT>
			</td>
			<td colspan="5" style="background-color: #edf1f6; ">
				<label>Reservation clôturée: </label>
				<select align="left" name="selectcloture" id="selectcloture" onchange="changedate()"/>
					<?php
					$sql = 'select RE_FACTURE, case when RE_FACTURE = "'.$clotureselect.'" then "SELECTED" else "" end as SELECTED
							from RESERVATION
							group by RE_FACTURE
							union ALL
							select "tous" as RE_FACTURE, case when "'.$clotureselect.'" = "tous" then "SELECTED" else " " end as SELECTED
							ORDER BY SELECTED DESC;';

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $r)
					{
						?>
						<option value="<?php echo $r['RE_FACTURE']; ?>"><?php echo $r['RE_FACTURE']; ?></option>
						<?php
					}
					?>
				</SELECT>
			</td>
		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; "><label style="padding-left: 50px;"></label> </td>


		</tr>

		<?php
		$tmp = explode("/", $datedebut);
		$datedebutrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		$tmp = explode("/", $datefin);
		$datefinrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		if($userselect=='')
		{
			$sqluserselect = ' AND RE_USER <> "'.$userselect.'"';
		}
		else
		{
			$sqluserselect = ' AND RE_USER = "'.$userselect.'"';
		}
		if($clotureselect=='tous')
		{
			$sqlclotureselect = ' AND RE_FACTURE <> "'.$clotureselect.'"';
		}
		else
		{
			$sqlclotureselect = ' AND RE_FACTURE = "'.$clotureselect.'"';
		}
		$sql = "select RE_USER, UT1.UT_NOM, UT1.UT_PRENOM, UT1.UT_ID2, UT2.UT_NOM as NOMMODIF, UT2.UT_PRENOM AS PRENOMMODIF, UT2.UT_ID2 AS ID2MODIF,
				RE_NUMRESA, RE_PRIXTOTALRESA, RE_LIBELLEEMPLACEMENT, RE_FACTURE, RE_REFFACTURE, RE_LIBART1, RE_PRIXART1, RE_QTEART1, RE_LIBART2, RE_PRIXART2, RE_QTEART2, RE_USERMODIF,
				date_format(RE_DATERESA,'%d %m %Y') AS DATEACTION, date_format(RE_DATE,'%d %m %Y') AS DATERESA
				from RESERVATION
				LEFT JOIN UTILISATEUR UT1 ON UT1.UT_LOGIN = RE_USER
				LEFT JOIN UTILISATEUR UT2 ON UT2.UT_LOGIN = RE_USERMODIF
				WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' AND UT1.UT_NOM IS NOT NULL
				AND UT1.UT_PRENOM IS NOT NULL
				AND (RE_PANIER IS NULL OR RE_PANIER <> 'OUI')
				AND RE_DATE >= '".$datedebutrech."' AND RE_DATE <= '".$datefinrech."' ".$sqluserselect." ".$sqlclotureselect.";";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{

			$i++;
			?>
			<tr>

				<td style="font-family: Calibri; color: rgb(1, 1, 0); font-weight: bold; ; text-align: center; vertical-align: middle;width: 25%;" align="center">
					<label><?php echo $r['RE_USER'] . ' - ' . decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold;  text-align: left; vertical-align: middle;width: 20%;" align="left">
					<label><?php echo "Réservation n° ".$r['RE_NUMRESA']." du : ".$r['DATERESA']; ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold;  text-align: left; vertical-align: middle;width: 15%;" align="left">
					<label><?php echo $r['RE_LIBELLEEMPLACEMENT']; ?></label>
				</td>

				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 15%;" align="left">
					<label><?php echo "Montant total : " .number_format($r['RE_PRIXTOTALRESA'],2,',','') ." €"; ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 5%;" align="left">
					<label><?php echo $r['RE_FACTURE']; ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 10%;" align="left">
					<label><?php echo $r['RE_REFFACTURE']; ?></label>
				</td>

				<td style="width: 5%" align="center" ><img border="0" src="img/preview.png" width="25" height="25" onclick="affiche(<?php echo $i; ?>)"/></td>

			</tr>
			<tr>
				<td id="detail_1_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none"><label></label></td>
				<td id="detail_2_<?php echo$i;?>" colspan="2" style="background-color: #CFCFCF; display: none">
					<label>Réservation effectuée le : <?php echo $r['DATEACTION']; ?></label>
				</td>
				<td id="detail_3_<?php echo$i;?>" colspan="4" style="background-color: #CFCFCF; display: none">
					<label>Par : <?php echo decrypt($r['PRENOMMODIF'],$r['ID2MODIF']) ." " .decrypt($r['NOMMODIF'],$r['ID2MODIF']); ?></label>
				</td>
			</tr>
			<tr>
				<td id="detail_4_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none"><label></label></td>
				<td id="detail_5_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
					<label><?php echo $r['RE_LIBART1']; ?></label>
				</td>
				<td id="detail_6_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
					<label>Quantité : <?php echo $r['RE_QTEART1']; ?></label>
				</td>
				<td id="detail_7_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
					<label><?php echo "Prix unitaire : " .number_format($r['RE_PRIXART1'],2,',','') ." €"; ?></label>
				</td>
				<td id="detail_8_<?php echo$i;?>" colspan="3" style="background-color: #CFCFCF; display: none">
					<label><?php echo "Prix total : " .number_format(($r['RE_PRIXART1'] * $r['RE_QTEART1']),2,',','') ." €"; ?></label>
				</td>

			</tr>
			<?php
			if (!empty($r['RE_LIBART2']))
			{
				?>
				<tr>
					<td id="detail_9_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none"><label></label></td>
					<td id="detail_10_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
						<label><?php echo $r['RE_LIBART2']; ?></label>
					</td>
					<td id="detail_11_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
						<label>Quantité : <?php echo $r['RE_QTEART2']; ?></label>
					</td>
					<td id="detail_12_<?php echo$i;?>" colspan="1" style="background-color: #CFCFCF; display: none">
						<label><?php echo "Prix unitaire : " .number_format($r['RE_PRIXART2'],2,',','') ." €"; ?></label>
					</td>
					<td id="detail_13_<?php echo$i;?>" colspan="3" style="background-color: #CFCFCF; display: none">
						<label><?php echo "Prix total : " .number_format(($r['RE_PRIXART2'] * $r['RE_QTEART2']),2,',','') ." €"; ?></label>
					</td>

				</tr>
				<?php
			}


		}
		?>
		<tr>
			<td colspan="5" >
				<label style="padding-left: 50px;"></label>
				<div id="NBRCLOTURE"  style="display: none;"><?php echo $i;?></div>

			</td>
		</tr>
	</div>

		<?php
	// fin connexion
}

function liste_factureV2()
{

	$i = 0;
	if (isset($_GET['datedebut']))
	{
		$datedebut = $_GET['datedebut'];
		$datefin = $_GET['datefin'];
	}
	else
	{
		$datedebut = date("d") .'/'. date("m") . '/'.date("Y");
		$datefin = date("d") .'/'. date("m") . '/'.date("Y");
		$sql = "select date_format(MIN(GP_DATEPIECE),'%d/%m/%Y') as DATEDEBUT, date_format(now(),'%d/%m/%Y') as DATEFIN
				FROM PIECE
				where GP_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			$datedebut = $r['DATEDEBUT'];
			$datefin = $r['DATEFIN'];
		}
	}

	$userselect = '';
	if(isset($_GET['user']))
	{
		if($_GET['user']!='tous')
		{
			$userselect = $_GET['user'];
		}
	}


	?>
	<center><div id="support1">
	<table  style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td colspan="7" style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 40%;" align="center">Liste des clôtures </td>


		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; ">Critère :</td>
		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; ">
				<label>Date début: </label>
				<input name="DateDebut" style="padding-left: 50px;" id="datepicker" type="text" size="28" value="<?php echo $datedebut;?>" size="12" required onchange="changedate()"/>
				<label style="padding-left: 50px;">Date fin: </label>
				<input style="padding-left: 50px;" name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $datefin;?>" size="12" required onchange="changedate()"/>
			</td>
		</tr>

		<tr>
			<td colspan="7" style="background-color: #edf1f6; ">
				<label>Utilisateur : </label>
				<select align="left" name="listuser" id="listuser" onchange="changedate()"/>
					<?php
					$sql = 'select GP_USER, GP_USERNOM, GP_USERPRENOM, GP_USERID, case when GP_USER = "'.$userselect.'" then "SELECTED" else "" end as SELECTED
							from PIECE
							group by GP_USER
							union ALL
							select "tous" as GP_USER, "" as GP_USERNOM, "" as GP_USERPRENOM, "" as GP_USERID ,  case when "'.$userselect.'" = " " then "SELECTED" else " " end as SELECTED
							ORDER BY SELECTED DESC;';

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $r)
					{
						if ($r['GP_USER'] == 'tous')
						{
							?>
							<option value="<?php echo $r['GP_USER']; ?>">Tous</option>
							<?php
						}
						else
						{
							?>
							<option value="<?php echo $r['GP_USER']; ?>"><?php echo decrypt($r['GP_USERPRENOM'],$r['GP_USERID']) ." " .decrypt($r['GP_USERNOM'],$r['GP_USERID']); ?></option>
							<?php
						}
					}
					?>
				</SELECT>
			</td>
		</tr>
		<tr>
			<td colspan="7" style="background-color: #edf1f6; "><label style="padding-left: 50px;"></label> </td>


		</tr>

		<?php
		$tmp = explode("/", $datedebut);
		$datedebutrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		$tmp = explode("/", $datefin);
		$datefinrech = $tmp[2]."-".$tmp[1]."-".$tmp[0];
		if($userselect=='')
		{
			$sqluserselect = ' AND GP_USER <> "'.$userselect.'"';
		}
		else
		{
			$sqluserselect = ' AND GP_USER = "'.$userselect.'"';
		}

		$sql = "SELECT date_format(GP_DATEPIECE,'%d/%m %Y') as JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT, GP_USERID, GP_USERNOM, GP_USERPRENOM,
				GP_PAYEMENT
				FROM PIECE
				WHERE GP_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
				AND GP_DATEPIECE >= '".$datedebutrech."' AND GP_DATEPIECE <= '".$datefinrech."' ".$sqluserselect." ;";


		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{

			$i++;
			?>
			<tr>

				<td style="font-family: Calibri; color: rgb(1, 1, 0); font-weight: bold; ; text-align: left; vertical-align: middle;width: 25%;" align="left">
					<label><?php echo $r['GP_USER'] . ' - ' . decrypt($r['GP_USERPRENOM'],$r['GP_USERID']) ." " .decrypt($r['GP_USERNOM'],$r['GP_USERID']); ?></label>
				</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold;  text-align: left; vertical-align: middle;width: 20%;" align="left">
					<label><?php echo $r['GP_REFFACTURE']." du : ".$r['JOUR']; ?></label>
				</td>


				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; text-align: left; vertical-align: middle;width: 15%;" align="left">
					<label><?php echo "Montant total : " .number_format($r['GP_TOTALHT'],2,',','') ." €"; ?></label>
				</td>


				<td style="width: 5%" align="center" ><img border="0" src="img/preview.png" width="25" height="25" onclick="window.open('pdf/print_facture.php?reffacture=<?php echo $r['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>



			</tr>

			<?php



		}
		?>
		<tr>
			<td colspan="5" >
				<label style="padding-left: 50px;"></label>
				<div id="test" style="display: none;"><?php echo $i;?></div>
				<input type="hidden" name="NBRCLOTURE" value="<?php echo $i;?>"</input>
			</td>
		</tr>
	</div>

		<?php
	// fin connexion
}

function affiche_user2()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
		// déclaration des variables
		$nbruseraffich = 0;
		?>



		<body>


		<center><div id="support1">
		<form  enctype="multipart/form-data" action="" method="post">
		<table  style=""width: 80%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">

		<tbody>
			<tr>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 250px;">Login</td>
				<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 200px;">Nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 100px;">Pr&eacute;nom</td>
				<td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); width: 300px;">Email</td>
				<?php
				if (!isset($_SESSION['SUPERADMIN']))
				{
				echo '<td style="text-align: center; font-family: Calibri; width: 80px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date début adhésion</td>';
				echo '<td style="text-align: center; font-family: Calibri; width: 81px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Date fin adhésion</td>';
				}
				?>
				<td colspan="2" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);">
				<span style="font-family: Calibri;"><input id="boutonaffiche<?php echo $nbruseraffich; ?>" class="bouton3" type="button" value="Nouveau" style="width: 60px;" onclick="javascript:cacher(<?php echo $nbruseraffich * 100; ?>);"></span>
				</td>
			</tr>


		<?php


		// Début Affichage de la bande de création d'un nouvelle utilisateur.

		$j = 0;
		?>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" name="tdparamuser_000_<?php echo $nbruseraffich; ?>" id="tdparamuser_000_<?php echo $nbruseraffich; ?>" class="liste1" colspan="8">Création d'un utilisateur </td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td colspan="2" id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" class="liste1">
				<label>Nom :</label>
				<input type="text" id="paramuser_001_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERNOM<?php echo $nbruseraffich; ?>" >
			</td>
			<td colspan ="6" id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="2" class="liste1">
				<label>Prénom :</label>
				<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERPRENOM<?php echo $nbruseraffich; ?>"  >
			</td>

		</tr>
		<tr style="background-color: #edf1f6; padding-bottom=5px;">
			<td colspan="8" id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" class="liste1">
				<label>Email :</label>
				<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USEREMAIL<?php echo $nbruseraffich; ?>"  >
			</td>
		</tr>

		<tr style="background-color: #edf1f6; padding-bottom=5px;">
			<td colspan="8" id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" class="liste1">
				<label>Code Tiers Comptabilité :</label>
				<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USERTIERSCOMPTA<?php echo $nbruseraffich; ?>" value="" >
			</td>
		</tr>

		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
		</tr>
		<?php
		if ((isset($data['ET_GESTIONCREDITCLIENT'])) && ($data['ET_GESTIONCREDITCLIENT'] == 'OUI'))
		{
			?>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" />
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="liste1">
					<label> Crédit autorisé : </label>
					<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERCREDITOK<?php echo $nbruseraffich;?>" style="width: 60px;">
						<OPTION  value="OUI">OUI</option>
						<OPTION selected="selected" value="NON">NON</option>
					</SELECT>
				</td>
			</tr>
			<?php
		}
		 ?>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">
				<label>Adhésion : </label>
				<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="ADHESION<?php echo $nbruseraffich;?>" style="width: 100px;" onchange="changeadhesion(<?php echo $nbruseraffich ; ?>)">
					<option value="VALIDE">Oui</option>
					<option selected value="ATTENTE">Non</option>
				</SELECT>
			</td>
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="2" class="liste1" >
				<label>Début d'adhésion : </label>
				<input name="DATEDEBUTADH<?php echo $nbruseraffich; ?>" id="datepicker_<?php echo $nbruseraffich; ?>" type="text" value="" size="12" style="display: none;" />
			</td>
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" class="liste1">
				<label>Fin d'adhésion : </label>
				<input name="DATEFINADH<?php echo $nbruseraffich; ?>" id="datepicker1_<?php echo $nbruseraffich; ?>" type="text" value="" size="12" style="display: none;"/>
			</td>

		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
		</tr>

		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">Administrateur de l'établissement: </td>
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="7" class="liste1">
					<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="USERADMINSITE<?php echo $nbruseraffich;?>" style="width: 100px;">
						<option selected="selected" value="">Non</option>
						<option value="<?php echo $_SESSION['ETABADMIN'];?>">Oui</option>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="2" class="liste1"><b>Voulez vous créer cette utilisateur ?</b></td>
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="6" class="liste1">
					<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERCREATOK<?php echo $nbruseraffich;?>" style="width: 60px;">
						<OPTION  value="OUI">OUI</option>
						<OPTION selected="selected" value="NON">NON</option>
					</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" align="center" class="liste1">
				<input class="bouton1" value="Valider" type="submit">
			</td>
		<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1">
						<input type="hidden" id="paramuser_011_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMODIF<?php echo $nbruseraffich; ?>" style="width: 300px;" value='NON'>

				</td>
		</tr>
		<?php
		// Fin Affichage de la bande de création d'un nouvelle utilisateur.

		//$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$nbretabvalid = 0;
		if (isset($_GET['PAGE']))
		{
			if ($_GET['PAGE'] == 0)
			{
				$limitmin = 0;
				$pagesuiv = 1;
				$pageprec = 0;
			}
			else
			{
				$limitmin = $_GET['PAGE'] * 10;
				$pagesuiv = $_GET['PAGE'] + 1;
				$pageprec = $_GET['PAGE'] - 1;
			}
		}
		else
		{
			$limitmin = 0;
			$pagesuiv = 1;
			$pageprec = 0;
		}
		$sql = "SELECT COUNT(ET_ETABLISSEMENT) AS CPT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbretabvalid = $data['CPT'];
		}

		if ((isset($_SESSION['SUPERADMIN'])) && ($_SESSION['SUPERADMIN'] == 'OUI'))
		{
			$sql = "select  '' as DEBUTADHESION, '' as FINADHESION, UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, UT_ID2 from UTILISATEUR
					WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL ORDER BY UT_LOGIN LIMIT ".$limitmin.",10";
		}
		else
		{
			if ($nbretabvalid == 1)
			{
				$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
								datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
								UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, UT_TIERSGESTION,
								UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
								UT_ID2, TE_COMPTATIERS
								FROM UTILISATEUR
								LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN
								LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND (TE_ETABLISSEMENT IS NULL OR TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."')
								ORDER BY UT_LOGIN
								LIMIT ".$limitmin.",10";
			}
			else
			{
				$sql = "select date_format(TE_DEBUTADHESION,'%d/%m/%Y') AS DEBUTADHESION, date_format(TE_DATEFINADHESION,'%d/%m/%Y') AS FINADHESION,
								datediff(TE_DATEFINADHESION,now()), case when datediff(TE_DATEFINADHESION,now()) <= 0 then 'NON'  when datediff(TE_DATEFINADHESION,now()) IS NULL THEN 'NON' ELSE 'OUI' END as ADHESIONOK,
								UT_LOGIN, UT_NOM, UT_PRENOM, UT_EMAIL, UT_TIERSGESTION,
								UT_ADRESSE1, UT_ADRESSE2, UT_CODEPOSTAL, UT_VILLE, ET_GESTIONCREDITCLIENT, TE_CREDITOK, TE_CREDITVAL, UT_ADMINSITE, UT_CIVILITE, UT_TYPE, UT_CONTACTNOM, UT_CONTACTPRENOM,
								UT_ID2, TE_COMPTATIERS
								FROM UTILISATEUR
								LEFT JOIN TIERSETAB ON TE_LOGIN = UT_LOGIN
								LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL AND TE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
								ORDER BY UT_LOGIN
								LIMIT ".$limitmin.",10";
			}
		}

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbruseraffich++;
			$j = 0;
			?>
			<tr>
			<td style="text-align: left; width: 250px;"><b><?php echo $data['UT_LOGIN']; ?></b></td>
			<td style="width: 200px;"><?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?></td>
			<td style="width: 100px;"><?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?></td>
			<td style="width: 300px;"><?php echo decrypt($data['UT_EMAIL'],$data['UT_ID2']); ?></td>
			<?php
			if (isset($_SESSION['ETABADMIN']))
			{
				?>
				<td style="text-align: center; width: 80px;"><?php echo $data['DEBUTADHESION']; ?></td>
				<td style="text-align: center; width: 81px;"><?php echo $data['FINADHESION']; ?></td>
				<?php
			}
			?>


			<td style="width: 30px;"><img class="photo" id="boutonaffiche<?php echo $nbruseraffich; ?>" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:cacher(<?php echo $nbruseraffich * 100; ?>);"></td>
			<td style="width: 30px;"><img class="photo" id="website2" border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('manageuser.php?ACTION=suppression&numero=<?php echo $data['UT_LOGIN']; ?>', 'exemple', 'height=200, width=1200, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr>

			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" name="tdparamuser_000_<?php echo $nbruseraffich; ?>" id="tdparamuser_000_<?php echo $nbruseraffich; ?>" class="liste1" colspan="8">Modification de l'utilisateur </td>
			</tr>



				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="2" class="liste1">
						<label>Nom : </label>
						<input type="text" id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" maxlength="50" size="50" name="USERNOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_NOM'] != '') { echo decrypt($data['UT_NOM'],$data['UT_ID2']);} ?>' >
					</td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="6" class="liste1">
						<label>Prénom : </label>
						<input type="text" id="paramuser_002_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERPRENOM<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_PRENOM'] != '') { echo decrypt($data['UT_PRENOM'],$data['UT_ID2']);} ?>' >
					</td>

				</tr>
				<?php


			 ?>
			<tr style="background-color: #edf1f6; ">

				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="liste1">
					<label>Email : </label>
					<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USEREMAIL<?php echo $nbruseraffich; ?>" value='<?php if($data['UT_EMAIL'] != '') { echo decrypt($data['UT_EMAIL'],$data['UT_ID2']);} ?>' >
				</td>
			</tr>

			<tr style="background-color: #edf1f6; ">
				<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="liste1">
					<label>Code Tiers Comptabilité : </label>
					<input type="text" id="paramuser_003_<?php echo $nbruseraffich ; ?>" maxlength="250" size="250" name="USERTIERSCOMPTA<?php echo $nbruseraffich; ?>"  value='<?php echo $data['TE_COMPTATIERS']; ?>' >
				</td>
			</tr>

			<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
				</tr>

			<?php

			if ((isset($data['ET_GESTIONCREDITCLIENT'])) && ($data['ET_GESTIONCREDITCLIENT'] == 'OUI'))
			{
				?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" />
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">Crédit autorisé : </td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">
							<select id="paramuser_008_<?php echo $nbruseraffich ; ?>" name="USERCREDITOK<?php echo $nbruseraffich;?>" style="width: 60px;">
							<?php
								if ($data['TE_CREDITOK'] == 'OUI')
								{
									echo '<OPTION selected="selected" value="OUI">OUI</option>';
									echo '<OPTION value="NON">NON</option>';
								}
								else
								{
									echo '<OPTION  value="OUI">OUI</option>';
									echo '<OPTION selected="selected" value="NON">NON</option>';
								}

								?>
							</SELECT>
					</td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">Montant du crédit : </td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" class="liste1"><input id="paramuser_009_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMNTCREDIT<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php if(!empty($data['TE_CREDITVAL'])) { echo $data['TE_CREDITVAL'];} ?>' disabled></td>
				</tr>
				<?php
			}
			 ?>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">
						<label>Adhésion : </label>
							<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="ADHESION<?php echo $nbruseraffich;?>" style="width: 100px;" >
							<?php
							if ($data['ADHESIONOK'] == 'OUI')
							{
								?>
								<option selected value="VALIDE">Oui</option>
								<option value="ATTENTE">Non</option>
								<?php
							}
							else
							{
								?>
								<option value="VALIDE">Oui</option>
								<option selected value="ATTENTE">Non</option>
								<?php
							}
							?>
							</SELECT>
					</td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="2" class="liste1">
						<label>Début d'adhésion : </label>
						<?php
						if ($data['ADHESIONOK'] == 'OUI')
						{
							?>
							<input name="DATEDEBUTADH<?php echo $nbruseraffich; ?>" id="datepicker_<?php echo $nbruseraffich; ?>" type="text" value="<?php echo $data['DEBUTADHESION']; ?>" size="12"/>
							<?php
						}
						else
						{
							?>
							<input name="DATEDEBUTADH<?php echo $nbruseraffich; ?>" id="datepicker_<?php echo $nbruseraffich; ?>" type="text" style="display: none;" value="<?php echo $data['DEBUTADHESION']; ?>" size="12" />
							<?php
						}
						?>
					</td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" class="liste1">
						<label>Fin d'adhésion : </label>
						<?php
						if ($data['ADHESIONOK'] == 'OUI')
						{
							?>
							<input name="DATEFINADH<?php echo $nbruseraffich; ?>" id="datepicker1_<?php echo $nbruseraffich; ?>" type="text" value="<?php echo $data['FINADHESION']; ?>" size="12" />
							<?php
						}
						else
						{
							?>
							<input name="DATEFINADH<?php echo $nbruseraffich; ?>" id="datepicker1_<?php echo $nbruseraffich; ?>" type="text" style="display: none;" value="<?php echo $data['FINADHESION']; ?>" size="12" />
							<?php
						}
						?>
					</td>

				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="1" class="liste1">Administrateur de l'établissement: </td>
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="7" class="liste1">
							<select id="paramuser_010_<?php echo $nbruseraffich ; ?>" name="USERADMINSITE<?php echo $nbruseraffich;?>" >
							<?php
							if ($data['UT_ADMINSITE'] == $_SESSION['ETABADMIN'])
							{
								?>
								<option selected value="<?php echo $data['UT_ADMINSITE'];?>">Oui</option>
								<option value="">Non</option>
								<?php
							}
							else
							{
								?>
								<option value="<?php echo $data['UT_ADMINSITE'];?>">Oui</option>
								<option selected value="">Non</option>
								<?php
							}
							?>
							</SELECT>
					</td>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1"/>
				</tr>
				<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="3" align="center" class="liste1">
						<input name="boutonvalid_<?php echo $nbruseraffich; ?>" id="boutonvalid_<?php echo $nbruseraffich; ?>" class="bouton1" value="Valider" type="submit">
					</td>
						<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="5" align="center" class="liste1">
						<input name="boutonmdp_<?php echo $nbruseraffich; ?>" id="boutonmdp_<?php echo $nbruseraffich; ?>" class="bouton1" value="Nouveau mot de passe" type="submit">
					</td>
				</tr>
			<tr style="background-color: #edf1f6; ">
					<td id="tdparamuser_<?php $j++; echo (($nbruseraffich * 100) + $j); ?>" colspan="8" class="interligne1">
							<input type="hidden" id="paramuser_011_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERMODIF<?php echo $nbruseraffich; ?>" style="width: 300px;" value='NON'>
							<input type="hidden" id="paramuser_012_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERLOGIN<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_LOGIN'];?>'>
							<input type="hidden" id="paramuser_013_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERID<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_ID2'];?>'>
							<input type="hidden" id="paramuser_017_<?php echo $nbruseraffich ; ?>" maxlength="50" size="50" name="USERIDTYPE<?php echo $nbruseraffich; ?>" style="width: 300px;" value='<?php echo $data['UT_TYPE'];?>'>
					</td>
				</tr>


		<?php
		}

		?>
		<tr><td><input type="hidden" name="nbruseraffich" id="nbruseraffich" value="<?php echo $nbruseraffich; ?>"></input></td></tr>
		<tr>
			<?php

			if($pageprec >= 0)
			{
				?>
				<td colspan="3" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/prec-black.png" width="25" height="25" onClick="window.location.href='manageuser.php?PAGE=<?php echo $pageprec; ?>'"/></td>
				<?php
			}
			else
			{
				?>
				<td colspan="3 "style="width: 30px;"></td>
				<?php
			}
			 ?>

		<td colspan="5" align="center" style="width: 30px;"><img class="photo" id="website2" border="0" src="img/suiv-black.png" width="25" height="25" onClick="window.location.href='manageuser.php?PAGE=<?php echo $pagesuiv; ?>'"/></td>
		</tr>
		</table>

	</form>
	</center>
	<?php
	}

}
