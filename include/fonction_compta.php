﻿	<?php
/**
 * fonction_compta.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
include ("include/fonction_general_admin.php");


function liste_payment()
{
	connectsql();
	if ($_POST['user'] != "Tous CoWorker")
	{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td align="center" style="width:15%"> Adhérant</td>
		<td align="center" style="width:20%"> Référence Facture</td>
		<td align="center" style="width:5%"> Date Facture</td>
		<td align="center" style="width:10%"> Montant</td>
		<td align="center" style="width:5%"> Date Paiement</td>
		<td align="center" style="width:5%"> Mode de Paiement</td>
		<td align="center" style="width:20%"> Référence Paiement</td>
		<td align="center" style="width:5%"> Date Relance</td>
		<td align="center" style="width:15%"> </td>
	</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%" style="border-color:white">
	<?php
	$sql = "SELECT GP_USERNOM, GP_USERPRENOM, DATE_FORMAT(GP_DATEPIECE,'%d/%m/%Y') AS JOUR, GP_USER, GP_REFFACTURE, GP_TOTALHT, GP_PAYEMENT, DATE_FORMAT(GP_DATEPAYEMENT,'%d/%m/%Y') AS DATEPAYMENT , GP_RELANCE, DATE_FORMAT(GP_DATERELANCE,'%d/%m/%Y') AS DATERELANCE , GP_NOMBRERELANCE, GP_REFPAYMENT, MO_LIBELLE
			FROM PIECE
			LEFT JOIN MODEPAIE ON MO_MODEPAIE = GP_MODEPAIE
			ORDER BY GP_DATEPIECE, GP_USER;";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
		{
		?>
		<tr style="border-top:1px 1px solid #000">
		<td align="left" style="width:15%" color="red"><?php echo $data['GP_USERPRENOM'] .' ' .$data['GP_USERNOM']; ?></td>
		<td align="left" style="width:20%"><b><?php echo $data['GP_REFFACTURE']; ?></b></a></td>
		<td align="center" style="width:5%"><?php echo $data['JOUR']; ?></td>
		<td align="center" style="width:10%"><?php echo number_format($data['GP_TOTALHT'],2,',','') .' €'; ?></td>
		<?php
		if ($data['GP_PAYEMENT']=='OUI')
		{
			?>
			<td align="center" style="width:5%"><?php echo $data['DATEPAYMENT']; ?></td>
			<td align="center" style="width:5%"><?php echo $data['MO_LIBELLE']; ?></td>
			<td align="center" style="width:20%"><?php echo $data['GP_REFPAYMENT']; ?></td>
			<?php
		}
		else
		{
			echo '<td align="center" style="width:5%"></td>';
			echo '<td align="center" style="width:5%"></td>';
			echo '<td align="center" style="width:20%"></td>';
		}
		if ($data['GP_NOMBRERELANCE']>0)
		{
			?>
			<td align="center" style="width:5%"><?php echo $data['DATERELANCE']; ?></td>
			<?php
		}
		else
		{
			echo '<td align="center" style="width:5%"></td>';
		}
		?>
		<td align="center" style="width:15%">
			<table><tr>
			<?php
			if ($data['GP_PAYEMENT']!='OUI')
			{
				?>
				<td><input type=button value="Payé" class="bouton2" onclick="window.open('payment.php?RefFacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=600, width=800, top=100, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td>
				<td><input type=button value="Relance" class="bouton2" onclick="window.open('relance_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
				<?php
			}
			?>

			<td><img alt="Imprimer la facture" border="0" src="img/logo_print.jpg" width="25" height="25" onclick="window.open('print_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=800, width=1200, top=100, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			<td><img border="0" src="img/logo-mail.jpg" width="25" height="25" onclick="window.open('email_facture.php?reffacture=<?php echo $data['GP_REFFACTURE']; ?>', 'exemple', 'height=200, width=200, top=10, left=10, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
			</tr></table>
		</tr>
		<?php
		}
	mysql_close;
	?>
	</table></center>

	<?php
}

function liste_modepaie()
{
	connectsql();
	if ($_POST['user'] != "Tous CoWorker")
	{$Client = $_POST['Nom_Client'];}

	?>

	<br>

	<center><div id="support"><table border="1" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td align="center" bgcolor="#1B9E6F" style="width:15%"> Code</td>
		<td align="center" bgcolor="#1B9E6F" style="width:20%"> Libéllé</td>
	</tr>
	</table></center>
	<center><div id="support1"><table border="1" cellpadding="2" cellspacing="0" width="100%">
	<?php
	$sql = "SELECT * FROM MODEPAIE;";

	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
		{
		?>
		<tr>
		<td align="left" style="width:15%"><?php echo $data['MO_MODEPAIE']; ?></td>
		<td align="left" style="width:20%"><b><?php echo $data['MO_LIBELLE']; ?></b></a></td>
		</tr>
		<?php
		}
	mysql_close;
	?>
	</table></center>

	<?php
}

function saisie_payment()
{
	if(isset($_GET["RefFacture"]))
	{
		?>
		<center><div id="support"><table border="1" cellpadding="2" cellspacing="0" width="100%">
		<tr>
		<?php
		$sql = "SELECT * FROM PIECE WHERE GP_REFFACTURE = '" .$_GET["RefFacture"] ."';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			?>
			<td align="left" bgcolor="#1B9E6F" style="border: none">Référence COWORKER : <b><?php echo $data['GP_USER']; ?></b></td>
			<td align="left" bgcolor="#1B9E6F" style="border: none" colspan=2>Nom COWORKER : <b><?php echo $data['GP_USERNOM'] .' ' .$data['GP_USERPRENOM']; ?></b></td></tr>
			<tr><td align="left" bgcolor="#1B9E6F" style="border: none"colspan=3>Référence Facture : <b><?php echo $data['GP_REFFACTURE']; ?></b></td></tr>
			<td align="left" bgcolor="#1B9E6F" style="border: none" colspan=3>Montant HT de la facture : <b><?php echo $data['GP_TOTALHT'] . ' €'; ?><b></td></tr>


		<?php
		}
		?>
		</table></center>
		<br>
		<div id="formplaning">
		<form class="formplaning" action="" method="post">
		<input type="hidden" value="<?php echo $_GET['RefFacture']; ?>" name="RefFacture">

		<?php
		// Affichage de la catégorie d'appel
		$sql = "SELECT * FROM MODEPAIE;";
		$req = mysql_query($sql) or die("Requete pas comprise");
		?>
		<br />

		<label>Mode de paiement  </label>
		<div id='CategorieCall' style='display:inline'>
		<select id="test" onChange="JAVASCRIPT:menuToSpan(this.options[this.selectedIndex]);">
		<option>Selectionner un mode de paiement</option>
		<?php
			while ($data = mysql_fetch_array($req)) {
		?>
			<option value="<?php echo $data['MO_MODEPAIE'];  ?>"><?php echo $data['MO_LIBELLE'];  ?></option>
		<?php
			} // fin while
		// liberation ressource
			mysql_free_result ($req);
		?>
		</select>
		</div>
		<
		<input type="text" name="Payment"  size="35" id="targetOption" required></p>
		<p>Date: <input name="DatePayment" id="datepicker" type="text" size="10" value="<?php echo date("d/m/Y"); ?>" size="12" required/></p>
		<!--<p>Date du paiement : <input name="DatePayment" id="datepicker" type="text" size="28" value="<?php echo date("d/m/Y"); ?>" size="12" required/></p> -->
		<p>Référence du paiement : <input type="text" name="RefPayment" required size="50"></p>
		<br />
		<center><input class="bouton2" type="submit" value="Valider" /></center>
		</form>
		<?php
		// fin connexion
		mysql_close();

	}
}

function valid_payment()
{
	connectsql();
	echo $_POST['Payment'];
	$sql = "SELECT * FROM MODEPAIE WHERE MO_LIBELLE = '" .$_POST['Payment'] ."';";
	$req = mysql_query($sql) or die("Requete pas comprise");
	while ($data = mysql_fetch_array($req))
	{
		$Modepaie = $data['MO_MODEPAIE'];
	}
	mysql_free_result ($req);

	$sql = "UPDATE PIECE SET GP_MODEPAIE = '" .$Modepaie ."', GP_REFPAYMENT = '".addslashes($_POST['RefPayment']) ."',
			GP_DATEPAYEMENT = STR_TO_DATE('" .$_POST['DatePayment']. "', '%d/%m/%Y %H:%i'), GP_PAYEMENT = 'OUI'
			WHERE GP_REFFACTURE = '" . $_POST['RefFacture'] ."';";
	$req = mysql_query($sql) or die("Requete pas comprise");
	echo $_POST['test'];
}

function popup_rapport_facture()
{
	$date = new DateTime();
	$date->modify('-1 month');
	?>
	<form action="" method="post">
	<div id="facture" style="width:40%;">
	<input name="action" type="hidden" value="RAPPORTFACTURE">
	<p align="center">sélecetion de la recherche</p>
	<p align="left">Date début: <input name="DateDebut" id="datepicker" type="text" size="10" value="<?php echo $date->format('d/m/Y'); ?>" size="12" required/></p>
	<p align="left">Date de fin: <input name="DateFin" id="datepicker1" type="text" size="10" value="<?php echo date("d/m/Y"); ?>" size="12" required/></p>
	<br>
	<p align="left">Facture payé    <input type="checkbox" id="FacturePayer" name="FacturePayer" checked></p>
	<p align="left">Facture non payé    <input type="checkbox" id="FactureNoPayer" name="FactureNoPayer"></p>
	<center><input type="submit" value="Valider" /></center>
	<br>
	</form>
	</div>

	<?php
	// fin connexion
}

function rapport_facture()
{
	require('../include/fpdf.php');
	define('EURO', chr(128));


	echo "1";

}
