﻿<?php
/**
 * fonction_general.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

require_once ("scripts/constantes.php");

require ("scripts/fonctions.php");





function header_page()
{
	?>
	<link rel="icon" href="img/laverriere.ico" />
	<title><?php echo Site_Title; ?></title>
	<?php
}

function encrypt($data,$key) {
  // Remove the base64 encoding from our key
  $encryption_key = base64_decode($key);
  // Generate an initialization vector
  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
  // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
  $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
  // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
  return base64_encode($encrypted . '::' . $iv);
}

function decrypt($data, $key) {
  $encryption_key = base64_decode($key);
  // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
  list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
  return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}

function old_entete_page($titre_footer)
{
?>

<table border="0" width="100%" height="20%">
<tr>
	<?php
	$sql="SELECT * FROM ETABLISSEMENT WHERE ET_ETABLISSEMENT = '".$_GET['etablissement']."'";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		?>
		<img src="img/<?php echo $data['ET_IMAGENOM']; ?>" class="hide_mobile">
		<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>" class="hide_desktop">
		<td width="20%"><img src="img/<?php echo $data['ET_IMAGENOM']; ?>" border="0" width="400" height="115" alt=""></td>
		<?php
	}
	?>

	<td>
	<table border="0" width="100%">
	<tr><td><div id="titre"><?php echo Site_Title; ?></div></td></tr>
	<tr align="center"><td align="center"><div id="text1"><?php echo $titre_footer ?></div></td></tr>
	<tr><td><?php echo barre_menu(); ?></td></tr>
	</table>
	</td>
	<td>
	<table>
	<?php
	echo '<tr><td width="20%"><div id="text1">'.$_SESSION['PRENOM']. ' '.$_SESSION['ETABADMIN']. '</div></td></tr>';
	if (isset($_SESSION['login']))
	{
		?>
		<tr><td><a style="margin-left:30px; color:#F69730" href="deconnection.php" ><B>Déconneddxion</b></a> </td></tr>
		<?php
	}
	?>
	</table>
</tr>
</table>
<hr><br>
<div>
<?php
}

function entete_page($titre_footer)
{
?>

<nav class="logged">
		<a href="#" class="logo">
		<?php

		$sql = "SELECT COUNT(ET_ETABLISSEMENT) AS CPT, ET_ETABLISSEMENT FROM ETABLISSEMENT WHERE ET_BLOQUE = 'NON';";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$cpt = $data['CPT'];
			$etablissement = $data['ET_ETABLISSEMENT'];
		}

		if ($cpt > 1 )
		{
			if(isset($_GET['etablissement']))
			{
		//		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
				$sql="SELECT * FROM ETABLISSEMENT WHERE ET_ETABLISSEMENT = '".$_GET['etablissement']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data)
				{
					?>
					<img src="img/<?php echo $data['ET_IMAGENOM']; ?>" class="hide_mobile">
					<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>" class="hide_desktop">
					<Version>
					<?php
				}
			}
			else
			{
				?>
				<img src="img/catalyst.jpg" class="hide_mobile">
				<img src="img/catalyst.jpg" class="hide_desktop">
				<?php
			}
		}
		else
		{
			$sql="SELECT * FROM ETABLISSEMENT WHERE ET_ETABLISSEMENT = '".$etablissement."'";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data)
			{
				?>
				<img src="img/<?php echo $data['ET_IMAGENOM']; ?>" class="hide_mobile">
				<img src="img/<?php echo $data['ET_PETITEIMAGENOM']; ?>" class="hide_desktop">
				<?php
			}
		}
		?>

		</a>
<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];



?>

	<?php
	if (isset($_SESSION['login']))
	{
		?>

		<div class="logged_in">
			<div class="user col-md-3 col-sm-3" data-user="<?php echo $_SESSION['login']; ?>">
				<span><p>Bienvenue</p></span>
				<span><p><strong><?php echo $_SESSION['PRENOM'].' '.$_SESSION['NOM'] ?></strong></p></span>
				<?php
				if ($cpt > 1)
				{
					if((isset($_SESSION['ETABADMIN'])) && (isset($_GET['etablissement'])))
					{
						if ($_SESSION['ETABADMIN'] == $_GET['etablissement'])
						{
							?>
							<span><p><a href="admacceuil.php" Administration</a>Gestion de l'établissement</p></span>
							<span><p><a href="admacceuil.php" Administration</a>Gestion de l'établissement</p></span>
							<?php
						}
					}
				}
				else
				{
					if(isset($_SESSION['ETABADMIN']))
					{
						if ($_SESSION['ETABADMIN'] == $etablissement)
						{
							?>
							<span><p><a href="admacceuil.php" Administration</a>Gestion de l'établissement </p></span>

							<?php
						}
					}
				}
				?>
			</div>
			<div class="user_infos">
				<?php
				if ($_SESSION['NbrPanier'] != 0)
				{
					?>
					<a href="mon_panier.php" class="resa_nav <?php if (strpos($url,'mon_panier.php') !== false) {echo 'active';} ?>">
						<img src="img/shopping-cart.png" class="hide_mobile" style="padding-top: 15px;">
						<img src="img/peite_caddie.png" class="hide_desktop" style="padding-top: 15px;">
					</a>
					<?php
				}
				?>
				
				<a href="accueil.php" class="resa_nav <?php if (strpos($url,'manageresa.php') !== false) {echo 'active';} ?>">Réservation</a>
				<a href="mon_compte.php" class="account_nav <?php if (strpos($url,'mon_compte.php') !== false) {echo 'active';} ?>">Mon compte</a>
				<a href="deconnection.php" class="logout_nav"></a>
			</div>

		</div>
		<!--<p><a href="admacceuil.php" Administration</a>Gestion de l'établissement </p>-->
		<?php
	} else {
	?>

	<div class="no_login">
		<p>Connexion</p>
	</div>

	<div class="user_infos">
		<a href="accueil.php" class="info_nav <?php if (strpos($url,'manageresa.php') !== false) {echo 'active';} ?>">Men</a>
	</div>


	<?php
	}
	?>

</nav>

<?php
}

function entete_page_login($titre_footer)
{
?>
<nav class="login">
		<a href="#" class="logo">
			<img src="img/logo-la-verriere.png" class="hide_mobile">
			<img src="img/logo-la-verriere-mini.svg" class="hide_desktop">
		</a>

	<div class="no_login">
		<p><?php echo $titre_footer; ?></p>
	</div>
</nav>
<?php
}

function petite_entete_page($titre_footer)
{
?>

<table border="0" width="100%" height="20%">
<tr>
	<td width="20%"><img src="img/logo-la-veriere.png" border="0" width="200" height="55" alt=""></td>
	<td>
	<table border="0" width="100%">
	<tr><td><div id="titre"><?php echo Site_Title; ?></div></td></tr>
	<tr><td><div id="text1"><?php echo $titre_footer ?></div></td></td>
	</table>
	</td>
	</tr>
</table>
<hr><br>
<div>
<?php
}

function barre_menu()
{
	?>
	<div id="menu">
	<ul>


	<li><a href="#">Utilisateur</a>
	<ul>
	<?php
		echo '<li><a href="manageuser.php">Gestion Utilisateur</a></li>';
		if (!isset($_SESSION['SUPERADMIN']))
		{
			echo '<li><a href="crediter_user.php">Créditer un utilisateur</a></li>';
			//echo '<li><a href="create_adhesion.php">Gestion des adhésion</a></li>';
		}
	  ?>

	 </ul>
	</li>

	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Comptabilité</a>';
		echo '<UL>';
		echo '<li><a href="liste_resa.php">Liste des réservations</a></li>';
		echo '<li><a href="cloture_resa.php">Clôture des réservations</a></li>';
		//echo '<li><a href="create_facture.php">Création de facture de réservation</a></li>';
		//echo '<li><a href="vente.php">Vente de produit ou service</a></li>';
		echo '<li><a href="admlist_facture.php">Gestion des factures</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>

	<li><a href="#">Réservation et Saisie</a>
	<ul>
		<?php
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql="SELECT * FROM ZONE WHERE EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND EM_BLOQUE = 'NON';";
		$req = $conn->query($sql) or die('Erreur SQL !<br>');
		while($data = mysqli_fetch_array($req))
		{
			?>
			<li><a href="adm_manageresa.php?etablissement=<?php echo $_SESSION['ETABADMIN'];?>&typeplace=<?php echo $data['EM_EMPLACEMENT']; ?>"><?php echo $data['EM_LIBELLE']; ?></a></li>
			<?php
		}
		?>
	 </ul>
	</li>
	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Paramétrage</a>';
		echo '<UL>';
		echo '<li><a href="manageetab.php">Etablissement</a></li>';
		echo '<li><a href="managezone.php">Zone</a></li>';
		echo '<li><a href="managearticle.php">Gestion des articles</a></li>';
		echo '<li><a href="manage_email.php">Gestion des emails</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>
	</ul>
	</div>
<?php
}

function supadm_barre_menu()
{
	?>
	<div id="menu">
	<ul>


	<li><a href="#">Utilisateur</a>
	<ul>
		<li><a href="manageuser.php">Gestion Utilisateur</a></li>
		<?php
		if (!isset($_SESSION['SUPERADMIN']))
		{
			echo '<li><a href="crediter_user.php">Créditer un utilisateur</a></li>';
			echo '<li><a href="create_adhesion.php">Gestion des adhésion</a></li>';
		}
		?>

	 </ul>
	</li>

	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Comptabilité</a>';
		echo '<UL>';

		if (!isset($_SESSION['SUPERADMIN']))
		{
			echo '<li><a href="liste_resa.php">Liste des réservations</a></li>';
			echo '<li><a href="create_facture.php">Création de facture de réservation</a></li>';
			echo '<li><a href="vente.php">Vente de produit ou service</a></li>';
			echo '<li><a href="admlist_facture.php">Gestion des factures</a></li>';
		}
		echo '</UL>';
		echo '</li>';
	}
	?>

	<li><a href="#">Réservation et Saisie</a>
	<ul>
		<?php
		if (!isset($_SESSION['SUPERADMIN']))
		{
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql="SELECT * FROM ZONE WHERE EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			while($data = mysqli_fetch_array($req))
			{
				?>
				<li><a href="adm_manageresa.php?etablissement=<?php echo $_SESSION['ETABADMIN'];?>&typeplace=<?php echo $data['EM_EMPLACEMENT']; ?>"><?php echo $data['EM_LIBELLE']; ?></a></li>
				<?php
			}
		}
		?>
	 </ul>
	</li>
	<?php
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Paramétrage</a>';
		echo '<UL>';
		echo '<li><a href="manageetab.php">Etablissement</a></li>';
		if (!isset($_SESSION['SUPERADMIN']))
		{

			echo '<li><a href="managezone.php">Zone</a></li>';
			echo '<li><a href="managearticle.php">Gestion des articles</a></li>';
			echo '<li><a href="managepromo.php">Gestion des promotions</a></li>';
		}
		echo '</UL>';
		echo '</li>';
	}
	?>
	</ul>
	</div>
<?php
}



function barre_menu_new()
{
	?>
	<div id="menu">
	<ul>
	<?php
	echo $_SESSION['STATUT'];
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Comptabilité</a>';
		echo '<UL>';
		echo '<li><a href="payment_facture.php">Paiement de facture</a></li>';
		echo '<li><a href="create_facture.php">Creation de facture</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	if (($_SESSION['STATUT']) == "ADMIN")
	{
	?>
		<li><a href="#">Gestion du planning CoWorking</a>
		<ul>
		<li><a href="visuplanning.php">Visualisation Planning des CoWorker</a></li>
		</ul>
		</li>
	<?php
	}
	else
	{
		?>
		<li><a href="#">Gestion de mon planning</a>
		<ul>
		<li><a href="planning.php">Planning</a></li>
		<li><a href="reservation.php">Réservation</a></li>
		</ul>
		</li>
		<?php
	}
	if (($_SESSION['STATUT']) == "ADMIN")
	{
	?>
		<li><a href="#">Gestion des adhérants</a>
		<ul>
		<li><a href="list_tiers.php">Liste des clients</a></li>
		</ul>
		</li>
	<?php
	}
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		echo '<li><a href="#">Paramétrage</a>';
		echo '<UL>';
		echo '<li><a href="modepaie.php">Mode de paiement</a></li>';
		echo '</UL>';
		echo '</li>';
	}
	?>
	</ul>
	</div>
<?php
}


function footer()
{
?>
<div style="text-align:center;height:100px;color:#F69730">
<br>
<hr />
<p><a style="color:#097855" href="acceuil.php">Acceuil</a>&nbsp;»</p>
</div>
<?php
}

function admheader_page()
{
	?>
	<link rel="icon" href="img/laverriere.ico" />
	<title><?php echo Site_Title; ?></title>
	<?php
}

function admentete_page($titre_footer)
{

?>

<table style="text-align: left; width: 100%; height: 20%;" border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
	<?php
	if (!isset($_SESSION['SUPERADMIN']) && (isset($_SESSION['ETABADMIN'])))
	{
		$sql="SELECT * FROM ETABLISSEMENT WHERE ET_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			?>
			<td style="width: 20%; height: 100px;">
				<img style="width: 300px; height: 81px;" alt="" src="img/<?php echo $data['ET_IMAGENOM']; ?>">
				<span>Version 3.1.0</span>
			</td>
			<?php
		}
	}
	?>



      <td style="width: 60%; height: 100px;">
      <table style="width: 75%; height: 100%; text-align: center; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
        <tbody>
          <tr><td><div id="titre">Gestion Espace de Coworking</div></td></tr>
          <tr align="center"><td align="center"><div id="text1"><?php echo $titre_footer ?></div></td></tr>
		  <?php
		  if (isset($_SESSION['login']))
		  {
			if ((isset($_SESSION['SUPERADMIN'] )) && (($_SESSION['SUPERADMIN']) == "OUI"))
			{
				 ?>
				<tr><td colspan=3 ><?php echo supadm_barre_menu(); ?></td></tr>
				<?php
			}
			if ((isset($_SESSION['ETABADMIN'] )) && (($_SESSION['ETABADMIN']) != ''))
			{
				 ?>
				<tr><td colspan=3 ><?php echo barre_menu(); ?></td></tr>
				<?php
			}


		  }
		  ?>

        </tbody>
      </table>
      </td>
      <td style="width: 20%; height: 100px;">
	  <table>
	<?php


	if (isset($_SESSION['login']))
	{
		echo '<tr><td width="20%" align="center"><div id="text1">'.$_SESSION['PRENOM']. ' '.$_SESSION['NOM']. '</div></td></tr>';
		?>

		<tr><td align="center"><a style="color:#F69730" href="deconnection.php" ><B>Déconnexion</b></a> </td></tr>
		<tr><td align="center"></td></tr>
		<tr><td align="center"><a style="color:#F69730" href="mon_compte.php" ><input type=button value="Mon compte" class="bouton1" ></a> </td></tr>
		<tr><td align="center"></td></tr>
		<tr><td align="center"><a style="color:#F69730" href="accueil.php" ><input type=button value="Retour au site classique" class="bouton1" ></a> </td></tr>
		<tr><td align="center"></td></tr>
		<?php


	}

	?>


	</table>
	  </td>
    </tr>


  </tbody>
</table>


<hr><br>
<div>
<?php
}

function supadmentete_page($titre_footer)
{

?>

<table style="text-align: left; width: 100%; height: 20%;" border="0" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>

      <td style="width: 60%; height: 100px;">
      <table style="width: 75%; height: 100%; text-align: center; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
        <tbody>
          <tr><td><div id="titre">Gestion Espace de Coworking</div></td></tr>
          <tr align="center"><td align="center"><div id="text1"><?php echo $titre_footer ?></div></td></tr>
		  <?php
		  if (isset($_SESSION['login']))
		  {
			if (($_SESSION['STATUT']) == "ADMIN")
			{
				 ?>
				<tr><td colspan=3 ><?php echo barre_menu(); ?></td></tr>
				<?php
			}


		  }
		  ?>

        </tbody>
      </table>
      </td>
      <td style="width: 20%; height: 100px;">
	  <table>
	<?php


	if (isset($_SESSION['login']))
	{
		echo '<tr><td width="20%" align="center"><div id="text1">'.$_SESSION['PRENOM']. ' '.$_SESSION['NOM']. '</div></td></tr>';
		?>

		<tr><td align="center"><a style="color:#F69730" href="deconnection.php" ><B>Déconnexion</b></a> </td></tr>
		<tr><td align="center"></td></tr>
		<tr><td align="center"><a style="color:#F69730" href="mon_compte.php" ><input type=button value="Mon compte" class="bouton1" ></a> </td></tr>
		<tr><td align="center"></td></tr>
		<tr><td align="center"><a style="color:#F69730" href="accueil.php" ><input type=button value="Retour au site classique" class="bouton1" ></a> </td></tr>
		<tr><td align="center"></td></tr>
		<?php


	}

	?>


	</table>
	  </td>
    </tr>


  </tbody>
</table>


<hr><br>
<div>
<?php
}
