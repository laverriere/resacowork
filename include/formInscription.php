<?php
/**
 * formInscription.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 ?>

<!-- Formulaire d'inscription -->
<form class="form-horizontal" action="traitement.php" method="POST">
<fieldset>
  <div class="form-group">
    <!-- Récupération du login -->
    <div class="col-md-4">
    <input name="user" type="hidden" placeholder="" class="form-control input-md" value="<?php
      $req = "SELECT UT_LOGIN FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."' ;";
      $result_req = $cnx_bdd->query($req);
      $tab_r = $result_req->fetchAll();
    foreach ($tab_r as $r) {
      echo $r['UT_LOGIN'];
    }
    ?>">
    </div>
  </div>

<div class="form-group">
  <label class="col-md-4 control-label" for="mdp">Nom d'utilisateur :</label>
  <div class="col-md-4">
    <input type="text" class="form-control input-md" disabled value="<?php
          $req = "SELECT UT_LOGIN FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."' ;";
          $result_req = $cnx_bdd->query($req);
          $tab_r = $result_req->fetchAll();
          $count = count($tab_r);
          if ($count == 1) {
            foreach ($tab_r as $r) {
              echo $r['UT_LOGIN'];
            }
          }
    ?>">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="mdp">Mot de passe :</label>
  <div class="col-md-4">
  <input name="mdp" type="password" placeholder="" class="form-control input-md" required="Requis" id="mdp" onkeyup='check_pass();'>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Vmdp">Vérification mot de passe :</label>
  <div class="col-md-4">
  <input name="Vmdp" type="password" placeholder="" class="form-control input-md" id="Vmdp" required="Requis" onkeyup='check_pass();'>
  </div>
</div>

<!-- Vérification des mdp -->
<script>
function check_pass() {
    var vert = "#66cc66";
    var rouge = "#ff6666";
    var mdp = document.getElementById('mdp').value;
    var Vmdp = document.getElementById('Vmdp').value;
    if (mdp == "") {
        document.getElementById('mdp').style.backgroundColor = rouge;
        document.getElementById('Vmdp').style.backgroundColor = rouge;
        document.getElementById('inscription').disabled = true;
    } else if(mdp == Vmdp) {
				document.getElementById('mdp').style.backgroundColor = vert;
        document.getElementById('Vmdp').style.backgroundColor = vert;
        document.getElementById('inscription').disabled = false;
    }else{
       		document.getElementById('mdp').style.backgroundColor = rouge;
        document.getElementById('Vmdp').style.backgroundColor = rouge;
        document.getElementById('inscription').disabled = true;
    }
}
</script>

<div class="form-group">
  <label class="col-md-4 control-label" for="nom">Nom :</label>
  <div class="col-md-4">
  <input name="nom" type="text" placeholder="" class="form-control input-md" required="Requis">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="prenom">Prenom :</label>
  <div class="col-md-4">
  <input name="prenom" type="text" placeholder="" class="form-control input-md" required="Requis">
  </div>
</div>

<!-- Récupération de l'email -->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email :</label>
  <div class="col-md-4">
  <input name="email" type="email" placeholder="" class="form-control input-md" required="Requis" value="<?php
    $req = "SELECT UT_EMAIL, UT_ID2 FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."' ;";
    $result_req = $cnx_bdd->query($req);
    $tab_r = $result_req->fetchAll();
    $count = count($tab_r);
    if ($count == 1 ) {
      foreach ($tab_r as $r) {
        $email = decrypt($r['UT_EMAIL'],$r['UT_ID2']);
        echo $email;
      }
    }
  ?>">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="inscription"></label>
  <div class="col-md-4">
    <input type="submit" name="inscription" value="Inscription" id="inscription" disabled class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3" style=
    "    position: relative;
        float: left;
        display: inline-block;
        background: #51b692;
        -webkit-border-radius: 25px;
        border-radius: 25px;
        padding: 10px 0;
        outline: none;
        border: none;
        color: #ffffff;
        font-size: 16px;
    ">
  </div>
</div>
</fieldset>
</form>
