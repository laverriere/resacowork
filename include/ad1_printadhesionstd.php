<?php

// Afficher les erreurs à l'écran

require('fpdf.php');

define('EURO', chr(128));

$joursemaine = array('dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi');

$sql = "SELECT *, date_format(TE_DATEVALIDATION,'%d/%m/%Y') as DATEVALIDATION,
			 date_format(TE_DEBUTADHESION,'%d/%m/%Y') as DATEDEBUTADH, date_format(TE_DATEFINADHESION,'%d/%m/%Y') as DATEFINADH
			 FROM TIERSETAB
			 LEFT JOIN UTILISATEUR ON UT_LOGIN = TE_LOGIN
			 LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = TE_ETABLISSEMENT
			 WHERE TE_LOGIN = '".$_GET['adherant']."' AND TE_ETABLISSEMENT = '".$_GET['etablissement']."'";

		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
	{
		$nometab = $data['ET_LIBELLE'];
		$adresseetab  = $data['ET_ADRESSE1'];
		$villeetab = $data['ET_CODEPOSTAL'] .' ' .$data['ET_VILLE'];
		$NomClient = decrypt2($data['UT_NOM'],$data['UT_ID2']) .' ' . decrypt2($data['UT_PRENOM'],$data['UT_ID2']) ;
		$EmailClient = 'Email : ' .decrypt2($data['UT_EMAIL'],$data['UT_ID2']);
		$emailetab  = $data['ET_EMAIL'];
		$webetab  = $data['ET_SITEWEB'];
		$contactadh = decrypt2($data['UT_CONTACTNOM'],$data['UT_ID2']) .' ' . decrypt2($data['UT_CONTACTPRENOM'],$data['UT_ID2']) ;
		$Adresseadh = decrypt2($data['UT_ADRESSE1'],$data['UT_ID2']);
		$Villeadh = decrypt2($data['UT_CODEPOSTAL'],$data['UT_ID2']) .' ' .decrypt2($data['UT_VILLE'],$data['UT_ID2']);

		$DateValidation = $data['DATEVALIDATION'];
		$DateDebutAd = $data['DATEDEBUTADH'];
		$DateFinAd = $data['DATEFINADH'];

		$napetab = $data['ET_NAP'];
		$CodeCient = $data['TE_LOGIN'];
		$logo = "img/".$data['ET_IMAGENOM'];


	}




class PDF extends FPDF
{

	function Header()
	{
		global $nometab;
		global $adresseetab;
		global $villeetab;
		global $NomClient;
		global $EmailClient;
		global $emailetab;
		global $webetab;
		global $contactadh;
		global $Adresseadh;
		global $Villeadh;

		global $DateValidation;
		global $DateDebutAd;
		global $DateFinAd;

		global $napetab;
		global $CodeCient;
		global $logo;

		$this->Image($logo,10,6,80);
		$this->SetFont('Arial','B',15);
		$this->SetX(-80);
		$this->Cell(0,15,'FACTURE',0,1,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-80);

		$this->Ln(10);
		$this->Rect(10,45,90,30);
		$this->Rect(110,45,90,30);

		$this->Cell(0,10,$nometab,0,0,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-100);
		$this->Cell(0,10,'Cient : ' .$CodeCient,0,1,'L');

		$this->SetFont('Arial','B',10);
		$this->Cell(0,0,$adresseetab,0,0,'L');
		$this->SetX(-100);


		$this->SetFont('Arial','B',10);
		$this->Cell(0,10,$villeetab,0,0,'L');


		// Saut de 'L'igne
		$this->Ln(5);
		$this->SetFillColor(0,204,255);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('Arial','B',10);
		$this->Cell(40,6,'Code Article',1,0,'C',true);
		$this->Cell(70,6,'Designation',1,0,'C',true);
		$this->Cell(20,6,'Quantite',1,0,'C',true);
		$this->Cell(30,6,'Prix Unitaire HT',1,0,'C',true);
		$this->Cell(30,6,'Prix Total HT',1,1,'C',true);
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->Rect(10,80,110,142);
		$this->Rect(120,80,20,142);
		$this->Rect(140,80,30,142);
		$this->Rect(170,80,30,142);
	}


}

$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,80);

$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(224,235,255);
$numligne = 0;
$libligne = '';
$nbrligneprint = 1;




//for ($i = 1; $i <= 50; $i++) {
//    $pdf->Cell(0,6,'test',1,1,'L',false);
//}
$pdf->Output();

?>
