<?php
/**
 * function_admresa.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//info BDD
//require ("scripts/constantes.php");
//require ("scripts/fonctions.php");
include ("include/fonction_general.php");

function affiche_resa($typeplace)
{
	$user = $_SESSION['login'];
	$jour_actuel = date("j", time());
	$mois_actuel = date("m", time());
	$an_actuel = date("Y", time());
	$jour = $jour_actuel;

	if (!isset($_GET['user']))
	{
		$user = $_SESSION['login'];
	}
	// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante
	if(!isset($_GET["mois"]))
	{
		$mois = $mois_actuel;
		$an = $an_actuel;
	}
	else
	{
		$mois = $_GET['mois'];
		$an = $_GET['an'];
	}

	//defini le mois suivant
	$mois_suivant = $mois + 1;
	$an_suivant = $an;
	if ($mois_suivant == 13)
	{
		$mois_suivant = 1;
		$an_suivant = $an + 1;
	}

	//defini le mois précédent
	$mois_prec = $mois - 1;
	$an_prec = $an;
	if ($mois_prec == 0)
	{
		$mois_prec = 12;
		$an_prec = $an - 1;
	}

	//affichage du mois et de l'année en french
	$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
	$mois_en_clair = $mois_de_annee[$mois - 1];

	$nombre_date = mktime(0,0,0, $mois, 1, $an);
	$premier_jour = date('w', $nombre_date);
	$dernier_jour = 28;

	//Recherche des datas
	$NbrPlaceDispo = array("0");
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select EM_NBRRESAMAX as NB from ZONE where EM_EMPLACEMENT = '".$typeplace."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		for ($i = 1; $i < 32; $i++)
		{
			array_push($NbrPlaceDispo,$data['NB']);
		}
	}

	$NbrPlaceOccup = array("0");
	for ($i = 1; $i < 32; $i++)
		{
			array_push($NbrPlaceOccup,"0");
		}

	$reserved = array("NON");

	for ($i = 1; $i < 32; $i++)
		{
			array_push($reserved,"NON");

			//$i++;
		}



	$reservable = array("NON");
	$datetime2 = new DateTime("now");
	$today = array("NON");
	for ($i = 1; $i < 32; $i++)
		{

			$datetime1 = new DateTime($an .'-' .$mois .'-' .$i);
			$datetime2 = new DateTime($an_actuel .'-' .$mois_actuel .'-' .$jour_actuel);
			$interval = $datetime1->diff($datetime2);
			if ($interval->format('%a days') == 0)
				array_push($today,"OUI");
			else
				array_push($today,"NON");

			if ($interval->format('%r%d days') < 0)
				array_push($reservable,"OUI");
			else
				array_push($reservable,"NON");
		}


	$sql = "SELECT RE_ZONE, RE_JOUR, RE_DATE,EM_NBRRESAMAX - count(*) as NBLIBRE,EM_TYPEZONE, EM_NBRRESAMAX from RESERVATION
			left join ZONE on EM_EMPLACEMENT = RE_ZONE
			where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
			AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI' group by RESERVATION.RE_JOUR, RE_DATE";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		if (isset($data['NBLIBRE']))
		{
			$NbrPlaceDispo[$data['RE_JOUR']] = $data['NBLIBRE'];
		}

	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_ANNEE = ".$an ." and RE_MOIS = ".$mois."
				and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' ";
	}
	else
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois."
				and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI' ";
	}
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalplaceresaplaceresa = $data['NBRESA'];
	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." AND RE_FACTURE = 'OUI'
				and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'";
	}
	else
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." AND RE_FACTURE = 'OUI'
				and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'";
	}


	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalfacture = $data['NBRESA'];
	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_ANNEE = ".$an ." and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'";
	}
	else
	{
		$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'";
	}

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalplaceresaplaceresaannuel = $data['NBRESA'];
	}
	if (($_SESSION['STATUT']) == "ADMIN")
	{
		$sql = "select RE_JOUR, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ."
				AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR";
	}
	else
	{
		$sql = "select RE_JOUR, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." AND RE_USER = '".$user."'
				and RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR";
	}

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$NbrPlaceOccup[$data['RE_JOUR']] = $data['NBOCCUP'];
		if ($data['NBOCCUP'] > 0)
		{
			$reserved[$data['RE_JOUR']] = 'OUI';
		}
	}

	if ($an >= date("Y", time()))
	{

	}
	else
	{
		for ($i = 1; $i < 32; $i++)
		{
			$reservable[$i] = "OUI";
		}
	}

	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");


	?>

	<table style="text-align: center; width: 100%;" border="0">
  <tbody>
    <tr>
      <td style="width: 212px; height: 102px;"></td>
      <td style="width: 21px; height: 102px;"></td>
      <td style="height: 102px; width: 1112px;">
      <table style="width: 1086px; text-align: center; margin-left: auto; margin-right: auto;" border="0" >
        <tbody>
			<tr>
            <td colspan="2" ></td>
			<?php
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "SELECT * FROM ZONE WHERE EM_EMPLACEMENT = '".$_GET['typeplace']."'";
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			while($data = mysqli_fetch_array($req))
			{
			?>
				<td colspan="3" rowspan="1"><?php echo $data['EM_LIBELLE']; ?></td>
				<?php
			}
			?>
            <td colspan="2" >
			<select name="zone" id="zone" onchange='refreshwindow()'>
					<option value="-1">Sélectionner une zone</option>
						<?php
						$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
						$sql = "SELECT * FROM ZONE WHERE EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND EM_EMPLACEMENT <> '" .$_GET['typeplace']."'";
						$req = $conn->query($sql) or die('Erreur SQL !<br>');
						while($data = mysqli_fetch_array($req))
						{
							?>
							<option value="<?php echo $data['EM_EMPLACEMENT']; ?>"><?php echo $data['EM_LIBELLE'] ; ?></option>';
							<?php
						}
						?>
					</select>
			</td>
            </tr>
          <tr>
          <tr>
            <td colspan="2" class="bandeauresa1">
				<a href="adm_manageresa.php?typeplace=<?php echo $_GET['typeplace']; ?>&mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>"<?php echo $mois_prec; ?>>
				<div align="center"><img border="0" src="img/prec.png" /></div>
				</a>
			</td>
            <td colspan="3" rowspan="1" class="bandeauresa1">Réservation du mois de <?php echo $mois_en_clair .' ' .$an; ?></td>
            <td colspan="2" class="bandeauresa1">
				<a href="adm_manageresa.php?typeplace=<?php echo $_GET['typeplace']; ?>&mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>"<?php echo $mois_prec; ?>>
				<div align="center"><img border="0" src="img/suiv.png" /></div>
				</a>
			</td>
            </tr>
          <tr>
            <td border="1" style="text-align: center; width: 140px; height: 17px; border-left=1px">Lundi</td>
            <td style="text-align: center; width: 140px; height: 17px;">Mardi</td>
            <td style="width: 140px; text-align: center; height: 17px;">Mercredi</td>
            <td style="width: 140px; text-align: center; height: 17px;">Jeudi</td>
            <td style="width: 140px; text-align: center; height: 17px;">Vendredi</td>
            <td style="width: 140px; text-align: center; height: 17px;">Samedi</td>
            <td style="width: 140px; text-align: center; height: 17px;">Dimanche</td>
          </tr>
          <tr>
		  <?php
		  // Affichage de la première semaine

			while (checkdate($mois, $dernier_jour + 1, $an))
				{ $dernier_jour++;}

			//Affichage de 7 jours du calendrier

			for ($i = 1; $i < 8; $i++)
			{
				if ($i < $premier_jour)
				{
					?>
					<td style="height: 95px; width: 100px; text-align: center; vertical-align: middle;">

					</td>
					<?php
				}
				else
				{
					$ce_jour = ($i+1) - $premier_jour
					?>
					<td style="height: 95px; width: 100px; text-align: center; vertical-align: middle; " onclick="test.php">
					<?php
						if ($reserved[$ce_jour] == 'OUI')
						{
							//echo $reserved[$ce_jour];
							?>
							<table style="text-align: left; margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="resaok" onclick="window.open('adm_manageresa.php?ACTION=affichedate&typeplace=<?php echo $typeplace; ?>&user=<?php echo $user; ?>&jour=<?php echo $ce_jour; ?>&mois=<?php echo $mois;?>&annee=<?php echo $an; ?>&reservable=<?php echo $reservable[$ce_jour]; ?>&placedispo=<?php echo $NbrPlaceDispo[$ce_jour]; ?> ', 'exemple', 'height=400, width=400, top=300, left=300, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
							<?php
						}
						else
						{
							?>
							<table style="text-align: left; margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="resa1" onclick="window.open('adm_manageresa.php?ACTION=affichedate&typeplace=<?php echo $typeplace; ?>&user=<?php echo $user; ?>&jour=<?php echo $ce_jour; ?>&mois=<?php echo $mois;?>&annee=<?php echo $an; ?>&reservable=<?php echo $reservable[$ce_jour]; ?>&placedispo=<?php echo $NbrPlaceDispo[$ce_jour]; ?> ', 'exemple', 'height=400, width=400, top=300, left=300, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
							<?php
						}
						?>


						<!--<table border="1" cellpadding="0" cellspacing="0" class="resa1" onclick="window.open('adm_manageresa.php?ACTION=affichedate&typeplace=<?php echo $typeplace; ?>&user=<?php echo $user; ?>&jour=<?php echo $ce_jour; ?>&mois=<?php echo $mois;?>&annee=<?php echo $an; ?>&reservable=<?php echo $reservable[$ce_jour]; ?>&placedispo=<?php echo $NbrPlaceDispo[$ce_jour]; ?> ', 'exemple', 'height=200, width=200, top=300, left=300, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">-->
						  <tbody>
							<tr align="center">
							  <td colspan="2" rowspan="1" style="width: 140px;"><?php echo $ce_jour .' ' .$mois_en_clair .' ' .$an; ?></td>
							</tr>
							<tr>
							  <td style="width: 95px; text-align: left;">&nbsp;place occup&eacute;</td>
							  <td style="width: 22px; text-align: center;"><?php echo $NbrPlaceOccup[$ce_jour]; ?></td>
							</tr>
							<?php
							if ($reservable[$ce_jour]=='OUI')
							{
								?>
								<tr>
								  <td style="width: 95px; text-align: left;">&nbsp;place libre</td>
								  <td style="width: 22px; text-align: center;"><?php echo $NbrPlaceDispo[$ce_jour]; ?></td>
								</tr>
								<?php
							}
							?>
						  </tbody>
						</table>
					</td>
					<?php
				}
			}
			$jour_suiv = ($i+1) - $premier_jour;
			for ($rangee = 0; $rangee <= 4; $rangee++)
			{
				echo '<tr>';
				for ($i = 0; $i < 7; $i++)
				{
					if($jour_suiv > $dernier_jour)
					{
						$rangee = 4;
						?>
						<td style="height: 95px; width: 100px; text-align: center; vertical-align: middle;"></td>
						<?php
					}
					else
					{
						if ($today[$jour_suiv] == 'OUI')
						{
							?>
							<td style="height: 95px; width: 100px; text-align: center; vertical-align: middle; background-color: rgb(164, 246, 255)" onclick="test.php">
							<?php
						}
						else
						{
							?>
							<td style="height: 95px; width: 100px; text-align: center; vertical-align: middle;" onclick="test.php">
							<?php
						}
						?>


							<?php
							if ($reserved[$jour_suiv] == 'OUI')
							{
								?>
								<table style="text-align: left; margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="resaok" onclick="window.open('adm_manageresa.php?ACTION=affichedate&typeplace=<?php echo $typeplace; ?>&user=<?php echo $user; ?>&jour=<?php echo $jour_suiv; ?>&mois=<?php echo $mois;?>&annee=<?php echo $an; ?>&reservable=<?php echo $reservable[$jour_suiv]; ?>&placedispo=<?php echo $NbrPlaceDispo[$jour_suiv]; ?> ', 'exemple', 'height=400, width=400, top=300, left=300, toolbar=no, menubar=no,location=no, resizable=no, scrollbars=no, status=no'); return false;">
								<?php
							}
							else
							{
								?>
								<table style="text-align: left; margin-left: auto; margin-right: auto;" border="1" cellpadding="0" cellspacing="0" class="resa1" onclick="window.open('adm_manageresa.php?ACTION=affichedate&typeplace=<?php echo $typeplace; ?>&user=<?php echo $user; ?>&jour=<?php echo $jour_suiv; ?>&mois=<?php echo $mois;?>&annee=<?php echo $an; ?>&reservable=<?php echo $reservable[$jour_suiv]; ?>&placedispo=<?php echo $NbrPlaceDispo[$jour_suiv]; ?> ', 'exemple', 'height=400, width=400, top=300, left=300, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
								<?php
							}
							?>
							  <tbody>
								<tr align="center">
								  <td colspan="2" rowspan="1" style="width: 140px;"><?php echo $jour_suiv .' ' .$mois_en_clair .' ' .$an; ?></td>
								</tr>
								<tr>
								  <td style="width: 95px; text-align: left;">&nbsp;place occup&eacute;</td>
								  <td style="width: 22px; text-align: center;"><?php echo $NbrPlaceOccup[$jour_suiv]; ?></td>
								</tr>
								<?php
								if ($reservable[$jour_suiv]=='OUI')
								{
									?>
									<tr>
									  <td style="width: 95px; text-align: left;">&nbsp;place libre</td>
									  <td style="width: 22px; text-align: center;"><?php echo $NbrPlaceDispo[$jour_suiv]; ?></td>
									</tr>
									<?php
								}
								?>
							  </tbody>
							</table>
						</td>
					<?php
					}
					$jour_suiv++;
				}
			}
			?>


          </tr>

        </tbody>
		</table>
		</td>
		<?php

		?>
      <td style="height: 102px; width: 245px;">
	  <table class="resa1" style="width: 180px; height: 314px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">
        <tbody>
          <tr align="center">
            <td colspan="1" rowspan="1" style="border-left=1px;">Saisie et r&eacute;servation du mois = <?php echo $nbtotalplaceresaplaceresa; ?></td>
          </tr>
          <tr align="center">
            <td colspan="1" rowspan="1">
			<?php
			if (($_SESSION['STATUT']) == "ADMIN")
			{
				?>
				<input type=button value="Détail" class="bouton1" onclick="window.open('list_resa.php?mois=<?php echo $mois; ?>&an=<?php echo $an; ?>&facture=non', 'exemple', 'height=500, width=500, top=100, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
				<?php
			}
			else
			{
				?>
				<input type=button value="Détail" class="bouton1" onclick="window.open('list_resa.php?mois=<?php echo $mois; ?>&an=<?php echo $an; ?>&user=<?php echo $user; ?> &facture=non', 'exemple', 'height=500, width=500, top=100, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;">
				<?php
			}
			?>
			</td>
          </tr>
		  <tr align="center">
            <td colspan="1" rowspan="1"></td>
          </tr>
          <tr align="center">
            <td colspan="1" rowspan="1">Saisie et r&eacute;servation de l'ann&eacute;e = <?php echo $nbtotalplaceresaplaceresaannuel; ?></td>
          </tr>
          <tr align="center">
            <td colspan="1" rowspan="1"></td>
          </tr>
          <tr align="center">
            <td>Saisie et r&eacute;servation de l'ann&eacute;e factur&eacute;e = <?php echo $nbtotalfacture; ?></td>
          </tr>
          <tr align="center">
            <td></td>
          </tr>
        </tbody>
      </table>


    </tr>
    </tbody>
</table>



	<?php

}

function affiche_place()
{
	?>

	<body>
<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">

  <tbody>
    <tr>
      <td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); width: 105px; font-weight: bold; background-color: rgb(70, 181, 147);">Num&eacute;ro d'emplacement</td>
      <td style="font-family: Calibri; color: rgb(0, 1, 0); width: 176px; font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Libell&eacute;</td>
      <td style="text-align: center; width: 337px; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">D&eacute;signation</td>
      <td style="width: 80px; text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Nombre de place</td>
      <td style="text-align: center; font-family: Calibri; width: 70px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Prix unitaire</td>
      <td style="text-align: center; font-family: Calibri; width: 81px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Prix par personne</td>
      <td colspan="3" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);"><span style="font-family: Calibri;">Type d'occuppation</span></td>
    </tr>
	</tbody></table>

	<center><div id="support1">
	<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">

	<tbody>
	<?php
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from DISPONIBLE";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))
	{
		?>
		<tr>
      <td style="text-align: center; width: 105px;"><b><?php echo $data['DI_NUMERO']; ?></b></td>
      <td style="width: 176px;"><?php echo $data['DI_LIBELLE']; ?></td>
      <td style="width: 337px;"><?php echo $data['DI_COMMENTAIRE']; ?></td>
      <td style="width: 80px;"><?php echo $data['DI_NOMBREPLACE']; ?></td>
      <td style="width: 70px;"><?php echo $data['DI_PRIX']; ?></td>
      <td style="width: 81px;"><?php echo $data['DI_PRIXPLACE']; ?></td>
      <td style="width: 68px;"><?php echo $data['DI_PRIX']; ?></td>
      <td style="width: 30px;"><img border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('manageplace.php?ACTION=manage&numero=<?php echo $data['DI_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
      <td style="width: 30px;"><img border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('delete_place.php?numero=<?php echo $data['DI_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>
	  </tr>

		<?php
		}
	mysql_close;
	?>
	<tr align="center" ><td width="800" colspan="9" rowspan="1">
		<table border = "0" cellpadding="15">
			<tr><td align="center" ><input type=button value="Ajouter un emplacement" class="bouton1" onclick="window.open('ajout_place.php', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td></tr>
		</table>
		</td></tr>
	</table></center>
	<?php

}

function list_resa()
{
	if ($_SESSION['STATUT'] == 'ADMIN')
	{
		if (isset($_GET['datedebut']))
		{
			$datedebut = $_GET['datedebut'];
			$datefin = $_GET['datefin'];
		}
		else
		{
			$datedebut = date("d") .'/'. date("m") . '/'.date("Y");
			$datefin = date("d") .'/'. date("m") . '/'.date("Y");
			$sql = "select date_format(MIN(RE_DATE),'%d/%m/%Y') as DATEDEBUT, MAX(RE_DATE),LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),
					date_format(LAST_DAY(DATE_SUB( MAX(RE_DATE), INTERVAL 1 MONTH)),'%d/%m/%Y') as DATEFIN
					FROM reservation 
					where RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";

			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r)
			{
				$datedebut = $r['DATEDEBUT'];
				$datefin = $r['DATEFIN'];
			}
		}
	?>

	<body>
		<!-- TABLE 1 DEBUT -->

		<style>
			#customers {
			    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			    border-collapse: collapse;
			    width: 100%;
			}

			#customers td, #customers th {
			    border: 1px solid #ddd;
			    padding: 8px;
			}

			#customers tr:nth-child(even){background-color: #f2f2f2;}

			#customers tr:hover {background-color: #ddd;}

			#customers th {
			    padding-top: 12px;
			    padding-bottom: 12px;
			    text-align: left;
			    background-color: #4CAF50;
			    color: white;
			}
		</style>

<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto; font-size : 14px;"cellpadding="0" cellspacing="0" id="customers">
	<form  action="" method="post">
  <tbody>
	<tr>
			<td colspan="6" style="background-color: #edf1f6; ">Critère :</td>
		</tr>
		<tr>
			<td colspan="6" style="background-color: #edf1f6; ">
				<label>Date début: </label>
				<input name="DateDebut" style="padding-left: 50px; border: 0px; solid: #edf1f6" id="datepicker" type="text" size="28" value="<?php echo $datedebut;?>" size="12" required onchange="changedate()"/>
				<label style="padding-left: 50px;border: 0px;">Date fin: </label>
				<input style="padding-left: 50px;border: 0px;" name="DateFin" id="datepicker1" type="text" size="28" value="<?php echo $datefin;?>" size="12" required onchange="changedate()"/>
			</td>
			
				
		</tr>
		<tr>
			<td colspan="6" style="background-color: #edf1f6; "><label style="padding-left: 50px;"></label> </td>
		</tr>
    <tr>
      <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 250px; font-weight: bold; background-color: rgb(70, 181, 147);">Utilisateur
	  <br>
	  <select name="ListUser" id="ListUser">
	  <?php
		//if (((isset($_POST['ListUser'])) && ($_POST['ListUser'] == 'Tous')) || (!isset($_POST['ListUser'])))
		if (!isset($_POST['ListUser']))
		{
			?>
			<option value="Tous" selected="selected">Tous</option>
			<?php
			$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2 from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' GROUP BY RE_USER";
		}
		if (isset($_POST['ListUser']))
		{
			if ($_POST['ListUser'] == 'Tous')
			{
				?>
				<option value="Tous" selected="selected">Tous</option>
				<?php
				$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2 from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND WHERE UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL GROUP BY RE_USER";
			}
			else
			{
				$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_USER = '".$_POST['ListUser']."' AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL GROUP BY RE_USER";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $r)
				{
					?>
					<option value="<?php echo $r['RE_USER']; ?>" selected="selected"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
					<option value="Tous">Tous</option>
					<?php
				}

				$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2  from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_USER <> '".$_POST['ListUser']."' AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL GROUP BY RE_USER";
			}

		}
		else
		{
			?>
			<option value="Tous" selected="selected">Tous</option>
			<?php
			$sql = "select RE_USER, UT_NOM, UT_PRENOM, UT_ID2 from RESERVATION LEFT JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND UT_NOM IS NOT NULL AND UT_PRENOM IS NOT NULL GROUP BY RE_USER";
		}


		// Après Antoine
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $r)
		{
			?>
			<option value="<?php echo $r['RE_USER']; ?>"><?php echo decrypt($r['UT_PRENOM'],$r['UT_ID2']) ." " .decrypt($r['UT_NOM'],$r['UT_ID2']); ?></option>
			<?php

		}




		?>
      </select>
	  </td>
	  <td style="text-align: left; font-family: Calibri; color: rgb(0, 1, 0); width: 200px; font-weight: bold; background-color: rgb(70, 181, 147);">Zone
	  <br>
	  <select name="ListZone" id="ListZone" >
      <?php
		if (((isset($_POST['ListZone'])) && ($_POST['ListZone'] == 'Tous')) || (!isset($_POST['zone'])))
		{
			?>
			<option value="Tous" selected="selected">Tous</option>
			<?php
		}
		if ((isset($_POST['ListZone'])) && ($_POST['ListZone'] != 'Tous'))
		{
			?>
			<option value="Tous" >Tous</option>
			<?php
		}
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from RESERVATION WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' GROUP BY RE_ZONE";



		$req = $conn->query($sql) or die('Erreur SQL !<br>');

		while($data = mysqli_fetch_array($req))
		{
			if (isset($_POST['ListZone']) && $data['RE_ZONE'] == $_POST['ListZone'])
			{
				?>
				<option value="<?php echo $data['RE_ZONE']; ?>" selected="selected"><?php echo $data['RE_ZONELIBELLE']; ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php echo $data['RE_ZONE']; ?>"><?php echo $data['RE_ZONELIBELLE']; ?></option>
				<?php
			}

		}

		?>
      </select>
	  </td>
      <td style="width: 100px;font-family: Calibri; color: rgb(0, 1, 0);font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Date</td>
      <td style="text-align: center; width: 50px; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Facturé
	  <br>
	  <select name="Facture" id="Facture">
			<?php
			if (isset($_POST['Facture']) && $_POST['Facture'] == 'OUI')
			{
				?>
				<option value="Oui" >Oui</option>
				<option value="Tous" >Tous</option>
				<option value="Non" >Non</option>
				<?php
			}
			elseif (isset($_POST['Facture']) && $_POST['Facture'] == 'NON')
			{
				?>
				<option value="Non" >Non</option>
				<option value="Tous" >Tous</option>
				<option value="Oui" >Oui</option>
				<?php
			}
			else
			{
				?>
				<option value="Tous" >Tous</option>
				<option value="OUI" >Oui</option>
				<option value="NON" >Non</option>
				<?php
			}
			?>
	  </select>
	  </td>
      <td style="width: 80px; text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Ref. Facture</td>
	  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);"><button value="Valid" name="Valid">Valider</button></td>
      </tr>
	  </form>

<!-- DEBUT -->


<?php
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$critere = 0;
$sqlcritere = " WHERE RE_VALIDEE = 'OUI' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN'] ."'";
if ((isset($_POST['ListUser'])) && ($_POST['ListUser'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND RE_USER = '".$_POST['ListUser']."' ";
	}
	else
	{
		$sqlcritere = $sqlcritere ." AND RE_USER = '".$_POST['ListUser']."' ";
	}
	$critere = 1;

}
if ((isset($_POST['ListZone'])) && ($_POST['ListZone'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND RE_ZONE = '".$_POST['ListZone']."' ";
	}
	else
	{
		$sqlcritere = $sqlcritere ." AND RE_ZONE = '".$_POST['ListZone']."' ";
	}
	$critere = 1;

}

if ((isset($_POST['Facture'])) && ($_POST['Facture'] != 'Tous'))
{
	if ($critere == 1)
	{
		$sqlcritere = $sqlcritere ." AND RE_FACTURE = '".$_POST['Facture']."' ";
	}
	else
	{
		$sqlcritere = $sqlcritere ." AND RE_FACTURE = '".$_POST['Facture']."' ";
	}
	$critere = 1;

}

if ($critere == 1)
{
	$sql = "select UT_ID2, UT_PRENOM, UT_NOM, RE_USER, RE_ZONELIBELLE, DATE_FORMAT(RE_DATE, '%d/%m/%Y') as RE_DATE, RE_FACTURE, RE_REFFACTURE from RESERVATION INNER JOIN UTILISATEUR ON UT_LOGIN = RE_USER" .$sqlcritere ." ORDER BY RE_DATE";
}
else
{
	$sql = "select UT_ID2, UT_PRENOM, UT_NOM, RE_USER, RE_ZONELIBELLE, DATE_FORMAT(RE_DATE, '%d/%m/%Y') as RE_DATE, RE_FACTURE, RE_REFFACTURE from RESERVATION  INNER JOIN UTILISATEUR ON UT_LOGIN = RE_USER WHERE RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' ORDER BY RE_DATE";
}

$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
foreach ($tab_r as $data)
{
	?>
	<tr>
		<td><?php echo decrypt($data['UT_PRENOM'], $data['UT_ID2']) . " ". decrypt($data['UT_NOM'], $data['UT_ID2']); ?></td>
		<td><?php echo $data['RE_ZONELIBELLE']; ?></td>
		<td><?php echo $data['RE_DATE']; ?></td>
		<td><?php echo $data['RE_FACTURE']; ?></td>
		<td colspan="2"><?php echo $data['RE_REFFACTURE']; ?></td>
	</tr>
<?php
}
?>


<!-- FIN -->




	<!-- </tbody></table> -->
	<!-- TABLE 1 FIN -->

		<!-- TABLE 2 -->

	<!-- TABLE 2 FIN -->
	<?php
	}

}

function update_zone()
{
	
			
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "UPDATE ZONE SET EM_LIBELLE = '" .$_POST['LIBELLE_'.$_POST['IDModif']] ."', EM_NBRRESAMAX = " .$_POST['NBRRESA_'.$_POST['IDModif']] .",EM_TYPEZONE = '" .$_POST['TYPEZONE_'.$_POST['IDModif']] ."',
					EM_NBRPLACESMAX = " .$_POST['NBRPLACE_'.$_POST['IDModif']] .", EM_PRIXPARZONE = '" .$_POST['PRIX_'.$_POST['IDModif']] ."', EM_PRIXPARPLACE = '" .$_POST['PRIXPARPERSONNE_'.$_POST['IDModif']] ."' ,
					EM_ARTZONE = '".$_POST['ARTZONE_'.$_POST['IDModif']]."', EM_ARTPLACE = '".$_POST['ARTPLACE_'.$_POST['IDModif']]."',
					EM_HEUREDEBUT = '".$_POST['HEUREDEBUT_'.$_POST['IDModif']] ."', EM_HEUREFIN = '".$_POST['HEUREFIN_'.$_POST['IDModif']]."', EM_BLOQUE = '".$_POST['ZONEBLOQUE_'.$_POST['IDModif']]."'
					WHERE EM_EMPLACEMENT = " .$_POST['ZONE_'.$_POST['IDModif']].";";
			
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			
}

function insert_zone()
{

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	
	$sql = "INSERT INTO ZONE (EM_LIBELLE, EM_NBRRESAMAX, EM_TYPEZONE, EM_NBRPLACESMAX, EM_PRIXPARZONE, EM_PRIXPARPLACE, EM_ARTZONE, EM_ARTPLACE, EM_HEUREDEBUT, EM_HEUREFIN, EM_BLOQUE, EM_ETABLISSEMENT)
			values ('" .$_POST['LIBELLE_0'] ."', " .$_POST['NBRRESA_0'] .", '" .$_POST['TYPEZONE_0'] ."', " .$_POST['NBRPLACE_0'] .",
			'" .$_POST['PRIX_0'] ."', '" .$_POST['PRIXPARPERSONNE_0'] ."' ,'".$_POST['ARTZONE_0']."', '".$_POST['ARTPLACE_0']."',
			'".$_POST['HEUREDEBUT_0'] ."', '".$_POST['HEUREFIN_0']."', '".$_POST['ZONEBLOQUE_0']."','".$_SESSION['ETABADMIN']."');";
			
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
			
}


function affiche_zone()
{
	
	?>
	
	<form  action="" method="post">

	<?php
	$nbrarticle = 0;
	$sql = 'SELECT * FROM ARTICLE WHERE AR_TYPE = "ARTRE" AND AR_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'" ;';
	
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data1)
	{
		$nbrarticle ++;
		// echo $data1['AR_CODEARTICLE'];
		?>
		<div class="infoprixarticle" id="<?php echo $data1['AR_CODEARTICLE'];?>_PrixArticle" style="display: none;"><?php echo $data1['AR_TARIF'];?></div>
		<div class="infoheuredebut" id="<?php echo $data1['AR_CODEARTICLE'];?>_HeureDebut" style="display: none;"><?php echo $data1['AR_RESAHEUREDEBUT'];?></div>
		<div class="infoheurefin" id="<?php echo $data1['AR_CODEARTICLE'];?>_HeureFin" style="display: none;"><?php echo $data1['AR_RESAHEUREFIN'];?></div>
		<?php
	}
	
	$sql = 'SELECT * FROM ZONE WHERE EM_ETABLISSEMENT = "'.$_SESSION['ETABADMIN'].'" ;';
	$nbrzone = 0;
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data1)
	{
		?>
		<div class="infozone" id="<?php echo $nbrzone;?>_infolibzone" style="display: none;"><?php echo $data1['EM_LIBELLE'];?></div>
		<div class="infozone" id="<?php echo $nbrzone;?>_infocodezone" style="display: none;"><?php echo $data1['EM_EMPLACEMENT'];?></div>
		<?php
		$nbrzone ++;
	}
	
	$nbrlistzone = 0;
	?>
	<center><div id="support1">
	<table  style="width: 90%; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="0" cellspacing="0">

	<tbody>
	<tr>
		<td colspan="2" style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle; width: 40%;" align="center">Zone</td>
		<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;width: 30%;" align="center">Type Zone</td>
		<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;width: 10%;" align="center">Nombre de place</td>
		<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;width: 10%;" align="center">Nombre de personne maximum par emplacement</td>
		<td style="font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;right;width: 10%;" align="center">
			<input type=button id="modifzone_<?php echo (($nbrlistzone) * 100) + 15; ?>"  value="Ajouter une nouvelle zone" class="bouton3" onclick="javascript:afficher(<?php echo $nbrlistzone * 100; ?>);"/>
		</td>
		
	</tr>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 1; ?>" name="modifzone_<?php echo ($nbrlistzone) * 100; ?>" colspan="3" style="width: 304px; height: 28px; padding-top: 5px; display: none;">
			<LABEL>Nom de l'emplacement : </label>
			<div class="infozone" id="<?php echo $nbrlistzone;?>_libzoneorg" style="display: none;"></div>
			<input maxlength="30" size="30" tabindex="1" name="LIBELLE_<?php echo $nbrlistzone; ?>" value='' onchange="veriflibzone(<?php echo $nbrlistzone; ?>)">
		</td>
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 2; ?>" colspan="4" style="width: 304px; height: 28px; padding-top: 5px; display: none;">
			<label>Type de zone : </label>
			<SELECT name="TYPEZONE_<?php echo $nbrlistzone; ?>" tabindex="2" onchange="changezone(<?php echo $nbrlistzone; ?>)">
			<option value="">Choisir une zone !</option>
			<?php
			$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'TYPEZONE';";
			$cnx_bdd = ConnexionBDD();
			$result_req = $cnx_bdd->query($sql);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $data1)
			{
				echo '<OPTION value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
			}
			?>
			</SELECT>
		</td>
	</tr>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 3; ?>" colspan="3" style="display: none;">
			<label>Article de l'emplacement : </label>
			<SELECT name="ART1_<?php echo $nbrlistzone; ?>" style="width: 350px;" tabindex="3" style="padding-top: 5px; " onchange="changeart1(<?php echo $nbrlistzone; ?>)">
				<option value="">Choisir un article !</option>
				<?php
				$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
				}
				?>
			</SELECT>
		</td>
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 4; ?>" colspan="4" style="padding-top: 5px; display: none;">
			<LABEL>Nombre de place disponible : </label>
			<input maxlength="30" size="30" tabindex="4" name="NBRRESA_<?php echo $nbrlistzone; ?>" value=''>
		</td>
	</tr>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 5; ?>" colspan="2" style="display: none;">
			<label>Article de l'utilisateur lié à l'emplacement : </label>
			<SELECT name="ART2_<?php echo $nbrlistzone; ?>" style="width: 350px;" tabindex="5" style="padding-top: 5px; display: none;" onchange='changeart2(<?php echo $nbrlistzone; ?>)'>
				<option value="">Choisir un article !</option>
				<?php
				$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
				}
				?>
			</SELECT>
		</td>
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 6; ?>" colspan="4" style="padding-top: 5px;display: none;">
			<LABEL>Nombre de personne maximum par place : </label>
			<input maxlength="30" size="30" tabindex="4" name="NBRPLACE_<?php echo $nbrlistzone; ?>" id="NBRPLACE_<?php echo $nbrlistzone; ?>" value=''>
		</td>
	</tr>
	
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 7; ?>"colspan="2" style="width: 304px; height: 28px; display: none">
			<label id="HEUREDEBUTTXT">Heure de début : </label>
			<input type="text" maxlength="5" size="5" tabindex="6" id="AFFHEUREDEBUT_<?php echo $nbrlistzone; ?>" name="AFFHEUREDEBUT_<?php echo $nbrlistzone; ?>" disabled value='' style="width: 50px">
			<input type="hidden" maxlength="5" size="5" tabindex="6" id="HEUREDEBUT_<?php echo $nbrlistzone; ?>" name="HEUREDEBUT_<?php echo $nbrlistzone; ?>"  value='' style="width: 50px">
		</td>
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 8; ?>" colspan="5" style="width: 304px; height: 28px;display: none;">
			<label id="HEUREDEBUTTXT">Heure de fin : </label>
			<input type="text" maxlength="5" size="5" tabindex="7" id="AFFHEUREFIN_<?php echo $nbrlistzone; ?>" name="AFFHEUREFIN_<?php echo $nbrlistzone; ?>" disabled value=''style="width: 50px">
			<input type="hidden" maxlength="5" size="5" tabindex="7" id="HEUREFIN_<?php echo $nbrlistzone; ?>" name="HEUREFIN_<?php echo $nbrlistzone; ?>"  value=''style="width: 50px">
		</td>
		
	</tr>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 9; ?>" colspan="7" style="padding-top: 5px;display: none;">
			<label>Zone Bloqué : </label>
			<SELECT name="ZONEBLOQUE_<?php echo $nbrlistzone; ?>" style="width: 80px;" tabindex="8" >
				<OPTION selected="selected" value="NON">NON</option>
				<OPTION value="OUI">OUI</option>
			</SELECT>
		</td>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone * 100) + 21); ?>" colspan="3" align="center" style="padding-top: 5px; padding-bottom: 5px; display: none;">
			<input class="bouton1" value="Valider" type="submit" name="BoutonValider" id="<?php echo $nbrlistzone; ?>">
		</td>
		<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 10; ?>" colspan="4" align="center" style="padding-top: 5px; padding-bottom: 5px; display: none">
			<input type=button value="Fermer" class="bouton1" onclick="javascript:cacher(<?php echo $nbrlistzone * 100; ?>);">
		</td>
	</tr>
	<tr style="background-color: #edf1f6; ">
		<td id="modifzone_<?php echo (($nbrlistzone * 100) + 20); ?>" colspan="8" style="height: 10px; display: none;">
			
			<input type="hidden" id="paramuser_011_<?php echo $nbrlistzone ; ?>" name="USERMODIF<?php echo $nbrlistzone; ?>" value='NON'>
			<input type="hidden" name="ARTZONE_<?php echo $nbrlistzone ; ?>" id="ARTZONE_<?php echo $nbrlistzone ; ?>" value='' />
			<input type="hidden" name="ARTPLACE_<?php echo $nbrlistzone ; ?>" id="ARTPLACE_<?php echo $nbrlistzone ; ?>" value='' />
			<input type="hidden" name="PRIX_<?php echo $nbrlistzone ; ?>" id="PRIX_<?php echo $nbrlistzone ; ?>" value='0' />
			<input type="hidden" name="PRIXPARPERSONNE_<?php echo $nbrlistzone ; ?>" id="PRIXPARPERSONNE_<?php echo $nbrlistzone ; ?>" value='0' />
		</td>
	</tr>
	<?php
	
	
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from ZONE where EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	
	while($data = mysqli_fetch_array($req))
	{
		$nbrlistzone ++;
		?>
		<tr>
		<?php
		if ($data['EM_BLOQUE'] == 'OUI')
		{
			?>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 31; ?>" style="width: 10%;text-align: center; color: red; " align="center"><?php echo $data['EM_EMPLACEMENT']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 32; ?>" style="width: 30%;text-align: left; color: red;"><?php echo $data['EM_LIBELLE']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 33; ?>" style="width: 30%; color: red;" align="center"><?php echo $data['EM_TYPEZONE']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 34; ?>" style="width: 10%; color: red;" align="center"><?php echo $data['EM_NBRRESAMAX']; ?> </td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 35; ?>" style="width: 10%; color: red;" align="center"><?php echo $data['EM_NBRPLACESMAX']; ?> </td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 36; ?>" style="width: 10%;" align="center"><img id="modifzone_<?php echo (($nbrlistzone) * 100); ?>" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:afficher(<?php echo $nbrlistzone * 100; ?>);"/></td>
			<?php
		}
		else
		{
			?>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 31; ?>" style="width: 10%;text-align: center; color: green; " align="center"><?php echo $data['EM_EMPLACEMENT']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 32; ?>" style="width: 30%;text-align: left; color: green;"><?php echo $data['EM_LIBELLE']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 33; ?>" style="width: 30%; color: green;" align="center"><?php echo $data['EM_TYPEZONE']; ?></td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 34; ?>" style="width: 10%; color: green;" align="center"><?php echo $data['EM_NBRRESAMAX']; ?> </td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 35; ?>" style="width: 10%; color: green;" align="center"><?php echo $data['EM_NBRPLACESMAX']; ?> </td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 36; ?>" style="width: 10%;" align="center"><img id="modifzone_<?php echo (($nbrlistzone) * 100); ?>" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:afficher(<?php echo $nbrlistzone * 100; ?>);"/></td>
		<?php
		}
		?>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 1; ?>" name="modifzone_<?php echo ($nbrlistzone) * 100; ?>" colspan="3" style="width: 304px; height: 28px; padding-top: 5px; display: none;">
				<LABEL>Nom de l'emplacement : </label>
				<div class="infozone" id="<?php echo $nbrlistzone;?>_libzoneorg" style="display: none;"><?php echo $data['EM_LIBELLE'];?></div>
				<input maxlength="30" size="30" tabindex="1" name="LIBELLE_<?php echo $nbrlistzone; ?>" id="LIBELLE_<?php echo $nbrlistzone; ?>" value='<?php echo $data['EM_LIBELLE']; ?>' onchange="veriflibzone(<?php echo $nbrlistzone; ?>)">
			</td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 2; ?>" colspan="4" style="width: 304px; height: 28px; padding-top: 5px; display: none;">
				<label>Type de zone : </label>
				<SELECT name="TYPEZONE_<?php echo $nbrlistzone; ?>" tabindex="2" onchange="changezone(<?php echo $nbrlistzone; ?>)">
				<?php
				$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'TYPEZONE';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					echo $data['EM_TYPEZONE'];
					if ($data1['CC_LIBELLE'] == $data['EM_TYPEZONE'])
					{
						echo '<OPTION selected="selected" value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
					}
					else
					{
						echo '<OPTION value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
					}
				}
				?>
				</SELECT>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 3; ?>" colspan="3" style="display: none;">
				<label>Article de l'emplacement : </label>
				<SELECT name="ART1_<?php echo $nbrlistzone; ?>" style="width: 350px;" tabindex="3" style="padding-top: 5px; " onchange="changeart1(<?php echo $nbrlistzone; ?>)">
					<?php
					$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{

						if ($data1['AR_CODEARTICLE'] == $data['EM_ARTZONE'])
						{
							echo '<OPTION selected="selected" value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
							
						}
						else
						{
							echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
						}
					}
					?>
				</SELECT>
			</td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 4; ?>" colspan="4" style="padding-top: 5px; display: none;">
				<LABEL>Nombre de place disponible : </label>
				<input maxlength="30" size="30" tabindex="4" name="NBRRESA_<?php echo $nbrlistzone; ?>" value='<?php echo $data['EM_NBRRESAMAX']; ?>'>
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 5; ?>" colspan="3" style="display: none;">
				<label>Article de l'utilisateur lié à l'emplacement : </label>
				<SELECT name="ART2_<?php echo $nbrlistzone; ?>" style="width: 350px;" tabindex="5" style="padding-top: 5px; display: none;" onchange='changeart2(<?php echo $nbrlistzone; ?>)'>
					<option value="">Choisir un article !</option>
					<?php
					$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{

						if ($data1['AR_CODEARTICLE'] == $data['EM_ARTPLACE'])
						{
							echo '<OPTION selected="selected" value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
							
						}
						else
						{
							echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
						}
					}
					?>
				</SELECT>
			</td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 6; ?>" colspan="4" style="padding-top: 5px;display: none;">
				<LABEL>Nombre de personne maximum par place : </label>
				<input maxlength="30" size="30" tabindex="4" name="NBRPLACE_<?php echo $nbrlistzone; ?>" id="NBRPLACE_<?php echo $nbrlistzone; ?>" value='<?php echo $data['EM_NBRPLACESMAX']; ?>'>
			</td>
		</tr>
		
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 7; ?>"colspan="2" style="width: 304px; height: 28px; display: none">
				<label id="HEUREDEBUTTXT">Heure de début : </label>
				<input type="text" maxlength="5" size="5" tabindex="6" id="AFFHEUREDEBUT_<?php echo $nbrlistzone; ?>" name="AFFHEUREDEBUT_<?php echo $nbrlistzone; ?>" disabled value='<?php echo $data['EM_HEUREDEBUT']; ?>' style="width: 50px">
				<input type="hidden" maxlength="5" size="5" tabindex="6" id="HEUREDEBUT_<?php echo $nbrlistzone; ?>" name="HEUREDEBUT_<?php echo $nbrlistzone; ?>"  value='<?php echo $data['EM_HEUREDEBUT']; ?>' style="width: 50px">
			</td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 8; ?>" colspan="5" style="width: 304px; height: 28px;display: none;">
				<label id="HEUREDEBUTTXT">Heure de fin : </label>
				<input type="text" maxlength="5" size="5" tabindex="7" id="AFFHEUREFIN_<?php echo $nbrlistzone; ?>" name="AFFHEUREFIN_<?php echo $nbrlistzone; ?>" disabled value='<?php echo $data['EM_HEUREFIN']; ?>'style="width: 50px">
				<input type="hidden" maxlength="5" size="5" tabindex="7" id="HEUREFIN_<?php echo $nbrlistzone; ?>" name="HEUREFIN_<?php echo $nbrlistzone; ?>"  value='<?php echo $data['EM_HEUREFIN']; ?>'style="width: 50px">
			</td>
			
        </tr>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 9; ?>" colspan="7" style="padding-top: 5px;display: none;">
				<label>Zone Bloqué : </label>
				<SELECT name="ZONEBLOQUE_<?php echo $nbrlistzone; ?>" style="width: 80px;" tabindex="8" >
					<?php
					if ($data['EM_BLOQUE'] == 'OUI')
					{
						echo '<OPTION selected="selected" value="OUI">OUI</option>';
						echo '<OPTION value="NON">NON</option>';
					}
					else
					{
						echo '<OPTION selected="selected" value="NON">NON</option>';
						echo '<OPTION value="OUI">OUI</option>';
					}
					?>
				</SELECT>
			</td>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone * 100) + 21); ?>" colspan="3" align="center" style="padding-top: 5px; padding-bottom: 5px; display: none;">
				<input class="bouton1" value="Valider" type="submit" name="BoutonValider" id="<?php echo $nbrlistzone; ?>">
			</td>
			<td id="modifzone_<?php echo (($nbrlistzone) * 100) + 10; ?>" colspan="4" align="center" style="padding-top: 5px; padding-bottom: 5px; display: none">
				<input type=button value="Fermer" class="bouton1" onclick="javascript:cacher(<?php echo $nbrlistzone * 100; ?>);">
			</td>
		</tr>
		<tr style="background-color: #edf1f6; ">
			<td id="modifzone_<?php echo (($nbrlistzone * 100) + 20); ?>" colspan="8" style="height: 10px; display: none;">
				
				<input type="hidden" id="paramuser_011_<?php echo $nbrlistzone ; ?>" name="USERMODIF<?php echo $nbrlistzone; ?>" value='NON'>
				<input type="hidden" name="ZONE_<?php echo $nbrlistzone ; ?>" id="ZONE_<?php echo $nbrlistzone ; ?>" value='<?php echo $data['EM_EMPLACEMENT']; ?>' />
				<input type="hidden" name="ARTZONE_<?php echo $nbrlistzone ; ?>" id="ARTZONE_<?php echo $nbrlistzone ; ?>" value='<?php echo $data['EM_ARTZONE']; ?>' />
				<input type="hidden" name="ARTPLACE_<?php echo $nbrlistzone ; ?>" id="ARTPLACE_<?php echo $nbrlistzone ; ?>" value='<?php echo $data['EM_ARTPLACE']; ?>' />
				<input type="hidden" name="PRIX_<?php echo $nbrlistzone ; ?>" id="PRIX_<?php echo $nbrlistzone ; ?>" value='<?php echo $data['EM_PRIXPARZONE']; ?>' />
				<input type="hidden" name="PRIXPARPERSONNE_<?php echo $nbrlistzone ; ?>" id="PRIXPARPERSONNE_<?php echo $nbrlistzone ; ?>" value='<?php echo $data['EM_PRIXPARPLACE']; ?>' />
			</td>
		</tr>

		<?php
		}

	?>
	<tr align="center" ><td width="800" colspan="9" rowspan="1">
		<input type="hidden" name="IDModif" id="IDModif" value="">
	</td></tr>
	</table></center>
	</form>
	<?php
	
	
}


function insert_new_place()
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = " select * from ZONE inner join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT
	where EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN'] ."' AND EM_EMPLACEMENT = '" .$_POST['ZONE'] ."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while ($data = mysqli_fetch_array($req))
	{
		$ET_LIBELLE = $data['ET_LIBELLE'];
		$ET_ADRESSE1 = $data['ET_ADRESSE1'];
		$ET_ADRESSE2 = $data['ET_ADRESSE2'];
		$ET_ADRESSE3 = $data['ET_ADRESSE3'];
		$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];
		$ET_VILLE = $data['ET_VILLE'];
		$EM_EMPLACEMENT = $data['EM_EMPLACEMENT'];
		$EM_LIBELLE = $data['EM_LIBELLE'];
	}
	$sql = "insert into DISPONIBLE (DI_LIBELLE, DI_COMMENTAIRE, DI_NOMBREPLACE, DI_PRIX, DI_PRIXPLACE) VALUES ";
	$sql = $sql ."('".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE'] ;
	$sql = $sql ."," .$_POST['PRIX'] ."," .$_POST['PRIXPARPERSONNE'] .")";

	$sql = "INSERT INTO `DISPONIBLE`(`DI_ETABLISSEMENT`, `DI_ETADRESSE1`, `DI_ETADRESSE2`, `DI_ETADRESSE3`,
			`DI_ETCODEPOSTAL`, `DI_ETVILLE`, `DI_ZONE`, `DI_LIBELLEZONE`, `DI_LIBELLE`, `DI_COMMENTAIRE`, `DI_NOMBREPLACE`, `DI_PRIX`, `DI_PRIXPLACE`,
			`Di_TYPEJOUR`) VALUES
			('".$_SESSION['ETABADMIN'] ."','".$ET_ADRESSE1."','".$ET_ADRESSE2."','".$ET_ADRESSE3."','".$ET_CODEPOSTAL."','".$ET_VILLE."','".$_POST['ZONE']."',
			'".$EM_LIBELLE."','".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE']."," .$_POST['PRIX'] .","
			.$_POST['PRIXPARPERSONNE'] .",".$_POST['TEMPSLOCATION'].")";

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	echo "Création de l'emplacement " .$_POST['LIBELLE'] ."terminé";

	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function insert_new_zone()
{
	$libellezone = $_POST['LIBELLE'];
	$typezone = $_POST['TYPEZONE'];
	if ($typezone == 'COWORKING')
	{
		$nbrpersonne = $_POST['NBRPLACE'];
		$nbrplace = 0;
		$artpersonne = '';
	}
	else
	{
		$nbrplace = $_POST['NBRPLACE'];
		$nbrpersonne = 0;
		$artpersonne = $_POST['ART4'];
	}
	
	$sql = "INSERT INTO ZONE(EM_ETABLISSEMENT, EM_LIBELLE, EM_NBRRESAMAX, EM_TYPEZONE, EM_NBRPLACESMAX, EM_DEMIJOURNEE, EM_ARTJOURNEE, EM_ARTMATIN, EM_ARTAPM, EM_ARTPERSONNE, EM_HEUREDEBUT, EM_HEUREFIN, EM_ARTZONE, EM_ARTPLACE)
					VALUES ('".$_SESSION['ETABADMIN'] ."','".$libellezone ."', ".$nbrpersonne.", '".$typezone."',$nbrplace,'".$demijournee."','". $ARTJOUR."','".$ARTMATIN."','".$ARTAPM."','".$artpersonne."','00:00','00:00','',''); ";
	
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);
	echo "Création de la zone " .$libellezone ." terminée!";

	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}

function new_zone()
{
?>
	<br />
<form  action="" method="post">
<table style='text-align: left; width: 90%; height: 281px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Création d'un emplacement</td>
			<tr>
				<td style="width: 304px; height: 28px;">Nom de l'emplacement</td>
				<td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" required></td>
				<td style="width: 304px; height: 28px;"><label>Type de zone : </label></td>
				<td>
					<select name="TYPEZONE" id="TYPEZONE" tabindex="2">
						<option value="COWORKING">Coworking</option>
						<option value="REUNION">Salle de réunion</option>
					</select>
				</td>
			<tr>
			  <td style="width: 304px; height: 28px;"><label id="NBRPLACETXT">Nombre d'emplacement (maximum)</label></td>
			  <td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="3" name="NBRPLACE" id="NBRPLACE" required></td>

			</tr>
			
			<tr>

			  <td style="height: 28px;"><label id="LISTART1TXT">Article de la zone</label></td>
			  <td colspan="3" style="height: 28px;">
			  <select name="ART1" id="ART1" tabindex="6" required>
				<option value="">Choisir un article !</option>
				<?php
				$sql = "select * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					?>
					<option value="<?php echo $data1['AR_ARTICLENO']; ?>"><?php echo $data1['AR_CODEARTICLE'] .' - ' .$data1['AR_DESIGNATION'] .' - ' .$data1['AR_TARIF']; ?></option>
					<?php
				}
				?>
				</select>
				</td>

			</tr>

			
			<tr>
			  <td style="height: 28px;"><label id="LISTART4TXT">Article par personne (dans le cadre d'une salle de réunion</label></td>
			  <td style="height: 28px;">
			  <select name="ART4" id="ART4" tabindex="9" >
				<option value="">Choisir un article !</option>
				<?php
				$sql = "select * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					?>
					<option value="<?php echo $data1['AR_ARTICLENO']; ?>"><?php echo $data1['AR_CODEARTICLE'] .' - ' .$data1['AR_DESIGNATION'] .' - ' .$data1['AR_TARIF']; ?></option>
					<?php
				}
				?>
				</select>
				</td>

			</tr>


			<tr align="center">
			  <td colspan="4" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
<input type="hidden" value="AJOUT" name="action">
</form>
<?php
}


function delete_place()
{

	if(!isset($_POST["action"]))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from DISPONIBLE WHERE DI_NUMERO =" .$_GET['numero'];
		$req = $conn->query($sql) or die('Erreur SQL !<br>');

		while($data = mysqli_fetch_array($req))
		{
?>
		<br />
		<form  action="" method="post">
		<table style="text-align: left; width: 474px; height: 281px;" border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Suppression d'un emplacement</td>
			<tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Numéro de l'emplacement</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['DI_NUMERO']; ?></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nom de l'emplacement</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['DI_LIBELLE']; ?></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">D&eacute;signationde l'emplacement</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['DI_COMMENTAIRE']; ?></td>
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;">Prix de l'emplacement</td>
				<td style="height: 28px; width: 212px;"><?php echo $data['DI_PRIX']; ?></td>
			</tr>
			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Supprimer</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="DELETE" name="action">
		</form>

<?php

		}
	}
	else
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "DELETE from DISPONIBLE WHERE DI_NUMERO =" .$_GET['numero'];

		$req = $conn->query($sql) or die('Erreur SQL !<br>');


		echo "Suppression de l'emplacement " .$_POST['LIBELLE'] ."terminé";

		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}

function delete_zone()
{

	if(!isset($_POST["action"]))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from ZONE WHERE EM_EMPLACEMENT ='" .$_GET['numero'] ."' AND EM_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']. "'" ;

		$req = $conn->query($sql) or die('Erreur SQL !<br>');

		while($data = mysqli_fetch_array($req))
		{
?>
		<br />
		<form  action="" method="post">
		<table style="text-align: left; width: 474px; height: 281px;" border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="2" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Suppression d'une zone</td>
			<tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Nom de la zone</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['EM_EMPLACEMENT']; ?></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Désignation de la zone</td>
			  <td style="height: 28px; width: 212px;"><?php echo $data['EM_LIBELLE']; ?></td>
			</tr>

			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Supprimer</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="DELETE" name="action">
		</form>

<?php

		}
	}
	else
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "DELETE from ZONE WHERE EM_EMPLACEMENT = '" .$_GET['numero']. "'";

		$req = $conn->query($sql) or die('Erreur SQL !<br>');


		echo "Suppression de la zone " .$_POST['LIBELLE'] ." terminée !";

		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}

function affiche_date()
{
	$nbplaceresa=0;
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$_GET['user'] ."' and RE_ANNEE = ".$_GET['annee'] ."
			and RE_MOIS = ".$_GET['mois']." and RE_JOUR = ".$_GET['jour']." and RE_ZONE = '".$_GET['typeplace']."'
			AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbplaceresa = $data['NBRESA'];
	}
	$nbplacedispo=0;
	$nbplacemax=0;
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select EM_NBRRESAMAX - COUNT(*) AS NBRDISPO, EM_NBRPLACESMAX
			FROM ZONE
			LEFT JOIN RESERVATION ON EM_EMPLACEMENT = RE_ZONE
			where RE_ANNEE = ".$_GET['annee'] ." and RE_MOIS = ".$_GET['mois']."
			and RE_JOUR = ".$_GET['jour']." and EM_EMPLACEMENT = '".$_GET['typeplace']."' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbplacedispo = $data['NBRDISPO'];
		$nbplacemax = $data['EM_NBRPLACESMAX'];
	}

	?>

	<table style="text-align: center; margin-left: auto; margin-right: auto; width: 400px;" border="0">
	<tbody>
    <form action="" method="post" name="resa">
	<input value="ajout" name="action" type="hidden">

	<?php
	if ($nbplacedispo > 0)
	{
		$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");
		$mois_en_clair = $mois_de_annee[$_GET['mois'] - 1];
		?>
		<tr>
			 <td colspan="2" rowspan="1" style="width: 140px;"><div id="demotext"><?php echo $_GET['jour'] .' ' .$mois_en_clair .' ' .$_GET['annee']; ?></DIV></td>
		</tr>
		<tr><td>


		<table style="text-align: center; margin-left: auto; margin-right: auto; width: 350px;" border="0" class="resa1">
		<tbody>

		<tr>
			<?php
			if ($_GET['reservable'] == 'OUI')
			{
				echo '<td colspan="2" rowspan="1"><div id="demotext">Ajouter une réservation</div></td>';
			}
			else
			{
				echo '<td colspan="2" rowspan="1"><div id="demotext">Ajouter une saisie</div></td>';
			}
			?>
		</tr>
		<?php
		if (($_SESSION['STATUT']) == "ADMIN")
		{
			?>
			<tr>
				<td colspan="2" rowspan="1"><label>Utilisateur : </label>
					<select name="user" id="user">
					<option value="-1">Sélectionner</option>
						<?php
						$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
						$sql = "SELECT UT_LOGIN, UT_PRENOM, UT_NOM, UT_ID1, UT_ID2 FROM UTILISATEUR WHERE UT_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'";
						$req = $conn->query($sql) or die('Erreur SQL !<br>');
						while($data = mysqli_fetch_array($req))
						{
							?>
							<option value="<?php echo $data['UT_LOGIN']; ?>"><?php echo decrypt($data['UT_PRENOM'],$data['UT_ID2']) .' ' .decrypt($data['UT_NOM'],$data['UT_ID2']); ?></option>';
							<?php
						}
						?>
					</select>
				</td>
			</tr>
			<?php
		}
		else
		{
			?>
			<input name="user" value="<?php echo $_SESSION['login'];?>" type="hidden">
			<?php
		}
		?>

		<tr>
			<td colspan="2" rowspan="1"><label>Emplacement : </label>
				<select name="ajout_emplacement" id="ajout_emplacement">
				<option value="-1">Sélectionner</option>
					<?php
					for ($i = 1; $i <= $nbplacedispo; $i++)
					{
					?>

					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2" rowspan="1">
			<label>Nombre de place : </label>
				<div id='ajout_nbrplace' style='display:inline'>
				<select name='ajout_nbrplace' id='ajout_nbrplace'>
					<option value='-1'>Sélectionner</option>
					<?php
					for ($i = 1; $i <= $nbplacemax; $i++)
					{
					?>

					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php
					}
					?>
				</select>
				</div>
			</td>
        </tr>
		<tr>
			<td align="center" colspan="2" rowspan="1" style="width: 20%;"><button class="boutonresa2" value="AddResa" name="Panier" >Ajouter </button></td>
			<br/><span id="test"></span>
			<input value="" name="nbrplace" id="nbrplace" type="hidden">
		</tr>
		</tbody>
		</table>
	</td></tr>
    <tr>
      <td style="height: 30px;"></td>
    </tr>
	<?php
	}
	?>
    <tr><td>
		<table style="text-align: left; margin-left: auto; margin-right: auto; width: 350px;" border="0" class="resa1">
		<tbody>
		<tr>
			<td colspan="2" rowspan="1"><div id="demotext">Supprimer une réservation</div></td>
		</tr>
		<tr>
			<td colspan="2" rowspan="1"><label>Emplacement : </label>
				<select name="delete_emplacement" id="delete_emplacement" onchange='go()'>
				<option value="-1">Sélectionner</option>
					<?php
					$userselect = '';
					echo $_SESSION['STATUT'];
					$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
					if (($_SESSION['STATUT']) == "ADMIN")
					{
						$sql = "select RE_NUMRESA, RE_USER,RE_LIBELLEEMPLACEMENT from RESERVATION where RE_ANNEE = ".$_GET['annee'] ." and RE_MOIS = ".$_GET['mois']."
							and RE_JOUR = ".$_GET['jour']." and RE_ZONE = '".$_GET['typeplace']."'
							AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."' AND RE_VALIDEE = 'OUI'
							 ORDER BY RE_USER";
					}
					else
					{
						$sql = "select RE_NUMRESA, RE_USER,RE_LIBELLEEMPLACEMENT from RESERVATION where RE_ANNEE = ".$_GET['annee'] ." and RE_MOIS = ".$_GET['mois']."
							and RE_JOUR = ".$_GET['jour']." and RE_ZONE = '".$_GET['typeplace']."' AND RE_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."'
							AND RE_USER = '".$_GET['user']."' ORDER BY RE_USER";
					}
					$req = $conn->query($sql) or die('Erreur SQL !<br>');

					while($data = mysqli_fetch_array($req))
					{
						if ($userselect != $data['RE_USER'])
						{
							?>
							<optgroup label="<?php echo $data['RE_USER']; ?>">
							<?php
							$userselect = $data['RE_USER'];
						}
						?>
						<option value="<?php echo $data['RE_NUMRESA']; ?>"><?php echo $data['RE_LIBELLEEMPLACEMENT']; ?></option>';
						<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2" rowspan="1" style="width: 20%;"><button class="boutonresa1" value="DeleteResa" name="Panier">Supprimer</button></td>
		</tr>
		</tbody>
		</table>
	</td></tr>
  </tbody>





	</table>
	<br>



	<?php


}

function insert_resa()
{
	$ajout = $_POST['ajout_emplacement'];
	$an = $_GET['annee'];
	$mois = $_GET['mois'];
	$jour = $_GET['jour'];
	$user =  $_POST['user'];
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = " select * from ZONE

				inner join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT

				where EM_ETABLISSEMENT = '".$_SESSION['ETABADMIN'] ."' AND EM_EMPLACEMENT = " .$_GET['typeplace'] ."";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{

		$ET_LIBELLE = $data['ET_LIBELLE'];

		$ET_ADRESSE1 = $data['ET_ADRESSE1'];

		$ET_ADRESSE2 = $data['ET_ADRESSE2'];

		$ET_ADRESSE3 = $data['ET_ADRESSE3'];

		$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];

		$ET_VILLE = $data['ET_VILLE'];

		$ET_EMAIL = $data['ET_EMAIL'];

		$EM_EMPLACEMENT = $_GET['typeplace'];

		$EM_LIBELLE = $data['EM_LIBELLE'];
		$EM_TYPEZONE = $data['EM_TYPEZONE'];
		$EM_NBRPLACESMAX = $data['EM_NBRPLACESMAX'];
		$EM_PRIXPARZONE = $data['EM_PRIXPARZONE'];
		$EM_NBRRESAMAX = $data['EM_NBRRESAMAX'];
		$EM_PRIXPARPLACE = $data['EM_PRIXPARPLACE'];
		$EM_HEUREDEBUT = $data['EM_HEUREDEBUT'];
		$EM_HEUREFIN = $data['EM_HEUREFIN'];
		$EM_ARTPLACE = $data['EM_ARTPLACE'];
		$EM_ARTZONE = $data['EM_ARTZONE'];
		if($EM_ARTPLACE != '')
			{
				$EM_QTEART1 = 1;
				$EM_PRIXART1 = $EM_PRIXPARZONE;
				$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTZONE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					$EM_LIBART1 = $data1['AR_DESIGNATION'];
				}
				$EM_QTEART2 = $_POST['ajout_nbrplace'];
				$EM_PRIXART2 = $EM_PRIXPARPLACE;
				$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTPLACE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					$EM_LIBART2 = $data1['AR_DESIGNATION'];
				}

			}
			else
			{
				$EM_QTEART1 = $_POST['ajout_nbrplace'];
				$EM_PRIXART1 = $EM_PRIXPARPLACE;
				$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTZONE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->query($sql);
				$tab_r = $result_req->fetchAll();
				foreach ($tab_r as $data1)
				{
					$EM_LIBART1 = $data1['AR_DESIGNATION'];
					echo $EM_LIBART1;
				}
				$EM_QTEART2 = 0;
				$EM_PRIXART2 = 0;
			}
		$EM_PRIXTOTALRESA = $EM_PRIXPARZONE + ($EM_PRIXPARPLACE * $_POST['ajout_nbrplace'] );

	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	for($i=0; $i<$ajout; $i++) {

	$sql = "INSERT INTO `RESERVATION` (`RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_ETABLISSEMENT`, RE_ETLIBELLE,
			`RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`,
			 `RE_ZONE`,`RE_ZONELIBELLE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`,
			 RE_TYPEZONE, RE_HEUREDEBUT, RE_HEUREFIN, RE_REFFACTURE, RE_USERANNUL,RE_DATEANNUL, RE_CODEART1, RE_QTEART1, RE_PRIXART1, RE_LIBART1, RE_CODEART2, RE_QTEART2, RE_PRIXART2, RE_LIBART2) VALUE
			('".$user."','" .date('Y-m-d', strtotime($mois ."/" .$jour ."/" .$an)) ."'," .$jour .",
			" .$mois ."," .$an .",'OUI','NON','".$_SESSION['ETABADMIN'] ."','".$ET_LIBELLE."','".$ET_ADRESSE1."','".$ET_ADRESSE2."',
			'".$ET_CODEPOSTAL."','".$ET_VILLE."',".$_GET['typeplace'].",'".$EM_LIBELLE."', '".$_POST['ajout_nbrplace']."','OUI','".$EM_EMPLACEMENT."',
			'".$EM_LIBELLE."','".$_SESSION['login']."','','".$EM_PRIXPARZONE."','".$EM_PRIXPARPLACE."','".$EM_PRIXTOTALRESA."',
			'".$EM_NBRRESAMAX."','".$EM_NBRPLACESMAX."','".$EM_TYPEZONE."','".$EM_HEUREDEBUT."','".$EM_HEUREFIN."','','','1900-01-01','".$EM_ARTZONE."',".$EM_QTEART1.",'".$EM_PRIXART1."','".$EM_LIBART1."',
			'".$EM_ARTPLACE."',".$EM_QTEART2.",'".$EM_PRIXART2."','".$EM_LIBART2."')";

	$req = $conn->query($sql) or die('Erreur SQL 1!!!<br>');
	}

	if ($_GET['reservable'] == 'OUI')
	{
		echo "Réservation mise à jour !";
	}
	else
	{
		echo "Date rajoutée au planning !";
	}

	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}


function retire_resa()
{

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "UPDATE RESERVATION SET RE_VALIDEE = 'NON', RE_USERANNUL = '".$_SESSION['login']."', RE_DATEANNUL = now() where RE_NUMRESA = ".$_POST['delete_emplacement'].";";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	if ($_GET['reservable'] == 'OUI')
	{
		echo "Réservation mise à jour !";
	}
	else
	{
		echo "Date supprimée du planning !";
	}
	?>
	<a href="javascript:myclosewindow();">Fermer</a>
	<?php
}


function modif_place()
{

	if(!isset($_POST["action"]))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from ZONE WHERE EM_EMPLACEMENT =" .$_GET['numero'];
		$req = $conn->query($sql) or die('Erreur SQL !<br>');

		while($data = mysqli_fetch_array($req))
		{
?>
		<form  action="" method="post">
		<table style='text-align: left; width: 900px; height: 150px; font-family: "Century Gothic", Geneva, sans-serif;' border="0" cellpadding="2" cellspacing="2">
		  <tbody>
			<tr>
			  <td colspan="4" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Modification de la zone</td>
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;">Nom de l'emplacement</td>
				<td style="height: 28px; width: 212px;"><input maxlength="30" size="30" tabindex="1" name="LIBELLE" value='<?php echo $data['EM_LIBELLE']; ?>'></td>

			</tr>
			<tr>
				<td style="width: 304px; height: 28px;"><label>Type de zone : </label></td>
				<td style="height: 28px; width: 212px;"><SELECT name="TYPEZONE">
					<?php
					$sql = "SELECT * FROM CHOIXCODE WHERE CC_TYPE = 'TYPEZONE';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						echo $data['EM_TYPEZONE'];
						if ($data1['CC_LIBELLE'] == $data['EM_TYPEZONE'])
						{
							echo '<OPTION selected="selected" value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
						}
						else
						{
							echo '<OPTION value="'.$data1['CC_LIBELLE'].'">'.$data1['CC_LIBELLE'].'</option>';
						}
					}
					?>
					</SELECT>
				</td>
				<td style="width: 304px; height: 28px;"></td>
				<td style="height: 28px; width: 212px;"></td>
			</tr>
			<tr>
				<td style="width: 304px; height: 28px;"><label>Article de l'emplacement : </label></td>
				<td style="height: 28px; width: 212px;"><SELECT name="ART1" onchange='changearticle1()'>
					<?php
					echo '<OPTION value="">Vide ...</option>';
					$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{

						if ($data1['AR_CODEARTICLE'] == $data['EM_ARTZONE'])
						{
							echo '<OPTION selected="selected" value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
							
						}
						else
						{
							echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_CODEARTICLE'] . ' - ' .$data1['AR_DESIGNATION'] . ' - ' .$data1['AR_TARIF'].' €</option>';
						}
						?>
						<div class="ChoixArticle_<?php echo $data1['AR_CODEARTICLE'];?>"><?php echo $data1['AR_TARIF'];?></div>
						<?php
					}
					?>
					</SELECT>
				</td>
				<td style="width: 304px; height: 28px;"><label>Article de l'emplacement : </label></td>
				<td style="height: 28px; width: 212px;"><SELECT name="ART2">
					<?php
					echo '<OPTION value="">Vide ...</option>';
					$sql = "SELECT * FROM ARTICLE WHERE AR_TYPE = 'ARTRE' AND AR_ETABLISSEMENT = '".$_SESSION['ETABADMIN']."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{

						if ($data1['AR_CODEARTICLE'] == $data['EM_ARTPLACE'])
						{
							echo '<OPTION selected="selected" value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION'].'</option>';
						}
						else
						{
							echo '<OPTION value="'.$data1['AR_CODEARTICLE'].'">'.$data1['AR_DESIGNATION'].'</option>';
						}
					}
					?>
					</SELECT>
				</td>
			</tr>

			<tr>
			  <td style="width: 304px; height: 28px;">Nombre d'emplacement (maximum)</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="4" name="NOMBREZONE" value='<?php echo $data['EM_NBRRESAMAX']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Nombre de personne par emplacement (maximum)</td>
				<td style="height: 28px; width: 212px;"><input maxlength="2" size="2" tabindex="4" name="NOMBREPERSONNE" value='<?php echo $data['EM_NBRPLACESMAX']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Prix par emplacement</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="3" name="PRIX" value='<?php echo $data['EM_PRIXPARZONE']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Prix par personne (uniquement si plus de 1 personne)</td>
			  <td style="height: 28px; width: 212px;"><input maxlength="5" size="5" tabindex="5" name="PRIXPARPERSONNE" value='<?php echo $data['EM_PRIXPARPLACE']; ?>'></td>
			</tr>
			<tr>
			  <td style="width: 304px; height: 28px;">Heure de début</td>
			  <td style="height: 28px; width: 212px;"><input type="time" id="heuredebut" name="heuredebut" value='<?php echo $data['EM_HEUREDEBUT']; ?>'></td>
			  <td style="width: 304px; height: 28px;">Heure de fin</td>
			  <td style="height: 28px; width: 212px;"><input type="time" id="heurefin" name="heurefin" value='<?php echo $data['EM_HEUREFIN']; ?>'></td>
			</tr>

			<tr align="center">
			  <td colspan="2" rowspan="1" style="width: 212px; height: 26px;"><button value="Valid" name="Valid">Valider</button></td>
			</tr>
		  </tbody>
		</table>
		<input type="hidden" value="MODIF" name="action">
		</form>

<?php
		}
	}
	else
	{
		if ($_POST['action'] == "MODIF")
		{
			echo 'kk';
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "UPDATE ZONE SET EM_LIBELLE = '" .$_POST['LIBELLE'] ."', EM_NBRRESAMAX = " .$_POST['NOMBREZONE'] .",EM_TYPEZONE = '" .$_POST['TYPEZONE'] ."',
					EM_NBRPLACESMAX = " .$_POST['NOMBREPERSONNE'] .", EM_PRIXPARZONE = '" .$_POST['PRIX'] ."', EM_PRIXPARPLACE = '" .$_POST['PRIXPARPERSONNE'] ."' ,
					EM_ARTZONE = '".$_POST['ART1']."', EM_ARTPLACE = '".$_POST['ART2']."',
					EM_HEUREDEBUT = '".$_POST['heuredebut'] ."', EM_HEUREFIN = '".$_POST['heurefin']."' WHERE EM_EMPLACEMENT =" .$_GET['numero'];
			
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			echo "Mise à jour de l'emplacement " .$_POST['LIBELLE'] ."terminé";
		}
		?>
		<a href="javascript:myclosewindow();">Fermer</a>
		<?php
	}
}
