<?php

/**
 * update.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
//include ("include/fonction_general.php");
require ("../scripts/constantes.php");
require ("../scripts/fonctions.php");

$sql = "DROP TABLE IF EXISTS `ARTICLE_TEMP`;"
$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->exec($sql);	

$sql = "CREATE TABLE IF NOT EXISTS `ARTICLE_TEMP` (
		`AR_ARTICLENO` int(5) NOT NULL AUTO_INCREMENT,
		`AR_CODEARTICLE` varchar(20) NOT NULL,
		`AR_DESIGNATION` varchar(50) NOT NULL,
		`AR_TYPE` varchar(20) NOT NULL,
		`AR_ETABLISSEMENT` varchar(10) NOT NULL,
		`AR_FAMILLE1` varchar(50) DEFAULT NULL,
		`AR_FAMILLE2` varchar(50) DEFAULT NULL,
		  `AR_TARIF` decimal(12,2) NOT NULL,
		  `AR_RESAHEUREDEBUT` time DEFAULT NULL,
		  `AR_RESAHEUREFIN` time DEFAULT NULL,
		  `AR_IMAGE` varchar(250) DEFAULT NULL,
		  UNIQUE KEY `AR_ARTICLENO` (`AR_ARTICLENO`)
		) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;"
$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->exec($sql);	

$champ_origine = array("0");
$sql= "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA like 'admi%'";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		array_push($champ_origine,$data['TABLE_NAME']); 
	}
														