<?php
/**
 * fonction_resa.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include ("include/fonction_general.php");
include ("include/fonction_email.php");





function update_resaold()
{

	$user = $_POST['user'];

	$jour = $_POST['jour'];

	$mois = $_POST['mois'];

	$an = $_POST['an'];

	$tokenresa = $_POST['tokenresa'];

	$typeplace = $_POST['typeplace'];

	$etablissement = $_POST['etablissement'];

	if (isset($_POST['nbrplaces']) && $_POST['nbrplaces'] != '1') {

		$nbrplaces = $_POST['nbrplaces'];

	} else {

		$nbrplaces = 1;

	};
	if (isset($_POST['adhesion']) && $_POST['adhesion'] != '')
	{

		$adhesion = $_POST['adhesion'];
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "SELECT count(*) as CPT FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."';";

		$req = $conn->query($sql) or die('Erreur SQL !! banane');

		$data = mysqli_fetch_array($req);
		if ($data['CPT'] != 0)
		{
			emailadhesion('renouvellement',$etablissement,$user);
		}
		else
		{
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "INSERT INTO TIERSETAB (TE_ETABLISSEMENT, TE_LOGIN, TE_DATEFINADHESION, TE_DEBUTADHESION, TE_STATUT) VALUE ('".$etablissement."','".$_SESSION['login']."','2000-01-01','2000-01-01','NOUVEAU');";


			$req = $conn->query($sql) or die('Erreur SQL !! banane');
			emailadhesion('nouvelle',$etablissement,$user);
		}

	}
	else
	{
		if (isset($_POST['ajout']) && $_POST['ajout'] != '')
		{
			$ajout = $_POST['ajout'];
		}

		if (isset($_POST['supression']) && $_POST['supression'] != '')
		{
			$suppr = explode('|',$_POST['supression']);
		}

		$typeplace = $_POST['typeplace'];

		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from RESERVATION where RE_SESSIONTOKEN = '".$tokenresa."'";
		$req = $conn->query($sql) or die('Erreur SQL !! banane');

		$data = mysqli_fetch_array($req);

		if ($data['RE_SESSIONTOKEN'] != $tokenresa) {

			/* AJOUTER DES RESA */

			if (isset($ajout) && $ajout != 0) {

				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

				$sql = " select * from ZONE

				inner join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT

				where EM_ETABLISSEMENT = '".$etablissement ."' AND EM_EMPLACEMENT = " .$typeplace ."";

				$req = $conn->query($sql) or die('Erreur SQL !!<br>');

				while ($data = mysqli_fetch_array($req))

				{

					$ET_LIBELLE = $data['ET_LIBELLE'];

					$ET_ADRESSE1 = $data['ET_ADRESSE1'];

					$ET_ADRESSE2 = $data['ET_ADRESSE2'];

					$ET_ADRESSE3 = $data['ET_ADRESSE3'];

					$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];

					$ET_VILLE = $data['ET_VILLE'];

					$ET_EMAIL = $data['ET_EMAIL'];

					$EM_EMPLACEMENT = $typeplace;

					$EM_LIBELLE = $data['EM_LIBELLE'];
					$EM_TYPEZONE = $data['EM_TYPEZONE'];
					$EM_NBRPLACESMAX = $data['EM_NBRPLACESMAX'];
					$EM_PRIXPARZONE = $data['EM_PRIXPARZONE'];
					$EM_NBRRESAMAX = $data['EM_NBRRESAMAX'];
					$EM_PRIXPARPLACE = $data['EM_PRIXPARPLACE'];
					$EM_HEUREDEBUT = $data['EM_HEUREDEBUT'];
					$EM_HEUREFIN = $data['EM_HEUREFIN'];
					$EM_ARTPLACE = $data['EM_ARTPLACE'];
					$EM_ARTZONE = $data['EM_ARTZONE'];
					$EM_LIBART1 = "";
					$EM_LIBART2 = "";
					if($EM_ARTPLACE != '')
					{
						$EM_QTEART1 = 1;
						$EM_PRIXART1 = $EM_PRIXPARZONE;
						$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTZONE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data1)
						{
							$EM_LIBART1 = $data1['AR_DESIGNATION'];
							$EM_PRIXART1 = $data1['AR_TARIF'];
							$EM_PRIXPARZONE = $data1['AR_TARIF'];
						}
						$EM_QTEART2 = $nbrplaces;
						$EM_PRIXART2 = $EM_PRIXPARPLACE;
						$EM_LIBART2 = '';
						$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTPLACE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data1)
						{
							$EM_LIBART2 = $data1['AR_DESIGNATION'];
							$EM_PRIXART2 = $data1['AR_TARIF'];
							$EM_PRIXPARPLACE = $data1['AR_TARIF'];
						}

					}
					else
					{
						$EM_QTEART1 = $nbrplaces;
						$EM_PRIXART1 = $EM_PRIXPARPLACE;
						//$EM_LIBART1 = '';
						$sql = "select * from ARTICLE WHERE AR_CODEARTICLE = '".$EM_ARTZONE."' AND AR_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";

						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->query($sql);
						$tab_r = $result_req->fetchAll();
						foreach ($tab_r as $data1)
						{
							$EM_LIBART1 = $data1['AR_DESIGNATION'];
							$EM_PRIXART1 = $data1['AR_TARIF'];
							$EM_PRIXPARZONE = $data1['AR_TARIF'];
						}
						$EM_QTEART2 = 0;
						$EM_PRIXART2 = 0;
					}
					$EM_PRIXTOTALRESA = $EM_PRIXPARZONE + ($EM_PRIXPARPLACE * $nbrplaces );

				}

echo $EM_LIBART1;
echo EM_PRIXART1;
				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);



				for($i=0; $i<$ajout; $i++) {

					$creditok = 'NON';
					$sql = "SELECT TE_CREDITOK FROM TIERSETAB where TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						$creditok = $data1['TE_CREDITOK'];
					}

					$sql = "INSERT INTO `RESERVATION` (`RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_ETABLISSEMENT`, RE_ETLIBELLE,
							`RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`,
							 `RE_ZONE`,`RE_ZONELIBELLE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`,
							 RE_TYPEZONE, RE_HEUREDEBUT, RE_HEUREFIN, RE_REFFACTURE,RE_USERANNUL,RE_DATEANNUL, RE_CODEART1, RE_QTEART1, RE_PRIXART1,  RE_LIBART1, RE_CODEART2, RE_QTEART2, RE_PRIXART2, RE_LIBART2, RE_CREDITCLI,
               RE_DATERESA, RE_JOURTYPE, RE_ARTJOURCODE, RE_ARTJOURLIB, RE_ARTJOURPRIX, RE_ARTMATINCODE, RE_ARTMATINLIB, RE_ARTMATINPRIX, RE_ARTAPMCODE, RE_ARTAPMLIB, 	RE_ARTAPMPRIX, RE_TOKENPANIER, RE_PANIER) VALUE

							('".$user."','" .date('Y-m-d', strtotime($mois ."/" .$jour ."/" .$an)) ."'," .$jour .",

							" .$mois ."," .$an .",'OUI','NON','".$etablissement ."','".$ET_LIBELLE."','".$ET_ADRESSE1."','".$ET_ADRESSE2."',

							'".$ET_CODEPOSTAL."','".$ET_VILLE."',".$typeplace.",'".$EM_LIBELLE."', '".$nbrplaces."','OUI','".$EM_EMPLACEMENT."',

							'".$EM_LIBELLE."','".$_SESSION['login']."','".$tokenresa."','".$EM_PRIXPARZONE."','".$EM_PRIXPARPLACE."','".$EM_PRIXTOTALRESA."',
							'".$EM_NBRRESAMAX."','".$EM_NBRPLACESMAX."','".$EM_TYPEZONE."','".$EM_HEUREDEBUT."','".$EM_HEUREFIN."','','','1900-01-01',
							'".$EM_ARTZONE."',".$EM_QTEART1.",".$EM_PRIXART1.",'".$EM_LIBART1."','".$EM_ARTPLACE."',".$EM_QTEART2.",".$EM_PRIXART2.",'".$EM_LIBART2."','".$creditok."',
              now(),'','','',0,'','',0, '', '',0,'".$_SESSION['token']."','OUI')";
echo $sql;
exit;
					$req = $conn->query($sql) or die('Erreur SQL 1!!!<br>'.$sql);

					envoieinvitation($etablissement, $user, $EM_TYPEZONE, $EM_LIBELLE);

					$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = TE_CREDITVAL - " .$EM_PRIXTOTALRESA." WHERE TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."' AND TE_CREDITOK = 'NON';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);

				}

			}



			/* SUPPRIMER DES RESA */

			if (isset($suppr) && $suppr != '') {


				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

				foreach($suppr as $supprimer) {

					$sql = "UPDATE RESERVATION SET RE_VALIDEE = 'NON', RE_USERANNUL = '".$_SESSION['login']."', RE_DATEANNUL = now() where RE_NUMRESA = ".$supprimer.";";

					$req = $conn->query($sql) or die('Erreur SQL !!!!<br>');
					emailEventSupr($etablissement, $user, $EM_TYPEZONE, $EM_LIBELLE);

					$mntcredit = 0;
					$sql = "SELECT RE_PRIXTOTALRESA FROM RESERVATION where RE_NUMRESA = ".$supprimer.";";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						$mntcredit = $data1['RE_PRIXTOTALRESA'];
					}
					$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = TE_CREDITVAL + " .$mntcredit." WHERE TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."' AND TE_CREDITOK = 'OUI';";

					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
				}
			}

		}

		mysqli_close($conn);
	}





}

function update_resa()
{

	$user = $_POST['user'];

	$jour = $_POST['jour'];

	$mois = $_POST['mois'];

	$an = $_POST['an'];

	$tokenresa = $_POST['tokenresa'];

	$typeplace = $_POST['typeplace'];

	$etablissement = $_POST['etablissement'];

	if (isset($_POST['nbrplaces']) && $_POST['nbrplaces'] != '1') {

		$nbrplaces = $_POST['nbrplaces'];

	} else {

		$nbrplaces = 1;

	};
	if (isset($_POST['adhesion']) && $_POST['adhesion'] != '')
	{

		$adhesion = $_POST['adhesion'];
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "SELECT count(*) as CPT FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."';";

		$req = $conn->query($sql) or die('Erreur SQL !! banane');

		$data = mysqli_fetch_array($req);
		if ($data['CPT'] != 0)
		{
			emailadhesion('renouvellement',$etablissement,$user);
		}
		else
		{
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			$sql = "INSERT INTO TIERSETAB (TE_ETABLISSEMENT, TE_LOGIN, TE_DATEFINADHESION, TE_DEBUTADHESION, TE_STATUT) VALUE ('".$etablissement."','".$_SESSION['login']."','2000-01-01','2000-01-01','NOUVEAU');";


			$req = $conn->query($sql) or die('Erreur SQL !! banane');
			emailadhesion('nouvelle',$etablissement,$user);
		}

	}
	else
	{
		if (isset($_POST['ajout']) && $_POST['ajout'] != '')
		{
			$ajout = $_POST['ajout'];
		}

		if (isset($_POST['supression']) && $_POST['supression'] != '')
		{
			$suppr = explode('|',$_POST['supression']);
		}

		$typeplace = $_POST['typeplace'];

		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select * from RESERVATION where RE_SESSIONTOKEN = '".$tokenresa."'";
		$req = $conn->query($sql) or die('Erreur SQL !! banane');

		$data = mysqli_fetch_array($req);

		if ($data['RE_SESSIONTOKEN'] != $tokenresa) {

			/* AJOUTER DES RESA */

			if (isset($ajout) && $ajout != 0) {

				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

				$sql = "select ZONE.*, ETABLISSEMENT.*, AR1.AR_CODEARTICLE AS ARTZONE, AR1.AR_DESIGNATION AS LIBARTZONE, AR1.AR_TARIF AS PRIXARTZONE, 
						AR1.AR_RESAHEUREDEBUT AS RESAHEUREDEUT, AR1.AR_RESAHEUREFIN AS RESAHEUREFIN, ET_GESTIONPANIER,
						AR2.AR_CODEARTICLE AS ARTPLACE, AR2.AR_DESIGNATION AS LIBARTPLACE, AR2.AR_TARIF AS PRIXARTPLACE, GE_TYPEENVOI
						from ZONE
						left join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT
						left join ARTICLE AR1 on AR1.AR_CODEARTICLE = EM_ARTZONE AND AR1.AR_ETABLISSEMENT = '".$etablissement ."'
						left join ARTICLE AR2 on AR2.AR_CODEARTICLE = EM_ARTPLACE AND AR2.AR_ETABLISSEMENT = '".$etablissement ."'
						left join GESTIONEMAIL ON GE_ETABLISSEMENT = '".$etablissement ."' AND GE_TYPE = 'CONFIRMERESERVATION'
						where EM_ETABLISSEMENT = '".$etablissement ."' AND EM_EMPLACEMENT = " .$typeplace ."";
				$req = $conn->query($sql) or die('Erreur SQL !!<br>');

				while ($data = mysqli_fetch_array($req))

				{

					$ET_LIBELLE = $data['ET_LIBELLE'];

					$ET_ADRESSE1 = $data['ET_ADRESSE1'];

					$ET_ADRESSE2 = $data['ET_ADRESSE2'];

					$ET_ADRESSE3 = $data['ET_ADRESSE3'];

					$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];

					$ET_VILLE = $data['ET_VILLE'];

					$ET_EMAIL = $data['ET_EMAIL'];
					
					$TYPEENVOI = $data['GE_TYPEENVOI'];

					$EM_EMPLACEMENT = $typeplace;

					$EM_LIBELLE = $data['EM_LIBELLE'];
					$EM_TYPEZONE = $data['EM_TYPEZONE'];
					$EM_NBRPLACESMAX = $data['EM_NBRPLACESMAX'];
					$EM_PRIXPARZONE = $data['EM_PRIXPARZONE'];
					$EM_NBRRESAMAX = $data['EM_NBRRESAMAX'];
					$EM_PRIXPARPLACE = $data['EM_PRIXPARPLACE'];
					$EM_HEUREDEBUT = $data['RESAHEUREDEUT'];
					$EM_HEUREFIN = $data['RESAHEUREFIN'];
					$EM_ARTPLACE = $data['ARTPLACE'];
					$EM_ARTZONE = $data['ARTZONE'];
					$EM_LIBART1 = $data['LIBARTZONE'];
					$EM_PRIXART1 = $data['PRIXARTZONE'];
					$EM_LIBART2 = $data['LIBARTPLACE'];
					$EM_PRIXART2 = $data['PRIXARTPLACE'];
					$EM_GESTIONPANIER = $data['ET_GESTIONPANIER'];
					if($EM_ARTPLACE != '')
					{
						$EM_QTEART1 = 1;
						$EM_QTEART2 = $nbrplaces;
					}
					else
					{
						$EM_QTEART1 = $nbrplaces;
						$EM_QTEART2 = 0;
						$EM_PRIXART2 = 0;
					}
					$EM_PRIXTOTALRESA = $EM_PRIXPARZONE + ($EM_PRIXPARPLACE * $nbrplaces );

				}



				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);



				for($i=0; $i<$ajout; $i++) {

					$creditok = 'NON';
					$sql = "SELECT TE_CREDITOK FROM TIERSETAB where TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."';";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						$creditok = $data1['TE_CREDITOK'];
					}

					$sql = "INSERT INTO `RESERVATION` (`RE_USER`, `RE_DATE`, `RE_JOUR`, `RE_MOIS`, `RE_ANNEE`, `RE_VALIDEE`, `RE_FACTURE`, `RE_ETABLISSEMENT`, RE_ETLIBELLE,
							`RE_ETADRESSE1`, `RE_ETADRESSE2`, `RE_CODEPOSTAL`, `RE_VILLE`, `RE_EMPLACEMENT`, `RE_LIBELLEEMPLACEMENT`, `RE_NBRPLACE`, `RE_RESA`,
							 `RE_ZONE`,`RE_ZONELIBELLE`, `RE_USERMODIF`, `RE_SESSIONTOKEN`, `RE_PRIXPLACE`, `RE_PRIXPARPERSONNE`, `RE_PRIXTOTALRESA`, `RE_NBRRESAMAX`, `RE_NBRPLACESMAX`,
							 RE_TYPEZONE, RE_HEUREDEBUT, RE_HEUREFIN, RE_REFFACTURE,RE_USERANNUL,RE_DATEANNUL, RE_CODEART1, RE_QTEART1, RE_PRIXART1,  RE_LIBART1, RE_CODEART2, RE_QTEART2, RE_PRIXART2, RE_LIBART2, RE_CREDITCLI,
               RE_DATERESA, RE_JOURTYPE, RE_ARTJOURCODE, RE_ARTJOURLIB, RE_ARTJOURPRIX, RE_ARTMATINCODE, RE_ARTMATINLIB, RE_ARTMATINPRIX, RE_ARTAPMCODE, RE_ARTAPMLIB, 	RE_ARTAPMPRIX, RE_TOKENPANIER, RE_PANIER) VALUE

							('".$user."','" .date('Y-m-d', strtotime($mois ."/" .$jour ."/" .$an)) ."'," .$jour .",

							" .$mois ."," .$an .",'OUI','NON','".$etablissement ."','".$ET_LIBELLE."','".$ET_ADRESSE1."','".$ET_ADRESSE2."',

							'".$ET_CODEPOSTAL."','".$ET_VILLE."',".$typeplace.",'".$EM_LIBELLE."', '".$nbrplaces."','OUI','".$EM_EMPLACEMENT."',

							'".$EM_LIBELLE."','".$_SESSION['login']."','".$tokenresa."','".$EM_PRIXPARZONE."','".$EM_PRIXPARPLACE."','".$EM_PRIXTOTALRESA."',
							'".$EM_NBRRESAMAX."','".$EM_NBRPLACESMAX."','".$EM_TYPEZONE."','".$EM_HEUREDEBUT."','".$EM_HEUREFIN."','','','1900-01-01',
							'".$EM_ARTZONE."',".$EM_QTEART1.",".$EM_PRIXART1.",'".$EM_LIBART1."','".$EM_ARTPLACE."',".$EM_QTEART2.",".$EM_PRIXART2.",'".$EM_LIBART2."','".$creditok."',
              now(),'','','',0,'','',0, '', '',0,'".$_SESSION['token']."','".$EM_GESTIONPANIER."')";
	  
					$req = $conn->query($sql) or die('Erreur SQL 1!!!<br>'.$sql);

					if ($data['ET_GESTIONPANIER'] != 'OUI')
					{
						if ($TYPEENVOI != 1)
						{
							envoieinvitation($etablissement, $user, $EM_TYPEZONE, $EM_LIBELLE);
						}
						else
						{
							envoiresasimple($etablissement, $user, $EM_TYPEZONE, $EM_LIBELLE);
						}
						$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = TE_CREDITVAL - " .$EM_PRIXTOTALRESA." WHERE TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."' AND TE_CREDITOK = 'NON';";
						$cnx_bdd = ConnexionBDD();
						$result_req = $cnx_bdd->exec($sql);
					}
					else
					{
						$_SESSION['NbrPanier'] ++;
					}

				}

			}



			/* SUPPRIMER DES RESA */

			if (isset($suppr) && $suppr != '') {


				$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

				foreach($suppr as $supprimer) {

					$sql = "UPDATE RESERVATION SET RE_PANIER = 'OUI', RE_TOKENPANIER = '".$_SESSION['token']."', RE_VALIDEE = 'NON', RE_USERANNUL = '".$_SESSION['login']."', RE_DATEANNUL = now() where RE_NUMRESA = ".$supprimer.";";

					$req = $conn->query($sql) or die('Erreur SQL !!!!<br>');
					//emailEventSupr($etablissement, $user, $EM_TYPEZONE, $EM_LIBELLE);

					$mntcredit = 0;
					$sql = "SELECT RE_PRIXTOTALRESA FROM RESERVATION where RE_NUMRESA = ".$supprimer.";";
					$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->query($sql);
					$tab_r = $result_req->fetchAll();
					foreach ($tab_r as $data1)
					{
						$mntcredit = $data1['RE_PRIXTOTALRESA'];
					}
					$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = TE_CREDITVAL + " .$mntcredit." WHERE TE_LOGIN = '".$user."' AND TE_ETABLISSEMENT = '".$etablissement."' AND TE_CREDITOK = 'OUI';";

					//$cnx_bdd = ConnexionBDD();
					//$result_req = $cnx_bdd->exec($sql);
				}
			}

		}

		mysqli_close($conn);
	}





}



function affiche_resa($typeplace, $etablissement)

{
    //select TIMESTAMPDIFF(MINUTE,now(),RE_DATERESA), minute(timediff(now(),RE_DATERESA)), (timediff(now(),RE_DATERESA)), RE_DATERESA, re_numresa from reservation where RE_PANIER = 'OUI'
	$user = $_SESSION['login'];
	$_SESSION['tokenresa'] = random(25);

	$jour_actuel = date("j", time());

	$mois_actuel = date("m", time());

	$an_actuel = date("Y", time());

	$jour = $jour_actuel;

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '".$etablissement."'";
	$req = $conn->query($sql) or die('Erreur SQL !!<br>');
	while($data = mysqli_fetch_array($req))
	{
		{
			$EtabNom = $data['ET_LIBELLE'];
		}
	}
//exit;
	if (!isset($typeplace))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select MIN(EM_EMPLACEMENT) as TYPEPLACE from ZONE where EM_ETABLISSEMENT = '".$etablissement."' AND EM_BLOQUE = 'NON'";
		$req = $conn->query($sql) or die('Erreur SQL !!<br>');
		while($data = mysqli_fetch_array($req))
		{
			{
				$typeplace = $data['TYPEPLACE'];
			}
		}

	}

	if ($typeplace == 0)
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select MIN(EM_EMPLACEMENT) as TYPEPLACE from ZONE where EM_ETABLISSEMENT = '".$etablissement."' AND EM_BLOQUE = 'NON'";
		$req = $conn->query($sql) or die('Erreur SQL !!<br>');
		while($data = mysqli_fetch_array($req))
		{
			{
				$typeplace = $data['TYPEPLACE'];
			}
		}

	}

	if (!isset($_GET['user']))

	{

		$user = $_SESSION['login'];

	}

	// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante

	if(!isset($_GET["mois"]))

	{

		$mois = $mois_actuel;

		$an = $an_actuel;

	}

	else

	{

		$mois = $_GET['mois'];

		$an = $_GET['an'];

	}



	//defini le mois suivant

	$mois_suivant = $mois + 1;

	$an_suivant = $an;

	if ($mois_suivant == 13)

	{

		$mois_suivant = 1;

		$an_suivant = $an + 1;

	}



	//defini le mois précédent

	$mois_prec = $mois - 1;

	$an_prec = $an;

	if ($mois_prec == 0)

	{

		$mois_prec = 12;

		$an_prec = $an - 1;

	}

	//affichage du mois et de l'année en french

	$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");

	$mois_en_clair = $mois_de_annee[$mois - 1];

	$nombre_date = mktime(0,0,0, $mois, 1, $an);

	$premier_jour = date('w', $nombre_date);

	$dernier_jour = 28;

	//Recherche des datas

	$NbrPlaceDispo = array("0");

	$NbrResaDispo = array("0");

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select EM_NBRRESAMAX as NB, EM_NBRPLACESMAX as NP from ZONE where EM_EMPLACEMENT = '".$typeplace."' AND EM_ETABLISSEMENT = '".$etablissement."'";

	$req = $conn->query($sql) or die('Erreur SQL !!<br>');

	while($data = mysqli_fetch_array($req))

	{

		for ($i = 1; $i < 32; $i++)

		{

			array_push($NbrPlaceDispo,$data['NB']);

			array_push($NbrResaDispo,$data['NP']);

		}

	}

	$NbrPlaceOccup = array("0");

	for ($i = 1; $i < 32; $i++)

		{

			array_push($NbrPlaceOccup,"0");

		}

	$reserved = array("NON");

	for ($i = 1; $i < 32; $i++)

		{

			array_push($reserved,"NON");


		}







	$reservable = array("NON");

	$datetime2 = new DateTime("now");

	$today = array("NON");

	for ($i = 1; $i < 32; $i++)

		{

			$datetime1 = new DateTime($an .'-' .$mois .'-' .$i);

			$datetime2 = new DateTime($an_actuel .'-' .$mois_actuel .'-' .$jour_actuel);

			$interval = $datetime1->diff($datetime2);

			if ($interval->format('%a days') == 0)

				array_push($today,"OUI");

			else

				array_push($today,"NON");


			if ($interval->format('%r%d days') < 0)

				array_push($reservable,"OUI");

			else

				array_push($reservable,"NON");

		}

	$sql = "SELECT RE_JOUR, RE_DATE,(select count(*) from DISPONIBLE where DI_ZONE = '".$typeplace."') - count(*) as NBLIBRE

			from RESERVATION

			where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." and RE_ETABLISSEMENT = '".$etablissement."'

			AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI' group by RESERVATION.RE_JOUR, RE_DATE";

	$sql = "SELECT RE_JOUR, RE_DATE,(select EM_NBRRESAMAX from ZONE where EM_EMPLACEMENT = '".$typeplace."' and EM_ETABLISSEMENT = '".$etablissement."') - count(*) as NBLIBRE from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI' group by RESERVATION.RE_JOUR, RE_DATE";



	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))

	{

		$NbrPlaceDispo[$data['RE_JOUR']] = $data['NBLIBRE'];

	}


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI' ";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))

	{

		$nbtotalplaceresaplaceresa = $data['NBRESA'];

	}


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." AND RE_FACTURE = 'OUI' and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalfacture = $data['NBRESA'];
	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalplaceresaplaceresaannuel = $data['NBRESA'];
	}

	$sql = "select RE_JOUR, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." AND RE_USER = '".$user."'
				and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$NbrPlaceOccup[$data['RE_JOUR']] = $data['NBOCCUP'];
		if ($data['NBOCCUP'] > 0)
		{
			$reserved[$data['RE_JOUR']] = 'OUI';
			$nbreserv[$data['RE_JOUR']] = $data['NBOCCUP'];
		}
	}

	$sql = "select RE_JOUR, RE_NUMRESA, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." AND RE_USER = '".$user."'
				and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR, RESERVATION.RE_NUMRESA";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		if ($data['NBOCCUP'] > 0)
		{
			$idresa[$data['RE_JOUR']][] = $data['RE_NUMRESA'];
		}
	}

	if ($an >= date("Y", time()))

	{

	}

	else

	{

		for ($i = 1; $i < 32; $i++)

		{

			$reservable[$i] = "OUI";


		}

	}



	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select EM_EMPLACEMENT, EM_LIBELLE, EM_NBRRESAMAX from ZONE where EM_ETABLISSEMENT = '".$etablissement."' AND EM_BLOQUE = 'NON'";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))

	{

		$zone_li[$data['EM_EMPLACEMENT']]=$data['EM_LIBELLE'];

	}


	mysqli_close($conn);


	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");

	?>



	<div class="head">

		<div class="head_container"><?php echo $EtabNom; ?>

			<ul class="zone_actuelle col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1"><span><?php echo $zone_li[$typeplace]  ;?></span>

					<?php foreach($zone_li as $id => $li) :?>

						<li><a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $id; ?>&mois=<?php echo $mois; ?>&an=<?php echo $an; ?>"><?php echo $li; ?></a></li>

					<?php endforeach; ?>

			</ul>

			<div class="mois_container col-sm-4">

				<div class="mois col-xs-8" data-mois="<?php echo $mois; ?>" data-an="<?php echo $an; ?>"><?php echo $mois_en_clair .' ' .$an; ?></div>

				<a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $typeplace; ?>&mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>" class="mois_prec"></a>

				<a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $typeplace; ?>&mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>" class="mois_suiv"></a>

			</div>

		</div>

	</div>

	<!-- SCRIPTS -->



	<script type="text/javascript">

		$(document).ready(function() {

			$('.zone_actuelle span').on('click', function(){

				var $this = $(this).parent();

				$this.toggleClass('open');

			});

			$('.zone_actuelle li').on('click', function(){

				var $this = $(this),

					txt = $this.html(),

					emplacement = $this.data('emplacement'),

					emp = emplacement.substr(emplacement.length - 1)

				$this.parent().find('span').empty().append(txt);

				$this.parent('ul').removeClass('open');

				$('#choix').attr('href',"manageresa.php?typeplace=" + emp);

			})

		})



	</script>





	<!-- CALENDRIER -->



	<div class="calendar_wrapper">

		<div class="calendar_container">

			<div class="jours_container">

				<div class="jours">

					<div class="jour col-sm-2">lundi</div>

					<div class="jour col-sm-2">mardi</div>

					<div class="jour col-sm-2">mercredi</div>

					<div class="jour col-sm-2">jeudi</div>

					<div class="jour col-sm-2">vendredi</div>

					<div class="jour col-sm-2">samedi</div>

				</div>

			</div>

			<div class="dates_container">

				<div class="dates">



			  <?php

			  // Affichage de la première semaine

				while (checkdate($mois, $dernier_jour + 1, $an))

					{ $dernier_jour++;}



				//Affichage de 7 jours du calendrier



				for ($i = 1; $i < 8; $i++)

				{

					if ($i < $premier_jour)

					{

						?>

						<div class="case <?php if($i%2 == 1) {echo ' impair';} else {echo ' pair';}; if($i == 7) {echo ' hide';}; if($i%8 == 6) {echo ' no_border';}?>"></div>

						<?php

					}

					else

					{

						$ce_jour = ($i+1) - $premier_jour

						?>

						<div class="case <?php if($i%2 == 1) {echo ' impair';} else {echo ' pair';}; if($i == 7) {echo ' hide';}; if($i%8 == 6) {echo ' no_border';}?>">

						<?php

							if ($reserved[$ce_jour] == 'OUI')

							{

								?>

								<div class="resaok" onclick="openresa($(this))">

									<p class="nbrreserv" data-nbreserv="<?php echo $NbrPlaceOccup[$ce_jour]; ?>" data-idresa="<?php for($a=0; $a<$NbrPlaceOccup[$ce_jour]; $a++) {if($a == 0) {echo $idresa[$ce_jour][$a];} else {echo '|'.$idresa[$ce_jour][$a];}} ?>"><span><?php echo $NbrPlaceOccup[$ce_jour]; ?></span> <?php if($NbrPlaceOccup[$ce_jour] > 1) {echo ' places <span class="hide_mobile">réservées</span>';} else {echo ' place <span class="hide_mobile">réservée</span>';} ?>

									</p>

								<?php

							}

							else

							{

								?>

								<div class="resa1" onclick="openresa($(this));">

								<?php

							}

							?>



								  <span class="date_jour"><?php echo $ce_jour?></span>

									<div class="placeslibre <?php if ($reservable[$ce_jour]=='NON' && $today[$ce_jour] == 'NON') {echo 'hide';} ?> <?php if ($NbrPlaceDispo[$ce_jour] == 0) {echo 'full';} ?>">

										<p data-places="<?php echo $NbrResaDispo[$ce_jour]; ?>"><?php echo $NbrPlaceDispo[$ce_jour]; ?></p>

									</div>



							</div>

						</div>

						<?php

					}

				}

				$jour_suiv = ($i+1) - $premier_jour;

				for ($rangee = 0; $rangee <= 4; $rangee++)

				{

					for ($i = 0; $i < 7; $i++)

					{

						if($jour_suiv > $dernier_jour)

						{

							$rangee = 4;

							?>

							<div class="case <?php if($i%2 ==  0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>"></div>

							<?php

						}

						else

						{

							if ($today[$jour_suiv] == 'OUI')

							{

								?>

								<div class="case <?php if($i%2 ==  0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>" onclick="test.php">

								<?php

							}

							else

							{

								?>

								<div  class="case <?php if($i%2 == 0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>" onclick="test.php">

								<?php

							}

							?>

								<?php

								if ($reserved[$jour_suiv] == 'OUI')

								{

									?>

									<div class="resaok" onclick="openresa($(this))">

									<p class="nbrreserv" data-nbreserv="<?php echo $NbrPlaceOccup[$jour_suiv]; ?>" data-idresa="<?php for($a=0; $a<$NbrPlaceOccup[$jour_suiv]; $a++) {if($a == 0) {echo $idresa[$jour_suiv][$a];} else {echo '|'.$idresa[$jour_suiv][$a];}} ?>"><span><?php echo $NbrPlaceOccup[$jour_suiv]; ?></span> <?php if($NbrPlaceOccup[$ce_jour] > 1) {echo ' places <span class="hide_mobile">réservées</span>';} else {echo ' place <span class="hide_mobile">réservée</span>';} ?></p>

									<?php

								}

								else

								{

									?>

									<div class="resa1" onclick="openresa($(this))">

									<?php

								}

								?>

									<span class="date_jour"><?php echo $jour_suiv; ?></span>

									<div class="placeslibre <?php if ($reservable[$jour_suiv]=='NON' && $today[$jour_suiv] == 'NON') {echo 'hide';} ?> <?php if ($NbrPlaceDispo[$jour_suiv] == 0) {echo 'full';} ?>">

										<p data-places="<?php echo $NbrResaDispo[$jour_suiv]; ?>"><?php echo $NbrPlaceDispo[$jour_suiv]; ?></p>

									</div>

								</div>

							</div>

						<?php

						}

						$jour_suiv++;

					}

				}

				?>





				</div>

			</div>

		</div>

	</div>

	<div class="resa_popin">

		<div class="popin_head">

			<div class="close_btn" onclick="closeresa();"></div>

			<h3>Réservation</h3>

		</div>

		<div class="popin_body">

			<div class="resa_libre_section">

				<div class="resa_libre"><span class="nbrlibre"></span> emplacements libres le <span class="jour_popin">3</span> <?php echo $mois_en_clair .' ' .$an; ?></div>

			</div>

			<?php
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			if ($mois < 10)
			{
				$sql = "SELECT count(*) as CPT, TE_DATEFINADHESION, datediff(TE_DATEFINADHESION, '".$an."-0".$mois."-01') AS FINADHESION
				FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."' GROUP BY TE_DATEFINADHESION;";
			}
			else
			{
				$sql = "SELECT count(*) as CPT, TE_DATEFINADHESION, datediff(TE_DATEFINADHESION, '".$an."-".$mois."-01') AS FINADHESION
				FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."' GROUP BY TE_DATEFINADHESION;";
			}
			$adhesionok = 0;
			$finadhesion = 0;
			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			while($data = mysqli_fetch_array($req))
			{
				$adhesionok = $data['CPT'];
				$finadhesion = $data['FINADHESION'];
			}

			if($adhesionok == 0)
			{
				?>
				<div class="nbrreserv">
				<p>Vous n'êtes pas adhérent à la structure !</p>
				</div>

				<div class="resa_valid">
					<a href="#" class="cancel" onclick="closeresa();">Annuler</a>
												<form method="post">
							<input type="hidden" id="jour" name="jour" value="">
							<input type="hidden" id="mois" name="mois" value="">
							<input type="hidden" id="an" name="an" value="">
							<input type="hidden" id="ajout" name="ajout" value="0">
							<input type="hidden" id="typeplace" name="typeplace" value="<?php echo $typeplace; ?>">
							<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">
							<input type="hidden" id="nbrplaces" name="nbrplaces" value="1">
							<input type="hidden" id="adhesion" name="adhesion" value="adhesion">
							<input type="hidden" id="supression" name="supression" value="">
							<input type="hidden" id="user" name="user" value="">
							<input type="hidden" id="tokenresa" name="tokenresa" value="<?php echo $_SESSION['tokenresa']; ?>">
							<input type="submit" id="submit" name="submit" class="valider" value="Adhésion">
						</form>


				</div>

				<?php
			}
			else
			{
				if($finadhesion<0)
				{
					?>
					<div class="nbrreserv">
					<p>Votre adhésion à la structure est terminée!</p>
					</div>

					<div class="resa_valid">
						<a href="#" class="cancel" onclick="closeresa();">Annuler</a>
						<form method="post">
							<input type="hidden" id="jour" name="jour" value="">
							<input type="hidden" id="mois" name="mois" value="">
							<input type="hidden" id="an" name="an" value="">
							<input type="hidden" id="ajout" name="ajout" value="0">
							<input type="hidden" id="typeplace" name="typeplace" value="<?php echo $typeplace; ?>">
							<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">
							<input type="hidden" id="nbrplaces" name="nbrplaces" value="1">
							<input type="hidden" id="adhesion" name="adhesion" value="adhesion">
							<input type="hidden" id="supression" name="supression" value="">
							<input type="hidden" id="user" name="user" value="">
							<input type="hidden" id="tokenresa" name="tokenresa" value="<?php echo $_SESSION['tokenresa']; ?>">
							<input type="submit" id="submit" name="submit" class="valider" value="Renouveller">
						</form>
					</form>

					</div>

					<?php
				}
				else
				{
					$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
					$sql = "SELECT CASE WHEN TE_CREDITOK = 'OUI' AND TE_CREDITVAL > 0 THEN 'OUI'
							WHEN TE_CREDITOK = 'OUI' AND TE_CREDITVAL <= 0 THEN 'NON'
							WHEN TE_CREDITOK = 'NON' THEN 'OUI' END AS CREDITOK
							FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."' ;";
					$creditok = 'NON';

					$req = $conn->query($sql) or die('Erreur SQL !<br>');
					while($data = mysqli_fetch_array($req))
					{
						$creditok = $data['CREDITOK'];
					}
					if($creditok == 'NON')
					{
						?>
						<div class="nbrreserv">
						<p>Votre crédit est épuisé!</p>
						</div>

						<div class="resa_valid">
							<a href="#" class="cancel" onclick="closeresa();">Annuler</a>

						</form>

						</div>
						<?php
					}
					else
					{
						?>
						<div class="resa_deja">

							<h4>Vous avez réservé <span></span> bureaux le 6 septembre</h4>

						</div>

						<div class="nbrreserv">
						<p>Emplacements à réserver :</p>

							<ul class="selectnbr"><span class="selected">0</span>

							</ul>

						</div>

						<div class="nbrreserv places">

							<p>Places à réserver :</p>

							<ul class="selectplace"><span class="selected">0</span>

							</ul>

						</div>

						<div class="resa_valid">

							<a href="#" class="cancel" onclick="closeresa();">Annuler</a>

							<form method="post">

								<input type="hidden" id="jour" name="jour" value="">

								<input type="hidden" id="mois" name="mois" value="">

								<input type="hidden" id="an" name="an" value="">

								<input type="hidden" id="ajout" name="ajout" value="0">

								<input type="hidden" id="typeplace" name="typeplace" value="<?php echo $typeplace; ?>">

								<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">

								<input type="hidden" id="nbrplaces" name="nbrplaces" value="1">

								<input type="hidden" id="supression" name="supression" value="">

								<input type="hidden" id="user" name="user" value="">

								<input type="hidden" id="tokenresa" name="tokenresa" value="<?php echo $_SESSION['tokenresa']; ?>">

								<input type="submit" id="submit" name="submit" class="valider" value="Valider">

							</form>

						</div>
						<?php
					}

				}

			}

		?>

		</div>

	</div>

	<div class="mask_popin" onclick="closeresa();"></div>

	<script type="text/javascript">

		$('.selectnbr span').on('click', function(){

			var $this = $(this).parent();

			$this.toggleClass('open');

		});



		$('.selectplace span').on('click', function(){

			var $this = $(this).parent();

			$this.toggleClass('open');

		});





		function openresa(e) {
		
			var placeslibres = $(e).find('.placeslibre').find('p').html(),

				full = $(e).find('.placeslibre'),//find('p').data('places'),

				placestotale = $(e).find('.placeslibre').find('p').data('places'),

				date = $(e).find('.date_jour').html(),

				mois = $('.mois_container').find('.mois').data('mois'),

				mois_en_clair = $('.mois_container').find('.mois').html(),

				annee = $('.mois_container').find('.mois').data('an'),

				user = $('nav').find('.logged_in').find('.user').data('user'),

				nbrreserv = $(e).find('.nbrreserv').data('nbreserv'),

				idresa = $(e).find('.nbrreserv').data('idresa'),

				idresas = new Array(),

				listeplaces ='';
				

				listeplacestotale ='';

				if(idresa) {

					if(idresa.length > 3) {var idresas = idresa.split('|');} else {idresas.push(idresa);};

				} else {

					idresa = "";

				}

			// On renseigne les inputs hidden de base :

			$('input#jour').val(date);

			$('input#mois').val(mois);

			$('input#an').val(annee);

			$('input#user').val(user);

			$('input#ajout').removeAttr('value');

			$('input#supression').removeAttr('value');

			$('.resa_popin').find('.popin_body').find('.nbrreserv').show();

			$('.resa_popin').find('.popin_body').find('.nbrreserv.places').show();

			$('.resa_popin').find('.popin_body').find('.resa_deja').show();



			// On reseigne le nombre de places libre

			$('.resa_popin').find('.popin_body').find('.resa_libre_section').find('.resa_libre').find('.nbrlibre').empty().append(placeslibres);

			if (placeslibres == 0) {

				$('.resa_popin').find('.popin_body').find('.nbrreserv').hide();

			} else {

				for(var i = 0; i <= placeslibres; i++) {

					listeplaces += '<li data-qte="' + i + '" onclick="addresa($(this))">' + i + '</li>';

				}

				$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectnbr').children('li').remove();

				$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectnbr').append(listeplaces);

				if (placestotale == 1) {

					$('.resa_popin').find('.popin_body').find('.nbrreserv.places').hide();

				} else {

					for(var i = 1; i <= placestotale; i++) {

						listeplacestotale += '<li data-qte="' + i + '" onclick="addresaplace($(this))">' + i + '</li>';

					}

					$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectplace').children('li').remove();

					$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectplace').append(listeplacestotale);

				}

			}


			// On reseigne la date

			$('.resa_popin').find('.popin_body').find('.resa_libre_section').find('.resa_libre').find('.jour_popin').empty().append(date);



			// On renseigne le nombre de place réservées par l'utilisateur

			if(!nbrreserv) {

				$('.resa_popin').find('.popin_body').find('.resa_deja').hide();

			} else {

				// Si déja des résa, on les liste

				var content = '<h4>Vous avez réservé <span>' + nbrreserv + '</span> bureaux le ' + date + ' ' + mois_en_clair + '</h4>';



				$('.resa_popin').find('.popin_body').find('.resa_deja').empty().show();

				if( !full.hasClass('hide')) {

					for (var i = 1; i <= nbrreserv; i++) {

						 content += '<span class="bureau_resa" data-resa="' + idresas[i-1] + '"><p>Emplacement ' + i + '</p><a href="#" onclick="deleteresa($(this))">supprimer</a></span>';

					}

				} else {

					content += '<p class="warning">Veuillez contacter l\'administrateur pour supprimer vos réservations passées.</p>';

				};

				$('.resa_popin').find('.popin_body').find('.resa_deja').append(content);

			}



			if(nbrreserv > 0) {

			}



			// On affiche la popin

			$('.mask_popin').addClass('view');

			$('.resa_popin').addClass('view');

		}



		function addresa(e) {

			var $this = $(e),

				txt = $this.html(),

				qte = $this.data('qte');

			$this.parent().find('span').empty().append(txt);

			$this.parent('ul').removeClass('open');

			$('input#ajout').val(qte);

		}

		function addresaplace(e) {

			var $this = $(e),

				txt = $this.html(),

				qte = $this.data('qte');

			$this.parent().find('span').empty().append(txt);

			$this.parent('ul').removeClass('open');

			$('input#nbrplaces').val(qte);

		}



		function deleteresa(e) {

			var $this = $(e),

				nbresa = $this.parent().parent().find('h4').find('span').html(),

				idresa = $this.parent().data('resa');

			$this.parent().slideUp(300);

			if(!$('input#supression').val()) {

				$('input#supression').val(idresa);

				nbresa--;

				$this.parent().parent().find('h4').find('span').empty().append(nbresa);

				if(nbresa == 0) {

					$this.parent().parent().find('h4').fadeOut();

					$this.parent().parent().slideUp();

				}

			} else {

				$('input#supression').val(function(i, val) {

					return val + '|' + idresa;

				});

				nbresa--;

				$this.parent().parent().find('h4').find('span').empty().append(nbresa);

				if(nbresa == 0) {

					$this.parent().parent().find('h4').fadeOut();

					$this.parent().parent().slideUp();

				}

			console.log(nbresa);



			};

		}



		function closeresa() {

			$('.mask_popin').removeClass('view');

			$('.resa_popin').removeClass('view');

		}



		$(document).keyup(function(e) {

		     if (e.keyCode == 27) { // escape key maps to keycode `27`

				closeresa()

		    }

		});

	</script>

	<?php

}

function affiche_resa_test($typeplace, $etablissement)

{

	$user = $_SESSION['login'];

	$jour_actuel = date("j", time());

	$mois_actuel = date("m", time());

	$an_actuel = date("Y", time());

	$jour = $jour_actuel;

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '".$etablissement."'";
	$req = $conn->query($sql) or die('Erreur SQL !!<br>');
	while($data = mysqli_fetch_array($req))
	{
		{
			$EtabNom = $data['ET_LIBELLE'];
		}
	}

	if (!isset($typeplace))
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select MIN(EM_EMPLACEMENT) as TYPEPLACE from ZONE where EM_ETABLISSEMENT = '".$etablissement."'";
		$req = $conn->query($sql) or die('Erreur SQL !!<br>');
		while($data = mysqli_fetch_array($req))
		{
			{
				$typeplace = $data['TYPEPLACE'];
			}
		}

	}

	if ($typeplace == 0)
	{
		$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
		$sql = "select MIN(EM_EMPLACEMENT) as TYPEPLACE from ZONE where EM_ETABLISSEMENT = '".$etablissement."'";
		$req = $conn->query($sql) or die('Erreur SQL !!<br>');
		while($data = mysqli_fetch_array($req))
		{
			{
				$typeplace = $data['TYPEPLACE'];
			}
		}

	}

	if (!isset($_GET['user']))

	{

		$user = $_SESSION['login'];

	}

	// si la variable mois n'existe pas, mois et année correspondent au mois et à l'année courante

	if(!isset($_GET["mois"]))

	{

		$mois = $mois_actuel;

		$an = $an_actuel;

	}

	else

	{

		$mois = $_GET['mois'];

		$an = $_GET['an'];

	}



	//defini le mois suivant

	$mois_suivant = $mois + 1;

	$an_suivant = $an;

	if ($mois_suivant == 13)

	{

		$mois_suivant = 1;

		$an_suivant = $an + 1;

	}



	//defini le mois précédent

	$mois_prec = $mois - 1;

	$an_prec = $an;

	if ($mois_prec == 0)

	{

		$mois_prec = 12;

		$an_prec = $an - 1;

	}



	//affichage du mois et de l'année en french

	$mois_de_annee = array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre");

	$mois_en_clair = $mois_de_annee[$mois - 1];

	$nombre_date = mktime(0,0,0, $mois, 1, $an);

	$premier_jour = date('w', $nombre_date);

	$dernier_jour = 28;

	//Recherche des datas

	$NbrPlaceDispo = array("0");

	$NbrResaDispo = array("0");

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select EM_NBRRESAMAX as NB, EM_NBRPLACESMAX as NP from ZONE where EM_EMPLACEMENT = '".$typeplace."' AND EM_ETABLISSEMENT = '".$etablissement."'";

	$req = $conn->query($sql) or die('Erreur SQL !!<br>');

	while($data = mysqli_fetch_array($req))

	{

		for ($i = 1; $i < 32; $i++)

		{

			array_push($NbrPlaceDispo,$data['NB']);

			array_push($NbrResaDispo,$data['NP']);


		}

	}


	$NbrPlaceOccup = array("0");

	for ($i = 1; $i < 32; $i++)

		{

			array_push($NbrPlaceOccup,"0");


		}

	$reserved = array("NON");

	for ($i = 1; $i < 32; $i++)

		{

			array_push($reserved,"NON");


		}







	$reservable = array("NON");

	$datetime2 = new DateTime("now");

	$today = array("NON");

	for ($i = 1; $i < 32; $i++)

		{

			$datetime1 = new DateTime($an .'-' .$mois .'-' .$i);

			$datetime2 = new DateTime($an_actuel .'-' .$mois_actuel .'-' .$jour_actuel);

			$interval = $datetime1->diff($datetime2);

			if ($interval->format('%a days') == 0)

				array_push($today,"OUI");

			else

				array_push($today,"NON");


			if ($interval->format('%r%d days') < 0)

				array_push($reservable,"OUI");

			else

				array_push($reservable,"NON");

		}

	$sql = "SELECT RE_JOUR, RE_DATE,(select count(*) from DISPONIBLE where DI_ZONE = '".$typeplace."') - count(*) as NBLIBRE

			from RESERVATION

			where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." and RE_ETABLISSEMENT = '".$etablissement."'

			AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI' group by RESERVATION.RE_JOUR, RE_DATE";

	$sql = "SELECT RE_JOUR, RE_DATE,(select EM_NBRRESAMAX from ZONE where EM_EMPLACEMENT = '".$typeplace."' and EM_ETABLISSEMENT = '".$etablissement."') - count(*) as NBLIBRE from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI' group by RESERVATION.RE_JOUR, RE_DATE";



	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))

	{

		$NbrPlaceDispo[$data['RE_JOUR']] = $data['NBLIBRE'];

	}


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI' ";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))

	{

		$nbtotalplaceresaplaceresa = $data['NBRESA'];

	}


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_MOIS = ".$mois." AND RE_FACTURE = 'OUI' and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalfacture = $data['NBRESA'];
	}

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
	$sql = "SELECT count(*) as NBRESA from RESERVATION where RE_USER = '" .$user ."' and RE_ANNEE = ".$an ." and RE_ETABLISSEMENT = '".$etablissement."' AND RE_VALIDEE = 'OUI'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$nbtotalplaceresaplaceresaannuel = $data['NBRESA'];
	}


	$sql = "select RE_JOUR, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." AND RE_USER = '".$user."'
				and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		$NbrPlaceOccup[$data['RE_JOUR']] = $data['NBOCCUP'];
		if ($data['NBOCCUP'] > 0)
		{
			$reserved[$data['RE_JOUR']] = 'OUI';
			$nbreserv[$data['RE_JOUR']] = $data['NBOCCUP'];
		}
	}


	$sql = "select RE_JOUR, RE_NUMRESA, count(*) as NBOCCUP from RESERVATION where RE_MOIS = " .$mois ." AND RE_ANNEE = " .$an ." AND RE_USER = '".$user."'
				and RE_ETABLISSEMENT = '".$etablissement."' AND RE_ZONE = '".$typeplace."' AND RE_VALIDEE = 'OUI'
				group by RESERVATION.RE_JOUR, RESERVATION.RE_NUMRESA";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	while($data = mysqli_fetch_array($req))
	{
		if ($data['NBOCCUP'] > 0)
		{
			$idresa[$data['RE_JOUR']][] = $data['RE_NUMRESA'];
		}
	}

	if ($an >= date("Y", time()))

	{

	}

	else

	{

		for ($i = 1; $i < 32; $i++)

		{

			$reservable[$i] = "OUI";


		}

	}



	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select EM_EMPLACEMENT, EM_LIBELLE, EM_NBRRESAMAX from ZONE where EM_ETABLISSEMENT = '".$etablissement."' ";


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))

	{

		$zone_li[$data['EM_EMPLACEMENT']]=$data['EM_LIBELLE'];

	}


	mysqli_close($conn);



	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");

	?>



	<div class="head">

		<div class="head_container"><?php echo $EtabNom; ?>

			<ul class="zone_actuelle col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-1"><span><?php echo $zone_li[$typeplace]  ;?></span>

					<?php foreach($zone_li as $id => $li) :?>

						<li><a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $id; ?>&mois=<?php echo $mois; ?>&an=<?php echo $an; ?>"><?php echo $li; ?></a></li>

					<?php endforeach; ?>

			</ul>

			<div class="mois_container col-sm-4">

				<div class="mois col-xs-8" data-mois="<?php echo $mois; ?>" data-an="<?php echo $an; ?>"><?php echo $mois_en_clair .' ' .$an; ?></div>

				<a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $typeplace; ?>&mois=<?php echo $mois_prec; ?>&an=<?php echo $an_prec; ?>" class="mois_prec"></a>

				<a href="manageresa.php?etablissement=<?php echo $etablissement; ?>&typeplace=<?php echo $typeplace; ?>&mois=<?php echo $mois_suivant; ?>&an=<?php echo $an_suivant; ?>" class="mois_suiv"></a>

			</div>

		</div>

	</div>

	<!-- SCRIPTS -->



	<script type="text/javascript">

		$(document).ready(function() {

			$('.zone_actuelle span').on('click', function(){

				var $this = $(this).parent();

				$this.toggleClass('open');

			});

			$('.zone_actuelle li').on('click', function(){

				var $this = $(this),

					txt = $this.html(),

					emplacement = $this.data('emplacement'),

					emp = emplacement.substr(emplacement.length - 1)

				$this.parent().find('span').empty().append(txt);

				$this.parent('ul').removeClass('open');

				$('#choix').attr('href',"manageresa.php?typeplace=" + emp);

			})

		})



	</script>





	<!-- CALENDRIER -->



	<div class="calendar_wrapper">

		<div class="calendar_container">

			<div class="jours_container">

				<div class="jours">

					<div class="jour col-sm-2">lundi</div>

					<div class="jour col-sm-2">mardi</div>

					<div class="jour col-sm-2">mercredi</div>

					<div class="jour col-sm-2">jeudi</div>

					<div class="jour col-sm-2">vendredi</div>

					<div class="jour col-sm-2">samedi</div>

				</div>

			</div>

			<div class="dates_container">

				<div class="dates">



			  <?php

			  // Affichage de la première semaine

				while (checkdate($mois, $dernier_jour + 1, $an))

					{ $dernier_jour++;}



				//Affichage de 7 jours du calendrier



				for ($i = 1; $i < 8; $i++)

				{

					if ($i < $premier_jour)

					{

						?>

						<div class="case <?php if($i%2 == 1) {echo ' impair';} else {echo ' pair';}; if($i == 7) {echo ' hide';}; if($i%8 == 6) {echo ' no_border';}?>"></div>

						<?php

					}

					else

					{

						$ce_jour = ($i+1) - $premier_jour

						?>

						<div class="case <?php if($i%2 == 1) {echo ' impair';} else {echo ' pair';}; if($i == 7) {echo ' hide';}; if($i%8 == 6) {echo ' no_border';}?>">

						<?php

							if ($reserved[$ce_jour] == 'OUI')

							{

								?>

								<div class="resaok" onclick="openresa($(this))">

									<p class="nbrreserv" data-nbreserv="<?php echo $NbrPlaceOccup[$ce_jour]; ?>" data-idresa="<?php for($a=0; $a<$NbrPlaceOccup[$ce_jour]; $a++) {if($a == 0) {echo $idresa[$ce_jour][$a];} else {echo '|'.$idresa[$ce_jour][$a];}} ?>"><span><?php echo $NbrPlaceOccup[$ce_jour]; ?></span> <?php if($NbrPlaceOccup[$ce_jour] > 1) {echo ' places <span class="hide_mobile">réservées</span>';} else {echo ' place <span class="hide_mobile">réservée</span>';} ?>

									</p>

								<?php

							}

							else

							{

								?>

								<div class="resa1" onclick="openresa($(this));">

								<?php

							}

							?>



								  <span class="date_jour"><?php echo $ce_jour?></span>

									<div class="placeslibre <?php if ($reservable[$ce_jour]=='NON' && $today[$ce_jour] == 'NON') {echo 'hide';} ?> <?php if ($NbrPlaceDispo[$ce_jour] == 0) {echo 'full';} ?>">

										<p data-places="<?php echo $NbrResaDispo[$ce_jour]; ?>"><?php echo $NbrPlaceDispo[$ce_jour]; ?></p>

									</div>



							</div>

						</div>

						<?php

					}

				}

				$jour_suiv = ($i+1) - $premier_jour;

				for ($rangee = 0; $rangee <= 4; $rangee++)

				{

					for ($i = 0; $i < 7; $i++)

					{

						if($jour_suiv > $dernier_jour)

						{

							$rangee = 4;

							?>

							<div class="case <?php if($i%2 ==  0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>"></div>

							<?php

						}

						else

						{

							if ($today[$jour_suiv] == 'OUI')

							{

								?>

								<div class="case <?php if($i%2 ==  0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>" onclick="test.php">

								<?php

							}

							else

							{

								?>

								<div  class="case <?php if($i%2 == 0) {echo ' impair';} else {echo ' pair';}; if($i == 6) {echo ' hide';}; if($i%7 == 5) {echo ' no_border';}?>" onclick="test.php">

								<?php

							}

							?>

								<?php

								if ($reserved[$jour_suiv] == 'OUI')

								{

									?>

									<div class="resaok" onclick="openresa($(this))">

									<p class="nbrreserv" data-nbreserv="<?php echo $NbrPlaceOccup[$jour_suiv]; ?>" data-idresa="<?php for($a=0; $a<$NbrPlaceOccup[$jour_suiv]; $a++) {if($a == 0) {echo $idresa[$jour_suiv][$a];} else {echo '|'.$idresa[$jour_suiv][$a];}} ?>"><span><?php echo $NbrPlaceOccup[$jour_suiv]; ?></span> <?php if($NbrPlaceOccup[$ce_jour] > 1) {echo ' places <span class="hide_mobile">réservées</span>';} else {echo ' place <span class="hide_mobile">réservée</span>';} ?></p>

									<?php

								}

								else

								{

									?>

									<div class="resa1" onclick="openresa($(this))">

									<?php

								}

								?>

									<span class="date_jour"><?php echo $jour_suiv; ?></span>

									<div class="placeslibre <?php if ($reservable[$jour_suiv]=='NON' && $today[$jour_suiv] == 'NON') {echo 'hide';} ?> <?php if ($NbrPlaceDispo[$jour_suiv] == 0) {echo 'full';} ?>">

										<p data-places="<?php echo $NbrResaDispo[$jour_suiv]; ?>"><?php echo $NbrPlaceDispo[$jour_suiv]; ?></p>

									</div>

								</div>

							</div>

						<?php

						}

						$jour_suiv++;

					}

				}

				?>





				</div>

			</div>

		</div>

	</div>

	<div class="resa_popin">

		<div class="popin_head">

			<div class="close_btn" onclick="closeresa();"></div>

			<h3>Réservation</h3>

		</div>

		<div class="popin_body">

			<div class="resa_libre_section">

				<div class="resa_libre"><span class="nbrlibre"></span> emplacements libres le <span class="jour_popin">3</span> <?php echo $mois_en_clair .' ' .$an; ?></div>

			</div>

			<div class="resa_deja">

				<h4>Vous avez réservé <span></span> bureaux le 6 septembre</h4>

			</div>

			<?php
			$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
			if ($mois < 10)
			{
				$sql = "SELECT count(*) as CPT, TE_DATEFINADHESION, datediff(TE_DATEFINADHESION, '".$an."-0".$mois."-01') AS FINADHESION FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."';";
			}
			else
			{
				$sql = "SELECT count(*) as CPT, TE_DATEFINADHESION, datediff(TE_DATEFINADHESION, '".$an."-".$mois."-01') AS FINADHESION FROM TIERSETAB where TE_ETABLISSEMENT = '".$etablissement."' AND TE_LOGIN = '".$_SESSION['login']."';";
			}

			$req = $conn->query($sql) or die('Erreur SQL !<br>');
			while($data = mysqli_fetch_array($req))
			{
				if($data['CPT'] == 0)
				{
					?>
					<div class="nbrreserv">
					<p>Vous n'êtes pas adhérent à la structure !</p>
					</div>

					<div class="resa_valid">
						<a href="#" class="cancel" onclick="closeresa();">Annuler</a>
						<form method="post">
							<input type="hidden" id="adhesion" name="adhesion" value="nouveau">
							<input type="hidden" id="user" name="user" value="<?php echo $_SESSION['login'];?>">
							<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">
							<input type="submit" id="submit" name="submit" class="valider" value="Adhérer">
						</form>

					</div>

					<?php
				}
				else
				{
					if($data['FINADHESION']<0)
					{
						?>
						<div class="nbrreserv">
						<p>Vous adhésion à la structure est terminée!</p>
						</div>

						<div class="resa_valid">
							<a href="#" class="cancel" onclick="closeresa();">Annuler</a>
							<form method="post">
							<input type="hidden" id="adhesion" name="adhesion" value="nouveau">
							<input type="hidden" id="user" name="user" value="<?php echo $_SESSION['login'];?>">
							<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">
							<input type="submit" id="submit" name="submit" class="valider" value="Adhérer">
						</form>

						</div>

						<?php
					}
					else
					{
						?>
						<div class="nbrreserv">
						<p>Emplacements à réserver :</p>

							<ul class="selectnbr"><span class="selected">0</span>

							</ul>

						</div>

						<div class="nbrreserv places">

							<p>Places à réserver :</p>

							<ul class="selectplace"><span class="selected">0</span>

							</ul>

						</div>

						<div class="resa_valid">

							<a href="#" class="cancel" onclick="closeresa();">Annuler</a>

							<form method="post">

								<input type="hidden" id="jour" name="jour" value="">

								<input type="hidden" id="mois" name="mois" value="">

								<input type="hidden" id="an" name="an" value="">

								<input type="hidden" id="ajout" name="ajout" value="0">

								<input type="hidden" id="typeplace" name="typeplace" value="<?php echo $typeplace; ?>">

								<input type="hidden" id="etablissement" name="etablissement" value="<?php echo $etablissement; ?>">

								<input type="hidden" id="nbrplaces" name="nbrplaces" value="1">

								<input type="hidden" id="supression" name="supression" value="">

								<input type="hidden" id="user" name="user" value="">

								<input type="hidden" id="tokenresa" name="tokenresa" value="<?php echo $_SESSION['tokenresa']; ?>">

								<input type="submit" id="submit" name="submit" class="valider" value="Valider">

							</form>

						</div>
						<?php

					}

				}

			}

			?>

		</div>

	</div>

	<div class="mask_popin" onclick="closeresa();"></div>

	<script type="text/javascript">

		$('.selectnbr span').on('click', function(){

			var $this = $(this).parent();

			$this.toggleClass('open');

		});



		$('.selectplace span').on('click', function(){

			var $this = $(this).parent();

			$this.toggleClass('open');

		});





		function openresa(e) {

			var placeslibres = $(e).find('.placeslibre').find('p').html(),

				full = $(e).find('.placeslibre'),//find('p').data('places'),

				placestotale = $(e).find('.placeslibre').find('p').data('places'),

				date = $(e).find('.date_jour').html(),

				mois = $('.mois_container').find('.mois').data('mois'),

				mois_en_clair = $('.mois_container').find('.mois').html(),

				annee = $('.mois_container').find('.mois').data('an'),

				user = $('nav').find('.logged_in').find('.user').data('user'),

				nbrreserv = $(e).find('.nbrreserv').data('nbreserv'),

				idresa = $(e).find('.nbrreserv').data('idresa'),

				idresas = new Array(),

				listeplaces ='';

				listeplacestotale ='';

				if(idresa) {

					if(idresa.length > 3) {var idresas = idresa.split('|');} else {idresas.push(idresa);};

				} else {

					idresa = "";

				}

			// On renseigne les inputs hidden de base :

			$('input#jour').val(date);

			$('input#mois').val(mois);

			$('input#an').val(annee);

			$('input#user').val(user);

			$('input#ajout').removeAttr('value');

			$('input#supression').removeAttr('value');

			$('.resa_popin').find('.popin_body').find('.nbrreserv').show();

			$('.resa_popin').find('.popin_body').find('.nbrreserv.places').show();

			$('.resa_popin').find('.popin_body').find('.resa_deja').show();



			// On reseigne le nombre de places libre

			$('.resa_popin').find('.popin_body').find('.resa_libre_section').find('.resa_libre').find('.nbrlibre').empty().append(placeslibres);

			if (placeslibres == 0) {

				$('.resa_popin').find('.popin_body').find('.nbrreserv').hide();

			} else {

				for(var i = 0; i <= placeslibres; i++) {

					listeplaces += '<li data-qte="' + i + '" onclick="addresa($(this))">' + i + '</li>';

				}

				$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectnbr').children('li').remove();

				$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectnbr').append(listeplaces);

				if (placestotale == 1) {

					$('.resa_popin').find('.popin_body').find('.nbrreserv.places').hide();

				} else {

					for(var i = 1; i <= placestotale; i++) {

						listeplacestotale += '<li data-qte="' + i + '" onclick="addresaplace($(this))">' + i + '</li>';

					}

					$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectplace').children('li').remove();

					$('.resa_popin').find('.popin_body').find('.nbrreserv').find('ul.selectplace').append(listeplacestotale);

				}

			}



			// On reseigne la date

			$('.resa_popin').find('.popin_body').find('.resa_libre_section').find('.resa_libre').find('.jour_popin').empty().append(date);



			// On renseigne le nombre de place réservées par l'utilisateur

			if(!nbrreserv) {

				$('.resa_popin').find('.popin_body').find('.resa_deja').hide();

			} else {

				// Si déja des résa, on les liste

				var content = '<h4>Vous avez réservé <span>' + nbrreserv + '</span> bureaux le ' + date + ' ' + mois_en_clair + '</h4>';



				$('.resa_popin').find('.popin_body').find('.resa_deja').empty().show();

				if( !full.hasClass('hide')) {

					for (var i = 1; i <= nbrreserv; i++) {

						 content += '<span class="bureau_resa" data-resa="' + idresas[i-1] + '"><p>Emplacement ' + i + '</p><a href="#" onclick="deleteresa($(this))">supprimer</a></span>';

					}

				} else {

					content += '<p class="warning">Veuillez contacter l\'administrateur pour supprimer vos réservations passées.</p>';

				};

				$('.resa_popin').find('.popin_body').find('.resa_deja').append(content);

			}



			if(nbrreserv > 0) {

			}



			// On affiche la popin

			$('.mask_popin').addClass('view');

			$('.resa_popin').addClass('view');

		}



		function addresa(e) {

			var $this = $(e),

				txt = $this.html(),

				qte = $this.data('qte');

			$this.parent().find('span').empty().append(txt);

			$this.parent('ul').removeClass('open');

			$('input#ajout').val(qte);

		}

		function addresaplace(e) {

			var $this = $(e),

				txt = $this.html(),

				qte = $this.data('qte');

			$this.parent().find('span').empty().append(txt);

			$this.parent('ul').removeClass('open');

			$('input#nbrplaces').val(qte);

		}



		function deleteresa(e) {

			var $this = $(e),

				nbresa = $this.parent().parent().find('h4').find('span').html(),

				idresa = $this.parent().data('resa');

			$this.parent().slideUp(300);

			if(!$('input#supression').val()) {

				$('input#supression').val(idresa);

				nbresa--;

				$this.parent().parent().find('h4').find('span').empty().append(nbresa);

				if(nbresa == 0) {

					$this.parent().parent().find('h4').fadeOut();

					$this.parent().parent().slideUp();

				}

			} else {

				$('input#supression').val(function(i, val) {

					return val + '|' + idresa;

				});

				nbresa--;

				$this.parent().parent().find('h4').find('span').empty().append(nbresa);

				if(nbresa == 0) {

					$this.parent().parent().find('h4').fadeOut();

					$this.parent().parent().slideUp();

				}

			console.log(nbresa);



			};

		}



		function closeresa() {

			$('.mask_popin').removeClass('view');

			$('.resa_popin').removeClass('view');

		}



		$(document).keyup(function(e) {

		     if (e.keyCode == 27) { // escape key maps to keycode `27`

				closeresa()

		    }

		});

	</script>

	<?php

}



function affiche_place()

{

	?>

	<body>

<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">



  <tbody>

    <tr>

      <td style="text-align: center; font-family: Calibri; color: rgb(0, 1, 0); width: 105px; font-weight: bold; background-color: rgb(70, 181, 147);">Num&eacute;ro d'emplacement</td>

      <td style="font-family: Calibri; color: rgb(0, 1, 0); width: 176px; font-weight: bold; background-color: rgb(70, 181, 147); text-align: center; vertical-align: middle;">Libell&eacute;</td>

      <td style="text-align: center; width: 337px; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">D&eacute;signation</td>

      <td style="width: 80px; text-align: center; font-family: Calibri; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Nombre de place</td>

      <td style="text-align: center; font-family: Calibri; width: 70px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Prix unitaire</td>

      <td style="text-align: center; font-family: Calibri; width: 81px; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Prix par personne</td>

      <td colspan="3" rowspan="1" style="width: 68px; color: rgb(0, 1, 0); font-weight: bold; text-align: center; vertical-align: middle; background-color: rgb(70, 181, 147);"><span style="font-family: Calibri;">Type d'occuppation</span></td>

    </tr>

	</tbody></table>

	<center><div id="support1">

	<table  style="width: 1076px; text-align: left; margin-left: auto; margin-right: auto;" border="0" cellpadding="2" cellspacing="2">



	<tbody>

	<?php

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select * from DISPONIBLE";


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while($data = mysqli_fetch_array($req))

	{

		?>

		<tr>

      <td style="text-align: center; width: 105px;"><b><?php echo $data['DI_NUMERO']; ?></b></td>

      <td style="width: 176px;"><?php echo $data['DI_LIBELLE']; ?></td>

      <td style="width: 337px;"><?php echo $data['DI_COMMENTAIRE']; ?></td>

      <td style="width: 80px;"><?php echo $data['DI_NOMBREPLACE']; ?></td>

      <td style="width: 70px;"><?php echo $data['DI_PRIX']; ?></td>

      <td style="width: 81px;"><?php echo $data['DI_PRIXPLACE']; ?></td>

      <td style="width: 68px;"><?php echo $data['DI_PRIX']; ?></td>

      <td style="width: 30px;"><img border="0" src="img/settings-gears.png" width="25" height="25" onclick="window.open('manageplace.php?ACTION=manage&numero=<?php echo $data['DI_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>

      <td style="width: 30px;"><img border="0" src="img/recycle-bin.png" width="25" height="25" onclick="window.open('delete_place.php?numero=<?php echo $data['DI_NUMERO']; ?>', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=yes, location=no, resizable=yes, scrollbars=no, status=no'); return false;"/></td>

	  </tr>



		<?php

		}

	mysql_close;

	?>

	<tr align="center" ><td width="800" colspan="9" rowspan="1">

		<table border = "0" cellpadding="15">

			<tr><td align="center" ><input type=button value="Ajouter un emplacement" class="bouton1" onclick="window.open('ajout_place.php', 'exemple', 'height=400, width=600, top=20, left=100, toolbar=no, menubar=no, location=no, resizable=no, scrollbars=no, status=no'); return false;"></td></tr>

		</table>

		</td></tr>

	</table></center>

	<?php

}

function modifresa()

{


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "select * from RESERVATION where RE_USER = '" . $_SESSION['login'] ."' and RE_MOIS = " .$_POST['mois'] ." AND RE_ANNEE = ".$_POST['year']. "

		AND RE_EMPLACEMENT = 'EM0001' AND RE_ETABLISSEMENT = 'ET0001' order by RE_JOUR;";


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	$JourOk = array("0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0");

	while($data = mysql_fetch_array($req))

	{

		$JourOk[$data['RE_JOUR']] = "1";

	}

	$nbrinsert = 0;

	$sqlinsert = "insert into RESERVATION (RE_USER, RE_DATE, RE_JOUR, RE_MOIS, RE_ANNEE, RE_ETABLISSEMENT, RE_EMPLACEMENT) VALUES ";

	$sqldelete = "delete from RESERVATION WHERE";

	$nbrdelete = 0;

	for ($i=1; $i<32; $i++)

	{

		if ($_POST['cbox'.$i] == "on" and $JourOk[$i] == "0")

		{

			if ($nbrinsert != 0)

			{

				$sqlinsert = $sqlinsert .",";

			}

			$sqlinsert = $sqlinsert ."('" .$_SESSION['login'] ."','" .date('Y-m-d', strtotime($_POST['mois'] ."/" .$i ."/" .$_POST['year'])) ."'," .$i .","

				.$_POST['mois'] ."," .$_POST['year'] .", 'ET0001', 'EM0001')";

			$nbrinsert = $nbrinsert + 1;

		}

		if ($_POST['cbox'.$i] != "on" and $JourOk[$i] == "1")

		{

			if ($nbrdelete != 0)

			{

				$sqldelete = $sqldelete ." OR";

			}

			$sqldelete = $sqldelete ." (RE_USER = '" .$_SESSION['login'] ."' AND RE_JOUR = " .$i ." AND RE_MOIS = " .$_POST['mois'] ." AND RE_ANNEE = ".$_POST['year'] ."

				AND RE_ETABLISSEMENT = 'ET0001' AND RE_EMPLACEMENT = 'EM0001')";

			$nbrdelete = $nbrdelete + 1;

		}

	}

	if ($nbrinsert != 0)

	{

		$req = $conn->query($sqlinsert) or die('Erreur SQL !<br>');


	}

	if ($nbrdelete != 0)

	{

		$req = $conn->query($sqldelete) or die('Erreur SQL !<br>');

	}

	mysqli_close();

	affiche_resa();

}



function insert_new_place()

{

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = " select * from ZONE inner join ETABLISSEMENT on ET_ETABLISSEMENT = EM_ETABLISSEMENT

	where EM_ETABLISSEMENT = '".$_SESSION['ETABLISSEMENT'] ."' AND EM_EMPLACEMENT = '" .$_POST['ZONE'] ."'";

	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	while ($data = mysqli_fetch_array($req))

	{

		$ET_LIBELLE = $data['ET_LIBELLE'];

		$ET_ADRESSE1 = $data['ET_ADRESSE1'];

		$ET_ADRESSE2 = $data['ET_ADRESSE2'];

		$ET_ADRESSE3 = $data['ET_ADRESSE3'];

		$ET_CODEPOSTAL = $data['ET_CODEPOSTAL'];

		$ET_VILLE = $data['ET_VILLE'];

		$EM_EMPLACEMENT = $data['EM_EMPLACEMENT'];

		$EM_LIBELLE = $data['EM_LIBELLE'];

	}

	mysqli_close();

	$sql = "insert into DISPONIBLE (DI_LIBELLE, DI_COMMENTAIRE, DI_NOMBREPLACE, DI_PRIX, DI_PRIXPLACE) VALUES ";

	$sql = $sql ."('".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE'] ;

	$sql = $sql ."," .$_POST['PRIX'] ."," .$_POST['PRIXPARPERSONNE'] .")";

	$sql = "INSERT INTO `DISPONIBLE`(`DI_ETABLISSEMENT`, `DI_ETADRESSE1`, `DI_ETADRESSE2`, `DI_ETADRESSE3`,

			`DI_ETCODEPOSTAL`, `DI_ETVILLE`, `DI_ZONE`, `DI_LIBELLEZONE`, `DI_LIBELLE`, `DI_COMMENTAIRE`, `DI_NOMBREPLACE`, `DI_PRIX`, `DI_PRIXPLACE`,

			`Di_TYPEJOUR`) VALUES

			('".$_SESSION['ETABLISSEMENT'] ."','".$ET_ADRESSE1."','".$ET_ADRESSE2."','".$ET_ADRESSE3."','".$ET_CODEPOSTAL."','".$ET_VILLE."','".$_POST['ZONE']."',

			'".$EM_LIBELLE."','".addslashes($_POST['LIBELLE']) ."','" .addslashes($_POST['DESIGNATION']) ."'," .$_POST['NOMBREPERSONNE']."," .$_POST['PRIX'] .","

			.$_POST['PRIXPARPERSONNE'] .",".$_POST['TEMPSLOCATION'].")";

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	mysqli_close();

	echo "Création de l'emplacement " .$_POST['LIBELLE'] ."terminé";

	?>

	<a href="javascript:myclosewindow();">Fermer</a>

	<?php

}



function insert_new_zone()

{

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "insert into ZONE (EM_ETABLISSEMENT, EM_LIBELLE) VALUES ";

	$sql = $sql ."('".addslashes($_SESSION['ETABLISSEMENT']) ."','" .addslashes($_POST['LIBELLE'])."')" ;

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	mysqli_close();

	echo "Création de la zone " .$_POST['LIBELLE'] ." terminée!";

	?>

	<a href="javascript:myclosewindow();">Fermer</a>

	<?php

}

function retire_resa()

{


	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

	$sql = "UPDATE RESERVATION SET RE_VALIDEE = 'NON', RE_USERANNUL = '".$_SESSION['login']."', RE_DATEANNUL = now() where RE_NUMRESA = ".$_POST['delete_emplacement'].";";


	$req = $conn->query($sql) or die('Erreur SQL !<br>');

	if ($_GET['reservable'] == 'OUI')

	{

		echo "Réservation mise à jour !";

	}

	else

	{

		echo "Date supprimée du planning !";

	}

	?>

	<a href="javascript:myclosewindow();">Fermer</a>

	<?php

}

function affiche_panier($statut)
{
	?>
	
	<div class="head">
		<div class="head_container account">
			<a href="mon_panier.php" class="account_tab active">Mon panier</a>
		</div>
	</div>
	<div class="account_container ">
		<section>
		<form action="" method="post">
	<?php
	$etablissement = '';
	$nbrligne = 0;
	$sql = "SELECT RE_NUMRESA, date_format(RE_DATE, '%d/%m %Y') AS DATERESA, RE_PRIXTOTALRESA , ET_LIBELLE, RE_ETABLISSEMENT, RE_TYPEZONE, RE_ZONELIBELLE
			FROM RESERVATION 
			LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = RE_ETABLISSEMENT
			WHERE RE_VALIDEE = 'OUI' AND RE_PANIER = 'OUI' AND RE_TOKENPANIER = '".$_SESSION['token']."';";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->query($sql);
	$tab_r = $result_req->fetchAll();
	foreach ($tab_r as $data)
	{
		$nbrligne ++;
		if ($etablissement != $data['RE_ETABLISSEMENT'])
		{
			?>
				<span class="section_head">
					<h3><?php echo $data['ET_LIBELLE'];?></h3>
					
					<!-- <a href="#" class="modif" onclick="openmodif($(this))">Modifier</a> -->
				</span>
			<?php
			$etablissement = $data['RE_ETABLISSEMENT'];
		}
		?>
		
					<div class="block_info">
						<span id="TEXT_<?php echo $nbrligne; ?>" class="value_info" data-field="login"><?php echo '   Réservation n° ' .$data['RE_NUMRESA'] .' du ' .$data['DATERESA'] .'  - Prix : ' .number_format($data['RE_PRIXTOTALRESA'], 2, ',', ' '); ; ?> </span>
						<a href="#" class="modif" onclick="supprimepanier(<?php echo $nbrligne;?>)" style="width: 15px; height: 150px; padding-left: 50px; ">Supprimer</a>
						<input type="hidden" id="PRIX_<?php echo $nbrligne;?>" name="PRIX_<?php echo $nbrligne;?>" value="<?php echo $data['RE_PRIXTOTALRESA'];?>" />
						<input type="hidden" id="NUMRESA_<?php echo $nbrligne;?>" name="NUMRESA_<?php echo $nbrligne;?>" value="<?php echo $data['RE_NUMRESA'];?>" />
						<input type="hidden" id="ETAB_<?php echo $nbrligne;?>" name="ETAB_<?php echo $nbrligne;?>" value="<?php echo $data['RE_ETABLISSEMENT'];?>" />
						<input type="hidden" id="ZONE_<?php echo $nbrligne;?>" name="ZONE_<?php echo $nbrligne;?>" value="<?php echo $data['RE_TYPEZONE'];?>" />
						<input type="hidden" id="ZONELIB_<?php echo $nbrligne;?>" name="ZONELIB_<?php echo $nbrligne;?>" value="<?php echo $data['RE_ZONELIBELLE'];?>" />
						<input type="hidden" id="VALID_<?php echo $nbrligne;?>" name="VALID_<?php echo $nbrligne;?>" value="OUI" />
					</div>
				
				</span>
			
		<?php
	}
	?>
	<div class="head" style="margin-top: 15px;">
		<div class="head_container account">
			<input type="hidden" id="nbrligne" name="nbrligne" value="<?php echo $nbrligne;?>" />
			<input class="account_tab active" type="submit" style="background: center; margin-top: -15px; border:0px" value="Valider" />
		</div>
	</div>

	</form>
	</section>
		</div>
		
	
	<script type="text/javascript">
	function supprimepanier(id)
	{
		$("#VALID_"+id).val('NON');
		$("#TEXT_"+id).css({'color':'red'});
		$("#TEXT_"+id).css({'textDecoration':'line-through'});
		
	}
	</script>
	<?php


}

function valide_panier()
{
	if ($_POST['nbrligne'] != 0)
	{
		
		for ($i=1; $i <= $_POST['nbrligne']; $i++)
		{
			if($_POST['VALID_'.$i] == 'OUI')
			{
				$sql = "UPDATE RESERVATION SET RE_PANIER = 'NON', RE_TOKENPANIER = '' WHERE RE_NUMRESA = ".$_POST['NUMRESA_'.$i]." ;";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				
				envoieinvitation($_POST['ETAB_'.$i], $_SESSION['login'], $_POST['ZONE_'.$i], $_POST['ZONELIB_'.$i]);

				$sql = "UPDATE TIERSETAB SET TE_CREDITVAL = TE_CREDITVAL - " .$_POST['PRIX_'.$i]." WHERE TE_LOGIN = '".$_SESSION['login']."' AND TE_ETABLISSEMENT = '".$_POST['ETAB_'.$i]."' AND TE_CREDITOK = 'NON';";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
				
				$_SESSION['NbrPanier'] = 0;
				
			}
			if($_POST['VALID_'.$i] == 'NON')
			{
				$sql = "UPDATE RESERVATION SET RE_VALIDEE = 'NON', RE_PANIER = 'NON', RE_TOKENPANIER = '' WHERE RE_NUMRESA = ".$_POST['NUMRESA_'.$i]." ;";
				$cnx_bdd = ConnexionBDD();
				$result_req = $cnx_bdd->exec($sql);
			}
		}
	}
}
