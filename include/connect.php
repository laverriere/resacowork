<?php
/**
 * connect.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


function decrypt3($data, $key) {
	$encryption_key = base64_decode($key);
  // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
  list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
  return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}

function random1($var)
{
	$string = "";
	$chaine = "a0b1c2d3e4f5g6h7i8j9klmnpqrstuvwxy123456789";
	srand((double)microtime()*1000000);
	for($i=0; $i<$var; $i++)
	{
		$string .= $chaine[rand()%strlen($chaine)];
	}
	return $string;
}


function connectsql()
{
	require_once ("scripts/constantes.php");

	global $db_host;
	global $db_user;
	global $db_pwd;
	global $db_name;
	global $etablissement;
	global $URL_Site;

	$db_host=bdd_hote;
	$db_user=bdd_util;
	$db_pwd=bdd_mdp;
	$db_name=bdd_nom;

	$URL_Site = 'http://reservation.coworking-laverriere.fr/laverrieretest';
}

function checkuser($user,$mdp)
{
	require_once ("scripts/constantes.php");

	global $db_host;
	global $db_user;
	global $db_pwd;
	global $db_name;
	global $etablissement;
	global $URL_Site;

	$db_host=bdd_hote;
	$db_user=bdd_util;
	$db_pwd=bdd_mdp;
	$db_name=bdd_nom;

	$URL_Site = 'http://reservation.coworking-laverriere.fr/laverrieretest';

	$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
	$sql = "SELECT * FROM UTILISATEUR WHERE UT_LOGIN = '".$user."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	if ($req->num_rows > 0)
	{
		while ($data = mysqli_fetch_array($req))
		{
			$id1 = $data['UT_ID1'];
			$id2 = $data['UT_ID2'];
		}
		$password = hash('sha512', $id1.$mdp.$id2);
		$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);

		$sql = "SELECT * FROM UTILISATEUR WHERE UT_LOGIN = '".$user."' and UT_PASSWORD = '".$password."'";

		$req = $conn->query($sql) or die('Erreur SQL !<br>');


		if ($req->num_rows > 0)

		{

			session_start ();

			$_SESSION['login'] = $_POST['login'];

			while ($data = mysqli_fetch_array($req))

				{

				$_SESSION['ETABADMIN'] = $data['UT_ADMINSITE'];

				if ($data['UT_ADMINSITE'] != '')
				{
					$_SESSION['STATUT'] = 'ADMIN';
				}
				else
				{
					$_SESSION['STATUT'] = 'CLIENT';
				}
				$_SESSION['NOM'] = decrypt3($data['UT_NOM'],$data['UT_ID2']);

				$_SESSION['PRENOM'] = decrypt3($data['UT_PRENOM'],$data['UT_ID2']);

				$_SESSION['ID'] = $data['UT_ID2'];

				$_SESSION['db_host'] = $db_host;

				$_SESSION['db_name'] = $db_name;

				$_SESSION['db_user'] = $db_user;

				$_SESSION['db_pwd'] = $db_pwd;
				$_SESSION['token'] = random1(25);
				$_SESSION['tokenresa'] = random1(25);

				$_SESSION['NbrPanier'] = 0;

				$UtilsateurValide = $data['UT_VALIDE'];

				}

			mysqli_close($conn);
			$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
			$sql = "UPDATE UTILISATEUR SET UT_LASTCONNECT = now() WHERE UT_LOGIN = '".$user."';";
			$req = $conn->query($sql) or die('Erreur SQL !!!!<br>');

		}

	}

}

function checkadmin($user,$mdp)
{
	require_once ("scripts/constantes.php");

	global $db_host;
	global $db_user;
	global $db_pwd;
	global $db_name;
	global $etablissement;
	global $URL_Site;

	$db_host=bdd_hote;
	$db_user=bdd_util;
	$db_pwd=bdd_mdp;
	$db_name=bdd_nom;

	$URL_Site = 'http://reservation.coworking-laverriere.fr/laverrieretest';

	$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
	$sql = "SELECT * FROM UTILISATEUR WHERE UT_LOGIN = '".$user."'";
	$req = $conn->query($sql) or die('Erreur SQL !<br>');
	if ($req->num_rows > 0)
	{
		while ($data = mysqli_fetch_array($req))
		{
			$id1 = $data['UT_ID1'];
			$id2 = $data['UT_ID2'];
		}
		$password = hash('sha512', $id1.$mdp.$id2);
		$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);

		$sql = "SELECT * FROM UTILISATEUR WHERE UT_LOGIN = '".$user."' and UT_PASSWORD = '".$password."' AND UT_SUPERADMIN = 'OUI';";

		$req = $conn->query($sql) or die('Erreur SQL !<br>');


		if ($req->num_rows > 0)

		{

			session_start ();

			$_SESSION['login'] = $_POST['login'];

			while ($data = mysqli_fetch_array($req))

				{

				if ($data['UT_SUPERADMIN'] == 'OUI')
				{
					$_SESSION['STATUT'] = 'ADMIN';
				}
				else
				{
					$_SESSION['STATUT'] = 'CLIENT';
				}
				$_SESSION['NOM'] = decrypt3($data['UT_NOM'],$data['UT_ID2']);

				$_SESSION['PRENOM'] = decrypt3($data['UT_PRENOM'],$data['UT_ID2']);

				$_SESSION['ID'] = $data['UT_ID2'];

				$_SESSION['db_host'] = $db_host;

				$_SESSION['db_name'] = $db_name;

				$_SESSION['db_user'] = $db_user;

				$_SESSION['db_pwd'] = $db_pwd;

				$_SESSION['NbrPanier'] = 0;
				$_SESSION['token'] = random1(25);
				$_SESSION['tokenresa'] = random1(25);

				$UtilsateurValide = $data['UT_VALIDE'];

				$_SESSION['SUPERADMIN'] = $data['UT_SUPERADMIN'];

				}

			mysqli_close($conn);
			$conn = mysqli_connect($db_host, $db_user, $db_pwd, $db_name);
			$sql = "UPDATE UTILISATEUR SET UT_LASTCONNECT = now() WHERE UT_LOGIN = '".$user."';";
			$req = $conn->query($sql) or die('Erreur SQL !!!!<br>');

		}

	}

}
