﻿﻿<?php
/**
 * fonction_email.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
//include ("include/fonction_general.php");
 
function emailInscri($destinataire, $rand, $user) {
	echo '1';
  $sujet = "Inscription Outil de réservation espace de coworking" ;
  $message = "<html>
      <body style=\"font-family: Arial;\">
        <div style=\"height: 46%; width : 70%; background-color: #E6FEF6; position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);\">
        <div style=\"height: 40%; width: 100%; background-color: #51b692;\">
            <p style=\"margin: 0%; padding: 1%;\">Ce mail vous a été envoyer par le site de réservation d'espace de coworking.</p>
            <p style=\"margin: 0%; padding: 1%;\">Vous trouverez ci-dessous la procédure d'inscription.</p>
            <p style=\"margin-left: 81%;\">L'équipe du site de .</p>
          </div>
          <h1 style=\"text-align: center;\">Merci de vous êtres inscrit</h1>
          <h2 style=\"text-align: center;\">Nom d'utilisateur $user</h2>
          <p style=\"text-align: center;\">Rendez-vous <a style=\"color: #F69730; font-weight: bold;\" href=\"".url_site."InscriForm.php?id=".$rand."\">ici</a></p>
        </div>
     </body>
  </html>"
;

  $headers = "MIME-Version: 1.0\r\n";
  $headers = "Content-type: text/html; charset=UTF-8\r\n";
  mail($destinataire,$sujet,$message,$headers);
}

function emailInscriUser($destinataire, $rand, $user) {
	echo '1';
  $sujet = "Inscription Outil de réservation espace de coworking" ;
  $message = "<html>
      <body style=\"font-family: Arial;\">
        <div style=\"height: 46%; width : 70%; background-color: #E6FEF6; position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);\">
        <div style=\"height: 40%; width: 100%; background-color: #51b692;\">
            <p style=\"margin: 0%; padding: 1%;\">Ce mail vous a été envoyer par le site de réservation d'espace de coworking.</p>
            <p style=\"margin: 0%; padding: 1%;\">Veuillez cliquer sur le lien pour saisir votre mot de passe dans le but de valider votre inscription.</p>
            <p style=\"margin-left: 81%;\">L'équipe du site de .</p>
          </div>
          <h1 style=\"text-align: center;\">Merci de vous êtres inscrit</h1>
          <h2 style=\"text-align: center;\">Nom d'utilisateur $user</h2>
          <p style=\"text-align: center;\">Rendez-vous <a style=\"color: #F69730; font-weight: bold;\" href=\"".url_site."ValidForm.php?id=".$user."\">ici</a></p>
        </div>
     </body>
  </html>"
;

  $headers = "MIME-Version: 1.0\r\n";
  $headers = "Content-type: text/html; charset=UTF-8\r\n";
  mail($destinataire,$sujet,$message,$headers);
}

function emailAdhesionValid($destinataire, $rand, $user, $etablissement, $email) {
	
  $sujet = "Adhésion à ".$etablissement ;
  $message = "<html>
      <body style=\"font-family: Arial;\">
        <div style=\"height: 46%; width : 70%; background-color: #E6FEF6; position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);\">
        <div style=\"height: 40%; width: 100%; background-color: #51b692;\">
            <p style=\"margin: 0%; padding: 1%;\">Ce mail vous a été envoyer par le site de réservation d'espace de coworking.</p>
			<p style=\"margin: 0%; padding: 1%;\">Votre adhésion à ".$etablissement. " a été validée.</p>
			<p style=\"text-align: center;\">Vous pouvez consulter ses information à partir de votre compte <a style=\"color: #F69730; font-weight: bold;\" href=\"".url_site."\">ici</a></p>
            
			<p style=\"margin: 0%; padding: 1%;\">Vous pouvez modifier supprimer vos données personnelles en envoyant un email à l'adresse suivante : ".$email. " </p>
            <p style=\"margin-left: 81%;\">L'équipe du site de réservation .</p>
          </div>
          <h1 style=\"text-align: center;\">Merci d'avoir choisi " .$etablissement ."</h1>
          <h2 style=\"text-align: center;\">Nom d'utilisateur $user</h2>
          
        </div>
     </body>
  </html>"
;

  $headers = "MIME-Version: 1.0\r\n";
  $headers = "Content-type: text/html; charset=UTF-8\r\n";
  mail($destinataire,$sujet,$message,$headers);
 
}


function emailNmdp($destinataire, $rand, $user){
  $sujet = "Nouveau mot de passe";
  $message = "<html>
    <body style=\"font-family: Arial;\">
      <div style=\"height: 46%; width : 70%; background-color: #E6FEF6; position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);\">
        <div style=\"height: 40%; width: 100%; background-color: #51b692;\">
          <p style=\"margin: 0%; padding: 1%;\">Ce mail vous a été envoyer par l'Outil de réservation espace de coworking.</p>
          <p style=\"margin: 0%; padding: 1%;\">Vous trouverez ci-dessous la procédure pour changer votre mot de passe.</p>
          <p style=\"margin-left: 81%;\">L'équipe.</p>
        </div>
        <h1 style=\"text-align: center;\">Demande de nouveau mot de passe de la part de l'utilisateur : $user</h1>
        <p style=\"text-align: center;\">Rendez-vous <a style=\"color: #F69730; font-weight: bold;\" href=\"".url_site."ChangementMdp.php?id=".$rand."\">ici</a></p>
      </div>
   </body>
  </html>";

 $headers = "MIME-Version: 1.0\r\n";
 $headers = "Content-type: text/html; charset=UTF-8\r\n";
 mail($destinataire,$sujet,$message,$headers);
}

function emailadhesion($type, $etablissement, $user) 
{
    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '" . $etablissement . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $fromAddr = $data['ET_EMAIL'];
        $lieu = $data['ET_LIBELLE'] + ' ' + $data['ET_ADRESSE1'] + ' ' + $data['ET_VILLE'];
        $organisateur = $data['ET_LIBELLE'];
        $sender_name = $data['ET_LIBELLE'];
        $organisateur_mail = $data['ET_EMAIL'];
        $sender_mail = $data['ET_EMAIL'];
    }

    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from UTILISATEUR where UT_LOGIN = '" . $user . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $recipientAddr = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $invited_mail = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $nom = decrypt($data['UT_NOM'], $_SESSION['ID']);
        $prenom = decrypt($data['UT_PRENOM'], $_SESSION['ID']);
    }
    if ($type == 'nouvelle') {
        $subjectStr = "Nouvelle adhesion";
    } else {
        $subjectStr = "Renouvellement d'adhesion";
    }

    /* Option de l'objet de rendez-vous "VCALENDAR" */
    $summary = "Résumé du rendez-vous";

    $to = $invited_mail . ';' . $fromAddr;

    // Subject
    $subject = 'Reservation ';

    // clé aléatoire de limite
    $boundary = md5(uniqid(microtime(), TRUE));

    // Headers
    $headers = 'From: resacowork@gmail.com' . "\r\n";
    $headers .= 'Mime-Version: 1.0' . "\r\n";
    $headers .= 'Content-Type: multipart/mixed;boundary=' . $boundary . "\r\n";
    $headers .= "\r\n";

    // Message
    $msg = 'This is a multipart/mixed message.' . "\r\n\r\n";

    // Texte
    $msg .= '--' . $boundary . "\r\n";
    $msg .= 'Content-type:text/plain;charset=utf-8' . "\r\n";
    $msg .= 'Content-transfer-encoding:8bit' . "\r\n";
    $msg .= 'Demande de ' . $subjectStr . " concernant " . $prenom . " " . $nom . "\r\n";
    $msg .= 'Login : ' . $user . "\r\n";

    // Fin
    $msg .= '--' . $boundary . "\r\n";

    // Function mail()
    mail($to, $subject, $msg, $headers);
}

function envoiresasimple($etablissement, $user, $typezone, $emlibelle) 
{
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '" . $etablissement . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $fromAddr = $data['ET_EMAIL'];
        $lieu = $data['ET_ADRESSE1'].", ".$data['ET_VILLE'];
        $organisateur = $data['ET_LIBELLE'];
        $sender_name = $data['ET_LIBELLE'];
        $organisateur_mail = $data['ET_EMAIL'];
        $sender_mail = $data['ET_EMAIL'];
    }
//echo $sql .'<BR>';

	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from GESTIONEMAIL where GE_ETABLISSEMENT = '" . $etablissement . "' and GE_TYPE = 'CONFIRMERESERVATION';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $Header = $data['GE_HEADER'];
        $Message = $data['GE_MESSAGE'];
        $Signature = $data['GE_SIGNATURE'];
        $copymail = $data['GE_CONTACT'];
    }
//echo $sql .'<BR>';

    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from UTILISATEUR where UT_LOGIN = '" . $user . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $recipientAddr = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $invited_mail = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $nom = decrypt($data['UT_NOM'], $_SESSION['ID']);
        $prenom = decrypt($data['UT_PRENOM'], $_SESSION['ID']);
    }
//echo $sql .'<BR>';
    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

    $sql = "select (timediff(now(),RE_DATERESA)) as toto, RE_DATERESA, date_format(RE_DATE,'%Y%m%d') as DATERESA, date_format(RE_HEUREDEBUT,'%H%i%s') as HEUREDEBUT,
		date_format(RE_HEUREFIN,'%H%i%s') as HEUREFIN, date_format(now(),'%Y%m%d') as DATECREATE, date_format(now(),'%H%i%s') as HEURECREATE, RE_NUMRESA,
		date_format(RE_DATE,'%d/%m/%Y') as DATERESATXT, date_format(RE_HEUREDEBUT,'%H:%i') as HEUREDEBUTTXT, date_format(RE_HEUREFIN,'%H:%i') as HEUREFINTXT,
		date_format(RE_DATERESA,'%d/%m/%Y') as DATETXT, RE_LIBELLEEMPLACEMENT
		from RESERVATION WHERE RE_USER = '" . $user . "' and RE_VALIDEE = 'OUI' 
		group by RE_DATERESA order by toto LIMIT 1";
//echo $sql .'<BR>';
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) 
	{
        $start_date = $data['DATERESA'] . 'T' . $data['HEUREDEBUT'];
        $end_date = $data['DATERESA'] . 'T' . $data['HEUREFIN'];
        $created_date = $data['DATECREATE'] . 'T' . $data['HEURECREATE'];
        $resanumber = $data['RE_NUMRESA'];
		$resadate = $data['DATERESATXT'] .' de ' .$data['HEUREDEBUTTXT'] .' a ' .$data['HEUREFINTXT'];
		$resaemplacement = $data['RE_LIBELLEEMPLACEMENT'];
    }

	$destinataire = 'resacowork@gmail.com';
	$sujet = $Header. " ".$prenom." ".$nom ." du " .$resadate;
	$message = "<html>
	<body style=\"font-family: Arial;\">
	  <div style=\"height: 46%; width : 70%; background-color: #E6FEF6; position: absolute; top: 30%; left: 50%; transform: translate(-50%, -50%);\">
		<div style=\"height: 40%; width: 100%; background-color: #51b692;\">
		  <p style=\"margin: 0%; padding: 1%;\">".$Message."</p>
		  <p style=\"margin-left: 81%;\">".$Signature."</p>
		</div>
		<p style=\"text-align: left;padding-left: 15px;\">Reservation : ".$resanumber."</p>
		<p style=\"text-align: left;padding-left: 15px;\">Date : ".$resadate."</p>
		<p style=\"text-align: left;padding-left: 15px;\">Emplacement : ".$resaemplacement."</p>
	  </div>
	</body>
	</html>";

 $headers[] = "MIME-Version: 1.0";
 $headers[] = "Content-type: text/html; charset=UTF-8";
 $headers[] = 'From: resacowork@gmail.com';
 $headers[] = 'Reply-To:resacowork@gmail.com';
 $headers[] = 'Bcc:'.$copymail;
 mail($destinataire,$sujet,$message,implode("\r\n", $headers));
}

function envoieinvitation($etablissement, $user, $typezone, $emlibelle) {

//Recherche de l'email expediteur
    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '" . $etablissement . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $fromAddr = $data['ET_EMAIL'];
        $lieu = $data['ET_ADRESSE1'].", ".$data['ET_VILLE'];
        $organisateur = $data['ET_LIBELLE'];
        $sender_name = $data['ET_LIBELLE'];
        $organisateur_mail = $data['ET_EMAIL'];
        $sender_mail = $data['ET_EMAIL'];
    }
	
	$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from GESTIONEMAIL where GE_ETABLISSEMENT = '" . $etablissement . "' and GE_TYPE = 'CONFIRMERESERVATION';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $Header = $data['GE_HEADER'];
        $Message = $data['GE_MESSAGE'];
        $Signature = $data['GE_SIGNATURE'];
        $copymail = $data['GE_CONTACT'];
    }

    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
    $sql = "select * from UTILISATEUR where UT_LOGIN = '" . $user . "';";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $recipientAddr = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $invited_mail = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
        $nom = decrypt($data['UT_NOM'], $_SESSION['ID']);
        $prenom = decrypt($data['UT_PRENOM'], $_SESSION['ID']);
    }

    $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

     $sql = "select (timediff(now(),RE_DATERESA)) as toto, RE_DATERESA, date_format(RE_DATE,'%Y%m%d') as DATERESA, date_format(RE_HEUREDEBUT,'%H%i%s') as HEUREDEBUT,
		date_format(RE_HEUREFIN,'%H%i%s') as HEUREFIN, date_format(now(),'%Y%m%d') as DATECREATE, date_format(now(),'%H%i%s') as HEURECREATE, RE_NUMRESA,
		date_format(RE_DATE,'%d/%m/%Y') as DATERESATXT, date_format(RE_HEUREDEBUT,'%H:%i') as HEUREDEBUTTXT, date_format(RE_HEUREFIN,'%H:%i') as HEUREFINTXT,
		date_format(RE_DATERESA,'%d/%m/%Y') as DATETXT, RE_LIBELLEEMPLACEMENT
		from RESERVATION WHERE RE_USER = '" . $user . "' and RE_VALIDEE = 'OUI' 
		group by RE_DATERESA order by toto LIMIT 1";
    $req = $conn->query($sql) or die('Erreur SQL !<br>');
    while ($data = mysqli_fetch_array($req)) {
        $start_date = $data['DATERESA'] . 'T' . $data['HEUREDEBUT'];
        $end_date = $data['DATERESA'] . 'T' . $data['HEUREFIN'];
        $created_date = $data['DATECREATE'] . 'T' . $data['HEURECREATE'];
        $resanumber = $data['RE_NUMRESA'];
		$resadate = $data['DATERESATXT'] .' de ' .$data['HEUREDEBUTTXT'] .' a ' .$data['HEUREFINTXT'];
		$resaemplacement = $data['RE_LIBELLEEMPLACEMENT'];
    }

    /* Option du MAIL */

    $subjectStr = $Header. " ".$prenom." ".$nom ." du " .$resadate;

    /* Option de l'objet de rendez-vous "VCALENDAR" */
    $summary = "Résumé du rendez-vous";

    $description = $emlibelle;

    $mineBoundaryStr = 'otecuncocehccj8234acnoc231'; //Offset pour l’interpréteur MIME : Doit être unique !
    $fichier = "ics/" . $resanumber . ".ics";
    $fp = fopen($fichier, "w+");
    fwrite($fp, "BEGIN:VCALENDAR\r\n");
    fwrite($fp, "PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\r\n");
    fwrite($fp, "VERSION:2.0\r\n");
    fwrite($fp, "CALSCALE:GREGORIAN\r\n");
    fwrite($fp, "BEGIN:VEVENT\r\n");
    fwrite($fp, "DTSTAMP:" . $created_date . "\r\n");
    fwrite($fp, "DTSTART:" . $start_date . "\r\n");
    fwrite($fp, "DTEND:" . $end_date . "\r\n");
    fwrite($fp, "STATUS:NEEDS-ACTION\r\n");
    fwrite($fp, "CATEGORIES:COWORKING \r\n");
    fwrite($fp, "SUMMARY:RESERVATION " . $resanumber ." ". $typezone . " " . $organisateur . "\r\n");
    fwrite($fp, "ORGANIZER:TOTO:MAILTO:". $fromAddr . "\r\n");
    fwrite($fp, "LOCATION:" . $lieu . "\r\n");
    fwrite($fp, "ATTENDEE:mailto:" . $fromAddr . "\r\n");
    fwrite($fp, "DESCRIPTION:Reservation de " . $nom ." ". $prenom . "\r\n");
    fwrite($fp, "END:VEVENT\r\n");
    fwrite($fp, "END:VCALENDAR\r\n");
    fclose($fp);

    $to = $invited_mail . ';' . $copymail;

// Subject
    $subject = $Header. " ".$prenom." ".$nom ." du " .$resadate;
//echo $subject;
// clé aléatoire de limite
    $boundary = md5(uniqid(microtime(), TRUE));

// Headers
    $headers = 'From: resacowork@gmail.com' . "\r\n";
    $headers .= 'Mime-Version: 1.0' . "\r\n";
    $headers .= 'Content-Type: multipart/mixed;boundary=' . $boundary . "\r\n";
    $headers .= "\r\n";

// Message
    $msg = 'This is a multipart/mixed message.' . "\r\n\r\n";

// Texte
     $msg .= '--' . $boundary . "\r\n";
    $msg .= 'Content-type:text/plain;charset=utf-8' . "\r\n";
    $msg .= 'Content-transfer-encoding:8bit' . "\r\n";
    $msg .= 'Un message avec une pièce jointe.' . "\r\n";

// Pièce jointe
    $file_name = $fichier;
    if (file_exists($file_name)) {
        $file_type = filetype($file_name);
        $file_size = filesize($file_name);

        $handle = fopen($file_name, 'r') or die('File ' . $file_name . 'can t be open');
        $content = fread($handle, $file_size);
        $content = chunk_split(base64_encode($content));
        $f = fclose($handle);

        $msg .= '--' . $boundary . "\r\n";
        $msg .= 'Content-type:' . $file_type . ';name=' . $file_name . "\r\n";
        $msg .= 'Content-transfer-encoding:base64' . "\r\n";
        $msg .= $content . "\r\n";
    }

// Fin
    $msg .= '--' . $boundary . "\r\n";

// Function mail()
	 mail($to, $subject, $msg, $headers);
	 
	  // suppression du fichier
	  $fichier = "ics/" . $resanumber . ".ics";
	  @unlink($fichier);
}

function emailEventSupr($etablissement, $user, $typezone, $emlibelle){

  //Recherche de l'email expediteur
      $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
      $sql = "select * from ETABLISSEMENT where ET_ETABLISSEMENT = '" . $etablissement . "';";
      $req = $conn->query($sql) or die('Erreur SQL !<br>');
      while ($data = mysqli_fetch_array($req)) {
          $fromAddr = $data['ET_EMAIL'];
          $lieu = $data['ET_ADRESSE1'].", ".$data['ET_VILLE'];
          $organisateur = $data['ET_LIBELLE'];
          $sender_name = $data['ET_LIBELLE'];
          $organisateur_mail = $data['ET_EMAIL'];
          $sender_mail = $data['ET_EMAIL'];
      }

      $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
      $sql = "select * from UTILISATEUR where UT_LOGIN = '" . $user . "';";
      $req = $conn->query($sql) or die('Erreur SQL !<br>');
      while ($data = mysqli_fetch_array($req)) {
          $recipientAddr = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
          $invited_mail = decrypt($data['UT_EMAIL'], $_SESSION['ID']);
          $nom = decrypt($data['UT_NOM'], $_SESSION['ID']);
          $prenom = decrypt($data['UT_PRENOM'], $_SESSION['ID']);
      }

      $conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);

      $sql = "select (timediff(now(),RE_DATERESA)) as toto, RE_DATERESA, date_format(RE_DATE,'%Y%m%d') as DATERESA, date_format(RE_HEUREDEBUT,'%H%i%s') as HEUREDEBUT,
  		date_format(RE_HEUREFIN,'%H%i%s') as HEUREFIN, date_format(now(),'%Y%m%d') as DATECREATE, date_format(now(),'%H%i%s') as HEURECREATE, RE_NUMRESA
  		from RESERVATION WHERE RE_USER = '" . $user . "' group by RE_DATERESA order by toto LIMIT 1";
      $req = $conn->query($sql) or die('Erreur SQL !<br>');
      while ($data = mysqli_fetch_array($req)) {
          $start_date = $data['DATERESA'] . 'T' . $data['HEUREDEBUT'];
          $end_date = $data['DATERESA'] . 'T' . $data['HEUREFIN'];
          $created_date = $data['DATECREATE'] . 'T' . $data['HEURECREATE'];
          $resanumber = $data['RE_NUMRESA'];
      }

      /* Option du MAIL */

      $subjectStr = "Reservation";

      /* Option de l'objet de rendez-vous "VCALENDAR" */
      $summary = "Résumé du rendez-vous";

      $description = $emlibelle;

      $mineBoundaryStr = 'otecuncocehccj8234acnoc231'; //Offset pour l’interpréteur MIME : Doit être unique !
      $fichier = "ics/" . $resanumber . ".ics";
      $fp = fopen($fichier, "w+");
      fwrite($fp, "BEGIN:VCALENDAR\r\n");
      fwrite($fp, "STATUS:CANCELLED\r\n");
      fwrite($fp, "METHOD:CANCEL\r\n");
      fwrite($fp, "PRODID:-//Microsoft Corporation//Outlook 12.0 MIMEDIR//EN\r\n");
      fwrite($fp, "VERSION:2.0\r\n");
      fwrite($fp, "CALSCALE:GREGORIAN\r\n");
      fwrite($fp, "BEGIN:VEVENT\r\n");
      fwrite($fp, "DTSTAMP:" . $created_date . "\r\n");
      fwrite($fp, "DTSTART:" . $start_date . "\r\n");
      fwrite($fp, "DTEND:" . $end_date . "\r\n");
      fwrite($fp, "STATUS:NEEDS-ACTION\r\n");
      fwrite($fp, "CATEGORIES:COWORKING \r\n");
      fwrite($fp, "SUMMARY:RESERVATION " . $resanumber ." ". $typezone . " " . $organisateur . "\r\n");
      fwrite($fp, "LOCATION:" . $lieu . "\r\n");
      fwrite($fp, "ATTENDEE:mailto:" . $fromAddr . "\r\n");
      fwrite($fp, "DESCRIPTION:Reservation de " . $nom ." ". $prenom . "\r\n");
      fwrite($fp, "END:VEVENT\r\n");
      fwrite($fp, "END:VCALENDAR\r\n");
      fclose($fp);

      $to = $invited_mail . ';' . $fromAddr;

  // Subject
      $subject = 'Reservation ';
  //echo $subject;
  // clé aléatoire de limite
      $boundary = md5(uniqid(microtime(), TRUE));

  // Headers
      $headers = 'From: resacowork@gmail.com' . "\r\n";
      $headers .= 'Mime-Version: 1.0' . "\r\n";
      $headers .= 'Content-Type: multipart/mixed;boundary=' . $boundary . "\r\n";
      $headers .= "\r\n";

  // Message
      $msg = 'This is a multipart/mixed message.' . "\r\n\r\n";

  // Texte
      $msg .= '--' . $boundary . "\r\n";
      $msg .= 'Content-type:text/plain;charset=utf-8' . "\r\n";
      $msg .= 'Content-transfer-encoding:8bit' . "\r\n";
      $msg .= 'Un message avec une pièce jointe.' . "\r\n";
exit;
  // Pièce jointe
      $file_name = $fichier;
      if (file_exists($file_name)) {
          $file_type = filetype($file_name);
          $file_size = filesize($file_name);

          $handle = fopen($file_name, 'r') or die('File ' . $file_name . 'can t be open');
          $content = fread($handle, $file_size);
          $content = chunk_split(base64_encode($content));
          $f = fclose($handle);
		  
		  @unlink($file_name);

          $msg .= '--' . $boundary . "\r\n";
          $msg .= 'Content-type:' . $file_type . ';name=' . $file_name . "\r\n";
          $msg .= 'Content-transfer-encoding:base64' . "\r\n";
          $msg .= $content . "\r\n";
      }

  // Fin
      $msg .= '--' . $boundary . "\r\n";
	  
	  // suppression du fichier
	  $fichier = "ics/" . $resanumber . ".ics";
	  
	  echo $fichier;
	  @unlink($fichier);
	  exit;

  // Function mail()
      mail($to, $subject, $msg, $headers);
	  
	  

}

function gestion_email($etablissement)
{
	include ("include/fonction_general.php");
	admentete_page("Gestion des emails");
	
	
	if((!isset($_POST["action"])) or ($_POST['action'] != "MODIF"))
	{
		?>
		<form   action="" method="post">
			<table style='text-align: left; width: 90%; height: 150px; font-family: "Century Gothic", Geneva, sans-serif; padding-left: 20px; padding-right: 20px;' border="0" cellpadding="0" cellspacing="0">
			  <tbody>
				<tr>
					<td colspan="3" rowspan="1" style="text-align: center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);">Gestion des emails </td>
					<td style="width: 30px;center; color: rgb(0, 1, 0); font-weight: bold; background-color: rgb(70, 181, 147);"><span style="font-family: Calibri;"><input id="boutonaffiche0" class="bouton3" type="button" value="Nouveau" style="width: 60px;" onclick="javascript:afficher(0);"></span>
				  </td>
				</tr>
		<?php
		if (isset($_GET['ETABLISSEMENT']))
		{
			$sql = "select * from GESTIONEMAIL WHERE GE_ETABLISSEMENT = '" .$_GET['ETABLISSEMENT']."'";
		}
		else
		{
			$sql = "select * from GESTIONEMAIL WHERE GE_ETABLISSEMENT = '" .$_SESSION['ETABADMIN']."'";
		}
		$nbparamemail = 0;
		$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
		{
			$nbparamemail++;
		?>
			<input type="hidden" value="<?php echo $data['GE_NUMERO']; ?>" name="IDEMAIL_<?php echo  $nbparamemail; ?>">
			<tr><td colspan="4" style="height: 10px;"/></tr>
			<tr>
				<td colspan="3" style="padding-bottom: 5px;"><u><?php echo $data['GE_LIBELLE']; ?>	</u></td>
				<td style="width: 30px;"><img class="photo" id="boutonaffiche<?php echo $nbparamemail; ?>" border="0" src="img/settings-gears.png" width="25" height="25" onclick="javascript:afficher(<?php echo $nbparamemail; ?>);"></td>
				
			</tr>
			<tr>
				<td style="height: 20px;" colspan="1" style="padding-bottom: 5px;">
					<label>Type d'email : </label>
					<input disabled maxlength="30" size="30" tabindex="1" name="" value='<?php echo $data['GE_TYPE']; ?>'>
				</td>
				<td style=" height: 20px;" colspan="2" style="padding-bottom: 5px;">
					<label>Libellé :</label>
					<input maxlength="30" size="30" tabindex="2" name="LIBELLE_<?php echo $nbparamemail; ?>" value='<?php echo $data['GE_LIBELLE']; ?>' required>
				</td>
			</tr>
			<?php $j=0; ?>
			<tr style="background-color: #edf1f6; ">
				<td colspan="4" class="liste1" id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<label>Type d'envoie (1=simple 2=avec fichier ICS) : </label>
					<SELECT name="TYPE_<?php echo $nbparamemail; ?>" id="TYPE_<?php echo $nbparamemail; ?>" style="width: 200px;">
						<?php
						if ($data['GE_TYPEENVOI'] == 1)
						{
						  echo '<OPTION VALUE="1" selected="selected">Envoi simple</OPTION>';
						  echo '<OPTION VALUE="2">Envoi avec fichier ICS</OPTION>';
						}
						else
						{
						  echo '<OPTION VALUE="2" selected="selected">Envoi avec fichier ICS</OPTION>';
						  echo '<OPTION VALUE="1">Envoi simple</OPTION>';
						}
						?>
					</SELECT>
					
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td class="liste1" colspan="4" id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<label>Entête message : </label>
					<TEXTAREA style="width: 800px;" name="HEADER_<?php echo $nbparamemail; ?>" rows="2" cols="200"><?php echo $data['GE_HEADER']; ?> </TEXTAREA>
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td class="liste1" colspan="4" id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<label>Corps du message: </label>
					<TEXTAREA style="width: 800px;" name="MESSAGE_<?php echo $nbparamemail; ?>" rows="6" cols="200"><?php echo $data['GE_MESSAGE']; ?> </TEXTAREA>
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td class="liste1" colspan="4" id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<label>Signature du message: </label>
					<TEXTAREA style="width: 800px;" name="SIGNATURE_<?php echo $nbparamemail; ?>" rows="2" cols="200"><?php echo $data['GE_SIGNATURE']; ?> </TEXTAREA>
				</td>
			</tr>
			<tr style="background-color: #edf1f6; ">
				<td class="liste1" colspan="4" id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<label>Email : </label>
					<input maxlength="250" size="250" tabindex="6" name="EMAIL_<?php echo $nbparamemail; ?>" value='<?php echo $data['GE_CONTACT']; ?>' style="width: 75%;" >
				</td>
			</tr>
			
			<tr style="background-color: #edf1f6; ">
				<td colspan="4" rowspan="1" class="liste1" align="center" style="width: 212px; height: 26px; " id="paramemail_<?php $j++; echo ((($nbparamemail) * 100) + $j); ?>">
					<input name="boutonvalid_<?php echo  $nbparamemail; ?>" id="boutonvalid_<?php echo $nbparamemail; ?>" class="bouton1" value="Valider" type="submit">
				</td>
			</tr>
		<?php
		}
		?>
		<tr>
		<input type="hidden" value="MODIF" name="action">
		<input type="hidden" value="<?php echo $nbparamemail; ?>" name="nbparamemail">
		</tr>
		</tbody>
		</table>
		
		</form>
		<?php
	}
	else
	{
		
		if ($_POST['action'] == "MODIF")
		{
			for ($i = 1; $i <= $_POST['nbparamemail']; $i++)
			{	
				if (isset($_POST['boutonvalid_' .$i]))
				{
					$sql = 'UPDATE GESTIONEMAIL SET GE_LIBELLE = "'.$_POST['LIBELLE_' .$i].'", GE_HEADER = "'.$_POST['HEADER_'.$i].'", GE_MESSAGE = "'.$_POST['MESSAGE_'.$i].'",
							GE_SIGNATURE = "'.$_POST['SIGNATURE_'.$i].'", GE_CONTACT = "'.$_POST['EMAIL_'.$i].'", GE_TYPEENVOI = "'.$_POST['TYPE_'.$i].'" WHERE GE_NUMERO = '.$_POST['IDEMAIL_'.$i].';';
						$cnx_bdd = ConnexionBDD();
					$result_req = $cnx_bdd->exec($sql);
				}
			}
			
		
			

			
		}
	}
}