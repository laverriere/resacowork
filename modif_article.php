<?php
/**
 * modif_article.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   3.0
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
    // Les informations de connexion sont bonnes, on affiche le contenu protég
	
	?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion CoWorking Haut de France</title>
			
			<script language="javascript">
			function myclosewindow()
			{
			window.close();
			window.opener.location.href="managearticle.php"
			}
			</script>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js" /></script>
			<script type="text/javascript">
			$(document).ready(function() {
			 
			
			 if ($('input[name="TYPEARTICLE"]').val() != 'ARTRE')
			 {
				$('#HEUREDEBUT').hide();   
				$('#HEUREDEBUT').attr('disabled', true);				
				$('#HEUREDEBUTTXT').hide(); 
				$('#HEUREFIN').hide();   
				$('#HEUREFIN').attr('disabled', false);				
				$('#HEUREFINTXT').hide();  // on cache le champ par défaut
			 }

			 
			});
			</script>
			
			<link rel="stylesheet" href="lib/file.css">

			</head>
			<body>
			
			<?php
			include ("include/fonction_admarticle.php"); 
			modif_article();
			?>
			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur
    
    header('Location: index.php');


}
?>