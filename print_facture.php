<?php
require('include/fpdf.php');

define('EURO', chr(128));

$months = array("janvier", "fevrier", "mars", "avril", "mai", "juin",
			"juillet", "aout", "septembre", "octobre", "novembre", "decembre");
$joursemaine = array('dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi');
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$sql = "SELECT *, date_format(GP_DATEPIECE,'%w') as NUMJOUR,date_format(GP_DATEPIECE,'%d') as JOUR, date_format(GP_DATEPIECE,'%m') as Mois, date_format(GP_DATEPIECE,'%Y') as YEARPIECE FROM PIECE LEFT JOIN ADRESSE ON AD_USER = GP_USER AND AD_TYPE = 'FACTURATION' WHERE GP_REFFACTURE = '" .$_GET['reffacture']. "';";
$req = $conn->query($sql) or die('Erreur SQL !!<br>');
while ($data = mysqli_fetch_array($req))
	{
		$nometab = $data['GP_ETLIBELLE'];
		$adresseetab  = $data['GP_ETADRESSE1'];
		$villeetab = $data['GP_ETCODEPOSTAL'] .' ' .$data['GP_ETVILLE'];
		$DateFacture = 'Le Quesnoy, le ' .$joursemaine[$data['NUMJOUR']] .' ' .$data['JOUR'] .' ' .$months[$data['Mois']-1] .' ' .$data['YEARPIECE'];
		$NomClient = $data['GP_USERNOM'] .' ' .$data['GP_USERPRENOM'];;
		$EmailClient = 'Email : ' .$data['GP_USER'];
		$NomFacture = $data['AD_NOM'];
		$AdresseFacture = $data['AD_ADRESSE1'];
		$VilleFacture = $data['AD_CODEPOSTAL'] .' ' .$data['AD_VILLE'];
		$RefFacture = 'Facture : ' .$_GET['reffacture'];
		$TotalHT = $data['GP_TOTALHT'];
		$TotalQteFact = $data['GP_TOTALQTEFACT'];

	}
mysqli_close();

class PDF extends FPDF
{

function Header()
{
	global $nometab;
	global $adresseetab;
	global $villeetab;
	global $NomClient;
	global $EmailClient;
	global $NomFacture;
	global $AdresseFacture;
	global $VilleFacture;
	global $RefFacture;
	global $DateFacture;
	$this->Image('img/logo-la-verriere.png',10,6,80);
    $this->SetFont('Arial','B',15);
	$this->SetX(-80);
    $this->Cell(0,15,'FACTURE',0,1,L);
	$this->Ln(10);
	$this->Cell(0,10,$nometab,0,1,L);
	$this->SetFont('Arial','B',10);
	$this->Cell(0,8,$adresseetab,0,0,L);
	$this->SetX(-80);
	$this->SetFont('Arial','',8);
	$this->Cell(0,5,$NomClient,0,1,L);
	$this->Ln(1);
	$this->SetFont('Arial','B',10);
	$this->Cell(0,8,$villeetab,0,0,L);
	$this->SetX(-80);
	$this->Cell(0,5,$NomFacture,0,1,L);
	$this->SetX(-80);
	$this->Cell(0,5,$AdresseFacture,0,1,L);
	$this->SetX(-80);
	$this->Cell(0,5,$VilleFacture,0,1,L);
	$this->SetX(-80);
	$this->Cell(0,5,$EmailClient,0,1,L);
	$this->Ln(5);
	$this->SetFont('Arial','B',15);
	$this->Cell(0,10,$RefFacture,0,0,L);
	$this->SetFont('Arial','B',10);
	$this->SetX(-100);
	$this->Cell(0,10,$DateFacture,0,1,R);
    // Saut de ligne
    $this->Ln(15);
	$this->SetFillColor(0,204,255);
	$this->SetTextColor(255);
	$this->SetDrawColor(0,0,0);
	$this->SetLineWidth(.3);
	$this->SetFont('Arial','B',10);
	$this->Cell(40,6,'Code Article',1,0,'C',true);
	$this->Cell(70,6,'Designation',1,0,'C',true);
	$this->Cell(20,6,'Quantite',1,0,'C',true);
	$this->Cell(30,6,'Prix Unitaire HT',1,0,'C',true);
	$this->Cell(30,6,'Prix Total HT',1,1,'C',true);
	$this->SetFillColor(224,235,255);
	$this->SetTextColor(0);
	$this->Rect(10,101,110,132);
$this->Rect(120,101,20,132);
$this->Rect(140,101,30,132);
$this->Rect(170,101,30,132);
}

function Footer()
{
	global $TotalHT;
	global $TotalQteFact;
	$this->SetY(-55);
	$this->SetFont('Arial','',10);
	$this->Cell(110,6,'TVA non applicable, Art 261-7 b du Code general des impots',0,0,L);
	$this->SetFont('Arial','',12);
	$this->Cell(50,6,'Total HT ',1,0,L);
	$this->Cell(30,6,$TotalHT .' '.chr(128),1,1,L);
	$this->SetX(-90);
	$this->SetFont('Arial','B',12);
	$this->Cell(50,6,'NET A PAYER ' .chr(128),1,0,L);
	$this->SetFillColor(224,235,255);
	$this->Cell(30,6,$TotalHT .' '.chr(128),1,1,L,true);
	$this->SetFont('Arial','',10);
	$this->SetY(-40);
	$this->Cell(0,6,utf8_decode('Réglement par chèque à l ordre de LA VERRIERE '),0,1,L);
	$this->Cell(20,6,'Ou par virement : ',0,1,L);
	$this->SetX(-180);
	$this->Cell(100,6,'IBAN FR7615629027250005452530108',0,1,L);
	$this->SetX(-180);
	$this->Cell(100,6,utf8_decode('Banque : 15629		Guichet : 02725		Compte N° 00054525301	Clé 08'),0,1,L);

    // Positionnement à 1,5 cm du bas
    $this->SetY(-15);
    // Police Arial italique 8
    $this->SetFont('Arial','I',8);
    // Numéro de page
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,60);

$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(224,235,255);
$sql = "SELECT * FROM LIGNE WHERE GL_REFFACTURE = '" .$_GET['reffacture']. "';";
$conn = mysqli_connect($_SESSION['db_host'], $_SESSION['db_user'], $_SESSION['db_pwd'], $_SESSION['db_name']);
$req = $conn->query($sql) or die('Erreur SQL !!<br>');
while ($data = mysqli_fetch_array($req))
	{
	$pdf->Cell(40,6,$data['GL_ARTICLENO'],1,0,'C',false);
	$pdf->Cell(70,6,$data['GL_ARTLIBELLE'],1,0,'L',false);
	$pdf->Cell(20,6,number_format($data['GL_QTEFACT'],2,',',''),1,0,'C',false);
	$pdf->Cell(30,6,number_format($data['GL_ARTTARIF'],2,',','') .' '.chr(128),1,0,'C',false);
	$pdf->Cell(30,6,number_format($data['GL_TOTALHT'],2,',','') .' '.chr(128),1,1,'C',false);
	$pdf->MultiCell(0,6,$data['GL_COMMENTAIRE'],1,'L',true);
	}
$pdf->Output();
?>
