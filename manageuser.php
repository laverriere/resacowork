﻿<?php
/**
 * manageuser.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2019-01-30 12:00:00 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   3.1.1
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{

    // Les informations de connexion sont bonnes, on affiche le contenu protégé

	if((isset($_GET['ACTION'])))
	{
	?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion CoWorking Haut de France</title>

			<script language="javascript">
			function myclosewindow()
			{
			window.close();
			window.opener.location.href="manageuser.php"
			}
			</script>

			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:0px;">
			<?php
			include ("include/fonction_admutilisateur.php");

			if ($_GET['ACTION'] == 'creation')
			{
				if(isset($_POST['action']))
				{
					if ($_POST['action']=='ajout')
					{
						admentete_page("Création d'un utilisateur");
						insert_utilisateur();
					}

				}
				else
				{
				admentete_page("Création d'un utilisateur");
				nouvel_utilisateur();
				}
			}
			if ($_GET['ACTION'] == 'suppression')
			{

				if(isset($_POST['action']))
				{
					if ($_POST['action']=='suppression')
					{
						delete_utilisateur();
					}

				}
				else
				{
					popup_delete_utilisateur('admin');
				}
			}
			if ($_GET['ACTION'] == 'modification')
			{
				if(isset($_POST['action']))
				{
					if ($_POST['action']=='modif')
					{
						update_utilsateur();
					}

				}
				else
				{

				modif_utilisateur('admin');
				}
			}


			?>

			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php

	}
	else
	{
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

			<link rel="icon" href="img/laverriere.ico" />

			<title>Gestion Tiers Lieux Haut de France</title>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="https://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>

			
			<script type="text/javascript">
				jQuery(function($){
			   $.datepicker.regional['fr'] = {
				  closeText: 'Fermer',
				  prevText: '&#x3c;Préc',
				  nextText: 'Suiv&#x3e;',
				  currentText: 'Courant',
				  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
				  'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				  monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
				  'Jul','Aoû','Sep','Oct','Nov','Déc'],
				  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
				  dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
				  dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
				  weekHeader: 'Sm',
				  //dateFormat: 'dd/mm/yy',
							dateFormat: 'dd/mm/yy',
				  firstDay: 1,
				  isRTL: false,
				  showMonthAfterYear: false,
				  yearSuffix: ''};
			   $.datepicker.setDefaults($.datepicker.regional['fr']);
			});

			function cachertout()
			{
				var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{

					// affichage du calendrier
					$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
					$( "#datepicker_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker1_" + i).datepicker( $.datepicker.regional[ "fr" ] );
					$( "#datepicker2_" + i ).datepicker( $.datepicker.regional[ "fr" ] );
				}
			};
			function cacher (id)
			{

				// Cacher tout
				var i;
				for (i = 0; i < $('#nbruseraffich').val() + 1; i++)
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = (i * 100) + j;
						$('#tdparamuser_' + index).hide();
					}
					$('#boutonaffiche'+i).hide();

				}

				$('#paramuser_010_0').attr('disabled', true);


				//Afficher seulement le choix
				var test = 'tdparamuser_'+ (id+1);
				if (id == 0)
				{
					$('#paramuser_010_0').attr('disabled', false);
				}
				if(document.getElementById(test).style.display=="none")
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = id + j;
						$('#tdparamuser_' + index).show();
					}
				}
				else
				{
					var j;
					for (j = 0; j < 30; j++)
					{
						var index = id + j;
						$('#tdparamuser_' + index).hide();
					}
				}

			};

			$(document).ready(function() {
				var j;

					var champ = 'ADHESION'+j;
					$('select').change(function() {
						var x = $(this).attr("id");
						var valeur = $(this).val(); // valeur sélectionnée
						var valname = x.substring(0,13);
						var valid = (x.substring(14));
						if (valname == 'paramuser_010')
						{
							if (valeur == 'VALIDE')
							{
								$('#datepicker_'+valid).show();
								$('#datepicker1_'+valid).show();

							}
							else
							{
								$('#datepicker_'+valid).hide();
								$('#datepicker1_'+valid).hide();
							}
						}
					});

			});
			</script>

			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body  onload="javascript:cachertout();">


			<?php
			include ("include/fonction_admutilisateur.php");
			if(isset($_POST['nbruseraffich']))
			{
				new_updateuser();
			}
			admentete_page("Gestion des utilisateurs");
			affiche_user2();

			//footer();
			?>

			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
    <?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur

    header('Location: index.php');


}
?>
