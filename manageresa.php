﻿<?php
/**
 * manageresa.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();

// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
    // Les informations de connexion sont bonnes, on affiche le contenu protégé
	if((isset($_GET['ACTION'])))
	{
		if ($_GET['ACTION']=='affichedate')
		{
			if ((isset($_POST['action']) && ($_POST['ajout_emplacement'] != -1) && ($_POST['nbrplace'] != '')) || (isset($_POST['action']) && ($_POST['delete_emplacement'] != -1)))
			{
				?>
				 <!-- Insérez ici le contenu à protéger -->
				<!DOCTYPE html>
				<html lang="fr">
				<head>
				<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<meta name="viewport" content="width=device-width">

				<link rel="icon" href="img/laverriere.ico" />
				<title>Gestion Tiers Lieux Haut de France</title>
				<?php
				$mois = $_GET['mois'];
				$an = $_GET['annee'];
				?>
				<script language="javascript">
				function myclosewindow()
				{
				window.close();
				window.opener.location.href='manageresa.php?typeplace=<?php echo $_GET['typeplace']; ?>&mois=<?php echo $mois; ?>&an=<?php echo $an; ?>';
				}
				</script>
				<link rel="stylesheet" href="lib/bootstrap.min.css">
				<link rel="stylesheet" href="lib/style.css">

				<!-- SCRIPTS -->
				<script
				  src="https://code.jquery.com/jquery-3.2.1.min.js"
				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				  crossorigin="anonymous"></script>
				</head>
				<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:0px;">
				<?php
				include ("include/fonction_resa.php");
				echo $_POST['ajout_nbrplace'];
				if ($_POST['Panier'] == "AddResa")
				{
					$_SESSION['NbrPanier'] = $_SESSION['NbrPanier'] + 1;
					insert_resa();
				}
				elseif ($_POST['Panier'] == "DeleteResa")
				{
					if ($_SESSION['NbrPanier'] > 0)
					{
					$_SESSION['NbrPanier'] = $_SESSION['NbrPanier'] - 1;
					}
					retire_resa();
				}
				?>
				</body>
				</html>
				<!-- Fin du contenu à protéger -->
				<?php
			}
			else
			{
			?>
				  <!DOCTYPE html>
					<html lang="fr">
					<head>
					<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
						<meta name="viewport" content="width=device-width">

					<link rel="icon" href="img/laverriere.ico" />
					<title>Gestion Tiers Lieux Haut de France</title>
					<script language="javascript">
					</script>
					<link rel="stylesheet" href="lib/bootstrap.min.css">
					<link rel="stylesheet" href="lib/style.css">

					<!-- SCRIPTS -->
					<script
					  src="https://code.jquery.com/jquery-3.2.1.min.js"
					  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
					  crossorigin="anonymous"></script>
					<script type='text/javascript'>
					function validation1(object)
					{
						var obj =document.getElementById("nbrplace");
						obj.value = object.value;
						if (object.value != -1)
						{
							obj =document.getElementById("nbrplace");
							obj.value = object.text;
						}
						else
						{
							obj =document.getElementById("nbrplace");
							obj.value = '';
						}
					}
					function getXhr(){
                                var xhr = null;
				if(window.XMLHttpRequest) // Firefox et autres
				   xhr = new XMLHttpRequest();
				else if(window.ActiveXObject){ // Internet Explorer
				   try {
			                xhr = new ActiveXObject("Msxml2.XMLHTTP");
			            } catch (e) {
			                xhr = new ActiveXObject("Microsoft.XMLHTTP");
			            }
				}
				else { // XMLHttpRequest non supporté par le navigateur
				   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
				   xhr = false;
				}
                                return xhr;
			}
			/**
			* Méthode qui sera appelée sur le click du bouton
			*/
			function go(){
				var xhr = getXhr();
				// On défini ce qu'on va faire quand on aura la réponse
				xhr.onreadystatechange = function(){
					// On ne fait quelque chose que si on a tout reçu et que le serveur est ok
					if(xhr.readyState == 4 && xhr.status == 200){
						leselect = xhr.responseText;
						// On se sert de innerHTML pour rajouter les options a la liste
						document.getElementById('ajout_nbrplace').innerHTML = leselect;
					}
				}

				// Ici on va voir comment faire du post
				xhr.open("POST","ajaxlivre.php",true);
				// ne pas oublier ça pour le post
				xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
				// ne pas oublier de poster les arguments
				// ici, l'id de l'auteur
				sel = document.getElementById('ajout_emplacement');
				idselectemplacement = sel.options[sel.selectedIndex].value;
				xhr.send("selectemplacement="+idselectemplacement);
			}
					</script>
					</head>
					<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:0px;">
					<?php
					include ("include/fonction_resa.php");
					affiche_date()
					?>
					</body>
					</html>
				  <!-- Fin du contenu à protéger -->
				<?php
			}
		}
		elseif ($_GET['ACTION']=="ajoutpanier")
		{
			?>
		  <!-- Insérez ici le contenu à protéger -->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<meta name="viewport" content="width=device-width">

			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
			<?php
			$mois = $_GET['mois'];
			$an = $_GET['annee'];
			//echo $_GET['typeplace'];
			?>
			<script language="javascript">
			function myclosewindow()
			{
			window.close();
			window.opener.location.href='manageresa.php?typeplace=<?php echo $_GET['typeplace']; ?>&mois=<?php echo $mois; ?>&an=<?php echo $an; ?>';
			}
			</script>
				<link rel="stylesheet" href="lib/bootstrap.min.css">
				<link rel="stylesheet" href="lib/style.css">

				<!-- SCRIPTS -->
				<script
				  src="https://code.jquery.com/jquery-3.2.1.min.js"
				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				  crossorigin="anonymous"></script>
			</head>
			<body style="margin: auto;margin-top: 0px;padding-bottom: 0px; padding:0px;">
			<?php
			include ("include/fonction_resa.php");
			echo $_GET['ajout'];
			if ($_GET['ajout'] == "OUI")
			{
				$_SESSION['NbrPanier'] = $_SESSION['NbrPanier'] + 1;
				insert_resa();
			}
			elseif ($_GET['ajout'] == "NON")
			{
				if ($_SESSION['NbrPanier'] > 0)
				{
				$_SESSION['NbrPanier'] = $_SESSION['NbrPanier'] - 1;
				}
			}
			?>
			</body>
			</html>
		  <!-- Fin du contenu à protéger -->
		<?php
		}
	}
	else
	{
		?>
		  <!-- Insérez ici le contenu à protéger -->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<?php
			include ("include/fonction_resa.php");
			?>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<meta name="viewport" content="width=device-width">
			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
			<script language="javascript">
			function refreshwindow()
			{
			sel = document.getElementById('zone');
			idselectemplacement = sel.options[sel.selectedIndex].value;
			window.close();
			window.location.href='manageresa.php?typeplace='+idselectemplacement;
			}
			</script>
				<link rel="stylesheet" href="lib/bootstrap.min.css">
				<link rel="stylesheet" href="lib/style.css">
				<!-- SCRIPTS -->
				<script
				  src="https://code.jquery.com/jquery-3.2.1.min.js"
				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				  crossorigin="anonymous"></script>
			</head>
			<body>
			<?php
				///$_SESSION['token'] = random(25); // clé aléatoire de 25 caractères créée a partir de la fonction
				if(isset($_POST['ajout']) || isset($_POST['suppression'])) {
					update_resa();
				};
				entete_page('');
				if (isset($_GET['typeplace']))
				{
					affiche_resa($_GET['typeplace'], $_GET['etablissement']);
				}
				else
				{
					affiche_resa(0, $_GET['etablissement']);
				}

			?>
			</body>
			</html>
		  <!-- Fin du contenu à protéger -->
    <?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur
    header('Location: index.php');


}
?>
