<?php

// Afficher les erreurs à l'écran


require('fpdf.php');
require ("../scripts/constantes.php");
require ("../scripts/fonctions.php");

define('EURO', chr(128));

$joursemaine = array('dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi');			
$mois = array('janvier','février','mars','avril','mai','juin','juillet','aout','septembre','octobre','novembre','décembre');
$sql = "SELECT *, date_format(GP_DATEPIECE,'%w') as NUMJOUR,date_format(GP_DATEPIECE,'%d') as JOUR, date_format(GP_DATEPIECE,'%m') as MOIS, date_format(GP_DATEPIECE,'%Y') as YEARPIECE, CC_LIBELLE,
		date_format(GP_DATEECHEANCE,'%w') as NUMJOURECH,date_format(GP_DATEECHEANCE,'%d') as JOURECH, date_format(GP_DATEECHEANCE,'%m') as MoisECHE, date_format(GP_DATEECHEANCE,'%Y') as YEARECHE
		FROM PIECE 
		LEFT JOIN ETABLISSEMENT ON ET_ETABLISSEMENT = GP_ETABLISSEMENT
		LEFT JOIN CHOIXCODE ON CC_TYPE = 'MOIS' AND CC_CODE = date_format(GP_DATEPIECE,'%m')
		WHERE GP_REFFACTURE = '" .$_GET['reffacture']. "';";

$cnx_bdd = ConnexionBDD();
		$result_req = $cnx_bdd->query($sql);
		$tab_r = $result_req->fetchAll();
		foreach ($tab_r as $data)
	{
		$nometab = $data['GP_ETLIBELLE'];
		$adresseetab  = $data['GP_ETADRESSE1'];
		$villeetab = $data['GP_ETCODEPOSTAL'] .' ' .$data['GP_ETVILLE'];
		$DateFacture = $joursemaine[$data['NUMJOUR']] .' ' .$data['JOUR'] .' ' .$data['CC_LIBELLE'] .' ' .$data['YEARPIECE'];
		$NomClient = decrypt2($data['GP_USERNOM'],$data['GP_USERID']) .' ' . decrypt2($data['GP_USERPRENOM'],$data['GP_USERID']) ;
		$EmailClient = decrypt2($data['GP_USEREMAIL'],$data['GP_USERID']);
		$emailetab  = $data['ET_EMAIL'];
		$webetab  = $data['ET_SITEWEB'];
		$NomFacture = decrypt2($data['GP_USERNOM'],$data['GP_USERID']) .' ' . decrypt2($data['GP_USERPRENOM'],$data['GP_USERID']) ;
		$AdresseFacture = decrypt2($data['GP_USERADRESSE1'],$data['GP_USERID']);
		$VilleFacture = decrypt2($data['GP_USERCODEPOSTAL'],$data['GP_USERID']) .' ' .decrypt2($data['GP_USERVILLE'],$data['GP_USERID']);
		$RefFacture = $_GET['reffacture'];
		$TotalHT = $data['GP_TOTALHT'];
		$TotalQteFact = $data['GP_TOTALQTEFACT'];
		$DateEcheance = $joursemaine[$data['NUMJOURECH']] .' ' .$data['JOURECH'] .' ' .$data['CC_LIBELLE'] .' ' .$data['YEARECHE'];
		$NomBanque = $data['ET_BANQUENOM'];
		$BanqueIBAN = $data['ET_IBAN'];
		$BanqueBIC = $data['ET_BANQUEBIC']; 
		$BanqueCode = $data['ET_BANQUECODE'];
		$banquecompte = $data['ET_BANQUECPT'];
		$banqueguichet = $data['ET_BANQUEGUICHET'];
		$banqueclef = $data['ET_BANQUECLEF']; 
		$siretetab  = $data['ET_SIRET']; 
		$napetab = $data['ET_NAP']; 
		$CodeCient = $data['GP_USER'];
		$logo = "../img/".$data['ET_IMAGENOM'];
		$createur = decrypt2($data['GP_CREATEUR'],$data['GP_USERID']);
		

	}
	
	
	
$sql = "SELECT COUNT(*) AS NBRLIGNE FROM LIGNE WHERE GL_REFFACTURE = '" .$_GET['reffacture']. "';";

$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
foreach ($tab_r as $data)
{
	$nbrligne = $data['NBRLIGNE'];
}


class PDF extends FPDF
{

	function Header()
	{
		global $nometab;
		global $adresseetab;
		global $villeetab;
		global $NomClient;
		global $EmailClient;
		global $NomFacture;
		global $AdresseFacture;
		global $VilleFacture;
		global $RefFacture;
		global $DateFacture;
		global $DateEcheance;
		global $CodeCient;
		global $emailetab;
		global $webetab;
		global $logo;
		global $nbrligne;
		global $createur;
		$this->Image($logo,10,6,80);
		$this->SetFont('Arial','B',15);
		$this->SetX(-80);
		$this->Cell(0,15,'FACTURE',0,1,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-80);
		$this->Cell(0,0,'Ref facture : '.$RefFacture ,0,1,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-80);
		$this->Cell(0,10,'Date facture : '.$DateFacture ,0,1,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-80);
		$this->Cell(0,0,utf8_decode('Date echéance : '.$DateEcheance) ,0,1,'L');
		$this->Ln(10);
		$this->Rect(10,45,90,30);
		$this->Rect(110,45,90,30);
		
		$this->Cell(0,10,utf8_decode($nometab),0,0,'L');
		$this->SetFont('Arial','B',10);
		$this->SetX(-100);
		$this->Cell(0,10,'Cient : ' .$CodeCient,0,1,'L');
		
		$this->SetFont('Arial','B',10);
		$this->Cell(0,0,utf8_decode($adresseetab),0,0,'L');
		$this->SetX(-100);
		$this->Cell(0,0,utf8_decode($NomFacture),0,1,'L');
		
		$this->SetFont('Arial','B',10);
		$this->Cell(0,10,utf8_decode($villeetab),0,0,'L');
		$this->SetX(-100);
		$this->Cell(0,10,utf8_decode($AdresseFacture),0,1,'L');
		
		$this->SetFont('Arial','B',10);
		$this->SetX(-100);
		$this->Cell(0,0,utf8_decode($VilleFacture),0,1,'L');
				
		$this->SetFont('Arial','B',10);
		$this->Cell(0,0,'Email : ' .utf8_decode($emailetab),0,1,'L');
		
		$this->SetFont('Arial','B',10);
		$this->Cell(0,10,'Web : ' .utf8_decode($webetab),0,0,'L');
		$this->SetX(-100);
		$this->Cell(0,10,'Email : ' .utf8_decode($EmailClient),0,1,'L');
		$this->Cell(0,10,utf8_decode('Facture établi par : ') .utf8_decode($createur),0,1,'L');
		
		// Saut de 'L'igne
		//$this->Ln(5);
		$this->SetFillColor(0,204,255);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,0,0);
		$this->SetLineWidth(.3);
		$this->SetFont('Arial','B',10);
		$this->Cell(40,6,'Code Article',1,0,'C',true);
		$this->Cell(70,6,'Designation',1,0,'C',true);
		$this->Cell(20,6,'Quantite',1,0,'C',true);
		$this->Cell(30,6,'Prix Unitaire HT',1,0,'C',true);
		$this->Cell(30,6,'Prix Total HT',1,1,'C',true);
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->Rect(10,91,110,130);
		$this->Rect(120,91,20,130);
		$this->Rect(140,91,30,130);
		$this->Rect(170,91,30,130);
	}
	
	function Footer()
	{
		global $TotalHT;
		global $TotalQteFact;
		global $nometab;
		global $NomBanque;
		global $BanqueCode;
		global $banqueguichet;
		global $banquecompte;
		global $BanqueIBAN;
		global $BanqueBIC;
		global $banqueclef;
		global $adresseetab;
		global $villeetab;
		global $siretetab;
		global $napetab;
		global $nbrligne;
		global $nbrligneprint;
		if ($nbrligneprint >= $nbrligne)
		{			
		$this->SetY(-75);
		//$this->Ln(5);
		//$this->SetX(-90);
		$this->SetFont('Arial','',10);
		$this->Cell(110,6,'TVA non applicable, Art 261-7 b du Code general des impots',0,0,'L');
		$this->SetFont('Arial','',12);
		$this->Cell(50,6,'Total HT ',1,0,'L');
		$this->Cell(30,6,$TotalHT .' '.chr(128),1,1,'L');
		$this->SetX(-90);
		$this->SetFont('Arial','B',12);
		
		$this->Cell(50,6,'NET A PAYER ' .chr(128),1,0,'L');
		$this->SetFillColor(224,235,255);
		//$this->Cell(30,6,$TotalHT .' '.chr(128),1,1,L,true);
		$this->Cell(30,6,$TotalHT .' '.chr(128),1,1,'L',true);
		}
		$this->SetFont('Arial','',10);
		$this->SetY(-60);
		$this->Cell(0,6,utf8_decode('Condition de règlement : A réception de la facture'),0,1,'L');
		
		$this->SetFont('Arial','',10);
		$this->SetY(-55);
		$this->Cell(0,6,utf8_decode('Réglement par chèque à l ordre de : ' .$nometab),0,1,'L');
		$this->Cell(20,6,'Ou par virement : ',0,1,'L');
		$this->SetFont('Arial','',8);
		$this->SetX(-180);
		$this->Cell(100,0,utf8_decode('Banque : ' .$NomBanque),0,1,'L');
		$this->SetX(-180);
		$this->Cell(100,6,utf8_decode('Banque : ' .$BanqueCode .' Guichet : ' .$banqueguichet .'	Compte N° ' .$banquecompte .' Clé : ' .$banqueclef),0,1,'L');
		$this->SetX(-180);
		$this->Cell(100,0,utf8_decode('Code IBAN : ' .$BanqueIBAN .' Code BCSWIFT : ' .$BanqueBIC ),0,1,'L');
		
		$this->SetY(-25);
		$this->SetFont('Arial','B',8);
		$this->Cell(0,0,utf8_decode('Siège social : ' .$nometab .' - ' .$adresseetab .' - ' .$villeetab ),0,1,'C');
		$this->SetFont('Arial','',8);
		$this->Cell(0,8,utf8_decode('Association loi 1901 ou assimilé - Siret : ' .$siretetab),0,1,'C');
		$this->Cell(0,0,utf8_decode('NAF-APE : ' .$napetab),0,1,'C');
		$this->SetFont('Arial','I',8);
		// Numéro de page
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		
	
	}
}

$pdf = new PDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true,80);

$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(224,235,255);
$numligne = 0;
$libligne = '';
$nbrligneprint = 1;
$sql = "SELECT * FROM LIGNE WHERE GL_REFFACTURE = '" .$_GET['reffacture']. "';";
$sql = "select GL_NUMLIGNE, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT,GL_PRIXPARPERSONNE, GL_PRIXPARPLACE, GL_TOTALHT, GL_ARTTARIF, GL_LIBELLE
		from LIGNE 
		WHERE GL_REFFACTURE = '" .$_GET['reffacture']. "'
		ORDER BY GL_NUMLIGNE;";

$cnx_bdd = ConnexionBDD();
$result_req = $cnx_bdd->query($sql);
$tab_r = $result_req->fetchAll();
foreach ($tab_r as $data)
	{
		if ($libligne != $data['GL_LIBELLE'])
		{
			if ($libligne != '')
			{
				$pdf->Ln(1);
			}
			$pdf->Cell(40,6,utf8_decode($data['GL_LIBELLE']),0,1,'L',false);
			$pdf->Ln(1);
			$libligne = $data['GL_LIBELLE'];
		}
		
		if($numligne != $data['GL_NUMLIGNE'] && $data['GL_QTEFACT'] != 0)
		{
			$pdf->Cell(40,6,utf8_decode($data['GL_ARTICLENO']),0,0,'C',false);
			$pdf->Cell(70,6,utf8_decode($data['GL_ARTLIBELLE']),0,0,'L',false);
			$pdf->Cell(20,6,number_format($data['GL_QTEFACT'],2,',',''),0,0,'C',false);
			$pdf->Cell(30,6,number_format($data['GL_ARTTARIF'],2,',',''),0,0,'C',false);
			$pdf->Cell(30,6,number_format($data['GL_TOTALHT'],2,',','') .' '.chr(128),0,1,'C',false);
			$numligne = $data['GL_NUMLIGNE'];
		}
		
		
		
		$nbrligneprint ++;
	
	}
	

//for ($i = 1; $i <= 50; $i++) {
//    $pdf->Cell(0,6,'test',1,1,'L',false);
//}
$pdf->Output();

?>
