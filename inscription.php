<?php
/**
 * inscription.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */
//info BDD

//Envoie mail
require ("include/fonction_email.php");
include ("include/fonction_general.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="img/laverriere.ico" />
        <title>Inscription</title>
        <link rel="stylesheet" href="lib/bootstrap.min.css">
        <link rel="stylesheet" href="lib/style.css">

    </head>

    <body>
        <?php
        entete_page_login("Inscription");
        ?>
        <div class="login_form">
            <form action="" method="post" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
                <p class="error"> Un email va vous êtres envoyer avec votre nom d'utilisateur et un lien pour compléter votre inscription. </p>
                <label for="email" class="col-xs-12"> Email :</label>
                <input type="email" name="Email" class="col-xs-12" id="Email" required="Requis">

                <input type="submit" name="Envoie" value="Envoie" class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                <a style="color:#F69730" href="index.php" class="forgot_passwd col-xs-12" >retour</a>
            </form>
        </div>

        <?php
        $cnx_bdd = ConnexionBDD();
        // Vérifie si la connection à la bdd est bonne
        if ($cnx_bdd != false) {
            // Quand l'user clique sur le bouton d'envoie
            if (isset($_POST['Envoie'])) {
                // Declaration des variable
				$id2 = rand(1,99999999);
                $Email = $_POST['Email'];
                $EmailCrypt = encrypt($Email,$id2);
                $date = date("Y-m-d");
                $Rand = random(20);

                $req = "INSERT INTO UTILISATEUR (UT_LOGIN, UT_VERIF, UT_PASSWORD, UT_NOM, UT_PRENOM, UT_ADRESSE1, UT_ADRESSE2, UT_VILLE, UT_CODEPOSTAL, UT_PAYS, UT_EMAIL, UT_STATUT, UT_ID1, UT_ID2, 
					UT_ETABLISSEMENT, UT_VALIDE, UT_INITIALE, UT_IDMODIF, UT_CIVILITE, UT_IDDATEDEMANDE, UT_TIERSGESTION, UT_ADMINSITE, UT_LASTCONNECT, UT_LASTMODIF) VALUES
				('', '$Rand','','','','','','','','','$EmailCrypt','','','".$id2."','','','','','','$date','','','1900-01-01','1900-01-01')";
				
				
                $result_req = $cnx_bdd->exec($req);
//$login = "C".str_pad($data1['UT_IDUSER'],8,"0",STR_PAD_LEFT);
                $req = "SELECT UT_IDUSER FROM UTILISATEUR WHERE UT_EMAIL = '$EmailCrypt' AND UT_VERIF = '".$Rand."'" ;
                $result_req = $cnx_bdd->query($req);
                $tab_r = $result_req->fetchALL();
                foreach ($tab_r as $r) {
                  $req = "UPDATE UTILISATEUR SET UT_LOGIN='C".str_pad($r['UT_IDUSER'],8,"0",STR_PAD_LEFT)."' WHERE UT_EMAIL= '".$EmailCrypt."' AND UT_VERIF = '".$Rand."'";
                  $result_req = $cnx_bdd->exec($req);
                }

                $req = "SELECT UT_LOGIN FROM UTILISATEUR WHERE UT_EMAIL = '$EmailCrypt' AND UT_VERIF = '$Rand'";
                $result_req = $cnx_bdd->query($req);
                $tab_r = $result_req->fetchALL();
                foreach ($tab_r as $r) {
                  emailInscri($Email, $Rand, $r['UT_LOGIN']);
                }

                ?> <script> alert('Un mail vous a été envoyé\r(Cela peut prendre quelque minutes.)')</script> <?php
                ?><script type="text/javascript"> window.location = "index.php"
                  </script>';
                <?php
            }
        } else {
            echo 'erreur bdd';
        }
        ?>
    </body>
</html>




<?php



$req = "SELECT UT_PRENOM, UT_NOM FROM UTILISATEUR";
$result_req = $cnx_bdd->query($req);
$tab_r = $result_req->fetch()




?>
