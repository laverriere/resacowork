﻿<?php
/**
 * cloture_resa.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2019-01-30 12:00:00 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   3.1.1
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
	//echo $_POST['login'];
    // Les informations de connexion sont bonnes, on affiche le contenu protégé
	if(isset($_POST['login'],$_POST['action']))
	{
		?>
		<!DOCTYPE html>
		<html lang="fr">
		<head>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

		<link rel="icon" href="img/laverriere.ico" />
		<title>Gestion Tiers Lieux Haut de France</title>

		<link rel="stylesheet" href="lib/file.css">
		</head>
		<body>
		<?php

		include ("include/fonction_admutilisateur.php");
		///$_SESSION['token'] = random(25); // clé aléatoire de 25 caractères créée a partir de la fonction
		admentete_page("Validation d'un facture coworking");

		if ($_POST['action']=='CREATEFACTURE')
		{
			creation_facture();
		}
		if ($_POST['action']=='VALIDEFACTURE')
		{
			valide_facture();
		}

		?>
		</body>
		</html>
		  <!-- Fin du contenu à protéger --->
		<?php

	//include ("includes/fonction_support.php");
	//	insert_appel($_POST['Nom_Client'], $_POST['DateCall'], $_POST['HeureCall'], $_SESSION['login'], $_POST['TitreCall'], $_POST['commentaires']);
	}
	else
	{
		?>
		  <!-- Insérez ici le contenu à protéger --->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

			<!--<link rel="stylesheet" href="style.css" type="text/css" media="all" />-->>
			<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/base/jquery-ui.css" type="text/css" media="all" />
			<link rel="stylesheet" href="https://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>

			<script type="text/javascript">
				jQuery(function($){
			   $.datepicker.regional['fr'] = {
				  closeText: 'Fermer',
				  prevText: '&#x3c;Préc',
				  nextText: 'Suiv&#x3e;',
				  currentText: 'Courant',
				  monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
				  'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
				  monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
				  'Jul','Aoû','Sep','Oct','Nov','Déc'],
				  dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
				  dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
				  dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
				  weekHeader: 'Sm',
				  //dateFormat: 'dd/mm/yy',
							dateFormat: 'dd/mm/yy',
				  firstDay: 1,
				  isRTL: false,
				  showMonthAfterYear: false,
				  yearSuffix: ''};
			   $.datepicker.setDefaults($.datepicker.regional['fr']);
			});

			function cocher_tout(val)
			{
				if (val.checked == true)
				{
					for (var i=1; i < 30; i++)
					{
						$('#ckeckresa'+i).attr('checked',true);
					}
				}
				else
				{
					for (var i=1; i < 30; i++)
					{
						$('#ckeckresa'+i).attr('checked',false);
					}
				}
			}

			</script>

			</script>

			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>

			<link rel="stylesheet" href="lib/file.css">
			</head>
			<body>

			<script>
			$(function() {
				$.datepicker.setDefaults( $.datepicker.regional[ "" ] );
				$( "#datepicker" ).datepicker( $.datepicker.regional[ "fr" ] );
				$( "#datepicker1" ).datepicker( $.datepicker.regional[ "fr" ] );
			});

			function changedate()
			{
				var datedebut=$( "#datepicker" ).val();
				var datefin=$( "#datepicker1" ).val();
				window.location.href = 'cloture_resa.php?datedebut='+datedebut+'&datefin='+datefin;
			}
			</script>

			<?php
			include ("include/fonction_admutilisateur.php");
			admentete_page("Clôture des réservation");
			if (isset($_POST['boutonvalid']))
			{
				valid_cloture();
				exit;
			}

			cloture_resa();

			?>

			</body>
			</html>
		  <!-- Fin du contenu à protéger --->
		<?php
	}
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur

    header('Location: index.php');


}
?>
