<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width">
    <link rel="icon" href="img/laverriere.ico" />
    <title>Erreur 404</title>
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="lib/style.css">
  </head>
  <body>
    <style>
    body{
                background: #51b692;
                color: white;
                font-family: 'Open Sans', sans-serif;
            }
            .c{
                text-align: center;
                display: block;
                position: relative;
                width:80%;
                margin:100px auto;
            }
            ._404{
                font-size: 220px;
                position: relative;
                display: inline-block;
                z-index: 2;
                height: 250px;
                letter-spacing: 15px;
            }
            ._1{
                text-align:center;
                display:block;
                position:relative;
                letter-spacing: 12px;
                font-size: 4em;
                line-height: 80%;
                margin-top: 2%;
            }
            ._2{
                text-align:center;
                display:block;
                position:relative;
                font-size: 2em;
                line-height: 80%;
                margin-top: 2%;
                text-decoration: none;
            }
            hr{
                padding: 0;
                border: none;
                border-top: 5px solid #fff;
                color: #fff;
                text-align: center;
                margin: 0px auto;
                width: 420px;
                height:10px;
                z-index: -10;
            }
            a{
              text-decoration: none;
              color: white;
            }
            a:hover{
              color: white;
            }

    </style>
    <div class='c'>
    <div class='_404'>404</div>
    <hr>
    <div class='_1'>PAGE INTROUVABLE</div>
    <div class='_2'><a href="../index.php">retour</a></div>
</div>
  </body>
</html>
