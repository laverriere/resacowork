<?php
//info BDD

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="viewport" content="width=device-width">
    <link rel="icon" href="img/laverriere.ico" />
    <title>Inscription</title>
    <link rel="stylesheet" href="lib/bootstrap.min.css">
    <link rel="stylesheet" href="lib/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<?php
  include ("include/fonction_general.php");
  entete_page_login("Traitement");
?>

<?php
$cnx_bdd = ConnexionBDD();
if(isset($_POST['inscription'])){
  // recup des variables du Formulaire
  $id1 = rand(1,99999999);
  $id2 = rand(1,99999999);
  $mdp = hash('sha512', $id1.$_POST['mdp'].$id2);
  $nom = encrypt($_POST['nom'],$id2);
  $prenom = encrypt($_POST['prenom'],$id2);
  $user = $_POST['user'];
  $email = encrypt($_POST['email'],$id2);
  

  $req = "
    UPDATE UTILISATEUR
    SET UT_VERIF = 'NULL',
      UT_PASSWORD = '$mdp',
      UT_NOM = '$nom',
      UT_PRENOM = '$prenom',
      UT_EMAIL = '$email',
      UT_STATUT = 'COWORKER',
      UT_VALIDE = 'O',
      UT_ID1 = '$id1',
      UT_ID2 = '$id2'
    WHERE UT_LOGIN='$user';
  ";
  $result_req = $cnx_bdd->exec($req);
  ?><div class="loader"></div>;<?php
  ?><script type="text/javascript"> window.location = "index.php"
    </script>';
  <?php

}
elseif (isset($_POST['validation']))
{
	$user = $_POST['user'];
	$sql = "SELECT * FROM UTILISATEUR WHERE UT_LOGIN = '$user';";	
	$result_req = $cnx_bdd->query($sql);
      $tab_r = $result_req->fetchAll();
    foreach ($tab_r as $r) {
		$mdp = hash('sha512', $r['UT_ID1'].$_POST['mdp'].$r['UT_ID2']);
    }
	$req = "
    UPDATE UTILISATEUR SET UT_PASSWORD = '$mdp' WHERE UT_LOGIN='$user';";
	$result_req = $cnx_bdd->exec($req);
  ?><div class="loader"></div>;<?php
  ?><script type="text/javascript"> window.location = "index.php"
    </script>';
  <?php
}
else
{
  //erreur
}
?>

</body>
