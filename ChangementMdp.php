<?php
/**
 * ChangementMdp.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */

//info BDD

//Envoie mail
require ("include/fonction_email.php");

include ("include/fonction_general.php");


$cnx_bdd = ConnexionBDD();
$req = "SELECT UT_VERIF FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."' ;";
$result_req = $cnx_bdd->query($req);
$tab_r = $result_req->fetchAll();
$count = count($tab_r);
if ($count == 0) {
  ?><script type="text/javascript"> window.location = "index.php"
    </script>';
  <?php
}
foreach ($tab_r as $r) {
  $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  $goodURL = url_siteverif."ChangementMdp.php?id=".$r['UT_VERIF'];
  
  if($url != $goodURL){
    ?><script type="text/javascript"> window.location = "index.php"
      </script>';
    <?php
  }elseif ($_GET['id']=="") {
	  
    ?><script type="text/javascript"> window.location = "index.php"
      </script>';
    <?php
  }else{
	  
	  
    //si la page est trouvé
  }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width">
        <link rel="icon" href="img/laverriere.ico" />
        <title>Nouveau mot de passe</title>
        <link rel="stylesheet" href="lib/bootstrap.min.css">
        <link rel="stylesheet" href="lib/style.css">
    </head>
    <body>
        <?php
        entete_page_login("Nouveau mot de passe");
        ?>

        <script>
        function check_pass() {
            var vert = "#66cc66";
            var rouge = "#ff6666";
            var mdp = document.getElementById('mdp').value;
            var Vmdp = document.getElementById('Vmdp').value;
            if (mdp == "") {
                document.getElementById('mdp').style.backgroundColor = rouge;
                document.getElementById('Vmdp').style.backgroundColor = rouge;
                document.getElementById('inscription').disabled = true;
            } else if(mdp == Vmdp) {
        				document.getElementById('mdp').style.backgroundColor = vert;
                document.getElementById('Vmdp').style.backgroundColor = vert;
                document.getElementById('inscription').disabled = false;
            }else{
               		document.getElementById('mdp').style.backgroundColor = rouge;
                document.getElementById('Vmdp').style.backgroundColor = rouge;
                document.getElementById('inscription').disabled = true;
            }
        }
        </script>

        <div class="login_form">
            <form action="traitementMdp.php" method="post" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
              <input type="hidden" name="login" class="col-xs-12" value="<?php
                $cnx_bdd = ConnexionBDD();
                $req = "SELECT UT_LOGIN FROM UTILISATEUR WHERE UT_VERIF='".$_GET['id']."';";
                $result_req = $cnx_bdd->query($req);
                $tab_r = $result_req->fetchAll();
              foreach ($tab_r as $r) {
                echo $r['UT_LOGIN'];
              }
              ?>">

                <label for="mdp" class="col-xs-12"> Nouveau mot de passe :</label>
                <input type="password" name="mdp" class="col-xs-12" id="mdp" required="Requis" onkeyup='check_pass();'>

                <label for="Vmdp" class="col-xs-12"> Vérifier mot de passe :</label>
                <input type="password" name="Vmdp" class="col-xs-12" id="Vmdp" required="Requis" onkeyup='check_pass();'>

                <input disabled id="inscription" type="submit" name="Envoie" value="Envoie" class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                <a style="color:#F69730" href="index.php" class="forgot_passwd col-xs-12" >retour</a>
            </form>
        </div>
    </body>
</html>
