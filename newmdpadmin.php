<?php
/**
 * fonction_email.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
require ("include/fonction_general.php");
//Envoie mail
require ("include/fonction_email.php");
//info BDD

admentete_page("Création d'un utilisateur");

?> <script> alert('Un mail a été envoyé\r(Cela peut prendre quelque minutes.)')</script> <?php
$cnx_bdd = ConnexionBDD();
$user = $_GET['newmdp'];
$req = "SELECT UT_EMAIL,UT_ID2 FROM UTILISATEUR WHERE UT_LOGIN='$user' ;";
$result_req = $cnx_bdd->query($req);
$tab_r = $result_req->fetchAll();
foreach ($tab_r as $r) {
$email = decrypt($r['UT_EMAIL'],$r['UT_ID2']);
$rand = random(20);
$req = "UPDATE UTILISATEUR SET UT_VERIF='$rand' WHERE UT_LOGIN='$user'	;";
$result_req = $cnx_bdd->exec($req);
emailNmdp($email, $rand, $user);
?> <script> alert('Un mail a été envoyé\r(Cela peut prendre quelque minutes.)')</script> <?php
?><script type="text/javascript"> window.location = "index.php"</script><?php

?>
