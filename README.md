# RESACOWORK - version stable 3.1.0

### Requiert :

PHP : >= 5.6 && <= 7.2 MySQL: > 5.4 && < 5.6

RESACOWORK est un outil de gestion et de réservation de ressources. 
RESACOWORK a été développé par les équipes de La Verrière - Espace de Coworking

Des correctifs peuvent être régulièrement apportés suites aux signalements sur le site framagit ou suite à des améliorations de fonctionnement.

## Installation

Pour obtenir une description complète de la procédure d'installation, veuillez vous reporter au fichier "INSTALLATION.MD".

## Licence

RESACOWORK est publié sous les termes de la MIT Licence, dont le contenu est disponible dans le fichier "LICENSE".

RESACOWORK est gratuit, vous pouvez le copier, le distribuer, et le modifier, à condition que chaque partie de RESACOWORK réutilisée ou modifiée reste sous licence MIT.
Par ailleurs et dans un soucis d'efficacité, merci de rester en contact avec le développeur pour éventuellement intégrer vos contributions à une distribution ultérieure.

Enfin, RESACOWORK est livré en l'état sans aucune garantie. Les auteurs de cet outil ne pourront en aucun cas être tenus pour responsables d'éventuels bugs.

## Remarques concernant la sécurité
La sécurisation de RESACOWORK est dépendante de celle du serveur. 
Nous vous recommandons d'utiliser un serveur Apache ou Nginx sous Linux, en utilisant le protocole https (transferts de données cryptées),
et en veillant à toujours utiliser les dernières versions des logiciels impliqués (notamment Apache/Nginx et PHP).

L'EQUIPE DE DEVELOPPEMENT DE RESACOWORK NE SAURAIT EN AUCUN CAS ETRE TENUE POUR RESPONSABLE EN CAS D'INTRUSION EXTERIEURE LIEE A UNE FAIBLESSE DE RESACOWORK OU DE SON SUPPORT SERVEUR.