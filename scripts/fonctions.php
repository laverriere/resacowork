<?php
function ConnexionBDD() {
	$cnx_dsn = 'mysql:host='.bdd_hote.';'.'port='.bdd_port.';'.'dbname='.bdd_nom;
	try{
		$cnx_bdd = new PDO($cnx_dsn,bdd_util,bdd_mdp);
	} catch (PDOException $ex) {
		echo $ex->getMessage();
	}
	if (isset($cnx_bdd)){
		return $cnx_bdd;
	} else {
		return false;
	}
}

function random($var)
{
	$string = "";
	$chaine = "a0b1c2d3e4f5g6h7i8j9klmnpqrstuvwxy123456789";
	srand((double)microtime()*1000000);
	for($i=0; $i<$var; $i++)
	{
		$string .= $chaine[rand()%strlen($chaine)];
	}
	return $string;
}

function decrypt2($data, $key) {
	$encryption_key = base64_decode($key);
  // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
  list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
  return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}

function encrypt2($data,$key) {
  // Remove the base64 encoding from our key
  $encryption_key = base64_decode($key);
  // Generate an initialization vector
  $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
  // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
  $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
  // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
  return base64_encode($encrypted . '::' . $iv);
}

function insertligne($libelle, $codeart, $libarticle, $qtearticle, $prixarticle, $userfacture, $etablissement, $reffacture, $numerodocument, $numligne)
{
	$sql = "INSERT INTO LIGNE (GL_NUMLIGNE, GL_REFFACTURE, GL_NUMERO, GL_DATEPIECE, GL_ETABLISSEMENT, GL_USER, GL_ARTICLENO, GL_ARTLIBELLE, GL_QTEFACT, GL_TOTALHT, GL_COMMENTAIRE, GL_ARTTARIF, GL_PRIXPARPERSONNE,
				GL_PRIXPARPLACE, GL_LIBELLE) VALUES
			(".$numligne.",'".$reffacture."',". $numerodocument.", now(), '".$etablissement."','".$userfacture."','".$codeart."','".$libarticle."',
				".$qtearticle.",".($qtearticle * $prixarticle).",'',".$prixarticle.",0,0,'".$libelle."');";
	$cnx_bdd = ConnexionBDD();
	$result_req = $cnx_bdd->exec($sql);
}
?>
