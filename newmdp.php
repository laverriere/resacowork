<?php
/**
 * newmdp.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// On vérifie si l'utilisateur a envoyé des informations de connexion

// Les informations de connexion sont bonnes, on affiche le contenu protégé
require ("include/fonction_general.php");
//Envoie mail
require ("include/fonction_email.php");
//info BDD

echo '';
//include ("include/fonction_utilisateur.php");
?>
 <!-- Insérez ici le contenu à protéger -->
	  <!DOCTYPE html>
		<html lang="fr">
		<head>
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			<meta name="viewport" content="width=device-width">

		<link rel="icon" href="img/laverriere.ico" />
		<title>Gestion Tiers Lieux Haut de France</title>
			<link rel="stylesheet" href="lib/bootstrap.min.css">
			<link rel="stylesheet" href="lib/style.css">

			<!-- SCRIPTS -->
			<script
			  src="https://code.jquery.com/jquery-3.2.1.min.js"
			  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			  crossorigin="anonymous">
      </script>
		</head>
		<body>
		<?php
		entete_page_login("Nouveau mot de passe");
		?>
	<div class="login_form">

	<form action="" method="post" class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">

	<p class="error"> Un email va vous être transmis afin de revalider votre mot de passe ! </p>
	<label for="newmpd" class="col-xs-12"> </label>
	<label for="newmpd" class="col-xs-12"> Saisir votre Login : </label>
	<input type="text" name="newmdp" id="newmpd" maxlength="50" class="col-xs-12">

	<input type="submit" name="valider" value=" Valider " class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">



	<a style="color:#F69730" href="index.php" class="forgot_passwd col-xs-12" >Retour à l'acceuil</a>

  </form>

  <?php
	//envoie du mail
    if(isset($_POST['valider'])){
      $cnx_bdd = ConnexionBDD();
			$user = $_POST['newmdp'];
			$req = "SELECT UT_EMAIL,UT_ID2 FROM UTILISATEUR WHERE UT_LOGIN='$user' ;";
			$result_req = $cnx_bdd->query($req);
			$tab_r = $result_req->fetchAll();
			foreach ($tab_r as $r) {
				$email = decrypt($r['UT_EMAIL'],$r['UT_ID2']);
				$rand = random(20);
				$req = "UPDATE UTILISATEUR SET UT_VERIF='$rand' WHERE UT_LOGIN='$user'	;";
				$result_req = $cnx_bdd->exec($req);
				emailNmdp($email, $rand, $user);
				?> <script> alert('Un mail vous a été envoyé\r(Cela peut prendre quelque minutes.)')</script> <?php
				?><script type="text/javascript"> window.location = "index.php"
					</script>';
					<?php
			}
    }else {
      // si on appuis pas sur le bouton valider
    }

  ?>

  <br>

</div>

	</body>
	</html>
  <!-- Fin du contenu à protéger --->
<?php

?>
