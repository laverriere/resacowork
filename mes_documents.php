<?php
/**
 * mes_factures.php
 * Ce script fait partie de l'application Gestion Coworking
 * Dernière modification : $Date: 2018-06-21 15:38:14 $
 * Dernière modification : $Date: 2009-10-09 07:55:48 $
 * @author    Jean-René Menu <jr.menu@coworking-laverriere.fr>
 * @copyright Copyright 2016-2018 Jean-René Menu
 * @link      http://www.gnu.org/licenses/licenses.html
 * @package   root
 * @version   $Id: Gestion Coworking V3.1.0  2018-12-25
 * @filesource
 *
 * This file is part of Gestion Coworking.
 *
 * Gestion Coworking is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gestion Coworking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gestion Coworking; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/**
 *
 *
 */


session_start ();


// On vérifie si l'utilisateur a envoyé des informations de connexion
if(isset($_SESSION['login']))
{
    // Les informations de connexion sont bonnes, on affiche le contenu protégé
    		if(isset($_POST['valider'])) {
    			update_utilsateur();
    		}
    		?>
		  <!-- Insérez ici le contenu à protéger -->
		  <!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<meta name="viewport" content="width=device-width">

			<link rel="icon" href="img/laverriere.ico" />
			<title>Gestion Tiers Lieux Haut de France</title>
				<link rel="stylesheet" href="lib/bootstrap.min.css">
				<link rel="stylesheet" href="lib/style.css">

				<!-- SCRIPTS -->
				<script
				  src="https://code.jquery.com/jquery-3.2.1.min.js"
				  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
				  crossorigin="anonymous"></script>
			</head>
			<body>
			<?php
			include ("include/fonction_utilisateur.php");
			
			entete_page('');
			visu_document('perso');

			?>
			</body>
			</html>
		  <!-- Fin du contenu à protéger -->
    <?php
}
else
{
    // Les informations de connexion sont incorrectes, on affiche une page d'erreur

    header('Location: index.php');


}
?>
